/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.protocol;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.openscada.doc.model.doc.fragment.FragmentPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.openscada.doc.protocol.ProtocolFactory
 * @model kind="package"
 * @generated
 */
public interface ProtocolPackage extends EPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n"; //$NON-NLS-1$

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "protocol"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:content:protocol"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "protocol"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    ProtocolPackage eINSTANCE = org.openscada.doc.protocol.impl.ProtocolPackageImpl.init ();

    /**
     * The meta object id for the '{@link org.openscada.doc.protocol.impl.StructureTableImpl <em>Structure Table</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.protocol.impl.StructureTableImpl
     * @see org.openscada.doc.protocol.impl.ProtocolPackageImpl#getStructureTable()
     * @generated
     */
    int STRUCTURE_TABLE = 0;

    /**
     * The feature id for the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE__CAPTION = FragmentPackage.CONTENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Structure Table</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE_FEATURE_COUNT = FragmentPackage.CONTENT_FEATURE_COUNT + 1;

    /**
     * Returns the meta object for class '{@link org.openscada.doc.protocol.StructureTable <em>Structure Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Structure Table</em>'.
     * @see org.openscada.doc.protocol.StructureTable
     * @generated
     */
    EClass getStructureTable ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.protocol.StructureTable#getCaption <em>Caption</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Caption</em>'.
     * @see org.openscada.doc.protocol.StructureTable#getCaption()
     * @see #getStructureTable()
     * @generated
     */
    EAttribute getStructureTable_Caption ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ProtocolFactory getProtocolFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '{@link org.openscada.doc.protocol.impl.StructureTableImpl <em>Structure Table</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.protocol.impl.StructureTableImpl
         * @see org.openscada.doc.protocol.impl.ProtocolPackageImpl#getStructureTable()
         * @generated
         */
        EClass STRUCTURE_TABLE = eINSTANCE.getStructureTable ();

        /**
         * The meta object literal for the '<em><b>Caption</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STRUCTURE_TABLE__CAPTION = eINSTANCE.getStructureTable_Caption ();

    }

} //ProtocolPackage
