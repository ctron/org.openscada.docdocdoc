/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.protocol;

import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.protocol.StructureTable#getCaption <em>Caption</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.protocol.ProtocolPackage#getStructureTable()
 * @model
 * @generated
 */
public interface StructureTable extends Content
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n"; //$NON-NLS-1$

    /**
     * Returns the value of the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Caption</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Caption</em>' attribute.
     * @see #setCaption(String)
     * @see org.openscada.doc.protocol.ProtocolPackage#getStructureTable_Caption()
     * @model
     * @generated
     */
    String getCaption ();

    /**
     * Sets the value of the '{@link org.openscada.doc.protocol.StructureTable#getCaption <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Caption</em>' attribute.
     * @see #getCaption()
     * @generated
     */
    void setCaption ( String value );

} // StructureTable
