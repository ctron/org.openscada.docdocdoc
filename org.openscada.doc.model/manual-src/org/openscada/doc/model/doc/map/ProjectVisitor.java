package org.openscada.doc.model.doc.map;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource.Factory;

public class ProjectVisitor implements FileVisitor.Visitor
{
    private final boolean createSection;

    private final String pluginId;

    private final Visitor visitor;

    private MapSection section;

    private final Map<String, Factory> extensionMap;

    public ProjectVisitor ( final Map<String, Factory> extensionMap, final boolean createSection, final String pluginId, final Visitor visitor )
    {
        this.extensionMap = extensionMap;
        this.createSection = createSection;
        this.pluginId = pluginId;
        this.visitor = visitor;
    }

    @Override
    public void visitFile ( final IFile file ) throws Exception
    {
        if ( this.createSection && this.section == null )
        {
            this.section = MapFactory.eINSTANCE.createMapSection ();
            this.section.setTitle ( this.pluginId );
            this.visitor.openSection ( this.section );
        }

        Helper.visitFile ( this.extensionMap, this.visitor, URI.createFileURI ( file.getLocation ().toString () ) );
    }

    public void done ()
    {
        if ( this.section != null )
        {
            this.visitor.closeSection ( this.section );
        }
    }
}