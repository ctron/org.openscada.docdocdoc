package org.openscada.doc.model.doc.map;

import java.util.List;

public interface SectionMarker
{

    public String getTitle ();

    public String getId ();

    public List<? extends Object> getContent ();

}
