package org.openscada.doc.model.doc.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

import com.google.common.io.CharStreams;

public class PlainTextFactory implements Factory
{

    @Override
    public Resource createResource ( final URI uri )
    {
        return new ResourceImpl ( uri ) {
            @Override
            protected void doLoad ( final InputStream inputStream, final Map<?, ?> options ) throws IOException
            {
                final InputStreamReader stream = new InputStreamReader ( inputStream );
                final String value = CharStreams.toString ( stream );

                final PlainTextContent content = FragmentFactory.eINSTANCE.createPlainTextContent ();
                content.setValue ( value );

                getContents ().add ( content );
            }
        };
    }
}
