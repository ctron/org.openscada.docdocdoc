package org.openscada.doc.model.doc.map;

import org.eclipse.emf.common.util.URI;
import org.openscada.doc.model.doc.fragment.Content;

public interface Visitor
{
    public void openSection ( MapSection section );

    public void closeSection ( MapSection section );

    public void visit ( URI uri, Content Content ) throws Exception;
}
