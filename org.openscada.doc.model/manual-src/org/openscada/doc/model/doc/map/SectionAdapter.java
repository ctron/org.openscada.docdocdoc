package org.openscada.doc.model.doc.map;

public interface SectionAdapter
{

    public SectionMarker adaptToSection ( Object object );

}
