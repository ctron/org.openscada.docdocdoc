package org.openscada.doc.model.doc.map;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.ui.databinding.AdapterHelper;

public class Helper
{

    public static List<SectionAdapter> getAdapters () throws CoreException
    {
        final List<SectionAdapter> result = new LinkedList<SectionAdapter> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( "org.openscada.doc.model.sectionAdapter" ) )
        {
            if ( !"sectionAdapter".equals ( ele.getName () ) )
            {
                continue;
            }

            result.add ( (SectionAdapter)ele.createExecutableExtension ( "class" ) );
        }

        return result;
    }

    public static void visitFile ( final Map<String, Factory> extensionMap, final Visitor visitor, final URI uri ) throws Exception
    {
        final ResourceSet resourceSet = new ResourceSetImpl ();
        resourceSet.getResourceFactoryRegistry ().getExtensionToFactoryMap ().putAll ( extensionMap );
        // FIXME: resourceSet.getResourceFactoryRegistry ().getExtensionToFactoryMap ().put ( "txt", new PlainTextFactory () );
        final Resource resource = resourceSet.createResource ( uri );

        try
        {
            resource.load ( null );
        }
        catch ( final IOException e )
        {
            throw new RuntimeException ( e );
        }

        final URI visitUri = resource.getURI ();

        visitContents ( visitUri, visitor, resource.getContents () );
    }

    private static void visitContents ( final URI visitUri, final Visitor visitor, final List<? extends Object> list ) throws Exception
    {
        for ( final Object o : list )
        {
            visitObject ( visitUri, visitor, o );
        }
    }

    protected static void visitObject ( final URI visitUri, final Visitor visitor, final Object o ) throws Exception
    {
        final Content content = AdapterHelper.adapt ( o, Content.class, true );
        if ( content != null )
        {
            visitor.visit ( visitUri, content );
            return;
        }

        final SectionMarker sm = AdapterHelper.adapt ( o, SectionMarker.class, true );
        if ( sm != null )
        {
            visitSection ( visitUri, visitor, sm );
            return;
        }

        final List<SectionAdapter> adapters = getAdapters ();
        for ( final SectionAdapter sa : adapters )
        {
            final SectionMarker section = sa.adaptToSection ( o );
            if ( section != null )
            {
                visitSection ( visitUri, visitor, section );
                return;
            }
        }

        // only walk if this was not a section or content
        walk ( visitUri, visitor, o );
    }

    private static void walk ( final URI visitUri, final Visitor visitor, final Object o ) throws Exception
    {

        if ( o instanceof EObject )
        {
            visitContents ( visitUri, visitor, ( (EObject)o ).eContents () );
        }
    }

    private static void visitSection ( final URI visitUri, final Visitor visitor, final SectionMarker section ) throws Exception
    {

        final MapSection mapSection = MapFactory.eINSTANCE.createMapSection ();
        mapSection.setTitle ( section.getTitle () );
        mapSection.setId ( section.getId () );

        visitor.openSection ( mapSection );

        visitContents ( visitUri, visitor, section.getContent () );

        visitor.closeSection ( mapSection );
    }

    public static Map<String, Factory> buildExtensionMap ( final EObject object )
    {
        final org.openscada.doc.model.doc.map.Map map = findMap ( object );
        if ( map == null )
        {
            return Collections.emptyMap ();
        }

        final Map<String, Factory> result = new HashMap<String, Resource.Factory> ();

        for ( final ExtensionMappingEntry entry : map.getExtensionMappings () )
        {
            try
            {
                result.put ( entry.getExtension (), createFactory ( entry.getFactory () ) );
            }
            catch ( final Exception e )
            {
                // ignore for now
            }
        }

        return result;
    }

    private static Factory createFactory ( final ResourceFactory factory ) throws Exception
    {
        return (Factory)factory.getClassName ().newInstance ();
    }

    private static org.openscada.doc.model.doc.map.Map findMap ( final EObject object )
    {
        EObject current = object;
        while ( current != null )
        {
            if ( current instanceof org.openscada.doc.model.doc.map.Map )
            {
                return (org.openscada.doc.model.doc.map.Map)current;
            }
            current = current.eContainer ();
        }
        return null;
    }
}
