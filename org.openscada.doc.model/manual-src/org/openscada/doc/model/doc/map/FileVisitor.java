package org.openscada.doc.model.doc.map;

import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.openscada.doc.model.Activator;
import org.openscada.utils.ExceptionHelper;

public class FileVisitor
{
    public static interface Visitor
    {
        public void visitFile ( IFile file ) throws Exception;
    }

    private final List<NameRule> rules;

    public FileVisitor ( final List<NameRule> fileNameRules )
    {
        this.rules = fileNameRules;
    }

    public void visit ( final Visitor visitor, final IContainer container ) throws CoreException
    {
        container.accept ( new IResourceVisitor () {

            @Override
            public boolean visit ( final IResource resource ) throws CoreException
            {
                try
                {
                    handleVisit ( visitor, resource, container );
                }
                catch ( final Exception e )
                {
                    throw new CoreException ( new Status ( IStatus.ERROR, Activator.PLUGIN_ID, ExceptionHelper.extractMessage ( e ), e ) );
                }
                return true;
            }
        }, IResource.DEPTH_INFINITE, IResource.NONE );
    }

    protected void handleVisit ( final Visitor visitor, final IResource resource, final IContainer container ) throws Exception
    {
        if ( ! ( resource instanceof IFile ) )
        {
            return;
        }

        final IFile file = (IFile)resource;

        if ( !file.isAccessible () )
        {
            return;
        }

        final IPath relativePath = file.getFullPath ().makeRelativeTo ( container.getFullPath () );
        final String relativePathString = relativePath.toString ();

        final RuleResult result = checkRules ( relativePathString );
        if ( result == RuleResult.ACCEPT )
        {
            // default is not to accept
            visitor.visitFile ( file );
        }
    }

    private RuleResult checkRules ( final String relativePathString )
    {
        for ( final NameRule rule : this.rules )
        {
            final RuleResult result = rule.checkRule ( relativePathString );
            if ( result != null )
            {
                return result;
            }
        }
        return null;
    }
}
