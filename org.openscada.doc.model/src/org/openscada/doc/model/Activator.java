package org.openscada.doc.model;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator
{
    public static final String PLUGIN_ID = "org.openscada.doc.model";

    private static Activator INSTANCE;

    public static Activator getDefault ()
    {
        return INSTANCE;
    }

    private BundleContext context;

    @Override
    public void start ( final BundleContext context ) throws Exception
    {
        this.context = context;
        INSTANCE = this;
    }

    @Override
    public void stop ( BundleContext context ) throws Exception
    {
        context = null;
        INSTANCE = null;
    }

    public BundleContext getContext ()
    {
        return this.context;
    }
}
