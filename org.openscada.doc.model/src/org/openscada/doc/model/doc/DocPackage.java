/**
 */
package org.openscada.doc.model.doc;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.DocFactory
 * @model kind="package"
 * @generated
 */
public interface DocPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "doc";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "urn:openscada:doc";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "doc";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    DocPackage eINSTANCE = org.openscada.doc.model.doc.impl.DocPackageImpl.init ();

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.impl.TestImpl <em>Test</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.impl.TestImpl
     * @see org.openscada.doc.model.doc.impl.DocPackageImpl#getTest()
     * @generated
     */
    int TEST = 0;

    /**
     * The number of structural features of the '<em>Test</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TEST_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.NumberingStyle
     * <em>Numbering Style</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @see org.openscada.doc.model.doc.impl.DocPackageImpl#getNumberingStyle()
     * @generated
     */
    int NUMBERING_STYLE = 1;

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.Test <em>Test</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Test</em>'.
     * @see org.openscada.doc.model.doc.Test
     * @generated
     */
    EClass getTest ();

    /**
     * Returns the meta object for enum '
     * {@link org.openscada.doc.model.doc.NumberingStyle
     * <em>Numbering Style</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Numbering Style</em>'.
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @generated
     */
    EEnum getNumberingStyle ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    DocFactory getDocFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.impl.TestImpl <em>Test</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.impl.TestImpl
         * @see org.openscada.doc.model.doc.impl.DocPackageImpl#getTest()
         * @generated
         */
        EClass TEST = eINSTANCE.getTest ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.NumberingStyle
         * <em>Numbering Style</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.NumberingStyle
         * @see org.openscada.doc.model.doc.impl.DocPackageImpl#getNumberingStyle()
         * @generated
         */
        EEnum NUMBERING_STYLE = eINSTANCE.getNumberingStyle ();

    }

} //DocPackage
