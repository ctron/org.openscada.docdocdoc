/**
 */
package org.openscada.doc.model.doc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.DocPackage#getTest()
 * @model
 * @generated
 */
public interface Test extends EObject
{
} // Test
