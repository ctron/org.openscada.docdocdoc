/**
 */
package org.openscada.doc.model.doc.fragment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getFragment()
 * @model extendedMetaData="name='fragment'"
 * @generated
 */
public interface Fragment extends Container
{
} // Fragment
