/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Copyright</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Copyright#getYear <em>Year
 * </em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Copyright#getAuthor <em>
 * Author</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getCopyright()
 * @model
 * @generated
 */
public interface Copyright extends EObject
{
    /**
     * Returns the value of the '<em><b>Year</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Year</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Year</em>' attribute.
     * @see #setYear(int)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getCopyright_Year()
     * @model unique="false" required="true"
     * @generated
     */
    int getYear ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.Copyright#getYear
     * <em>Year</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Year</em>' attribute.
     * @see #getYear()
     * @generated
     */
    void setYear ( int value );

    /**
     * Returns the value of the '<em><b>Author</b></em>' reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Author}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Author</em>' reference isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Author</em>' reference list.
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getCopyright_Author()
     * @model required="true"
     * @generated
     */
    EList<Author> getAuthor ();

} // Copyright
