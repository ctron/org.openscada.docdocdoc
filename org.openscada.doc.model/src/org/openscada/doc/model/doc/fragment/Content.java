/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Content</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getContent()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Content extends EObject
{
} // Content
