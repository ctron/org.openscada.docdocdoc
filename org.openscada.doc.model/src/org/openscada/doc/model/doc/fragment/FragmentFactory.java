/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage
 * @generated
 */
public interface FragmentFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    FragmentFactory eINSTANCE = org.openscada.doc.model.doc.fragment.impl.FragmentFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Fragment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Fragment</em>'.
     * @generated
     */
    Fragment createFragment ();

    /**
     * Returns a new object of class '<em>Plain Text Content</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Plain Text Content</em>'.
     * @generated
     */
    PlainTextContent createPlainTextContent ();

    /**
     * Returns a new object of class '<em>Author</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Author</em>'.
     * @generated
     */
    Author createAuthor ();

    /**
     * Returns a new object of class '<em>Copyright</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Copyright</em>'.
     * @generated
     */
    Copyright createCopyright ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    FragmentPackage getFragmentPackage ();

} //FragmentFactory
