/**
 */
package org.openscada.doc.model.doc.fragment.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Container;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.fragment.Fragment;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.PlainTextContent;
import org.openscada.doc.model.doc.fragment.Section;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the
 * model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage
 * @generated
 */
public class FragmentAdapterFactory extends AdapterFactoryImpl
{
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static FragmentPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentAdapterFactory ()
    {
        if ( modelPackage == null )
        {
            modelPackage = FragmentPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the
     * model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType ( final Object object )
    {
        if ( object == modelPackage )
        {
            return true;
        }
        if ( object instanceof EObject )
        {
            return ( (EObject)object ).eClass ().getEPackage () == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FragmentSwitch<Adapter> modelSwitch = new FragmentSwitch<Adapter> () {
        @Override
        public Adapter caseFragment ( final Fragment object )
        {
            return createFragmentAdapter ();
        }

        @Override
        public Adapter caseSection ( final Section object )
        {
            return createSectionAdapter ();
        }

        @Override
        public Adapter caseContainer ( final Container object )
        {
            return createContainerAdapter ();
        }

        @Override
        public Adapter casePlainTextContent ( final PlainTextContent object )
        {
            return createPlainTextContentAdapter ();
        }

        @Override
        public Adapter caseContent ( final Content object )
        {
            return createContentAdapter ();
        }

        @Override
        public Adapter caseAuthor ( final Author object )
        {
            return createAuthorAdapter ();
        }

        @Override
        public Adapter caseCopyright ( final Copyright object )
        {
            return createCopyrightAdapter ();
        }

        @Override
        public Adapter defaultCase ( final EObject object )
        {
            return createEObjectAdapter ();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target
     *            the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter ( final Notifier target )
    {
        return this.modelSwitch.doSwitch ( (EObject)target );
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Fragment <em>Fragment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Fragment
     * @generated
     */
    public Adapter createFragmentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Section <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Section
     * @generated
     */
    public Adapter createSectionAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Container <em>Container</em>}
     * '.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Container
     * @generated
     */
    public Adapter createContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent
     * <em>Plain Text Content</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.PlainTextContent
     * @generated
     */
    public Adapter createPlainTextContentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Content
     * @generated
     */
    public Adapter createContentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Author <em>Author</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Author
     * @generated
     */
    public Adapter createAuthorAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Copyright <em>Copyright</em>}
     * '.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Copyright
     * @generated
     */
    public Adapter createCopyrightAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter ()
    {
        return null;
    }

} //FragmentAdapterFactory
