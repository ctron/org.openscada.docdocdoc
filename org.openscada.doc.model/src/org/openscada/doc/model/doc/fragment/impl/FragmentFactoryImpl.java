/**
 */
package org.openscada.doc.model.doc.fragment.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.fragment.Fragment;
import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class FragmentFactoryImpl extends EFactoryImpl implements FragmentFactory
{
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static FragmentFactory init ()
    {
        try
        {
            final FragmentFactory theFragmentFactory = (FragmentFactory)EPackage.Registry.INSTANCE.getEFactory ( "urn:openscada:doc:fragment" );
            if ( theFragmentFactory != null )
            {
                return theFragmentFactory;
            }
        }
        catch ( final Exception exception )
        {
            EcorePlugin.INSTANCE.log ( exception );
        }
        return new FragmentFactoryImpl ();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentFactoryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create ( final EClass eClass )
    {
        switch ( eClass.getClassifierID () )
        {
            case FragmentPackage.FRAGMENT:
                return createFragment ();
            case FragmentPackage.PLAIN_TEXT_CONTENT:
                return createPlainTextContent ();
            case FragmentPackage.AUTHOR:
                return createAuthor ();
            case FragmentPackage.COPYRIGHT:
                return createCopyright ();
            default:
                throw new IllegalArgumentException ( "The class '" + eClass.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Fragment createFragment ()
    {
        final FragmentImpl fragment = new FragmentImpl ();
        return fragment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public PlainTextContent createPlainTextContent ()
    {
        final PlainTextContentImpl plainTextContent = new PlainTextContentImpl ();
        return plainTextContent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Author createAuthor ()
    {
        final AuthorImpl author = new AuthorImpl ();
        return author;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Copyright createCopyright ()
    {
        final CopyrightImpl copyright = new CopyrightImpl ();
        return copyright;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FragmentPackage getFragmentPackage ()
    {
        return (FragmentPackage)getEPackage ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static FragmentPackage getPackage ()
    {
        return FragmentPackage.eINSTANCE;
    }

} //FragmentFactoryImpl
