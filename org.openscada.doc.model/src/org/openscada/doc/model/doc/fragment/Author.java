/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Author</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Author#getName <em>Name</em>}
 * </li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Author#getRef <em>Ref</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Author#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getAuthor()
 * @model
 * @generated
 */
public interface Author extends EObject
{
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getAuthor_Name()
     * @model unique="false" required="true"
     * @generated
     */
    String getName ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.Author#getName <em>Name</em>}
     * ' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName ( String value );

    /**
     * Returns the value of the '<em><b>Ref</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ref</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ref</em>' attribute.
     * @see #setRef(String)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getAuthor_Ref()
     * @model unique="false"
     * @generated
     */
    String getRef ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.Author#getRef <em>Ref</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Ref</em>' attribute.
     * @see #getRef()
     * @generated
     */
    void setRef ( String value );

    /**
     * Returns the value of the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Id</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Id</em>' attribute.
     * @see #setId(String)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getAuthor_Id()
     * @model id="true" required="true"
     * @generated
     */
    String getId ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.Author#getId <em>Id</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Id</em>' attribute.
     * @see #getId()
     * @generated
     */
    void setId ( String value );

} // Author
