/**
 */
package org.openscada.doc.model.doc.fragment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.Section;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.impl.SectionImpl#getContent
 * <em>Content</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.fragment.impl.SectionImpl#getAnyContent
 * <em>Any Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.impl.SectionImpl#getSection
 * <em>Section</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.impl.SectionImpl#getTitle
 * <em>Title</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class SectionImpl extends EObjectImpl implements Section
{
    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}'
     * attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected FeatureMap content;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SectionImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return FragmentPackage.Literals.SECTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FeatureMap getContent ()
    {
        if ( this.content == null )
        {
            this.content = new BasicFeatureMap ( this, FragmentPackage.SECTION__CONTENT );
        }
        return this.content;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Section> getSection ()
    {
        return getContent ().list ( FragmentPackage.Literals.CONTAINER__SECTION );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Content> getAnyContent ()
    {
        return getContent ().list ( FragmentPackage.Literals.CONTAINER__ANY_CONTENT );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return this.title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle ( final String newTitle )
    {
        final String oldTitle = this.title;
        this.title = newTitle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, FragmentPackage.SECTION__TITLE, oldTitle, this.title ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case FragmentPackage.SECTION__CONTENT:
                return ( (InternalEList<?>)getContent () ).basicRemove ( otherEnd, msgs );
            case FragmentPackage.SECTION__ANY_CONTENT:
                return ( (InternalEList<?>)getAnyContent () ).basicRemove ( otherEnd, msgs );
            case FragmentPackage.SECTION__SECTION:
                return ( (InternalEList<?>)getSection () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case FragmentPackage.SECTION__CONTENT:
                if ( coreType )
                {
                    return getContent ();
                }
                return ( (FeatureMap.Internal)getContent () ).getWrapper ();
            case FragmentPackage.SECTION__ANY_CONTENT:
                return getAnyContent ();
            case FragmentPackage.SECTION__SECTION:
                return getSection ();
            case FragmentPackage.SECTION__TITLE:
                return getTitle ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case FragmentPackage.SECTION__CONTENT:
                ( (FeatureMap.Internal)getContent () ).set ( newValue );
                return;
            case FragmentPackage.SECTION__ANY_CONTENT:
                getAnyContent ().clear ();
                getAnyContent ().addAll ( (Collection<? extends Content>)newValue );
                return;
            case FragmentPackage.SECTION__SECTION:
                getSection ().clear ();
                getSection ().addAll ( (Collection<? extends Section>)newValue );
                return;
            case FragmentPackage.SECTION__TITLE:
                setTitle ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case FragmentPackage.SECTION__CONTENT:
                getContent ().clear ();
                return;
            case FragmentPackage.SECTION__ANY_CONTENT:
                getAnyContent ().clear ();
                return;
            case FragmentPackage.SECTION__SECTION:
                getSection ().clear ();
                return;
            case FragmentPackage.SECTION__TITLE:
                setTitle ( TITLE_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case FragmentPackage.SECTION__CONTENT:
                return this.content != null && !this.content.isEmpty ();
            case FragmentPackage.SECTION__ANY_CONTENT:
                return !getAnyContent ().isEmpty ();
            case FragmentPackage.SECTION__SECTION:
                return !getSection ().isEmpty ();
            case FragmentPackage.SECTION__TITLE:
                return TITLE_EDEFAULT == null ? this.title != null : !TITLE_EDEFAULT.equals ( this.title );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (content: " );
        result.append ( this.content );
        result.append ( ", title: " );
        result.append ( this.title );
        result.append ( ')' );
        return result.toString ();
    }

} //SectionImpl
