/**
 */
package org.openscada.doc.model.doc.fragment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plain Text Content</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.PlainTextContent#getValue
 * <em>Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getPlainTextContent()
 * @model extendedMetaData="kind='simple'"
 * @generated
 */
public interface PlainTextContent extends Content
{

    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(String)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getPlainTextContent_Value()
     * @model extendedMetaData="name=':0' kind='simple'"
     * @generated
     */
    String getValue ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent#getValue
     * <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue ( String value );

} // PlainTextContent
