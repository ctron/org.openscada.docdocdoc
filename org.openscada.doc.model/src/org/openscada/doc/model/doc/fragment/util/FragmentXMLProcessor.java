/**
 */
package org.openscada.doc.model.doc.fragment.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.openscada.doc.model.doc.fragment.FragmentPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class FragmentXMLProcessor extends XMLProcessor
{

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentXMLProcessor ()
    {
        super ( EPackage.Registry.INSTANCE );
        FragmentPackage.eINSTANCE.eClass ();
    }

    /**
     * Register for "*" and "xml" file extensions the
     * FragmentResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations ()
    {
        if ( this.registrations == null )
        {
            super.getRegistrations ();
            this.registrations.put ( XML_EXTENSION, new FragmentResourceFactoryImpl () );
            this.registrations.put ( STAR_EXTENSION, new FragmentResourceFactoryImpl () );
        }
        return this.registrations;
    }

} //FragmentXMLProcessor
