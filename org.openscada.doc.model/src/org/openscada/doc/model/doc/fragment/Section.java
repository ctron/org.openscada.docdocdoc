/**
 */
package org.openscada.doc.model.doc.fragment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Section#getTitle <em>Title
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getSection()
 * @model interface="true" abstract="true"
 *        extendedMetaData="name='section'"
 * @generated
 */
public interface Section extends Container
{
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getSection_Title()
     * @model required="true"
     *        extendedMetaData="kind='group'"
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.fragment.Section#getTitle
     * <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

} // Section
