/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentFactory
 * @model kind="package"
 * @generated
 */
public interface FragmentPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "fragment";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:fragment";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "fragment";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    FragmentPackage eINSTANCE = org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl.init ();

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.Container <em>Container</em>}
     * ' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Container
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getContainer()
     * @generated
     */
    int CONTAINER = 2;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER__CONTENT = 0;

    /**
     * The feature id for the '<em><b>Any Content</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER__ANY_CONTENT = 1;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER__SECTION = 2;

    /**
     * The number of structural features of the '<em>Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.impl.FragmentImpl
     * <em>Fragment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentImpl
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getFragment()
     * @generated
     */
    int FRAGMENT = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FRAGMENT__CONTENT = CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Any Content</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FRAGMENT__ANY_CONTENT = CONTAINER__ANY_CONTENT;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FRAGMENT__SECTION = CONTAINER__SECTION;

    /**
     * The number of structural features of the '<em>Fragment</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FRAGMENT_FEATURE_COUNT = CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.Section <em>Section</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Section
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getSection()
     * @generated
     */
    int SECTION = 1;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SECTION__CONTENT = CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Any Content</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SECTION__ANY_CONTENT = CONTAINER__ANY_CONTENT;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SECTION__SECTION = CONTAINER__SECTION;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SECTION__TITLE = CONTAINER_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Section</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SECTION_FEATURE_COUNT = CONTAINER_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Content
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getContent()
     * @generated
     */
    int CONTENT = 4;

    /**
     * The number of structural features of the '<em>Content</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.impl.PlainTextContentImpl
     * <em>Plain Text Content</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.impl.PlainTextContentImpl
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getPlainTextContent()
     * @generated
     */
    int PLAIN_TEXT_CONTENT = 3;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLAIN_TEXT_CONTENT__VALUE = CONTENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Plain Text Content</em>'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLAIN_TEXT_CONTENT_FEATURE_COUNT = CONTENT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.impl.AuthorImpl
     * <em>Author</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.impl.AuthorImpl
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getAuthor()
     * @generated
     */
    int AUTHOR = 5;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUTHOR__NAME = 0;

    /**
     * The feature id for the '<em><b>Ref</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUTHOR__REF = 1;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUTHOR__ID = 2;

    /**
     * The number of structural features of the '<em>Author</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUTHOR_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.fragment.impl.CopyrightImpl
     * <em>Copyright</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.impl.CopyrightImpl
     * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getCopyright()
     * @generated
     */
    int COPYRIGHT = 6;

    /**
     * The feature id for the '<em><b>Year</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COPYRIGHT__YEAR = 0;

    /**
     * The feature id for the '<em><b>Author</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COPYRIGHT__AUTHOR = 1;

    /**
     * The number of structural features of the '<em>Copyright</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COPYRIGHT_FEATURE_COUNT = 2;

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Fragment <em>Fragment</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Fragment</em>'.
     * @see org.openscada.doc.model.doc.fragment.Fragment
     * @generated
     */
    EClass getFragment ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Section <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Section</em>'.
     * @see org.openscada.doc.model.doc.fragment.Section
     * @generated
     */
    EClass getSection ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.Section#getTitle
     * <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.model.doc.fragment.Section#getTitle()
     * @see #getSection()
     * @generated
     */
    EAttribute getSection_Title ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Container <em>Container</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Container</em>'.
     * @see org.openscada.doc.model.doc.fragment.Container
     * @generated
     */
    EClass getContainer ();

    /**
     * Returns the meta object for the attribute list '
     * {@link org.openscada.doc.model.doc.fragment.Container#getContent
     * <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Content</em>'.
     * @see org.openscada.doc.model.doc.fragment.Container#getContent()
     * @see #getContainer()
     * @generated
     */
    EAttribute getContainer_Content ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.fragment.Container#getSection
     * <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Section</em>'.
     * @see org.openscada.doc.model.doc.fragment.Container#getSection()
     * @see #getContainer()
     * @generated
     */
    EReference getContainer_Section ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.fragment.Container#getAnyContent
     * <em>Any Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Any Content</em>'.
     * @see org.openscada.doc.model.doc.fragment.Container#getAnyContent()
     * @see #getContainer()
     * @generated
     */
    EReference getContainer_AnyContent ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent
     * <em>Plain Text Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plain Text Content</em>'.
     * @see org.openscada.doc.model.doc.fragment.PlainTextContent
     * @generated
     */
    EClass getPlainTextContent ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent#getValue
     * <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see org.openscada.doc.model.doc.fragment.PlainTextContent#getValue()
     * @see #getPlainTextContent()
     * @generated
     */
    EAttribute getPlainTextContent_Value ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Content</em>'.
     * @see org.openscada.doc.model.doc.fragment.Content
     * @generated
     */
    EClass getContent ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Author <em>Author</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Author</em>'.
     * @see org.openscada.doc.model.doc.fragment.Author
     * @generated
     */
    EClass getAuthor ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.Author#getName <em>Name</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.openscada.doc.model.doc.fragment.Author#getName()
     * @see #getAuthor()
     * @generated
     */
    EAttribute getAuthor_Name ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.Author#getRef <em>Ref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ref</em>'.
     * @see org.openscada.doc.model.doc.fragment.Author#getRef()
     * @see #getAuthor()
     * @generated
     */
    EAttribute getAuthor_Ref ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.Author#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see org.openscada.doc.model.doc.fragment.Author#getId()
     * @see #getAuthor()
     * @generated
     */
    EAttribute getAuthor_Id ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.fragment.Copyright <em>Copyright</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Copyright</em>'.
     * @see org.openscada.doc.model.doc.fragment.Copyright
     * @generated
     */
    EClass getCopyright ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.fragment.Copyright#getYear
     * <em>Year</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Year</em>'.
     * @see org.openscada.doc.model.doc.fragment.Copyright#getYear()
     * @see #getCopyright()
     * @generated
     */
    EAttribute getCopyright_Year ();

    /**
     * Returns the meta object for the reference list '
     * {@link org.openscada.doc.model.doc.fragment.Copyright#getAuthor
     * <em>Author</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Author</em>'.
     * @see org.openscada.doc.model.doc.fragment.Copyright#getAuthor()
     * @see #getCopyright()
     * @generated
     */
    EReference getCopyright_Author ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    FragmentFactory getFragmentFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.impl.FragmentImpl
         * <em>Fragment</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentImpl
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getFragment()
         * @generated
         */
        EClass FRAGMENT = eINSTANCE.getFragment ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.Section <em>Section</em>}
         * ' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.Section
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getSection()
         * @generated
         */
        EClass SECTION = eINSTANCE.getSection ();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SECTION__TITLE = eINSTANCE.getSection_Title ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.Container
         * <em>Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.Container
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getContainer()
         * @generated
         */
        EClass CONTAINER = eINSTANCE.getContainer ();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute
         * list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTAINER__CONTENT = eINSTANCE.getContainer_Content ();

        /**
         * The meta object literal for the '<em><b>Section</b></em>' containment
         * reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONTAINER__SECTION = eINSTANCE.getContainer_Section ();

        /**
         * The meta object literal for the '<em><b>Any Content</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONTAINER__ANY_CONTENT = eINSTANCE.getContainer_AnyContent ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.impl.PlainTextContentImpl
         * <em>Plain Text Content</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.impl.PlainTextContentImpl
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getPlainTextContent()
         * @generated
         */
        EClass PLAIN_TEXT_CONTENT = eINSTANCE.getPlainTextContent ();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLAIN_TEXT_CONTENT__VALUE = eINSTANCE.getPlainTextContent_Value ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}
         * ' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.Content
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getContent()
         * @generated
         */
        EClass CONTENT = eINSTANCE.getContent ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.impl.AuthorImpl
         * <em>Author</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.impl.AuthorImpl
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getAuthor()
         * @generated
         */
        EClass AUTHOR = eINSTANCE.getAuthor ();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute AUTHOR__NAME = eINSTANCE.getAuthor_Name ();

        /**
         * The meta object literal for the '<em><b>Ref</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute AUTHOR__REF = eINSTANCE.getAuthor_Ref ();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute AUTHOR__ID = eINSTANCE.getAuthor_Id ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.fragment.impl.CopyrightImpl
         * <em>Copyright</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.fragment.impl.CopyrightImpl
         * @see org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl#getCopyright()
         * @generated
         */
        EClass COPYRIGHT = eINSTANCE.getCopyright ();

        /**
         * The meta object literal for the '<em><b>Year</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COPYRIGHT__YEAR = eINSTANCE.getCopyright_Year ();

        /**
         * The meta object literal for the '<em><b>Author</b></em>' reference
         * list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COPYRIGHT__AUTHOR = eINSTANCE.getCopyright_Author ();

    }

} //FragmentPackage
