/**
 */
package org.openscada.doc.model.doc.fragment;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getContent <em>
 * Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getAnyContent <em>
 * Any Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getSection <em>
 * Section</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getContainer()
 * @model interface="true" abstract="true"
 *        extendedMetaData="kind='elementOnly'"
 * @generated
 */
public interface Container extends EObject
{
    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute list.
     * The list contents are of type
     * {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Content</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Content</em>' attribute list.
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getContainer_Content()
     * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry"
     *        many="true"
     *        extendedMetaData="name='group:0' kind='group'"
     * @generated
     */
    FeatureMap getContent ();

    /**
     * Returns the value of the '<em><b>Section</b></em>' containment reference
     * list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Section}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Section</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Section</em>' containment reference list.
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getContainer_Section()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData=
     *        "namespace='##targetNamespace' name='section' group='#group:0'"
     * @generated
     */
    EList<Section> getSection ();

    /**
     * Returns the value of the '<em><b>Any Content</b></em>' containment
     * reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Content}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Any Content</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Any Content</em>' containment reference
     *         list.
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#getContainer_AnyContent()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData=
     *        "kind='element' namespace='##targetNamespace' name='anyContent' group='#group:0'"
     * @generated
     */
    EList<Content> getAnyContent ();

} // Container
