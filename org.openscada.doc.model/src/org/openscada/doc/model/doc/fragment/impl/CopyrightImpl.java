/**
 */
package org.openscada.doc.model.doc.fragment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.fragment.FragmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copyright</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.impl.CopyrightImpl#getYear
 * <em>Year</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.impl.CopyrightImpl#getAuthor
 * <em>Author</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CopyrightImpl extends EObjectImpl implements Copyright
{
    /**
     * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getYear()
     * @generated
     * @ordered
     */
    protected static final int YEAR_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getYear()
     * @generated
     * @ordered
     */
    protected int year = YEAR_EDEFAULT;

    /**
     * The cached value of the '{@link #getAuthor() <em>Author</em>}' reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAuthor()
     * @generated
     * @ordered
     */
    protected EList<Author> author;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CopyrightImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return FragmentPackage.Literals.COPYRIGHT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getYear ()
    {
        return this.year;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setYear ( final int newYear )
    {
        final int oldYear = this.year;
        this.year = newYear;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, FragmentPackage.COPYRIGHT__YEAR, oldYear, this.year ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Author> getAuthor ()
    {
        if ( this.author == null )
        {
            this.author = new EObjectResolvingEList<Author> ( Author.class, this, FragmentPackage.COPYRIGHT__AUTHOR );
        }
        return this.author;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case FragmentPackage.COPYRIGHT__YEAR:
                return getYear ();
            case FragmentPackage.COPYRIGHT__AUTHOR:
                return getAuthor ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case FragmentPackage.COPYRIGHT__YEAR:
                setYear ( (Integer)newValue );
                return;
            case FragmentPackage.COPYRIGHT__AUTHOR:
                getAuthor ().clear ();
                getAuthor ().addAll ( (Collection<? extends Author>)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case FragmentPackage.COPYRIGHT__YEAR:
                setYear ( YEAR_EDEFAULT );
                return;
            case FragmentPackage.COPYRIGHT__AUTHOR:
                getAuthor ().clear ();
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case FragmentPackage.COPYRIGHT__YEAR:
                return this.year != YEAR_EDEFAULT;
            case FragmentPackage.COPYRIGHT__AUTHOR:
                return this.author != null && !this.author.isEmpty ();
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (year: " );
        result.append ( this.year );
        result.append ( ')' );
        return result.toString ();
    }

} //CopyrightImpl
