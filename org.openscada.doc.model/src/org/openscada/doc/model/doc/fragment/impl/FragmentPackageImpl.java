/**
 */
package org.openscada.doc.model.doc.fragment.impl;

import static org.openscada.doc.model.doc.fragment.FragmentPackage.CONTAINER;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl;
import org.openscada.doc.model.doc.book.impl.BookPackageImpl;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.fragment.Fragment;
import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.PlainTextContent;
import org.openscada.doc.model.doc.fragment.Section;
import org.openscada.doc.model.doc.impl.DocPackageImpl;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.impl.MapPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class FragmentPackageImpl extends EPackageImpl implements FragmentPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass fragmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass containerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass plainTextContentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass contentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass authorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass copyrightEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.model.doc.fragment.FragmentPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private FragmentPackageImpl ()
    {
        super ( eNS_URI, FragmentFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link FragmentPackage#eINSTANCE} when
     * that field is accessed. Clients should not invoke it directly. Instead,
     * they should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static FragmentPackage init ()
    {
        if ( isInited )
        {
            return (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );
        }

        // Obtain or create and register package
        final FragmentPackageImpl theFragmentPackage = (FragmentPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof FragmentPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new FragmentPackageImpl () );

        isInited = true;

        // Obtain or create and register interdependencies
        final DocPackageImpl theDocPackage = (DocPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) instanceof DocPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) : DocPackage.eINSTANCE );
        final MapPackageImpl theMapPackage = (MapPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) instanceof MapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) : MapPackage.eINSTANCE );
        final BookPackageImpl theBookPackage = (BookPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) instanceof BookPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) : BookPackage.eINSTANCE );
        final BuilderPackageImpl theBuilderPackage = (BuilderPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) instanceof BuilderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) : BuilderPackage.eINSTANCE );

        // Create package meta-data objects
        theFragmentPackage.createPackageContents ();
        theDocPackage.createPackageContents ();
        theMapPackage.createPackageContents ();
        theBookPackage.createPackageContents ();
        theBuilderPackage.createPackageContents ();

        // Initialize created meta-data
        theFragmentPackage.initializePackageContents ();
        theDocPackage.initializePackageContents ();
        theMapPackage.initializePackageContents ();
        theBookPackage.initializePackageContents ();
        theBuilderPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theFragmentPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( FragmentPackage.eNS_URI, theFragmentPackage );
        return theFragmentPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFragment ()
    {
        return this.fragmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSection ()
    {
        return this.sectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSection_Title ()
    {
        return (EAttribute)this.sectionEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContainer ()
    {
        return this.containerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContainer_Content ()
    {
        return (EAttribute)this.containerEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getContainer_Section ()
    {
        return (EReference)this.containerEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getContainer_AnyContent ()
    {
        return (EReference)this.containerEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPlainTextContent ()
    {
        return this.plainTextContentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPlainTextContent_Value ()
    {
        return (EAttribute)this.plainTextContentEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContent ()
    {
        return this.contentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAuthor ()
    {
        return this.authorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAuthor_Name ()
    {
        return (EAttribute)this.authorEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAuthor_Ref ()
    {
        return (EAttribute)this.authorEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAuthor_Id ()
    {
        return (EAttribute)this.authorEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCopyright ()
    {
        return this.copyrightEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCopyright_Year ()
    {
        return (EAttribute)this.copyrightEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getCopyright_Author ()
    {
        return (EReference)this.copyrightEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FragmentFactory getFragmentFactory ()
    {
        return (FragmentFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents ()
    {
        if ( this.isCreated )
        {
            return;
        }
        this.isCreated = true;

        // Create classes and their features
        this.fragmentEClass = createEClass ( FRAGMENT );

        this.sectionEClass = createEClass ( SECTION );
        createEAttribute ( this.sectionEClass, SECTION__TITLE );

        this.containerEClass = createEClass ( CONTAINER );
        createEAttribute ( this.containerEClass, CONTAINER__CONTENT );
        createEReference ( this.containerEClass, CONTAINER__ANY_CONTENT );
        createEReference ( this.containerEClass, CONTAINER__SECTION );

        this.plainTextContentEClass = createEClass ( PLAIN_TEXT_CONTENT );
        createEAttribute ( this.plainTextContentEClass, PLAIN_TEXT_CONTENT__VALUE );

        this.contentEClass = createEClass ( CONTENT );

        this.authorEClass = createEClass ( AUTHOR );
        createEAttribute ( this.authorEClass, AUTHOR__NAME );
        createEAttribute ( this.authorEClass, AUTHOR__REF );
        createEAttribute ( this.authorEClass, AUTHOR__ID );

        this.copyrightEClass = createEClass ( COPYRIGHT );
        createEAttribute ( this.copyrightEClass, COPYRIGHT__YEAR );
        createEReference ( this.copyrightEClass, COPYRIGHT__AUTHOR );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( this.isInitialized )
        {
            return;
        }
        this.isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        this.fragmentEClass.getESuperTypes ().add ( getContainer () );
        this.sectionEClass.getESuperTypes ().add ( getContainer () );
        this.plainTextContentEClass.getESuperTypes ().add ( getContent () );

        // Initialize classes and features; add operations and parameters
        initEClass ( this.fragmentEClass, Fragment.class, "Fragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        initEClass ( this.sectionEClass, Section.class, "Section", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getSection_Title (), this.ecorePackage.getEString (), "title", null, 1, 1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.containerEClass, org.openscada.doc.model.doc.fragment.Container.class, "Container", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getContainer_Content (), this.ecorePackage.getEFeatureMapEntry (), "content", null, 0, -1, org.openscada.doc.model.doc.fragment.Container.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getContainer_AnyContent (), getContent (), null, "anyContent", null, 0, -1, org.openscada.doc.model.doc.fragment.Container.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED );
        initEReference ( getContainer_Section (), getSection (), null, "section", null, 0, -1, org.openscada.doc.model.doc.fragment.Container.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED );

        initEClass ( this.plainTextContentEClass, PlainTextContent.class, "PlainTextContent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getPlainTextContent_Value (), this.ecorePackage.getEString (), "value", null, 0, 1, PlainTextContent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.contentEClass, Content.class, "Content", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        initEClass ( this.authorEClass, Author.class, "Author", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getAuthor_Name (), this.ecorePackage.getEString (), "name", null, 1, 1, Author.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getAuthor_Ref (), this.ecorePackage.getEString (), "ref", null, 0, 1, Author.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getAuthor_Id (), this.ecorePackage.getEString (), "id", null, 1, 1, Author.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.copyrightEClass, Copyright.class, "Copyright", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getCopyright_Year (), this.ecorePackage.getEInt (), "year", null, 1, 1, Copyright.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getCopyright_Author (), getAuthor (), null, "author", null, 1, -1, Copyright.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations ();
    }

    /**
     * Initializes the annotations for
     * <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createExtendedMetaDataAnnotations ()
    {
        final String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
        addAnnotation ( this.fragmentEClass, source, new String[] { "name", "fragment" } );
        addAnnotation ( this.sectionEClass, source, new String[] { "name", "section" } );
        addAnnotation ( getSection_Title (), source, new String[] { "kind", "group" } );
        addAnnotation ( this.containerEClass, source, new String[] { "kind", "elementOnly" } );
        addAnnotation ( getContainer_Content (), source, new String[] { "name", "group:0", "kind", "group" } );
        addAnnotation ( getContainer_AnyContent (), source, new String[] { "kind", "element", "namespace", "##targetNamespace", "name", "anyContent", "group", "#group:0" } );
        addAnnotation ( getContainer_Section (), source, new String[] { "namespace", "##targetNamespace", "name", "section", "group", "#group:0" } );
        addAnnotation ( this.plainTextContentEClass, source, new String[] { "kind", "simple" } );
        addAnnotation ( getPlainTextContent_Value (), source, new String[] { "name", ":0", "kind", "simple" } );
    }

} //FragmentPackageImpl
