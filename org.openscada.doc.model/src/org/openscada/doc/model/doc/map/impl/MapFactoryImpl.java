/**
 */
package org.openscada.doc.model.doc.map.impl;

import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.File;
import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapFactory;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.ResourceFactory;
import org.openscada.doc.model.doc.map.RuleResult;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MapFactoryImpl extends EFactoryImpl implements MapFactory
{
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static MapFactory init ()
    {
        try
        {
            final MapFactory theMapFactory = (MapFactory)EPackage.Registry.INSTANCE.getEFactory ( "urn:openscada:doc:map" );
            if ( theMapFactory != null )
            {
                return theMapFactory;
            }
        }
        catch ( final Exception exception )
        {
            EcorePlugin.INSTANCE.log ( exception );
        }
        return new MapFactoryImpl ();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapFactoryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create ( final EClass eClass )
    {
        switch ( eClass.getClassifierID () )
        {
            case MapPackage.MAP:
                return createMap ();
            case MapPackage.FILE:
                return createFile ();
            case MapPackage.MAP_SECTION:
                return createMapSection ();
            case MapPackage.FEATURE:
                return createFeature ();
            case MapPackage.INCLUDE_PATTERN_RULE:
                return createIncludePatternRule ();
            case MapPackage.EXCLUDE_PATTERN_RULE:
                return createExcludePatternRule ();
            case MapPackage.RESOURCE_FACTORY:
                return createResourceFactory ();
            case MapPackage.EXTENSION_MAPPING_ENTRY:
                return createExtensionMappingEntry ();
            default:
                throw new IllegalArgumentException ( "The class '" + eClass.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object createFromString ( final EDataType eDataType, final String initialValue )
    {
        switch ( eDataType.getClassifierID () )
        {
            case MapPackage.RULE_RESULT:
                return createRuleResultFromString ( eDataType, initialValue );
            case MapPackage.PATTERN:
                return createPatternFromString ( eDataType, initialValue );
            default:
                throw new IllegalArgumentException ( "The datatype '" + eDataType.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String convertToString ( final EDataType eDataType, final Object instanceValue )
    {
        switch ( eDataType.getClassifierID () )
        {
            case MapPackage.RULE_RESULT:
                return convertRuleResultToString ( eDataType, instanceValue );
            case MapPackage.PATTERN:
                return convertPatternToString ( eDataType, instanceValue );
            default:
                throw new IllegalArgumentException ( "The datatype '" + eDataType.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Map createMap ()
    {
        final MapImpl map = new MapImpl ();
        return map;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public File createFile ()
    {
        final FileImpl file = new FileImpl ();
        return file;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public MapSection createMapSection ()
    {
        final MapSectionImpl mapSection = new MapSectionImpl ();
        return mapSection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Feature createFeature ()
    {
        final FeatureImpl feature = new FeatureImpl ();
        return feature;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IncludePatternRule createIncludePatternRule ()
    {
        final IncludePatternRuleImpl includePatternRule = new IncludePatternRuleImpl ();
        return includePatternRule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ExcludePatternRule createExcludePatternRule ()
    {
        final ExcludePatternRuleImpl excludePatternRule = new ExcludePatternRuleImpl ();
        return excludePatternRule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceFactory createResourceFactory ()
    {
        final ResourceFactoryImpl resourceFactory = new ResourceFactoryImpl ();
        return resourceFactory;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ExtensionMappingEntry createExtensionMappingEntry ()
    {
        final ExtensionMappingEntryImpl extensionMappingEntry = new ExtensionMappingEntryImpl ();
        return extensionMappingEntry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public RuleResult createRuleResultFromString ( final EDataType eDataType, final String initialValue )
    {
        final RuleResult result = RuleResult.get ( initialValue );
        if ( result == null )
        {
            throw new IllegalArgumentException ( "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName () + "'" );
        }
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertRuleResultToString ( final EDataType eDataType, final Object instanceValue )
    {
        return instanceValue == null ? null : instanceValue.toString ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public Pattern createPatternFromString ( final EDataType eDataType, final String initialValue )
    {
        return Pattern.compile ( initialValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertPatternToString ( final EDataType eDataType, final Object instanceValue )
    {
        return super.convertToString ( eDataType, instanceValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public MapPackage getMapPackage ()
    {
        return (MapPackage)getEPackage ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static MapPackage getPackage ()
    {
        return MapPackage.eINSTANCE;
    }

} //MapFactoryImpl
