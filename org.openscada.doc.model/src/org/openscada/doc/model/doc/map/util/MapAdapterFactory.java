/**
 */
package org.openscada.doc.model.doc.map.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.map.ContentGenerator;
import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.File;
import org.openscada.doc.model.doc.map.Import;
import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapContainer;
import org.openscada.doc.model.doc.map.MapElement;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.NameRule;
import org.openscada.doc.model.doc.map.PatternRule;
import org.openscada.doc.model.doc.map.ResourceFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the
 * model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage
 * @generated
 */
public class MapAdapterFactory extends AdapterFactoryImpl
{
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static MapPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapAdapterFactory ()
    {
        if ( modelPackage == null )
        {
            modelPackage = MapPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the
     * model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType ( final Object object )
    {
        if ( object == modelPackage )
        {
            return true;
        }
        if ( object instanceof EObject )
        {
            return ( (EObject)object ).eClass ().getEPackage () == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapSwitch<Adapter> modelSwitch = new MapSwitch<Adapter> () {
        @Override
        public Adapter caseMap ( final Map object )
        {
            return createMapAdapter ();
        }

        @Override
        public Adapter caseImport ( final Import object )
        {
            return createImportAdapter ();
        }

        @Override
        public Adapter caseFile ( final File object )
        {
            return createFileAdapter ();
        }

        @Override
        public Adapter caseMapSection ( final MapSection object )
        {
            return createMapSectionAdapter ();
        }

        @Override
        public Adapter caseMapElement ( final MapElement object )
        {
            return createMapElementAdapter ();
        }

        @Override
        public Adapter caseFeature ( final Feature object )
        {
            return createFeatureAdapter ();
        }

        @Override
        public Adapter caseNameRule ( final NameRule object )
        {
            return createNameRuleAdapter ();
        }

        @Override
        public Adapter casePatternRule ( final PatternRule object )
        {
            return createPatternRuleAdapter ();
        }

        @Override
        public Adapter caseIncludePatternRule ( final IncludePatternRule object )
        {
            return createIncludePatternRuleAdapter ();
        }

        @Override
        public Adapter caseExcludePatternRule ( final ExcludePatternRule object )
        {
            return createExcludePatternRuleAdapter ();
        }

        @Override
        public Adapter caseResourceFactory ( final ResourceFactory object )
        {
            return createResourceFactoryAdapter ();
        }

        @Override
        public Adapter caseExtensionMappingEntry ( final ExtensionMappingEntry object )
        {
            return createExtensionMappingEntryAdapter ();
        }

        @Override
        public Adapter caseContentGenerator ( final ContentGenerator object )
        {
            return createContentGeneratorAdapter ();
        }

        @Override
        public Adapter caseMapContainer ( final MapContainer object )
        {
            return createMapContainerAdapter ();
        }

        @Override
        public Adapter caseContent ( final Content object )
        {
            return createContentAdapter ();
        }

        @Override
        public Adapter defaultCase ( final EObject object )
        {
            return createEObjectAdapter ();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target
     *            the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter ( final Notifier target )
    {
        return this.modelSwitch.doSwitch ( (EObject)target );
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.Map <em>Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.Map
     * @generated
     */
    public Adapter createMapAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.Import <em>Import</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.Import
     * @generated
     */
    public Adapter createImportAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.File <em>File</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.File
     * @generated
     */
    public Adapter createFileAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.MapSection <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.MapSection
     * @generated
     */
    public Adapter createMapSectionAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.MapElement <em>Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.MapElement
     * @generated
     */
    public Adapter createMapElementAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.Feature <em>Feature</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.Feature
     * @generated
     */
    public Adapter createFeatureAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.NameRule <em>Name Rule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.NameRule
     * @generated
     */
    public Adapter createNameRuleAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.PatternRule <em>Pattern Rule</em>}
     * '.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.PatternRule
     * @generated
     */
    public Adapter createPatternRuleAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.IncludePatternRule
     * <em>Include Pattern Rule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.IncludePatternRule
     * @generated
     */
    public Adapter createIncludePatternRuleAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.ExcludePatternRule
     * <em>Exclude Pattern Rule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.ExcludePatternRule
     * @generated
     */
    public Adapter createExcludePatternRuleAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.ResourceFactory
     * <em>Resource Factory</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.ResourceFactory
     * @generated
     */
    public Adapter createResourceFactoryAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry
     * <em>Extension Mapping Entry</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.ExtensionMappingEntry
     * @generated
     */
    public Adapter createExtensionMappingEntryAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.ContentGenerator
     * <em>Content Generator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.ContentGenerator
     * @generated
     */
    public Adapter createContentGeneratorAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.MapContainer <em>Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.MapContainer
     * @generated
     */
    public Adapter createMapContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Content
     * @generated
     */
    public Adapter createContentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter ()
    {
        return null;
    }

} //MapAdapterFactory
