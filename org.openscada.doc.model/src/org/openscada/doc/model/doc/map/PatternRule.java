/**
 */
package org.openscada.doc.model.doc.map;

import java.util.regex.Pattern;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.PatternRule#getPattern <em>Pattern
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getPatternRule()
 * @model abstract="true"
 * @generated
 */
public interface PatternRule extends NameRule
{
    /**
     * Returns the value of the '<em><b>Pattern</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Pattern</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Pattern</em>' attribute.
     * @see #setPattern(Pattern)
     * @see org.openscada.doc.model.doc.map.MapPackage#getPatternRule_Pattern()
     * @model dataType="org.openscada.doc.model.doc.map.Pattern" required="true"
     * @generated
     */
    Pattern getPattern ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.PatternRule#getPattern
     * <em>Pattern</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Pattern</em>' attribute.
     * @see #getPattern()
     * @generated
     */
    void setPattern ( Pattern value );

} // PatternRule
