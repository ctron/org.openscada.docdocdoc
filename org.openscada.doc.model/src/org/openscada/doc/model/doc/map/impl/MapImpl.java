/**
 */
package org.openscada.doc.model.doc.map.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapElement;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.Visitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapImpl#getElements <em>
 * Elements</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapImpl#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapImpl#getExtensionMappings
 * <em>Extension Mappings</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MapImpl extends EObjectImpl implements Map
{
    /**
     * The cached value of the '{@link #getElements() <em>Elements</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getElements()
     * @generated
     * @ordered
     */
    protected EList<MapElement> elements;

    /**
     * The default value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected static final NumberingStyle NUMBERING_STYLE_EDEFAULT = NumberingStyle.ARABIC;

    /**
     * The cached value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected NumberingStyle numberingStyle = NUMBERING_STYLE_EDEFAULT;

    /**
     * The cached value of the '{@link #getExtensionMappings()
     * <em>Extension Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtensionMappings()
     * @generated
     * @ordered
     */
    protected EList<ExtensionMappingEntry> extensionMappings;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.MAP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<MapElement> getElements ()
    {
        if ( this.elements == null )
        {
            this.elements = new EObjectContainmentEList.Resolving<MapElement> ( MapElement.class, this, MapPackage.MAP__ELEMENTS );
        }
        return this.elements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NumberingStyle getNumberingStyle ()
    {
        return this.numberingStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumberingStyle ( final NumberingStyle newNumberingStyle )
    {
        final NumberingStyle oldNumberingStyle = this.numberingStyle;
        this.numberingStyle = newNumberingStyle == null ? NUMBERING_STYLE_EDEFAULT : newNumberingStyle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.MAP__NUMBERING_STYLE, oldNumberingStyle, this.numberingStyle ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<ExtensionMappingEntry> getExtensionMappings ()
    {
        if ( this.extensionMappings == null )
        {
            this.extensionMappings = new EObjectContainmentEList.Resolving<ExtensionMappingEntry> ( ExtensionMappingEntry.class, this, MapPackage.MAP__EXTENSION_MAPPINGS );
        }
        return this.extensionMappings;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public void visit ( final Visitor visitor )
    {
        if ( this.elements != null )
        {
            for ( final MapElement element : this.elements )
            {
                element.visit ( visitor );
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case MapPackage.MAP__ELEMENTS:
                return ( (InternalEList<?>)getElements () ).basicRemove ( otherEnd, msgs );
            case MapPackage.MAP__EXTENSION_MAPPINGS:
                return ( (InternalEList<?>)getExtensionMappings () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case MapPackage.MAP__ELEMENTS:
                return getElements ();
            case MapPackage.MAP__NUMBERING_STYLE:
                return getNumberingStyle ();
            case MapPackage.MAP__EXTENSION_MAPPINGS:
                return getExtensionMappings ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case MapPackage.MAP__ELEMENTS:
                getElements ().clear ();
                getElements ().addAll ( (Collection<? extends MapElement>)newValue );
                return;
            case MapPackage.MAP__NUMBERING_STYLE:
                setNumberingStyle ( (NumberingStyle)newValue );
                return;
            case MapPackage.MAP__EXTENSION_MAPPINGS:
                getExtensionMappings ().clear ();
                getExtensionMappings ().addAll ( (Collection<? extends ExtensionMappingEntry>)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.MAP__ELEMENTS:
                getElements ().clear ();
                return;
            case MapPackage.MAP__NUMBERING_STYLE:
                setNumberingStyle ( NUMBERING_STYLE_EDEFAULT );
                return;
            case MapPackage.MAP__EXTENSION_MAPPINGS:
                getExtensionMappings ().clear ();
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.MAP__ELEMENTS:
                return this.elements != null && !this.elements.isEmpty ();
            case MapPackage.MAP__NUMBERING_STYLE:
                return this.numberingStyle != NUMBERING_STYLE_EDEFAULT;
            case MapPackage.MAP__EXTENSION_MAPPINGS:
                return this.extensionMappings != null && !this.extensionMappings.isEmpty ();
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (numberingStyle: " );
        result.append ( this.numberingStyle );
        result.append ( ')' );
        return result.toString ();
    }

} //MapImpl
