/**
 */
package org.openscada.doc.model.doc.map.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.map.MapElement;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.Visitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapSectionImpl#getElements
 * <em>Elements</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.map.impl.MapSectionImpl#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapSectionImpl#getTitle <em>
 * Title</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.MapSectionImpl#getId <em>Id
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MapSectionImpl extends EObjectImpl implements MapSection
{
    /**
     * The cached value of the '{@link #getElements() <em>Elements</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getElements()
     * @generated
     * @ordered
     */
    protected EList<MapElement> elements;

    /**
     * The default value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected static final NumberingStyle NUMBERING_STYLE_EDEFAULT = NumberingStyle.ARABIC;

    /**
     * The cached value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected NumberingStyle numberingStyle = NUMBERING_STYLE_EDEFAULT;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapSectionImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.MAP_SECTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<MapElement> getElements ()
    {
        if ( this.elements == null )
        {
            this.elements = new EObjectContainmentEList.Resolving<MapElement> ( MapElement.class, this, MapPackage.MAP_SECTION__ELEMENTS );
        }
        return this.elements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NumberingStyle getNumberingStyle ()
    {
        return this.numberingStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumberingStyle ( final NumberingStyle newNumberingStyle )
    {
        final NumberingStyle oldNumberingStyle = this.numberingStyle;
        this.numberingStyle = newNumberingStyle == null ? NUMBERING_STYLE_EDEFAULT : newNumberingStyle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.MAP_SECTION__NUMBERING_STYLE, oldNumberingStyle, this.numberingStyle ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return this.title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle ( final String newTitle )
    {
        final String oldTitle = this.title;
        this.title = newTitle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.MAP_SECTION__TITLE, oldTitle, this.title ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId ()
    {
        return this.id;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId ( final String newId )
    {
        final String oldId = this.id;
        this.id = newId;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.MAP_SECTION__ID, oldId, this.id ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public void visit ( final Visitor visitor )
    {
        visitor.openSection ( this );

        if ( this.elements != null )
        {
            for ( final MapElement element : this.elements )
            {
                element.visit ( visitor );
            }
        }

        visitor.closeSection ( this );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case MapPackage.MAP_SECTION__ELEMENTS:
                return ( (InternalEList<?>)getElements () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case MapPackage.MAP_SECTION__ELEMENTS:
                return getElements ();
            case MapPackage.MAP_SECTION__NUMBERING_STYLE:
                return getNumberingStyle ();
            case MapPackage.MAP_SECTION__TITLE:
                return getTitle ();
            case MapPackage.MAP_SECTION__ID:
                return getId ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case MapPackage.MAP_SECTION__ELEMENTS:
                getElements ().clear ();
                getElements ().addAll ( (Collection<? extends MapElement>)newValue );
                return;
            case MapPackage.MAP_SECTION__NUMBERING_STYLE:
                setNumberingStyle ( (NumberingStyle)newValue );
                return;
            case MapPackage.MAP_SECTION__TITLE:
                setTitle ( (String)newValue );
                return;
            case MapPackage.MAP_SECTION__ID:
                setId ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.MAP_SECTION__ELEMENTS:
                getElements ().clear ();
                return;
            case MapPackage.MAP_SECTION__NUMBERING_STYLE:
                setNumberingStyle ( NUMBERING_STYLE_EDEFAULT );
                return;
            case MapPackage.MAP_SECTION__TITLE:
                setTitle ( TITLE_EDEFAULT );
                return;
            case MapPackage.MAP_SECTION__ID:
                setId ( ID_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.MAP_SECTION__ELEMENTS:
                return this.elements != null && !this.elements.isEmpty ();
            case MapPackage.MAP_SECTION__NUMBERING_STYLE:
                return this.numberingStyle != NUMBERING_STYLE_EDEFAULT;
            case MapPackage.MAP_SECTION__TITLE:
                return TITLE_EDEFAULT == null ? this.title != null : !TITLE_EDEFAULT.equals ( this.title );
            case MapPackage.MAP_SECTION__ID:
                return ID_EDEFAULT == null ? this.id != null : !ID_EDEFAULT.equals ( this.id );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (numberingStyle: " );
        result.append ( this.numberingStyle );
        result.append ( ", title: " );
        result.append ( this.title );
        result.append ( ", id: " );
        result.append ( this.id );
        result.append ( ')' );
        return result.toString ();
    }

} //MapSectionImpl
