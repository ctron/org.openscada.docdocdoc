/**
 */
package org.openscada.doc.model.doc.map.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.map.ContentGenerator;
import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.File;
import org.openscada.doc.model.doc.map.Import;
import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapContainer;
import org.openscada.doc.model.doc.map.MapElement;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.NameRule;
import org.openscada.doc.model.doc.map.PatternRule;
import org.openscada.doc.model.doc.map.ResourceFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)} to invoke
 * the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage
 * @generated
 */
public class MapSwitch<T> extends Switch<T>
{
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static MapPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapSwitch ()
    {
        if ( modelPackage == null )
        {
            modelPackage = MapPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @parameter ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor ( final EPackage ePackage )
    {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns
     * a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code>
     *         call.
     * @generated
     */
    @Override
    protected T doSwitch ( final int classifierID, final EObject theEObject )
    {
        switch ( classifierID )
        {
            case MapPackage.MAP:
            {
                final Map map = (Map)theEObject;
                T result = caseMap ( map );
                if ( result == null )
                {
                    result = caseMapContainer ( map );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.IMPORT:
            {
                final Import import_ = (Import)theEObject;
                T result = caseImport ( import_ );
                if ( result == null )
                {
                    result = caseMapElement ( import_ );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.FILE:
            {
                final File file = (File)theEObject;
                T result = caseFile ( file );
                if ( result == null )
                {
                    result = caseImport ( file );
                }
                if ( result == null )
                {
                    result = caseMapElement ( file );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.MAP_SECTION:
            {
                final MapSection mapSection = (MapSection)theEObject;
                T result = caseMapSection ( mapSection );
                if ( result == null )
                {
                    result = caseMapContainer ( mapSection );
                }
                if ( result == null )
                {
                    result = caseMapElement ( mapSection );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.MAP_ELEMENT:
            {
                final MapElement mapElement = (MapElement)theEObject;
                T result = caseMapElement ( mapElement );
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.FEATURE:
            {
                final Feature feature = (Feature)theEObject;
                T result = caseFeature ( feature );
                if ( result == null )
                {
                    result = caseImport ( feature );
                }
                if ( result == null )
                {
                    result = caseMapElement ( feature );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.NAME_RULE:
            {
                final NameRule nameRule = (NameRule)theEObject;
                T result = caseNameRule ( nameRule );
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.PATTERN_RULE:
            {
                final PatternRule patternRule = (PatternRule)theEObject;
                T result = casePatternRule ( patternRule );
                if ( result == null )
                {
                    result = caseNameRule ( patternRule );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.INCLUDE_PATTERN_RULE:
            {
                final IncludePatternRule includePatternRule = (IncludePatternRule)theEObject;
                T result = caseIncludePatternRule ( includePatternRule );
                if ( result == null )
                {
                    result = casePatternRule ( includePatternRule );
                }
                if ( result == null )
                {
                    result = caseNameRule ( includePatternRule );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.EXCLUDE_PATTERN_RULE:
            {
                final ExcludePatternRule excludePatternRule = (ExcludePatternRule)theEObject;
                T result = caseExcludePatternRule ( excludePatternRule );
                if ( result == null )
                {
                    result = casePatternRule ( excludePatternRule );
                }
                if ( result == null )
                {
                    result = caseNameRule ( excludePatternRule );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.RESOURCE_FACTORY:
            {
                final ResourceFactory resourceFactory = (ResourceFactory)theEObject;
                T result = caseResourceFactory ( resourceFactory );
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.EXTENSION_MAPPING_ENTRY:
            {
                final ExtensionMappingEntry extensionMappingEntry = (ExtensionMappingEntry)theEObject;
                T result = caseExtensionMappingEntry ( extensionMappingEntry );
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.CONTENT_GENERATOR:
            {
                final ContentGenerator contentGenerator = (ContentGenerator)theEObject;
                T result = caseContentGenerator ( contentGenerator );
                if ( result == null )
                {
                    result = caseMapElement ( contentGenerator );
                }
                if ( result == null )
                {
                    result = caseContent ( contentGenerator );
                }
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            case MapPackage.MAP_CONTAINER:
            {
                final MapContainer mapContainer = (MapContainer)theEObject;
                T result = caseMapContainer ( mapContainer );
                if ( result == null )
                {
                    result = defaultCase ( theEObject );
                }
                return result;
            }
            default:
                return defaultCase ( theEObject );
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMap ( final Map object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Import</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Import</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseImport ( final Import object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>File</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>File</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFile ( final File object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Section</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Section</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMapSection ( final MapSection object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMapElement ( final MapElement object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Feature</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Feature</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFeature ( final Feature object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Name Rule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Name Rule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNameRule ( final NameRule object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Pattern Rule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Pattern Rule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePatternRule ( final PatternRule object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Include Pattern Rule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Include Pattern Rule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIncludePatternRule ( final IncludePatternRule object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Exclude Pattern Rule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Exclude Pattern Rule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExcludePatternRule ( final ExcludePatternRule object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Resource Factory</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Resource Factory</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResourceFactory ( final ResourceFactory object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Extension Mapping Entry</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Extension Mapping Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExtensionMappingEntry ( final ExtensionMappingEntry object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Content Generator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Content Generator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContentGenerator ( final ContentGenerator object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMapContainer ( final MapContainer object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>Content</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>Content</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContent ( final Content object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '
     * <em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the
     * last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '
     *         <em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase ( final EObject object )
    {
        return null;
    }

} //MapSwitch
