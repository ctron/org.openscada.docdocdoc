/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getMapElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface MapElement extends EObject
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model visitorDataType="org.openscada.doc.model.doc.map.Visitor"
     * @generated
     */
    void visit ( Visitor visitor );

} // MapElement
