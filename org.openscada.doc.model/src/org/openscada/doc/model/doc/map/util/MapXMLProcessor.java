/**
 */
package org.openscada.doc.model.doc.map.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.openscada.doc.model.doc.map.MapPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MapXMLProcessor extends XMLProcessor
{

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapXMLProcessor ()
    {
        super ( EPackage.Registry.INSTANCE );
        MapPackage.eINSTANCE.eClass ();
    }

    /**
     * Register for "*" and "xml" file extensions the MapResourceFactoryImpl
     * factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations ()
    {
        if ( this.registrations == null )
        {
            super.getRegistrations ();
            this.registrations.put ( XML_EXTENSION, new MapResourceFactoryImpl () );
            this.registrations.put ( STAR_EXTENSION, new MapResourceFactoryImpl () );
        }
        return this.registrations;
    }

} //MapXMLProcessor
