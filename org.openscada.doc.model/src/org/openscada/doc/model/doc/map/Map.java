/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.Map#getExtensionMappings <em>
 * Extension Mappings</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getMap()
 * @model
 * @generated
 */
public interface Map extends MapContainer
{
    /**
     * Returns the value of the '<em><b>Extension Mappings</b></em>' containment
     * reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Extension Mappings</em>' containment reference
     * list isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Extension Mappings</em>' containment
     *         reference list.
     * @see org.openscada.doc.model.doc.map.MapPackage#getMap_ExtensionMappings()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<ExtensionMappingEntry> getExtensionMappings ();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model visitorDataType="org.openscada.doc.model.doc.map.Visitor"
     * @generated
     */
    void visit ( Visitor visitor );

} // Map
