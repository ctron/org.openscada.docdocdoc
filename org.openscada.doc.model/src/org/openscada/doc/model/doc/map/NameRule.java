/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name Rule</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getNameRule()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface NameRule extends EObject
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    RuleResult checkRule ( String name );

} // NameRule
