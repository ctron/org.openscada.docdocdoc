/**
 */
package org.openscada.doc.model.doc.map.impl;

import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl;
import org.openscada.doc.model.doc.book.impl.BookPackageImpl;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl;
import org.openscada.doc.model.doc.impl.DocPackageImpl;
import org.openscada.doc.model.doc.map.ContentGenerator;
import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.File;
import org.openscada.doc.model.doc.map.Import;
import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapContainer;
import org.openscada.doc.model.doc.map.MapElement;
import org.openscada.doc.model.doc.map.MapFactory;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.NameRule;
import org.openscada.doc.model.doc.map.PatternRule;
import org.openscada.doc.model.doc.map.ResourceFactory;
import org.openscada.doc.model.doc.map.RuleResult;
import org.openscada.doc.model.doc.map.Visitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MapPackageImpl extends EPackageImpl implements MapPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass mapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass importEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass fileEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass mapSectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass mapElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass featureEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass nameRuleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass patternRuleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass includePatternRuleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass excludePatternRuleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass resourceFactoryEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass extensionMappingEntryEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass contentGeneratorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass mapContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum ruleResultEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType visitorEDataType = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType patternEDataType = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.model.doc.map.MapPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private MapPackageImpl ()
    {
        super ( eNS_URI, MapFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link MapPackage#eINSTANCE} when that
     * field is accessed. Clients should not invoke it directly. Instead, they
     * should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static MapPackage init ()
    {
        if ( isInited )
        {
            return (MapPackage)EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI );
        }

        // Obtain or create and register package
        final MapPackageImpl theMapPackage = (MapPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof MapPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new MapPackageImpl () );

        isInited = true;

        // Obtain or create and register interdependencies
        final DocPackageImpl theDocPackage = (DocPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) instanceof DocPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) : DocPackage.eINSTANCE );
        final FragmentPackageImpl theFragmentPackage = (FragmentPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) instanceof FragmentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) : FragmentPackage.eINSTANCE );
        final BookPackageImpl theBookPackage = (BookPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) instanceof BookPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) : BookPackage.eINSTANCE );
        final BuilderPackageImpl theBuilderPackage = (BuilderPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) instanceof BuilderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) : BuilderPackage.eINSTANCE );

        // Create package meta-data objects
        theMapPackage.createPackageContents ();
        theDocPackage.createPackageContents ();
        theFragmentPackage.createPackageContents ();
        theBookPackage.createPackageContents ();
        theBuilderPackage.createPackageContents ();

        // Initialize created meta-data
        theMapPackage.initializePackageContents ();
        theDocPackage.initializePackageContents ();
        theFragmentPackage.initializePackageContents ();
        theBookPackage.initializePackageContents ();
        theBuilderPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theMapPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( MapPackage.eNS_URI, theMapPackage );
        return theMapPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMap ()
    {
        return this.mapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getMap_ExtensionMappings ()
    {
        return (EReference)this.mapEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getImport ()
    {
        return this.importEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFile ()
    {
        return this.fileEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFile_Path ()
    {
        return (EAttribute)this.fileEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMapSection ()
    {
        return this.mapSectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getMapSection_Title ()
    {
        return (EAttribute)this.mapSectionEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getMapSection_Id ()
    {
        return (EAttribute)this.mapSectionEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMapElement ()
    {
        return this.mapElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFeature ()
    {
        return this.featureEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFeature_FeatureId ()
    {
        return (EAttribute)this.featureEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFeature_CreateSection ()
    {
        return (EAttribute)this.featureEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getFeature_FileNameRules ()
    {
        return (EReference)this.featureEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getNameRule ()
    {
        return this.nameRuleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPatternRule ()
    {
        return this.patternRuleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPatternRule_Pattern ()
    {
        return (EAttribute)this.patternRuleEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIncludePatternRule ()
    {
        return this.includePatternRuleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getExcludePatternRule ()
    {
        return this.excludePatternRuleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getResourceFactory ()
    {
        return this.resourceFactoryEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getResourceFactory_ClassName ()
    {
        return (EAttribute)this.resourceFactoryEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getExtensionMappingEntry ()
    {
        return this.extensionMappingEntryEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getExtensionMappingEntry_Factory ()
    {
        return (EReference)this.extensionMappingEntryEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getExtensionMappingEntry_Extension ()
    {
        return (EAttribute)this.extensionMappingEntryEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContentGenerator ()
    {
        return this.contentGeneratorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMapContainer ()
    {
        return this.mapContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getMapContainer_Elements ()
    {
        return (EReference)this.mapContainerEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getMapContainer_NumberingStyle ()
    {
        return (EAttribute)this.mapContainerEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getRuleResult ()
    {
        return this.ruleResultEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getVisitor ()
    {
        return this.visitorEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getPattern ()
    {
        return this.patternEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public MapFactory getMapFactory ()
    {
        return (MapFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents ()
    {
        if ( this.isCreated )
        {
            return;
        }
        this.isCreated = true;

        // Create classes and their features
        this.mapEClass = createEClass ( MAP );
        createEReference ( this.mapEClass, MAP__EXTENSION_MAPPINGS );

        this.importEClass = createEClass ( IMPORT );

        this.fileEClass = createEClass ( FILE );
        createEAttribute ( this.fileEClass, FILE__PATH );

        this.mapSectionEClass = createEClass ( MAP_SECTION );
        createEAttribute ( this.mapSectionEClass, MAP_SECTION__TITLE );
        createEAttribute ( this.mapSectionEClass, MAP_SECTION__ID );

        this.mapElementEClass = createEClass ( MAP_ELEMENT );

        this.featureEClass = createEClass ( FEATURE );
        createEAttribute ( this.featureEClass, FEATURE__FEATURE_ID );
        createEAttribute ( this.featureEClass, FEATURE__CREATE_SECTION );
        createEReference ( this.featureEClass, FEATURE__FILE_NAME_RULES );

        this.nameRuleEClass = createEClass ( NAME_RULE );

        this.patternRuleEClass = createEClass ( PATTERN_RULE );
        createEAttribute ( this.patternRuleEClass, PATTERN_RULE__PATTERN );

        this.includePatternRuleEClass = createEClass ( INCLUDE_PATTERN_RULE );

        this.excludePatternRuleEClass = createEClass ( EXCLUDE_PATTERN_RULE );

        this.resourceFactoryEClass = createEClass ( RESOURCE_FACTORY );
        createEAttribute ( this.resourceFactoryEClass, RESOURCE_FACTORY__CLASS_NAME );

        this.extensionMappingEntryEClass = createEClass ( EXTENSION_MAPPING_ENTRY );
        createEReference ( this.extensionMappingEntryEClass, EXTENSION_MAPPING_ENTRY__FACTORY );
        createEAttribute ( this.extensionMappingEntryEClass, EXTENSION_MAPPING_ENTRY__EXTENSION );

        this.contentGeneratorEClass = createEClass ( CONTENT_GENERATOR );

        this.mapContainerEClass = createEClass ( MAP_CONTAINER );
        createEReference ( this.mapContainerEClass, MAP_CONTAINER__ELEMENTS );
        createEAttribute ( this.mapContainerEClass, MAP_CONTAINER__NUMBERING_STYLE );

        // Create enums
        this.ruleResultEEnum = createEEnum ( RULE_RESULT );

        // Create data types
        this.visitorEDataType = createEDataType ( VISITOR );
        this.patternEDataType = createEDataType ( PATTERN );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( this.isInitialized )
        {
            return;
        }
        this.isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Obtain other dependent packages
        final FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );
        final DocPackage theDocPackage = (DocPackage)EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        this.mapEClass.getESuperTypes ().add ( getMapContainer () );
        this.importEClass.getESuperTypes ().add ( getMapElement () );
        this.fileEClass.getESuperTypes ().add ( getImport () );
        this.mapSectionEClass.getESuperTypes ().add ( getMapContainer () );
        this.mapSectionEClass.getESuperTypes ().add ( getMapElement () );
        this.featureEClass.getESuperTypes ().add ( getImport () );
        this.patternRuleEClass.getESuperTypes ().add ( getNameRule () );
        this.includePatternRuleEClass.getESuperTypes ().add ( getPatternRule () );
        this.excludePatternRuleEClass.getESuperTypes ().add ( getPatternRule () );
        this.contentGeneratorEClass.getESuperTypes ().add ( getMapElement () );
        this.contentGeneratorEClass.getESuperTypes ().add ( theFragmentPackage.getContent () );

        // Initialize classes and features; add operations and parameters
        initEClass ( this.mapEClass, Map.class, "Map", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getMap_ExtensionMappings (), getExtensionMappingEntry (), null, "extensionMappings", null, 0, -1, Map.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        EOperation op = addEOperation ( this.mapEClass, null, "visit", 0, 1, IS_UNIQUE, IS_ORDERED );
        addEParameter ( op, getVisitor (), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED );

        initEClass ( this.importEClass, Import.class, "Import", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        initEClass ( this.fileEClass, File.class, "File", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getFile_Path (), this.ecorePackage.getEString (), "path", null, 1, 1, File.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.mapSectionEClass, MapSection.class, "MapSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getMapSection_Title (), this.ecorePackage.getEString (), "title", null, 1, 1, MapSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getMapSection_Id (), this.ecorePackage.getEString (), "id", null, 0, 1, MapSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.mapElementEClass, MapElement.class, "MapElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        op = addEOperation ( this.mapElementEClass, null, "visit", 0, 1, IS_UNIQUE, IS_ORDERED );
        addEParameter ( op, getVisitor (), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED );

        initEClass ( this.featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getFeature_FeatureId (), this.ecorePackage.getEString (), "featureId", null, 1, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getFeature_CreateSection (), this.ecorePackage.getEBoolean (), "createSection", null, 1, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getFeature_FileNameRules (), getNameRule (), null, "fileNameRules", null, 0, -1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.nameRuleEClass, NameRule.class, "NameRule", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        op = addEOperation ( this.nameRuleEClass, getRuleResult (), "checkRule", 0, 1, IS_UNIQUE, IS_ORDERED );
        addEParameter ( op, this.ecorePackage.getEString (), "name", 0, 1, IS_UNIQUE, IS_ORDERED );

        initEClass ( this.patternRuleEClass, PatternRule.class, "PatternRule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getPatternRule_Pattern (), getPattern (), "pattern", null, 1, 1, PatternRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.includePatternRuleEClass, IncludePatternRule.class, "IncludePatternRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        op = addEOperation ( this.includePatternRuleEClass, getRuleResult (), "checkRule", 0, 1, IS_UNIQUE, IS_ORDERED );
        addEParameter ( op, this.ecorePackage.getEString (), "name", 0, 1, IS_UNIQUE, IS_ORDERED );

        initEClass ( this.excludePatternRuleEClass, ExcludePatternRule.class, "ExcludePatternRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        op = addEOperation ( this.excludePatternRuleEClass, getRuleResult (), "checkRule", 0, 1, IS_UNIQUE, IS_ORDERED );
        addEParameter ( op, this.ecorePackage.getEString (), "name", 0, 1, IS_UNIQUE, IS_ORDERED );

        initEClass ( this.resourceFactoryEClass, ResourceFactory.class, "ResourceFactory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getResourceFactory_ClassName (), this.ecorePackage.getEJavaClass (), "className", null, 1, 1, ResourceFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.extensionMappingEntryEClass, ExtensionMappingEntry.class, "ExtensionMappingEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getExtensionMappingEntry_Factory (), getResourceFactory (), null, "factory", null, 1, 1, ExtensionMappingEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getExtensionMappingEntry_Extension (), this.ecorePackage.getEString (), "extension", null, 1, 1, ExtensionMappingEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.contentGeneratorEClass, ContentGenerator.class, "ContentGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        initEClass ( this.mapContainerEClass, MapContainer.class, "MapContainer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getMapContainer_Elements (), getMapElement (), null, "elements", null, 0, -1, MapContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getMapContainer_NumberingStyle (), theDocPackage.getNumberingStyle (), "numberingStyle", null, 1, 1, MapContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        // Initialize enums and add enum literals
        initEEnum ( this.ruleResultEEnum, RuleResult.class, "RuleResult" );
        addEEnumLiteral ( this.ruleResultEEnum, RuleResult.ACCEPT );
        addEEnumLiteral ( this.ruleResultEEnum, RuleResult.REJECT );

        // Initialize data types
        initEDataType ( this.visitorEDataType, Visitor.class, "Visitor", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS );
        initEDataType ( this.patternEDataType, Pattern.class, "Pattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS );
    }

} //MapPackageImpl
