/**
 */
package org.openscada.doc.model.doc.map.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.RuleResult;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Include Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class IncludePatternRuleImpl extends PatternRuleImpl implements IncludePatternRule
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IncludePatternRuleImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.INCLUDE_PATTERN_RULE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public RuleResult checkRule ( final String name )
    {
        return this.pattern.matcher ( name ).matches () ? RuleResult.ACCEPT : null;
    }

} //IncludePatternRuleImpl
