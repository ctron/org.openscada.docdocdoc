/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage
 * @generated
 */
public interface MapFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    MapFactory eINSTANCE = org.openscada.doc.model.doc.map.impl.MapFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Map</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Map</em>'.
     * @generated
     */
    Map createMap ();

    /**
     * Returns a new object of class '<em>File</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>File</em>'.
     * @generated
     */
    File createFile ();

    /**
     * Returns a new object of class '<em>Section</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Section</em>'.
     * @generated
     */
    MapSection createMapSection ();

    /**
     * Returns a new object of class '<em>Feature</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Feature</em>'.
     * @generated
     */
    Feature createFeature ();

    /**
     * Returns a new object of class '<em>Include Pattern Rule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Include Pattern Rule</em>'.
     * @generated
     */
    IncludePatternRule createIncludePatternRule ();

    /**
     * Returns a new object of class '<em>Exclude Pattern Rule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Exclude Pattern Rule</em>'.
     * @generated
     */
    ExcludePatternRule createExcludePatternRule ();

    /**
     * Returns a new object of class '<em>Resource Factory</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Resource Factory</em>'.
     * @generated
     */
    ResourceFactory createResourceFactory ();

    /**
     * Returns a new object of class '<em>Extension Mapping Entry</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Extension Mapping Entry</em>'.
     * @generated
     */
    ExtensionMappingEntry createExtensionMappingEntry ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    MapPackage getMapPackage ();

} //MapFactory
