/**
 */
package org.openscada.doc.model.doc.map.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.ResourceFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Factory</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.impl.ResourceFactoryImpl#getClassName
 * <em>Class Name</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ResourceFactoryImpl extends EObjectImpl implements ResourceFactory
{
    /**
     * The cached value of the '{@link #getClassName() <em>Class Name</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getClassName()
     * @generated
     * @ordered
     */
    protected Class className;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ResourceFactoryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.RESOURCE_FACTORY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Class getClassName ()
    {
        return this.className;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setClassName ( final Class newClassName )
    {
        final Class oldClassName = this.className;
        this.className = newClassName;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.RESOURCE_FACTORY__CLASS_NAME, oldClassName, this.className ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case MapPackage.RESOURCE_FACTORY__CLASS_NAME:
                return getClassName ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case MapPackage.RESOURCE_FACTORY__CLASS_NAME:
                setClassName ( (Class)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.RESOURCE_FACTORY__CLASS_NAME:
                setClassName ( (Class)null );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.RESOURCE_FACTORY__CLASS_NAME:
                return this.className != null;
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (className: " );
        result.append ( this.className );
        result.append ( ')' );
        return result.toString ();
    }

} //ResourceFactoryImpl
