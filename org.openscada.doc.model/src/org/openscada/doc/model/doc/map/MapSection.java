/**
 */
package org.openscada.doc.model.doc.map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.MapSection#getTitle <em>Title
 * </em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.MapSection#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getMapSection()
 * @model
 * @generated
 */
public interface MapSection extends MapContainer, MapElement
{
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.doc.model.doc.map.MapPackage#getMapSection_Title()
     * @model required="true"
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.MapSection#getTitle
     * <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

    /**
     * Returns the value of the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Id</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Id</em>' attribute.
     * @see #setId(String)
     * @see org.openscada.doc.model.doc.map.MapPackage#getMapSection_Id()
     * @model
     * @generated
     */
    String getId ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.MapSection#getId <em>Id</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Id</em>' attribute.
     * @see #getId()
     * @generated
     */
    void setId ( String value );

} // MapSection
