/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapFactory
 * @model kind="package"
 * @generated
 */
public interface MapPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "map";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:map";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "map";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    MapPackage eINSTANCE = org.openscada.doc.model.doc.map.impl.MapPackageImpl.init ();

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.MapImpl <em>Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.MapImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMap()
     * @generated
     */
    int MAP = 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.MapElement <em>Element</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapElement
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapElement()
     * @generated
     */
    int MAP_ELEMENT = 4;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.MapContainer <em>Container</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapContainer
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapContainer()
     * @generated
     */
    int MAP_CONTAINER = 13;

    /**
     * The feature id for the '<em><b>Elements</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_CONTAINER__ELEMENTS = 0;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_CONTAINER__NUMBERING_STYLE = 1;

    /**
     * The number of structural features of the '<em>Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_CONTAINER_FEATURE_COUNT = 2;

    /**
     * The feature id for the '<em><b>Elements</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP__ELEMENTS = MAP_CONTAINER__ELEMENTS;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP__NUMBERING_STYLE = MAP_CONTAINER__NUMBERING_STYLE;

    /**
     * The feature id for the '<em><b>Extension Mappings</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP__EXTENSION_MAPPINGS = MAP_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_FEATURE_COUNT = MAP_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_ELEMENT_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.Import <em>Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.Import
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getImport()
     * @generated
     */
    int IMPORT = 1;

    /**
     * The number of structural features of the '<em>Import</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMPORT_FEATURE_COUNT = MAP_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.FileImpl <em>File</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.FileImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getFile()
     * @generated
     */
    int FILE = 2;

    /**
     * The feature id for the '<em><b>Path</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FILE__PATH = IMPORT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>File</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FILE_FEATURE_COUNT = IMPORT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.MapSectionImpl
     * <em>Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.MapSectionImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapSection()
     * @generated
     */
    int MAP_SECTION = 3;

    /**
     * The feature id for the '<em><b>Elements</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_SECTION__ELEMENTS = MAP_CONTAINER__ELEMENTS;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_SECTION__NUMBERING_STYLE = MAP_CONTAINER__NUMBERING_STYLE;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_SECTION__TITLE = MAP_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_SECTION__ID = MAP_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Section</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MAP_SECTION_FEATURE_COUNT = MAP_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.FeatureImpl <em>Feature</em>}
     * ' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.FeatureImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getFeature()
     * @generated
     */
    int FEATURE = 5;

    /**
     * The feature id for the '<em><b>Feature Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FEATURE__FEATURE_ID = IMPORT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Create Section</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FEATURE__CREATE_SECTION = IMPORT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>File Name Rules</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FEATURE__FILE_NAME_RULES = IMPORT_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Feature</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FEATURE_FEATURE_COUNT = IMPORT_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.NameRule <em>Name Rule</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.NameRule
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getNameRule()
     * @generated
     */
    int NAME_RULE = 6;

    /**
     * The number of structural features of the '<em>Name Rule</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NAME_RULE_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.PatternRuleImpl
     * <em>Pattern Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.PatternRuleImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getPatternRule()
     * @generated
     */
    int PATTERN_RULE = 7;

    /**
     * The feature id for the '<em><b>Pattern</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PATTERN_RULE__PATTERN = NAME_RULE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Pattern Rule</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PATTERN_RULE_FEATURE_COUNT = NAME_RULE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.IncludePatternRuleImpl
     * <em>Include Pattern Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.IncludePatternRuleImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getIncludePatternRule()
     * @generated
     */
    int INCLUDE_PATTERN_RULE = 8;

    /**
     * The feature id for the '<em><b>Pattern</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INCLUDE_PATTERN_RULE__PATTERN = PATTERN_RULE__PATTERN;

    /**
     * The number of structural features of the '<em>Include Pattern Rule</em>'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INCLUDE_PATTERN_RULE_FEATURE_COUNT = PATTERN_RULE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.ExcludePatternRuleImpl
     * <em>Exclude Pattern Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.ExcludePatternRuleImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getExcludePatternRule()
     * @generated
     */
    int EXCLUDE_PATTERN_RULE = 9;

    /**
     * The feature id for the '<em><b>Pattern</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXCLUDE_PATTERN_RULE__PATTERN = PATTERN_RULE__PATTERN;

    /**
     * The number of structural features of the '<em>Exclude Pattern Rule</em>'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXCLUDE_PATTERN_RULE_FEATURE_COUNT = PATTERN_RULE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.ResourceFactoryImpl
     * <em>Resource Factory</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.ResourceFactoryImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getResourceFactory()
     * @generated
     */
    int RESOURCE_FACTORY = 10;

    /**
     * The feature id for the '<em><b>Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_FACTORY__CLASS_NAME = 0;

    /**
     * The number of structural features of the '<em>Resource Factory</em>'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESOURCE_FACTORY_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl
     * <em>Extension Mapping Entry</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getExtensionMappingEntry()
     * @generated
     */
    int EXTENSION_MAPPING_ENTRY = 11;

    /**
     * The feature id for the '<em><b>Factory</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTENSION_MAPPING_ENTRY__FACTORY = 0;

    /**
     * The feature id for the '<em><b>Extension</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTENSION_MAPPING_ENTRY__EXTENSION = 1;

    /**
     * The number of structural features of the '
     * <em>Extension Mapping Entry</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTENSION_MAPPING_ENTRY_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.impl.ContentGeneratorImpl
     * <em>Content Generator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.impl.ContentGeneratorImpl
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getContentGenerator()
     * @generated
     */
    int CONTENT_GENERATOR = 12;

    /**
     * The number of structural features of the '<em>Content Generator</em>'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_GENERATOR_FEATURE_COUNT = MAP_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.map.RuleResult <em>Rule Result</em>}'
     * enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.RuleResult
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getRuleResult()
     * @generated
     */
    int RULE_RESULT = 14;

    /**
     * The meta object id for the '<em>Visitor</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.Visitor
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getVisitor()
     * @generated
     */
    int VISITOR = 15;

    /**
     * The meta object id for the '<em>Pattern</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.util.regex.Pattern
     * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getPattern()
     * @generated
     */
    int PATTERN = 16;

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.Map <em>Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Map</em>'.
     * @see org.openscada.doc.model.doc.map.Map
     * @generated
     */
    EClass getMap ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.map.Map#getExtensionMappings
     * <em>Extension Mappings</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Extension Mappings</em>'.
     * @see org.openscada.doc.model.doc.map.Map#getExtensionMappings()
     * @see #getMap()
     * @generated
     */
    EReference getMap_ExtensionMappings ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.Import <em>Import</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Import</em>'.
     * @see org.openscada.doc.model.doc.map.Import
     * @generated
     */
    EClass getImport ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.File <em>File</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>File</em>'.
     * @see org.openscada.doc.model.doc.map.File
     * @generated
     */
    EClass getFile ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.File#getPath <em>Path</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Path</em>'.
     * @see org.openscada.doc.model.doc.map.File#getPath()
     * @see #getFile()
     * @generated
     */
    EAttribute getFile_Path ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.MapSection <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Section</em>'.
     * @see org.openscada.doc.model.doc.map.MapSection
     * @generated
     */
    EClass getMapSection ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.MapSection#getTitle
     * <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.model.doc.map.MapSection#getTitle()
     * @see #getMapSection()
     * @generated
     */
    EAttribute getMapSection_Title ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.MapSection#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see org.openscada.doc.model.doc.map.MapSection#getId()
     * @see #getMapSection()
     * @generated
     */
    EAttribute getMapSection_Id ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.MapElement <em>Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Element</em>'.
     * @see org.openscada.doc.model.doc.map.MapElement
     * @generated
     */
    EClass getMapElement ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.Feature <em>Feature</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Feature</em>'.
     * @see org.openscada.doc.model.doc.map.Feature
     * @generated
     */
    EClass getFeature ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.Feature#getFeatureId
     * <em>Feature Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Feature Id</em>'.
     * @see org.openscada.doc.model.doc.map.Feature#getFeatureId()
     * @see #getFeature()
     * @generated
     */
    EAttribute getFeature_FeatureId ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.Feature#isCreateSection
     * <em>Create Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Create Section</em>'.
     * @see org.openscada.doc.model.doc.map.Feature#isCreateSection()
     * @see #getFeature()
     * @generated
     */
    EAttribute getFeature_CreateSection ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.map.Feature#getFileNameRules
     * <em>File Name Rules</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>File Name Rules</em>'.
     * @see org.openscada.doc.model.doc.map.Feature#getFileNameRules()
     * @see #getFeature()
     * @generated
     */
    EReference getFeature_FileNameRules ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.NameRule <em>Name Rule</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Name Rule</em>'.
     * @see org.openscada.doc.model.doc.map.NameRule
     * @generated
     */
    EClass getNameRule ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.PatternRule <em>Pattern Rule</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Pattern Rule</em>'.
     * @see org.openscada.doc.model.doc.map.PatternRule
     * @generated
     */
    EClass getPatternRule ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.PatternRule#getPattern
     * <em>Pattern</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Pattern</em>'.
     * @see org.openscada.doc.model.doc.map.PatternRule#getPattern()
     * @see #getPatternRule()
     * @generated
     */
    EAttribute getPatternRule_Pattern ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.IncludePatternRule
     * <em>Include Pattern Rule</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Include Pattern Rule</em>'.
     * @see org.openscada.doc.model.doc.map.IncludePatternRule
     * @generated
     */
    EClass getIncludePatternRule ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.ExcludePatternRule
     * <em>Exclude Pattern Rule</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Exclude Pattern Rule</em>'.
     * @see org.openscada.doc.model.doc.map.ExcludePatternRule
     * @generated
     */
    EClass getExcludePatternRule ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.ResourceFactory
     * <em>Resource Factory</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Resource Factory</em>'.
     * @see org.openscada.doc.model.doc.map.ResourceFactory
     * @generated
     */
    EClass getResourceFactory ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.ResourceFactory#getClassName
     * <em>Class Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Class Name</em>'.
     * @see org.openscada.doc.model.doc.map.ResourceFactory#getClassName()
     * @see #getResourceFactory()
     * @generated
     */
    EAttribute getResourceFactory_ClassName ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry
     * <em>Extension Mapping Entry</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Extension Mapping Entry</em>'.
     * @see org.openscada.doc.model.doc.map.ExtensionMappingEntry
     * @generated
     */
    EClass getExtensionMappingEntry ();

    /**
     * Returns the meta object for the containment reference '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getFactory
     * <em>Factory</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Factory</em>'.
     * @see org.openscada.doc.model.doc.map.ExtensionMappingEntry#getFactory()
     * @see #getExtensionMappingEntry()
     * @generated
     */
    EReference getExtensionMappingEntry_Factory ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getExtension
     * <em>Extension</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Extension</em>'.
     * @see org.openscada.doc.model.doc.map.ExtensionMappingEntry#getExtension()
     * @see #getExtensionMappingEntry()
     * @generated
     */
    EAttribute getExtensionMappingEntry_Extension ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.ContentGenerator
     * <em>Content Generator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Content Generator</em>'.
     * @see org.openscada.doc.model.doc.map.ContentGenerator
     * @generated
     */
    EClass getContentGenerator ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.map.MapContainer <em>Container</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Container</em>'.
     * @see org.openscada.doc.model.doc.map.MapContainer
     * @generated
     */
    EClass getMapContainer ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.map.MapContainer#getElements
     * <em>Elements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Elements</em>'.
     * @see org.openscada.doc.model.doc.map.MapContainer#getElements()
     * @see #getMapContainer()
     * @generated
     */
    EReference getMapContainer_Elements ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.map.MapContainer#getNumberingStyle
     * <em>Numbering Style</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Numbering Style</em>'.
     * @see org.openscada.doc.model.doc.map.MapContainer#getNumberingStyle()
     * @see #getMapContainer()
     * @generated
     */
    EAttribute getMapContainer_NumberingStyle ();

    /**
     * Returns the meta object for enum '
     * {@link org.openscada.doc.model.doc.map.RuleResult <em>Rule Result</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Rule Result</em>'.
     * @see org.openscada.doc.model.doc.map.RuleResult
     * @generated
     */
    EEnum getRuleResult ();

    /**
     * Returns the meta object for data type '
     * {@link org.openscada.doc.model.doc.map.Visitor <em>Visitor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Visitor</em>'.
     * @see org.openscada.doc.model.doc.map.Visitor
     * @model instanceClass="org.openscada.doc.model.doc.map.Visitor"
     *        serializeable="false"
     * @generated
     */
    EDataType getVisitor ();

    /**
     * Returns the meta object for data type '{@link java.util.regex.Pattern
     * <em>Pattern</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Pattern</em>'.
     * @see java.util.regex.Pattern
     * @model instanceClass="java.util.regex.Pattern"
     * @generated
     */
    EDataType getPattern ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    MapFactory getMapFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.MapImpl <em>Map</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.MapImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMap()
         * @generated
         */
        EClass MAP = eINSTANCE.getMap ();

        /**
         * The meta object literal for the '<em><b>Extension Mappings</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference MAP__EXTENSION_MAPPINGS = eINSTANCE.getMap_ExtensionMappings ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.Import <em>Import</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.Import
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getImport()
         * @generated
         */
        EClass IMPORT = eINSTANCE.getImport ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.FileImpl <em>File</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.FileImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getFile()
         * @generated
         */
        EClass FILE = eINSTANCE.getFile ();

        /**
         * The meta object literal for the '<em><b>Path</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FILE__PATH = eINSTANCE.getFile_Path ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.MapSectionImpl
         * <em>Section</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.MapSectionImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapSection()
         * @generated
         */
        EClass MAP_SECTION = eINSTANCE.getMapSection ();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute MAP_SECTION__TITLE = eINSTANCE.getMapSection_Title ();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute MAP_SECTION__ID = eINSTANCE.getMapSection_Id ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.MapElement <em>Element</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.MapElement
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapElement()
         * @generated
         */
        EClass MAP_ELEMENT = eINSTANCE.getMapElement ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.FeatureImpl
         * <em>Feature</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.FeatureImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getFeature()
         * @generated
         */
        EClass FEATURE = eINSTANCE.getFeature ();

        /**
         * The meta object literal for the '<em><b>Feature Id</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FEATURE__FEATURE_ID = eINSTANCE.getFeature_FeatureId ();

        /**
         * The meta object literal for the '<em><b>Create Section</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FEATURE__CREATE_SECTION = eINSTANCE.getFeature_CreateSection ();

        /**
         * The meta object literal for the '<em><b>File Name Rules</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference FEATURE__FILE_NAME_RULES = eINSTANCE.getFeature_FileNameRules ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.NameRule <em>Name Rule</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.NameRule
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getNameRule()
         * @generated
         */
        EClass NAME_RULE = eINSTANCE.getNameRule ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.PatternRuleImpl
         * <em>Pattern Rule</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.PatternRuleImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getPatternRule()
         * @generated
         */
        EClass PATTERN_RULE = eINSTANCE.getPatternRule ();

        /**
         * The meta object literal for the '<em><b>Pattern</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PATTERN_RULE__PATTERN = eINSTANCE.getPatternRule_Pattern ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.IncludePatternRuleImpl
         * <em>Include Pattern Rule</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.IncludePatternRuleImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getIncludePatternRule()
         * @generated
         */
        EClass INCLUDE_PATTERN_RULE = eINSTANCE.getIncludePatternRule ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.ExcludePatternRuleImpl
         * <em>Exclude Pattern Rule</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.ExcludePatternRuleImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getExcludePatternRule()
         * @generated
         */
        EClass EXCLUDE_PATTERN_RULE = eINSTANCE.getExcludePatternRule ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.ResourceFactoryImpl
         * <em>Resource Factory</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.ResourceFactoryImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getResourceFactory()
         * @generated
         */
        EClass RESOURCE_FACTORY = eINSTANCE.getResourceFactory ();

        /**
         * The meta object literal for the '<em><b>Class Name</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute RESOURCE_FACTORY__CLASS_NAME = eINSTANCE.getResourceFactory_ClassName ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl
         * <em>Extension Mapping Entry</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getExtensionMappingEntry()
         * @generated
         */
        EClass EXTENSION_MAPPING_ENTRY = eINSTANCE.getExtensionMappingEntry ();

        /**
         * The meta object literal for the '<em><b>Factory</b></em>' containment
         * reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference EXTENSION_MAPPING_ENTRY__FACTORY = eINSTANCE.getExtensionMappingEntry_Factory ();

        /**
         * The meta object literal for the '<em><b>Extension</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXTENSION_MAPPING_ENTRY__EXTENSION = eINSTANCE.getExtensionMappingEntry_Extension ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.impl.ContentGeneratorImpl
         * <em>Content Generator</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.impl.ContentGeneratorImpl
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getContentGenerator()
         * @generated
         */
        EClass CONTENT_GENERATOR = eINSTANCE.getContentGenerator ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.MapContainer
         * <em>Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.MapContainer
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getMapContainer()
         * @generated
         */
        EClass MAP_CONTAINER = eINSTANCE.getMapContainer ();

        /**
         * The meta object literal for the '<em><b>Elements</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference MAP_CONTAINER__ELEMENTS = eINSTANCE.getMapContainer_Elements ();

        /**
         * The meta object literal for the '<em><b>Numbering Style</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute MAP_CONTAINER__NUMBERING_STYLE = eINSTANCE.getMapContainer_NumberingStyle ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.map.RuleResult
         * <em>Rule Result</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.RuleResult
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getRuleResult()
         * @generated
         */
        EEnum RULE_RESULT = eINSTANCE.getRuleResult ();

        /**
         * The meta object literal for the '<em>Visitor</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.map.Visitor
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getVisitor()
         * @generated
         */
        EDataType VISITOR = eINSTANCE.getVisitor ();

        /**
         * The meta object literal for the '<em>Pattern</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.util.regex.Pattern
         * @see org.openscada.doc.model.doc.map.impl.MapPackageImpl#getPattern()
         * @generated
         */
        EDataType PATTERN = eINSTANCE.getPattern ();

    }

} //MapPackage
