/**
 */
package org.openscada.doc.model.doc.map.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.openscada.doc.model.doc.map.ContentGenerator;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.Visitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Content Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public abstract class ContentGeneratorImpl extends EObjectImpl implements ContentGenerator
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ContentGeneratorImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.CONTENT_GENERATOR;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public void visit ( final Visitor visitor )
    {
        try
        {
            visitor.visit ( null, EcoreUtil.copy ( this ) );
        }
        catch ( final Exception e )
        {
            throw new RuntimeException ( e );
        }
    }

} //ContentGeneratorImpl
