/**
 */
package org.openscada.doc.model.doc.map;

import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Content Generator</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getContentGenerator()
 * @model abstract="true"
 * @generated
 */
public interface ContentGenerator extends MapElement, Content
{
} // ContentGenerator
