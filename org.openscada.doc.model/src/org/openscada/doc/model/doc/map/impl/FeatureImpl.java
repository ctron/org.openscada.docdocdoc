/**
 */
package org.openscada.doc.model.doc.map.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.Activator;
import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.FileVisitor;
import org.openscada.doc.model.doc.map.Helper;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.NameRule;
import org.openscada.doc.model.doc.map.ProjectVisitor;
import org.openscada.doc.model.doc.map.Visitor;
import org.openscada.doc.util.BundleVisitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.impl.FeatureImpl#getFeatureId <em>
 * Feature Id</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.FeatureImpl#isCreateSection
 * <em>Create Section</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.impl.FeatureImpl#getFileNameRules
 * <em>File Name Rules</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FeatureImpl extends EObjectImpl implements Feature
{
    /**
     * The default value of the '{@link #getFeatureId() <em>Feature Id</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFeatureId()
     * @generated
     * @ordered
     */
    protected static final String FEATURE_ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getFeatureId() <em>Feature Id</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFeatureId()
     * @generated
     * @ordered
     */
    protected String featureId = FEATURE_ID_EDEFAULT;

    /**
     * The default value of the '{@link #isCreateSection()
     * <em>Create Section</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isCreateSection()
     * @generated
     * @ordered
     */
    protected static final boolean CREATE_SECTION_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isCreateSection()
     * <em>Create Section</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isCreateSection()
     * @generated
     * @ordered
     */
    protected boolean createSection = CREATE_SECTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getFileNameRules()
     * <em>File Name Rules</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFileNameRules()
     * @generated
     * @ordered
     */
    protected EList<NameRule> fileNameRules;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FeatureImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.FEATURE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFeatureId ()
    {
        return this.featureId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFeatureId ( final String newFeatureId )
    {
        final String oldFeatureId = this.featureId;
        this.featureId = newFeatureId;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.FEATURE__FEATURE_ID, oldFeatureId, this.featureId ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isCreateSection ()
    {
        return this.createSection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCreateSection ( final boolean newCreateSection )
    {
        final boolean oldCreateSection = this.createSection;
        this.createSection = newCreateSection;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.FEATURE__CREATE_SECTION, oldCreateSection, this.createSection ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<NameRule> getFileNameRules ()
    {
        if ( this.fileNameRules == null )
        {
            this.fileNameRules = new EObjectContainmentEList.Resolving<NameRule> ( NameRule.class, this, MapPackage.FEATURE__FILE_NAME_RULES );
        }
        return this.fileNameRules;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public void visit ( final Visitor visitor )
    {
        final Map<String, Factory> extensionMap = Helper.buildExtensionMap ( this );

        try
        {
            new BundleVisitor ( Activator.getDefault ().getContext (), this.featureId ).visit ( new BundleVisitor.Visitor () {

                @Override
                public void visit ( final String pluginId, final IProject project )
                {
                    process ( extensionMap, visitor, pluginId, project );
                }
            } );
        }
        catch ( final Exception e )
        {
            throw new RuntimeException ( e );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case MapPackage.FEATURE__FILE_NAME_RULES:
                return ( (InternalEList<?>)getFileNameRules () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    protected void process ( final Map<String, Factory> extensionMap, final Visitor visitor, final String pluginId, final IProject project )
    {
        if ( project == null )
        {
            return;
        }

        try
        {
            final FileVisitor fv = new FileVisitor ( this.fileNameRules );
            final ProjectVisitor pv = new ProjectVisitor ( extensionMap, isCreateSection (), pluginId, visitor );
            fv.visit ( pv, project );
            pv.done ();
        }
        catch ( final Exception e )
        {
            throw new RuntimeException ( e );
        }

    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case MapPackage.FEATURE__FEATURE_ID:
                return getFeatureId ();
            case MapPackage.FEATURE__CREATE_SECTION:
                return isCreateSection ();
            case MapPackage.FEATURE__FILE_NAME_RULES:
                return getFileNameRules ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case MapPackage.FEATURE__FEATURE_ID:
                setFeatureId ( (String)newValue );
                return;
            case MapPackage.FEATURE__CREATE_SECTION:
                setCreateSection ( (Boolean)newValue );
                return;
            case MapPackage.FEATURE__FILE_NAME_RULES:
                getFileNameRules ().clear ();
                getFileNameRules ().addAll ( (Collection<? extends NameRule>)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.FEATURE__FEATURE_ID:
                setFeatureId ( FEATURE_ID_EDEFAULT );
                return;
            case MapPackage.FEATURE__CREATE_SECTION:
                setCreateSection ( CREATE_SECTION_EDEFAULT );
                return;
            case MapPackage.FEATURE__FILE_NAME_RULES:
                getFileNameRules ().clear ();
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.FEATURE__FEATURE_ID:
                return FEATURE_ID_EDEFAULT == null ? this.featureId != null : !FEATURE_ID_EDEFAULT.equals ( this.featureId );
            case MapPackage.FEATURE__CREATE_SECTION:
                return this.createSection != CREATE_SECTION_EDEFAULT;
            case MapPackage.FEATURE__FILE_NAME_RULES:
                return this.fileNameRules != null && !this.fileNameRules.isEmpty ();
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (featureId: " );
        result.append ( this.featureId );
        result.append ( ", createSection: " );
        result.append ( this.createSection );
        result.append ( ')' );
        return result.toString ();
    }

} //FeatureImpl
