/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Factory</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.ResourceFactory#getClassName <em>
 * Class Name</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getResourceFactory()
 * @model
 * @generated
 */
public interface ResourceFactory extends EObject
{
    /**
     * Returns the value of the '<em><b>Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Class Name</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Class Name</em>' attribute.
     * @see #setClassName(Class)
     * @see org.openscada.doc.model.doc.map.MapPackage#getResourceFactory_ClassName()
     * @model required="true"
     * @generated
     */
    Class getClassName ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.ResourceFactory#getClassName
     * <em>Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Class Name</em>' attribute.
     * @see #getClassName()
     * @generated
     */
    void setClassName ( Class value );

} // ResourceFactory
