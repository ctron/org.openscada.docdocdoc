/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.Feature#getFeatureId <em>Feature
 * Id</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.Feature#isCreateSection <em>Create
 * Section</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.Feature#getFileNameRules <em>File
 * Name Rules</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getFeature()
 * @model
 * @generated
 */
public interface Feature extends Import
{
    /**
     * Returns the value of the '<em><b>Feature Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Feature Id</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Feature Id</em>' attribute.
     * @see #setFeatureId(String)
     * @see org.openscada.doc.model.doc.map.MapPackage#getFeature_FeatureId()
     * @model required="true"
     * @generated
     */
    String getFeatureId ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.Feature#getFeatureId
     * <em>Feature Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Feature Id</em>' attribute.
     * @see #getFeatureId()
     * @generated
     */
    void setFeatureId ( String value );

    /**
     * Returns the value of the '<em><b>Create Section</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Create Section</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Create Section</em>' attribute.
     * @see #setCreateSection(boolean)
     * @see org.openscada.doc.model.doc.map.MapPackage#getFeature_CreateSection()
     * @model required="true"
     * @generated
     */
    boolean isCreateSection ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.Feature#isCreateSection
     * <em>Create Section</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Create Section</em>' attribute.
     * @see #isCreateSection()
     * @generated
     */
    void setCreateSection ( boolean value );

    /**
     * Returns the value of the '<em><b>File Name Rules</b></em>' containment
     * reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.map.NameRule}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>File Name Rules</em>' containment reference
     * list isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>File Name Rules</em>' containment reference
     *         list.
     * @see org.openscada.doc.model.doc.map.MapPackage#getFeature_FileNameRules()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<NameRule> getFileNameRules ();

} // Feature
