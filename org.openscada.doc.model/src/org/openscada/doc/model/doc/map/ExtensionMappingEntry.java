/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extension Mapping Entry</b></em>
 * '.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getFactory
 * <em>Factory</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getExtension
 * <em>Extension</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getExtensionMappingEntry()
 * @model
 * @generated
 */
public interface ExtensionMappingEntry extends EObject
{
    /**
     * Returns the value of the '<em><b>Factory</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Factory</em>' containment reference isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Factory</em>' containment reference.
     * @see #setFactory(ResourceFactory)
     * @see org.openscada.doc.model.doc.map.MapPackage#getExtensionMappingEntry_Factory()
     * @model containment="true" resolveProxies="true" required="true"
     * @generated
     */
    ResourceFactory getFactory ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getFactory
     * <em>Factory</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Factory</em>' containment reference.
     * @see #getFactory()
     * @generated
     */
    void setFactory ( ResourceFactory value );

    /**
     * Returns the value of the '<em><b>Extension</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Extension</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Extension</em>' attribute.
     * @see #setExtension(String)
     * @see org.openscada.doc.model.doc.map.MapPackage#getExtensionMappingEntry_Extension()
     * @model required="true"
     * @generated
     */
    String getExtension ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry#getExtension
     * <em>Extension</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Extension</em>' attribute.
     * @see #getExtension()
     * @generated
     */
    void setExtension ( String value );

} // ExtensionMappingEntry
