/**
 */
package org.openscada.doc.model.doc.map.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.RuleResult;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exclude Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class ExcludePatternRuleImpl extends PatternRuleImpl implements ExcludePatternRule
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExcludePatternRuleImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.EXCLUDE_PATTERN_RULE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public RuleResult checkRule ( final String name )
    {
        return this.pattern.matcher ( name ).matches () ? RuleResult.REJECT : null;
    }

} //ExcludePatternRuleImpl
