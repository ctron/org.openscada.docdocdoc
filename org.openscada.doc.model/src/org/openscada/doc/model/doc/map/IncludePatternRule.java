/**
 */
package org.openscada.doc.model.doc.map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Include Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getIncludePatternRule()
 * @model
 * @generated
 */
public interface IncludePatternRule extends PatternRule
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model annotation=
     *        "http://www.eclipse.org/emf/2002/GenModel body='return pattern.matcher ( name ).matches () ? RuleResult.ACCEPT : null;'"
     * @generated
     */
    @Override
    RuleResult checkRule ( String name );
} // IncludePatternRule
