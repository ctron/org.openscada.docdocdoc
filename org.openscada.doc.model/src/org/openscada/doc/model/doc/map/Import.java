/**
 */
package org.openscada.doc.model.doc.map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getImport()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Import extends MapElement
{
} // Import
