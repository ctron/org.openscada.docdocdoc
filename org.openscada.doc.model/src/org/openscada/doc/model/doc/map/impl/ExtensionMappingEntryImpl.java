/**
 */
package org.openscada.doc.model.doc.map.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.ResourceFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '
 * <em><b>Extension Mapping Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl#getFactory
 * <em>Factory</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.map.impl.ExtensionMappingEntryImpl#getExtension
 * <em>Extension</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ExtensionMappingEntryImpl extends EObjectImpl implements ExtensionMappingEntry
{
    /**
     * The cached value of the '{@link #getFactory() <em>Factory</em>}'
     * containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFactory()
     * @generated
     * @ordered
     */
    protected ResourceFactory factory;

    /**
     * The default value of the '{@link #getExtension() <em>Extension</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtension()
     * @generated
     * @ordered
     */
    protected static final String EXTENSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getExtension() <em>Extension</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtension()
     * @generated
     * @ordered
     */
    protected String extension = EXTENSION_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtensionMappingEntryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return MapPackage.Literals.EXTENSION_MAPPING_ENTRY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceFactory getFactory ()
    {
        if ( this.factory != null && this.factory.eIsProxy () )
        {
            final InternalEObject oldFactory = (InternalEObject)this.factory;
            this.factory = (ResourceFactory)eResolveProxy ( oldFactory );
            if ( this.factory != oldFactory )
            {
                final InternalEObject newFactory = (InternalEObject)this.factory;
                NotificationChain msgs = oldFactory.eInverseRemove ( this, EOPPOSITE_FEATURE_BASE - MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, null, null );
                if ( newFactory.eInternalContainer () == null )
                {
                    msgs = newFactory.eInverseAdd ( this, EOPPOSITE_FEATURE_BASE - MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, null, msgs );
                }
                if ( msgs != null )
                {
                    msgs.dispatch ();
                }
                if ( eNotificationRequired () )
                {
                    eNotify ( new ENotificationImpl ( this, Notification.RESOLVE, MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, oldFactory, this.factory ) );
                }
            }
        }
        return this.factory;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ResourceFactory basicGetFactory ()
    {
        return this.factory;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetFactory ( final ResourceFactory newFactory, NotificationChain msgs )
    {
        final ResourceFactory oldFactory = this.factory;
        this.factory = newFactory;
        if ( eNotificationRequired () )
        {
            final ENotificationImpl notification = new ENotificationImpl ( this, Notification.SET, MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, oldFactory, newFactory );
            if ( msgs == null )
            {
                msgs = notification;
            }
            else
            {
                msgs.add ( notification );
            }
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFactory ( final ResourceFactory newFactory )
    {
        if ( newFactory != this.factory )
        {
            NotificationChain msgs = null;
            if ( this.factory != null )
            {
                msgs = ( (InternalEObject)this.factory ).eInverseRemove ( this, EOPPOSITE_FEATURE_BASE - MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, null, msgs );
            }
            if ( newFactory != null )
            {
                msgs = ( (InternalEObject)newFactory ).eInverseAdd ( this, EOPPOSITE_FEATURE_BASE - MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, null, msgs );
            }
            msgs = basicSetFactory ( newFactory, msgs );
            if ( msgs != null )
            {
                msgs.dispatch ();
            }
        }
        else if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY, newFactory, newFactory ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getExtension ()
    {
        return this.extension;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExtension ( final String newExtension )
    {
        final String oldExtension = this.extension;
        this.extension = newExtension;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, MapPackage.EXTENSION_MAPPING_ENTRY__EXTENSION, oldExtension, this.extension ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY:
                return basicSetFactory ( null, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY:
                if ( resolve )
                {
                    return getFactory ();
                }
                return basicGetFactory ();
            case MapPackage.EXTENSION_MAPPING_ENTRY__EXTENSION:
                return getExtension ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY:
                setFactory ( (ResourceFactory)newValue );
                return;
            case MapPackage.EXTENSION_MAPPING_ENTRY__EXTENSION:
                setExtension ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY:
                setFactory ( (ResourceFactory)null );
                return;
            case MapPackage.EXTENSION_MAPPING_ENTRY__EXTENSION:
                setExtension ( EXTENSION_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case MapPackage.EXTENSION_MAPPING_ENTRY__FACTORY:
                return this.factory != null;
            case MapPackage.EXTENSION_MAPPING_ENTRY__EXTENSION:
                return EXTENSION_EDEFAULT == null ? this.extension != null : !EXTENSION_EDEFAULT.equals ( this.extension );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (extension: " );
        result.append ( this.extension );
        result.append ( ')' );
        return result.toString ();
    }

} //ExtensionMappingEntryImpl
