/**
 */
package org.openscada.doc.model.doc.map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.File#getPath <em>Path</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getFile()
 * @model
 * @generated
 */
public interface File extends Import
{
    /**
     * Returns the value of the '<em><b>Path</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Path</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Path</em>' attribute.
     * @see #setPath(String)
     * @see org.openscada.doc.model.doc.map.MapPackage#getFile_Path()
     * @model required="true"
     * @generated
     */
    String getPath ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.File#getPath <em>Path</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Path</em>' attribute.
     * @see #getPath()
     * @generated
     */
    void setPath ( String value );

} // File
