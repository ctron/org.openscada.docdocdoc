/**
 */
package org.openscada.doc.model.doc.map;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '
 * <em><b>Rule Result</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getRuleResult()
 * @model
 * @generated
 */
public enum RuleResult implements Enumerator
{
    /**
     * The '<em><b>ACCEPT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ACCEPT_VALUE
     * @generated
     * @ordered
     */
    ACCEPT ( 0, "ACCEPT", "ACCEPT" ),

    /**
     * The '<em><b>REJECT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #REJECT_VALUE
     * @generated
     * @ordered
     */
    REJECT ( 1, "REJECT", "REJECT" );

    /**
     * The '<em><b>ACCEPT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ACCEPT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ACCEPT
     * @model
     * @generated
     * @ordered
     */
    public static final int ACCEPT_VALUE = 0;

    /**
     * The '<em><b>REJECT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>REJECT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #REJECT
     * @model
     * @generated
     * @ordered
     */
    public static final int REJECT_VALUE = 1;

    /**
     * An array of all the '<em><b>Rule Result</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final RuleResult[] VALUES_ARRAY = new RuleResult[] { ACCEPT, REJECT, };

    /**
     * A public read-only list of all the '<em><b>Rule Result</b></em>'
     * enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<RuleResult> VALUES = Collections.unmodifiableList ( Arrays.asList ( VALUES_ARRAY ) );

    /**
     * Returns the '<em><b>Rule Result</b></em>' literal with the specified
     * literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static RuleResult get ( final String literal )
    {
        for ( int i = 0; i < VALUES_ARRAY.length; ++i )
        {
            final RuleResult result = VALUES_ARRAY[i];
            if ( result.toString ().equals ( literal ) )
            {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Rule Result</b></em>' literal with the specified
     * name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static RuleResult getByName ( final String name )
    {
        for ( int i = 0; i < VALUES_ARRAY.length; ++i )
        {
            final RuleResult result = VALUES_ARRAY[i];
            if ( result.getName ().equals ( name ) )
            {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Rule Result</b></em>' literal with the specified
     * integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static RuleResult get ( final int value )
    {
        switch ( value )
        {
            case ACCEPT_VALUE:
                return ACCEPT;
            case REJECT_VALUE:
                return REJECT;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private RuleResult ( final int value, final String name, final String literal )
    {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue ()
    {
        return this.value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName ()
    {
        return this.name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral ()
    {
        return this.literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string
     * representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        return this.literal;
    }

} //RuleResult
