/**
 */
package org.openscada.doc.model.doc.map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.NumberingStyle;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.map.MapContainer#getElements <em>
 * Elements</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.map.MapContainer#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.map.MapPackage#getMapContainer()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface MapContainer extends EObject
{
    /**
     * Returns the value of the '<em><b>Elements</b></em>' containment reference
     * list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.map.MapElement}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Elements</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Elements</em>' containment reference list.
     * @see org.openscada.doc.model.doc.map.MapPackage#getMapContainer_Elements()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<MapElement> getElements ();

    /**
     * Returns the value of the '<em><b>Numbering Style</b></em>' attribute.
     * The literals are from the enumeration
     * {@link org.openscada.doc.model.doc.NumberingStyle}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Defines the numbering style for all contained sections.
     * <!-- end-model-doc -->
     * 
     * @return the value of the '<em>Numbering Style</em>' attribute.
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @see #setNumberingStyle(NumberingStyle)
     * @see org.openscada.doc.model.doc.map.MapPackage#getMapContainer_NumberingStyle()
     * @model required="true"
     * @generated
     */
    NumberingStyle getNumberingStyle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.map.MapContainer#getNumberingStyle
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Numbering Style</em>' attribute.
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @see #getNumberingStyle()
     * @generated
     */
    void setNumberingStyle ( NumberingStyle value );

} // MapContainer
