/**
 */
package org.openscada.doc.model.doc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '
 * <em><b>Numbering Style</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.DocPackage#getNumberingStyle()
 * @model
 * @generated
 */
public enum NumberingStyle implements Enumerator
{
    /**
     * The '<em><b>ARABIC</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ARABIC_VALUE
     * @generated
     * @ordered
     */
    ARABIC ( 0, "ARABIC", "ARABIC" ),

    /**
     * The '<em><b>ROMAN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ROMAN_VALUE
     * @generated
     * @ordered
     */
    ROMAN ( 1, "ROMAN", "ROMAN" ),

    /**
     * The '<em><b>LATIN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #LATIN_VALUE
     * @generated
     * @ordered
     */
    LATIN ( 2, "LATIN", "LATIN" );

    /**
     * The '<em><b>ARABIC</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ARABIC</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ARABIC
     * @model
     * @generated
     * @ordered
     */
    public static final int ARABIC_VALUE = 0;

    /**
     * The '<em><b>ROMAN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ROMAN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ROMAN
     * @model
     * @generated
     * @ordered
     */
    public static final int ROMAN_VALUE = 1;

    /**
     * The '<em><b>LATIN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>LATIN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #LATIN
     * @model
     * @generated
     * @ordered
     */
    public static final int LATIN_VALUE = 2;

    /**
     * An array of all the '<em><b>Numbering Style</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final NumberingStyle[] VALUES_ARRAY = new NumberingStyle[] { ARABIC, ROMAN, LATIN, };

    /**
     * A public read-only list of all the '<em><b>Numbering Style</b></em>'
     * enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<NumberingStyle> VALUES = Collections.unmodifiableList ( Arrays.asList ( VALUES_ARRAY ) );

    /**
     * Returns the '<em><b>Numbering Style</b></em>' literal with the specified
     * literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static NumberingStyle get ( final String literal )
    {
        for ( int i = 0; i < VALUES_ARRAY.length; ++i )
        {
            final NumberingStyle result = VALUES_ARRAY[i];
            if ( result.toString ().equals ( literal ) )
            {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Numbering Style</b></em>' literal with the specified
     * name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static NumberingStyle getByName ( final String name )
    {
        for ( int i = 0; i < VALUES_ARRAY.length; ++i )
        {
            final NumberingStyle result = VALUES_ARRAY[i];
            if ( result.getName ().equals ( name ) )
            {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Numbering Style</b></em>' literal with the specified
     * integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static NumberingStyle get ( final int value )
    {
        switch ( value )
        {
            case ARABIC_VALUE:
                return ARABIC;
            case ROMAN_VALUE:
                return ROMAN;
            case LATIN_VALUE:
                return LATIN;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private NumberingStyle ( final int value, final String name, final String literal )
    {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue ()
    {
        return this.value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName ()
    {
        return this.name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral ()
    {
        return this.literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string
     * representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        return this.literal;
    }

} //NumberingStyle
