/**
 */
package org.openscada.doc.model.doc.book;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.BookPackage
 * @generated
 */
public interface BookFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    BookFactory eINSTANCE = org.openscada.doc.model.doc.book.impl.BookFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Book</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Book</em>'.
     * @generated
     */
    Book createBook ();

    /**
     * Returns a new object of class '<em>Section</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Section</em>'.
     * @generated
     */
    BookSection createBookSection ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    BookPackage getBookPackage ();

} //BookFactory
