/**
 */
package org.openscada.doc.model.doc.book.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl;
import org.openscada.doc.model.doc.impl.DocPackageImpl;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.impl.MapPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BookPackageImpl extends EPackageImpl implements BookPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass bookEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass bookSectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass bookContainerEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.model.doc.book.BookPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private BookPackageImpl ()
    {
        super ( eNS_URI, BookFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link BookPackage#eINSTANCE} when that
     * field is accessed. Clients should not invoke it directly. Instead, they
     * should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static BookPackage init ()
    {
        if ( isInited )
        {
            return (BookPackage)EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI );
        }

        // Obtain or create and register package
        final BookPackageImpl theBookPackage = (BookPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof BookPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new BookPackageImpl () );

        isInited = true;

        // Obtain or create and register interdependencies
        final DocPackageImpl theDocPackage = (DocPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) instanceof DocPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) : DocPackage.eINSTANCE );
        final MapPackageImpl theMapPackage = (MapPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) instanceof MapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) : MapPackage.eINSTANCE );
        final FragmentPackageImpl theFragmentPackage = (FragmentPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) instanceof FragmentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) : FragmentPackage.eINSTANCE );
        final BuilderPackageImpl theBuilderPackage = (BuilderPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) instanceof BuilderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) : BuilderPackage.eINSTANCE );

        // Create package meta-data objects
        theBookPackage.createPackageContents ();
        theDocPackage.createPackageContents ();
        theMapPackage.createPackageContents ();
        theFragmentPackage.createPackageContents ();
        theBuilderPackage.createPackageContents ();

        // Initialize created meta-data
        theBookPackage.initializePackageContents ();
        theDocPackage.initializePackageContents ();
        theMapPackage.initializePackageContents ();
        theFragmentPackage.initializePackageContents ();
        theBuilderPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theBookPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( BookPackage.eNS_URI, theBookPackage );
        return theBookPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBook ()
    {
        return this.bookEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBook_Authors ()
    {
        return (EReference)this.bookEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBook_Title ()
    {
        return (EAttribute)this.bookEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBook_Version ()
    {
        return (EAttribute)this.bookEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBook_Copyright ()
    {
        return (EReference)this.bookEClass.getEStructuralFeatures ().get ( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBook_CopyrightMarker ()
    {
        return (EAttribute)this.bookEClass.getEStructuralFeatures ().get ( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBook_CopyrightText ()
    {
        return (EAttribute)this.bookEClass.getEStructuralFeatures ().get ( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBookSection ()
    {
        return this.bookSectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookSection_Id ()
    {
        return (EAttribute)this.bookSectionEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookSection_Number ()
    {
        return (EAttribute)this.bookSectionEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookSection_Title ()
    {
        return (EAttribute)this.bookSectionEClass.getEStructuralFeatures ().get ( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookSection_FullNumber ()
    {
        return (EAttribute)this.bookSectionEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBookContainer ()
    {
        return this.bookContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBookContainer_Sections ()
    {
        return (EReference)this.bookContainerEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookContainer_NumberingStyle ()
    {
        return (EAttribute)this.bookContainerEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBookContainer_Content ()
    {
        return (EReference)this.bookContainerEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BookFactory getBookFactory ()
    {
        return (BookFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents ()
    {
        if ( this.isCreated )
        {
            return;
        }
        this.isCreated = true;

        // Create classes and their features
        this.bookEClass = createEClass ( BOOK );
        createEReference ( this.bookEClass, BOOK__AUTHORS );
        createEAttribute ( this.bookEClass, BOOK__TITLE );
        createEAttribute ( this.bookEClass, BOOK__VERSION );
        createEReference ( this.bookEClass, BOOK__COPYRIGHT );
        createEAttribute ( this.bookEClass, BOOK__COPYRIGHT_MARKER );
        createEAttribute ( this.bookEClass, BOOK__COPYRIGHT_TEXT );

        this.bookSectionEClass = createEClass ( BOOK_SECTION );
        createEAttribute ( this.bookSectionEClass, BOOK_SECTION__ID );
        createEAttribute ( this.bookSectionEClass, BOOK_SECTION__NUMBER );
        createEAttribute ( this.bookSectionEClass, BOOK_SECTION__FULL_NUMBER );
        createEAttribute ( this.bookSectionEClass, BOOK_SECTION__TITLE );

        this.bookContainerEClass = createEClass ( BOOK_CONTAINER );
        createEReference ( this.bookContainerEClass, BOOK_CONTAINER__CONTENT );
        createEReference ( this.bookContainerEClass, BOOK_CONTAINER__SECTIONS );
        createEAttribute ( this.bookContainerEClass, BOOK_CONTAINER__NUMBERING_STYLE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( this.isInitialized )
        {
            return;
        }
        this.isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Obtain other dependent packages
        final BuilderPackage theBuilderPackage = (BuilderPackage)EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI );
        final FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );
        final DocPackage theDocPackage = (DocPackage)EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI );

        // Add subpackages
        getESubpackages ().add ( theBuilderPackage );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        this.bookEClass.getESuperTypes ().add ( getBookContainer () );
        this.bookSectionEClass.getESuperTypes ().add ( getBookContainer () );

        // Initialize classes and features; add operations and parameters
        initEClass ( this.bookEClass, Book.class, "Book", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getBook_Authors (), theFragmentPackage.getAuthor (), null, "authors", null, 0, -1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBook_Title (), this.ecorePackage.getEString (), "title", null, 1, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBook_Version (), this.ecorePackage.getEString (), "version", null, 1, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getBook_Copyright (), theFragmentPackage.getCopyright (), null, "copyright", null, 0, -1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBook_CopyrightMarker (), this.ecorePackage.getEString (), "copyrightMarker", null, 1, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBook_CopyrightText (), this.ecorePackage.getEString (), "copyrightText", null, 1, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.bookSectionEClass, BookSection.class, "BookSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getBookSection_Id (), this.ecorePackage.getEString (), "id", null, 1, 1, BookSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookSection_Number (), this.ecorePackage.getEInt (), "number", null, 1, 1, BookSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookSection_FullNumber (), this.ecorePackage.getEString (), "fullNumber", null, 1, 1, BookSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookSection_Title (), this.ecorePackage.getEString (), "title", null, 1, 1, BookSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.bookContainerEClass, BookContainer.class, "BookContainer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getBookContainer_Content (), theFragmentPackage.getContent (), null, "content", null, 0, -1, BookContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getBookContainer_Sections (), getBookSection (), null, "sections", null, 0, -1, BookContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookContainer_NumberingStyle (), theDocPackage.getNumberingStyle (), "numberingStyle", null, 1, 1, BookContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
    }

} //BookPackageImpl
