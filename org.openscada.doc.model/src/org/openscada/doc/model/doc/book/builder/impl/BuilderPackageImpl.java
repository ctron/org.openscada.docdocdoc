/**
 */
package org.openscada.doc.model.doc.book.builder.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.builder.BookBuilder;
import org.openscada.doc.model.doc.book.builder.BuilderFactory;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;
import org.openscada.doc.model.doc.book.impl.BookPackageImpl;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl;
import org.openscada.doc.model.doc.impl.DocPackageImpl;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.impl.MapPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BuilderPackageImpl extends EPackageImpl implements BuilderPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass bookBuilderEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass propertyEntryEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private BuilderPackageImpl ()
    {
        super ( eNS_URI, BuilderFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link BuilderPackage#eINSTANCE} when
     * that field is accessed. Clients should not invoke it directly. Instead,
     * they should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static BuilderPackage init ()
    {
        if ( isInited )
        {
            return (BuilderPackage)EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI );
        }

        // Obtain or create and register package
        final BuilderPackageImpl theBuilderPackage = (BuilderPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof BuilderPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new BuilderPackageImpl () );

        isInited = true;

        // Obtain or create and register interdependencies
        final DocPackageImpl theDocPackage = (DocPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) instanceof DocPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI ) : DocPackage.eINSTANCE );
        final MapPackageImpl theMapPackage = (MapPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) instanceof MapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) : MapPackage.eINSTANCE );
        final FragmentPackageImpl theFragmentPackage = (FragmentPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) instanceof FragmentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) : FragmentPackage.eINSTANCE );
        final BookPackageImpl theBookPackage = (BookPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) instanceof BookPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) : BookPackage.eINSTANCE );

        // Create package meta-data objects
        theBuilderPackage.createPackageContents ();
        theDocPackage.createPackageContents ();
        theMapPackage.createPackageContents ();
        theFragmentPackage.createPackageContents ();
        theBookPackage.createPackageContents ();

        // Initialize created meta-data
        theBuilderPackage.initializePackageContents ();
        theDocPackage.initializePackageContents ();
        theMapPackage.initializePackageContents ();
        theFragmentPackage.initializePackageContents ();
        theBookPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theBuilderPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( BuilderPackage.eNS_URI, theBuilderPackage );
        return theBuilderPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBookBuilder ()
    {
        return this.bookBuilderEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookBuilder_Title ()
    {
        return (EAttribute)this.bookBuilderEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookBuilder_Version ()
    {
        return (EAttribute)this.bookBuilderEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBookBuilder_Authors ()
    {
        return (EReference)this.bookBuilderEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookBuilder_License ()
    {
        return (EAttribute)this.bookBuilderEClass.getEStructuralFeatures ().get ( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBookBuilder_Copyright ()
    {
        return (EReference)this.bookBuilderEClass.getEStructuralFeatures ().get ( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBookBuilder_CopyrightMarker ()
    {
        return (EAttribute)this.bookBuilderEClass.getEStructuralFeatures ().get ( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBookBuilder_Properties ()
    {
        return (EReference)this.bookBuilderEClass.getEStructuralFeatures ().get ( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPropertyEntry ()
    {
        return this.propertyEntryEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPropertyEntry_Key ()
    {
        return (EAttribute)this.propertyEntryEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPropertyEntry_Value ()
    {
        return (EAttribute)this.propertyEntryEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BuilderFactory getBuilderFactory ()
    {
        return (BuilderFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents ()
    {
        if ( this.isCreated )
        {
            return;
        }
        this.isCreated = true;

        // Create classes and their features
        this.bookBuilderEClass = createEClass ( BOOK_BUILDER );
        createEAttribute ( this.bookBuilderEClass, BOOK_BUILDER__TITLE );
        createEAttribute ( this.bookBuilderEClass, BOOK_BUILDER__VERSION );
        createEReference ( this.bookBuilderEClass, BOOK_BUILDER__AUTHORS );
        createEAttribute ( this.bookBuilderEClass, BOOK_BUILDER__LICENSE );
        createEReference ( this.bookBuilderEClass, BOOK_BUILDER__COPYRIGHT );
        createEAttribute ( this.bookBuilderEClass, BOOK_BUILDER__COPYRIGHT_MARKER );
        createEReference ( this.bookBuilderEClass, BOOK_BUILDER__PROPERTIES );

        this.propertyEntryEClass = createEClass ( PROPERTY_ENTRY );
        createEAttribute ( this.propertyEntryEClass, PROPERTY_ENTRY__KEY );
        createEAttribute ( this.propertyEntryEClass, PROPERTY_ENTRY__VALUE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( this.isInitialized )
        {
            return;
        }
        this.isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Obtain other dependent packages
        final MapPackage theMapPackage = (MapPackage)EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI );
        final FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        this.bookBuilderEClass.getESuperTypes ().add ( theMapPackage.getMap () );

        // Initialize classes and features; add operations and parameters
        initEClass ( this.bookBuilderEClass, BookBuilder.class, "BookBuilder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getBookBuilder_Title (), this.ecorePackage.getEString (), "title", null, 0, 1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookBuilder_Version (), this.ecorePackage.getEString (), "version", null, 0, 1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getBookBuilder_Authors (), theFragmentPackage.getAuthor (), null, "authors", null, 0, -1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookBuilder_License (), this.ecorePackage.getEString (), "license", null, 1, 1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getBookBuilder_Copyright (), theFragmentPackage.getCopyright (), null, "copyright", null, 0, -1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getBookBuilder_CopyrightMarker (), this.ecorePackage.getEString (), "copyrightMarker", null, 1, 1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getBookBuilder_Properties (), getPropertyEntry (), null, "properties", null, 0, -1, BookBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( this.propertyEntryEClass, PropertyEntry.class, "PropertyEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getPropertyEntry_Key (), this.ecorePackage.getEString (), "key", null, 1, 1, PropertyEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getPropertyEntry_Value (), this.ecorePackage.getEString (), "value", null, 0, 1, PropertyEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
    }

} //BuilderPackageImpl
