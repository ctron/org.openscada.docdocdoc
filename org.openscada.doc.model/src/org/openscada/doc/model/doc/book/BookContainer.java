/**
 */
package org.openscada.doc.model.doc.book;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.book.BookContainer#getContent <em>
 * Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.BookContainer#getSections <em>
 * Sections</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.BookContainer#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.book.BookPackage#getBookContainer()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface BookContainer extends EObject
{
    /**
     * Returns the value of the '<em><b>Sections</b></em>' containment reference
     * list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.book.BookSection}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sections</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Sections</em>' containment reference list.
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookContainer_Sections()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<BookSection> getSections ();

    /**
     * Returns the value of the '<em><b>Numbering Style</b></em>' attribute.
     * The literals are from the enumeration
     * {@link org.openscada.doc.model.doc.NumberingStyle}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Numbering Style</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Numbering Style</em>' attribute.
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @see #setNumberingStyle(NumberingStyle)
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookContainer_NumberingStyle()
     * @model required="true"
     * @generated
     */
    NumberingStyle getNumberingStyle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.BookContainer#getNumberingStyle
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Numbering Style</em>' attribute.
     * @see org.openscada.doc.model.doc.NumberingStyle
     * @see #getNumberingStyle()
     * @generated
     */
    void setNumberingStyle ( NumberingStyle value );

    /**
     * Returns the value of the '<em><b>Content</b></em>' containment reference
     * list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Content}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Content</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Content</em>' containment reference list.
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookContainer_Content()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<Content> getContent ();

} // BookContainer
