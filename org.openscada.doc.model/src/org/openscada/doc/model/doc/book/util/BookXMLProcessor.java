/**
 */
package org.openscada.doc.model.doc.book.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.openscada.doc.model.doc.book.BookPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BookXMLProcessor extends XMLProcessor
{

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BookXMLProcessor ()
    {
        super ( EPackage.Registry.INSTANCE );
        BookPackage.eINSTANCE.eClass ();
    }

    /**
     * Register for "*" and "xml" file extensions the BookResourceFactoryImpl
     * factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations ()
    {
        if ( this.registrations == null )
        {
            super.getRegistrations ();
            this.registrations.put ( XML_EXTENSION, new BookResourceFactoryImpl () );
            this.registrations.put ( STAR_EXTENSION, new BookResourceFactoryImpl () );
        }
        return this.registrations;
    }

} //BookXMLProcessor
