/**
 */
package org.openscada.doc.model.doc.book.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.util.BookResourceFactoryImpl
 * @generated
 */
public class BookResourceImpl extends XMLResourceImpl
{
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param uri
     *            the URI of the new resource.
     * @generated
     */
    public BookResourceImpl ( final URI uri )
    {
        super ( uri );
    }

} //BookResourceImpl
