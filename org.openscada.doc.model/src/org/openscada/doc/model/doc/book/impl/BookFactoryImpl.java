/**
 */
package org.openscada.doc.model.doc.book.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.BookSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BookFactoryImpl extends EFactoryImpl implements BookFactory
{
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static BookFactory init ()
    {
        try
        {
            final BookFactory theBookFactory = (BookFactory)EPackage.Registry.INSTANCE.getEFactory ( "urn:openscada:doc:book" );
            if ( theBookFactory != null )
            {
                return theBookFactory;
            }
        }
        catch ( final Exception exception )
        {
            EcorePlugin.INSTANCE.log ( exception );
        }
        return new BookFactoryImpl ();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BookFactoryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create ( final EClass eClass )
    {
        switch ( eClass.getClassifierID () )
        {
            case BookPackage.BOOK:
                return createBook ();
            case BookPackage.BOOK_SECTION:
                return createBookSection ();
            default:
                throw new IllegalArgumentException ( "The class '" + eClass.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Book createBook ()
    {
        final BookImpl book = new BookImpl ();
        return book;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BookSection createBookSection ()
    {
        final BookSectionImpl bookSection = new BookSectionImpl ();
        return bookSection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BookPackage getBookPackage ()
    {
        return (BookPackage)getEPackage ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static BookPackage getPackage ()
    {
        return BookPackage.eINSTANCE;
    }

} //BookFactoryImpl
