/**
 */
package org.openscada.doc.model.doc.book.builder;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.openscada.doc.model.doc.map.MapPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.builder.BuilderFactory
 * @model kind="package"
 * @generated
 */
public interface BuilderPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "builder";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:book:builder";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "builder";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    BuilderPackage eINSTANCE = org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl.init ();

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl
     * <em>Book Builder</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl
     * @see org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl#getBookBuilder()
     * @generated
     */
    int BOOK_BUILDER = 0;

    /**
     * The feature id for the '<em><b>Elements</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__ELEMENTS = MapPackage.MAP__ELEMENTS;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__NUMBERING_STYLE = MapPackage.MAP__NUMBERING_STYLE;

    /**
     * The feature id for the '<em><b>Extension Mappings</b></em>' containment
     * reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__EXTENSION_MAPPINGS = MapPackage.MAP__EXTENSION_MAPPINGS;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__TITLE = MapPackage.MAP_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__VERSION = MapPackage.MAP_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Authors</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__AUTHORS = MapPackage.MAP_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>License</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__LICENSE = MapPackage.MAP_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Copyright</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__COPYRIGHT = MapPackage.MAP_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Copyright Marker</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__COPYRIGHT_MARKER = MapPackage.MAP_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Properties</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER__PROPERTIES = MapPackage.MAP_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>Book Builder</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_BUILDER_FEATURE_COUNT = MapPackage.MAP_FEATURE_COUNT + 7;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.book.builder.impl.PropertyEntryImpl
     * <em>Property Entry</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.book.builder.impl.PropertyEntryImpl
     * @see org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl#getPropertyEntry()
     * @generated
     */
    int PROPERTY_ENTRY = 1;

    /**
     * The feature id for the '<em><b>Key</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROPERTY_ENTRY__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROPERTY_ENTRY__VALUE = 1;

    /**
     * The number of structural features of the '<em>Property Entry</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PROPERTY_ENTRY_FEATURE_COUNT = 2;

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder
     * <em>Book Builder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Book Builder</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder
     * @generated
     */
    EClass getBookBuilder ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getTitle
     * <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getTitle()
     * @see #getBookBuilder()
     * @generated
     */
    EAttribute getBookBuilder_Title ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getVersion
     * <em>Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getVersion()
     * @see #getBookBuilder()
     * @generated
     */
    EAttribute getBookBuilder_Version ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getAuthors
     * <em>Authors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Authors</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getAuthors()
     * @see #getBookBuilder()
     * @generated
     */
    EReference getBookBuilder_Authors ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getLicense
     * <em>License</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>License</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getLicense()
     * @see #getBookBuilder()
     * @generated
     */
    EAttribute getBookBuilder_License ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyright
     * <em>Copyright</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Copyright</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyright()
     * @see #getBookBuilder()
     * @generated
     */
    EReference getBookBuilder_Copyright ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyrightMarker
     * <em>Copyright Marker</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Copyright Marker</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyrightMarker()
     * @see #getBookBuilder()
     * @generated
     */
    EAttribute getBookBuilder_CopyrightMarker ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getProperties
     * <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Properties</em>'.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder#getProperties()
     * @see #getBookBuilder()
     * @generated
     */
    EReference getBookBuilder_Properties ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.book.builder.PropertyEntry
     * <em>Property Entry</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Property Entry</em>'.
     * @see org.openscada.doc.model.doc.book.builder.PropertyEntry
     * @generated
     */
    EClass getPropertyEntry ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.PropertyEntry#getKey
     * <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Key</em>'.
     * @see org.openscada.doc.model.doc.book.builder.PropertyEntry#getKey()
     * @see #getPropertyEntry()
     * @generated
     */
    EAttribute getPropertyEntry_Key ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.builder.PropertyEntry#getValue
     * <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see org.openscada.doc.model.doc.book.builder.PropertyEntry#getValue()
     * @see #getPropertyEntry()
     * @generated
     */
    EAttribute getPropertyEntry_Value ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    BuilderFactory getBuilderFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl
         * <em>Book Builder</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl
         * @see org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl#getBookBuilder()
         * @generated
         */
        EClass BOOK_BUILDER = eINSTANCE.getBookBuilder ();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_BUILDER__TITLE = eINSTANCE.getBookBuilder_Title ();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_BUILDER__VERSION = eINSTANCE.getBookBuilder_Version ();

        /**
         * The meta object literal for the '<em><b>Authors</b></em>' containment
         * reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK_BUILDER__AUTHORS = eINSTANCE.getBookBuilder_Authors ();

        /**
         * The meta object literal for the '<em><b>License</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_BUILDER__LICENSE = eINSTANCE.getBookBuilder_License ();

        /**
         * The meta object literal for the '<em><b>Copyright</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK_BUILDER__COPYRIGHT = eINSTANCE.getBookBuilder_Copyright ();

        /**
         * The meta object literal for the '<em><b>Copyright Marker</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_BUILDER__COPYRIGHT_MARKER = eINSTANCE.getBookBuilder_CopyrightMarker ();

        /**
         * The meta object literal for the '<em><b>Properties</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK_BUILDER__PROPERTIES = eINSTANCE.getBookBuilder_Properties ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.book.builder.impl.PropertyEntryImpl
         * <em>Property Entry</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.book.builder.impl.PropertyEntryImpl
         * @see org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl#getPropertyEntry()
         * @generated
         */
        EClass PROPERTY_ENTRY = eINSTANCE.getPropertyEntry ();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PROPERTY_ENTRY__KEY = eINSTANCE.getPropertyEntry_Key ();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PROPERTY_ENTRY__VALUE = eINSTANCE.getPropertyEntry_Value ();

    }

} //BuilderPackage
