/**
 */
package org.openscada.doc.model.doc.book.builder.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.book.builder.BookBuilder;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;
import org.openscada.doc.model.doc.map.Map;
import org.openscada.doc.model.doc.map.MapContainer;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the
 * model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.builder.BuilderPackage
 * @generated
 */
public class BuilderAdapterFactory extends AdapterFactoryImpl
{
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static BuilderPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BuilderAdapterFactory ()
    {
        if ( modelPackage == null )
        {
            modelPackage = BuilderPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the
     * model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType ( final Object object )
    {
        if ( object == modelPackage )
        {
            return true;
        }
        if ( object instanceof EObject )
        {
            return ( (EObject)object ).eClass ().getEPackage () == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BuilderSwitch<Adapter> modelSwitch = new BuilderSwitch<Adapter> () {
        @Override
        public Adapter caseBookBuilder ( final BookBuilder object )
        {
            return createBookBuilderAdapter ();
        }

        @Override
        public Adapter casePropertyEntry ( final PropertyEntry object )
        {
            return createPropertyEntryAdapter ();
        }

        @Override
        public Adapter caseMapContainer ( final MapContainer object )
        {
            return createMapContainerAdapter ();
        }

        @Override
        public Adapter caseMap ( final Map object )
        {
            return createMapAdapter ();
        }

        @Override
        public Adapter defaultCase ( final EObject object )
        {
            return createEObjectAdapter ();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target
     *            the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter ( final Notifier target )
    {
        return this.modelSwitch.doSwitch ( (EObject)target );
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder
     * <em>Book Builder</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.book.builder.BookBuilder
     * @generated
     */
    public Adapter createBookBuilderAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.book.builder.PropertyEntry
     * <em>Property Entry</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.book.builder.PropertyEntry
     * @generated
     */
    public Adapter createPropertyEntryAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.MapContainer <em>Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.MapContainer
     * @generated
     */
    public Adapter createMapContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '
     * {@link org.openscada.doc.model.doc.map.Map <em>Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.Map
     * @generated
     */
    public Adapter createMapAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter ()
    {
        return null;
    }

} //BuilderAdapterFactory
