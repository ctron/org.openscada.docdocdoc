/**
 */
package org.openscada.doc.model.doc.book;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.BookFactory
 * @model kind="package"
 * @generated
 */
public interface BookPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "book";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:book";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "book";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    BookPackage eINSTANCE = org.openscada.doc.model.doc.book.impl.BookPackageImpl.init ();

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.book.BookContainer <em>Container</em>}
     * ' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.book.BookContainer
     * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBookContainer()
     * @generated
     */
    int BOOK_CONTAINER = 2;

    /**
     * The feature id for the '<em><b>Content</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_CONTAINER__CONTENT = 0;

    /**
     * The feature id for the '<em><b>Sections</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_CONTAINER__SECTIONS = 1;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_CONTAINER__NUMBERING_STYLE = 2;

    /**
     * The number of structural features of the '<em>Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_CONTAINER_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.book.impl.BookImpl <em>Book</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.book.impl.BookImpl
     * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBook()
     * @generated
     */
    int BOOK = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__CONTENT = BOOK_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Sections</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__SECTIONS = BOOK_CONTAINER__SECTIONS;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__NUMBERING_STYLE = BOOK_CONTAINER__NUMBERING_STYLE;

    /**
     * The feature id for the '<em><b>Authors</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__AUTHORS = BOOK_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__TITLE = BOOK_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__VERSION = BOOK_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Copyright</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__COPYRIGHT = BOOK_CONTAINER_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Copyright Marker</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__COPYRIGHT_MARKER = BOOK_CONTAINER_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Copyright Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK__COPYRIGHT_TEXT = BOOK_CONTAINER_FEATURE_COUNT + 5;

    /**
     * The number of structural features of the '<em>Book</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_FEATURE_COUNT = BOOK_CONTAINER_FEATURE_COUNT + 6;

    /**
     * The meta object id for the '
     * {@link org.openscada.doc.model.doc.book.impl.BookSectionImpl
     * <em>Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.book.impl.BookSectionImpl
     * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBookSection()
     * @generated
     */
    int BOOK_SECTION = 1;

    /**
     * The feature id for the '<em><b>Content</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__CONTENT = BOOK_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Sections</b></em>' containment reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__SECTIONS = BOOK_CONTAINER__SECTIONS;

    /**
     * The feature id for the '<em><b>Numbering Style</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__NUMBERING_STYLE = BOOK_CONTAINER__NUMBERING_STYLE;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__ID = BOOK_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__NUMBER = BOOK_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Full Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__FULL_NUMBER = BOOK_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION__TITLE = BOOK_CONTAINER_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Section</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BOOK_SECTION_FEATURE_COUNT = BOOK_CONTAINER_FEATURE_COUNT + 4;

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.book.Book <em>Book</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Book</em>'.
     * @see org.openscada.doc.model.doc.book.Book
     * @generated
     */
    EClass getBook ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.Book#getAuthors <em>Authors</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Authors</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getAuthors()
     * @see #getBook()
     * @generated
     */
    EReference getBook_Authors ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.Book#getTitle <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getTitle()
     * @see #getBook()
     * @generated
     */
    EAttribute getBook_Title ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.Book#getVersion <em>Version</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getVersion()
     * @see #getBook()
     * @generated
     */
    EAttribute getBook_Version ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.Book#getCopyright
     * <em>Copyright</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Copyright</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getCopyright()
     * @see #getBook()
     * @generated
     */
    EReference getBook_Copyright ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.Book#getCopyrightMarker
     * <em>Copyright Marker</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Copyright Marker</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getCopyrightMarker()
     * @see #getBook()
     * @generated
     */
    EAttribute getBook_CopyrightMarker ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.Book#getCopyrightText
     * <em>Copyright Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Copyright Text</em>'.
     * @see org.openscada.doc.model.doc.book.Book#getCopyrightText()
     * @see #getBook()
     * @generated
     */
    EAttribute getBook_CopyrightText ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.book.BookSection <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Section</em>'.
     * @see org.openscada.doc.model.doc.book.BookSection
     * @generated
     */
    EClass getBookSection ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.BookSection#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see org.openscada.doc.model.doc.book.BookSection#getId()
     * @see #getBookSection()
     * @generated
     */
    EAttribute getBookSection_Id ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.BookSection#getNumber
     * <em>Number</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Number</em>'.
     * @see org.openscada.doc.model.doc.book.BookSection#getNumber()
     * @see #getBookSection()
     * @generated
     */
    EAttribute getBookSection_Number ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.BookSection#getFullNumber
     * <em>Full Number</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Full Number</em>'.
     * @see org.openscada.doc.model.doc.book.BookSection#getFullNumber()
     * @see #getBookSection()
     * @generated
     */
    EAttribute getBookSection_FullNumber ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.BookSection#getTitle
     * <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.model.doc.book.BookSection#getTitle()
     * @see #getBookSection()
     * @generated
     */
    EAttribute getBookSection_Title ();

    /**
     * Returns the meta object for class '
     * {@link org.openscada.doc.model.doc.book.BookContainer <em>Container</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Container</em>'.
     * @see org.openscada.doc.model.doc.book.BookContainer
     * @generated
     */
    EClass getBookContainer ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.BookContainer#getContent
     * <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Content</em>'.
     * @see org.openscada.doc.model.doc.book.BookContainer#getContent()
     * @see #getBookContainer()
     * @generated
     */
    EReference getBookContainer_Content ();

    /**
     * Returns the meta object for the containment reference list '
     * {@link org.openscada.doc.model.doc.book.BookContainer#getSections
     * <em>Sections</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '
     *         <em>Sections</em>'.
     * @see org.openscada.doc.model.doc.book.BookContainer#getSections()
     * @see #getBookContainer()
     * @generated
     */
    EReference getBookContainer_Sections ();

    /**
     * Returns the meta object for the attribute '
     * {@link org.openscada.doc.model.doc.book.BookContainer#getNumberingStyle
     * <em>Numbering Style</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Numbering Style</em>'.
     * @see org.openscada.doc.model.doc.book.BookContainer#getNumberingStyle()
     * @see #getBookContainer()
     * @generated
     */
    EAttribute getBookContainer_NumberingStyle ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    BookFactory getBookFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.book.impl.BookImpl <em>Book</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.book.impl.BookImpl
         * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBook()
         * @generated
         */
        EClass BOOK = eINSTANCE.getBook ();

        /**
         * The meta object literal for the '<em><b>Authors</b></em>' containment
         * reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK__AUTHORS = eINSTANCE.getBook_Authors ();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK__TITLE = eINSTANCE.getBook_Title ();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK__VERSION = eINSTANCE.getBook_Version ();

        /**
         * The meta object literal for the '<em><b>Copyright</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK__COPYRIGHT = eINSTANCE.getBook_Copyright ();

        /**
         * The meta object literal for the '<em><b>Copyright Marker</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK__COPYRIGHT_MARKER = eINSTANCE.getBook_CopyrightMarker ();

        /**
         * The meta object literal for the '<em><b>Copyright Text</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK__COPYRIGHT_TEXT = eINSTANCE.getBook_CopyrightText ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.book.impl.BookSectionImpl
         * <em>Section</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.book.impl.BookSectionImpl
         * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBookSection()
         * @generated
         */
        EClass BOOK_SECTION = eINSTANCE.getBookSection ();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_SECTION__ID = eINSTANCE.getBookSection_Id ();

        /**
         * The meta object literal for the '<em><b>Number</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_SECTION__NUMBER = eINSTANCE.getBookSection_Number ();

        /**
         * The meta object literal for the '<em><b>Full Number</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_SECTION__FULL_NUMBER = eINSTANCE.getBookSection_FullNumber ();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_SECTION__TITLE = eINSTANCE.getBookSection_Title ();

        /**
         * The meta object literal for the '
         * {@link org.openscada.doc.model.doc.book.BookContainer
         * <em>Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.openscada.doc.model.doc.book.BookContainer
         * @see org.openscada.doc.model.doc.book.impl.BookPackageImpl#getBookContainer()
         * @generated
         */
        EClass BOOK_CONTAINER = eINSTANCE.getBookContainer ();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' containment
         * reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK_CONTAINER__CONTENT = eINSTANCE.getBookContainer_Content ();

        /**
         * The meta object literal for the '<em><b>Sections</b></em>'
         * containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BOOK_CONTAINER__SECTIONS = eINSTANCE.getBookContainer_Sections ();

        /**
         * The meta object literal for the '<em><b>Numbering Style</b></em>'
         * attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BOOK_CONTAINER__NUMBERING_STYLE = eINSTANCE.getBookContainer_NumberingStyle ();

    }

} //BookPackage
