/**
 */
package org.openscada.doc.model.doc.book.builder;

import org.eclipse.emf.common.util.EList;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.map.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Book Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.book.builder.BookBuilder#getTitle <em>
 * Title</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.builder.BookBuilder#getVersion
 * <em>Version</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.builder.BookBuilder#getAuthors
 * <em>Authors</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.builder.BookBuilder#getLicense
 * <em>License</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyright
 * <em>Copyright</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyrightMarker
 * <em>Copyright Marker</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getProperties
 * <em>Properties</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder()
 * @model
 * @generated
 */
public interface BookBuilder extends Map
{
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_Title()
     * @model
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getTitle
     * <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

    /**
     * Returns the value of the '<em><b>Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Version</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Version</em>' attribute.
     * @see #setVersion(String)
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_Version()
     * @model
     * @generated
     */
    String getVersion ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getVersion
     * <em>Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Version</em>' attribute.
     * @see #getVersion()
     * @generated
     */
    void setVersion ( String value );

    /**
     * Returns the value of the '<em><b>Authors</b></em>' containment reference
     * list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Author}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Authors</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Authors</em>' containment reference list.
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_Authors()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<Author> getAuthors ();

    /**
     * Returns the value of the '<em><b>License</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>License</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>License</em>' attribute.
     * @see #setLicense(String)
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_License()
     * @model required="true"
     * @generated
     */
    String getLicense ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getLicense
     * <em>License</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>License</em>' attribute.
     * @see #getLicense()
     * @generated
     */
    void setLicense ( String value );

    /**
     * Returns the value of the '<em><b>Copyright</b></em>' containment
     * reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.fragment.Copyright}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Copyright</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Copyright</em>' containment reference list.
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_Copyright()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<Copyright> getCopyright ();

    /**
     * Returns the value of the '<em><b>Copyright Marker</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Copyright Marker</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Copyright Marker</em>' attribute.
     * @see #setCopyrightMarker(String)
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_CopyrightMarker()
     * @model required="true"
     * @generated
     */
    String getCopyrightMarker ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.builder.BookBuilder#getCopyrightMarker
     * <em>Copyright Marker</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Copyright Marker</em>' attribute.
     * @see #getCopyrightMarker()
     * @generated
     */
    void setCopyrightMarker ( String value );

    /**
     * Returns the value of the '<em><b>Properties</b></em>' containment
     * reference list.
     * The list contents are of type
     * {@link org.openscada.doc.model.doc.book.builder.PropertyEntry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Properties</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Properties</em>' containment reference
     *         list.
     * @see org.openscada.doc.model.doc.book.builder.BuilderPackage#getBookBuilder_Properties()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<PropertyEntry> getProperties ();

} // BookBuilder
