/**
 */
package org.openscada.doc.model.doc.book;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.book.BookSection#getId <em>Id</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.BookSection#getNumber <em>Number
 * </em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.BookSection#getFullNumber <em>
 * Full Number</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.BookSection#getTitle <em>Title
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.openscada.doc.model.doc.book.BookPackage#getBookSection()
 * @model
 * @generated
 */
public interface BookSection extends BookContainer
{
    /**
     * Returns the value of the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Id</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Id</em>' attribute.
     * @see #setId(String)
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookSection_Id()
     * @model id="true" required="true"
     * @generated
     */
    String getId ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.BookSection#getId <em>Id</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Id</em>' attribute.
     * @see #getId()
     * @generated
     */
    void setId ( String value );

    /**
     * Returns the value of the '<em><b>Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Number</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Number</em>' attribute.
     * @see #setNumber(int)
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookSection_Number()
     * @model required="true"
     * @generated
     */
    int getNumber ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.BookSection#getNumber
     * <em>Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Number</em>' attribute.
     * @see #getNumber()
     * @generated
     */
    void setNumber ( int value );

    /**
     * Returns the value of the '<em><b>Full Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Full Number</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Full Number</em>' attribute.
     * @see #setFullNumber(String)
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookSection_FullNumber()
     * @model required="true"
     * @generated
     */
    String getFullNumber ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.BookSection#getFullNumber
     * <em>Full Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Full Number</em>' attribute.
     * @see #getFullNumber()
     * @generated
     */
    void setFullNumber ( String value );

    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.doc.model.doc.book.BookPackage#getBookSection_Title()
     * @model required="true"
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '
     * {@link org.openscada.doc.model.doc.book.BookSection#getTitle
     * <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

} // BookSection
