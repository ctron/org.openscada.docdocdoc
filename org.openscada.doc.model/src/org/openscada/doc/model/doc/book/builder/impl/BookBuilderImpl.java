/**
 */
package org.openscada.doc.model.doc.book.builder.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.book.builder.BookBuilder;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.model.doc.map.impl.MapImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getTitle
 * <em>Title</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getVersion
 * <em>Version</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getAuthors
 * <em>Authors</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getLicense
 * <em>License</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getCopyright
 * <em>Copyright</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getCopyrightMarker
 * <em>Copyright Marker</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.builder.impl.BookBuilderImpl#getProperties
 * <em>Properties</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class BookBuilderImpl extends MapImpl implements BookBuilder
{
    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getVersion() <em>Version</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVersion()
     * @generated
     * @ordered
     */
    protected static final String VERSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getVersion() <em>Version</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVersion()
     * @generated
     * @ordered
     */
    protected String version = VERSION_EDEFAULT;

    /**
     * The cached value of the '{@link #getAuthors() <em>Authors</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAuthors()
     * @generated
     * @ordered
     */
    protected EList<Author> authors;

    /**
     * The default value of the '{@link #getLicense() <em>License</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLicense()
     * @generated
     * @ordered
     */
    protected static final String LICENSE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getLicense() <em>License</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLicense()
     * @generated
     * @ordered
     */
    protected String license = LICENSE_EDEFAULT;

    /**
     * The cached value of the '{@link #getCopyright() <em>Copyright</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyright()
     * @generated
     * @ordered
     */
    protected EList<Copyright> copyright;

    /**
     * The default value of the '{@link #getCopyrightMarker()
     * <em>Copyright Marker</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightMarker()
     * @generated
     * @ordered
     */
    protected static final String COPYRIGHT_MARKER_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCopyrightMarker()
     * <em>Copyright Marker</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightMarker()
     * @generated
     * @ordered
     */
    protected String copyrightMarker = COPYRIGHT_MARKER_EDEFAULT;

    /**
     * The cached value of the '{@link #getProperties() <em>Properties</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProperties()
     * @generated
     * @ordered
     */
    protected EList<PropertyEntry> properties;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookBuilderImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return BuilderPackage.Literals.BOOK_BUILDER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return this.title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle ( final String newTitle )
    {
        final String oldTitle = this.title;
        this.title = newTitle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BuilderPackage.BOOK_BUILDER__TITLE, oldTitle, this.title ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getVersion ()
    {
        return this.version;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setVersion ( final String newVersion )
    {
        final String oldVersion = this.version;
        this.version = newVersion;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BuilderPackage.BOOK_BUILDER__VERSION, oldVersion, this.version ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Author> getAuthors ()
    {
        if ( this.authors == null )
        {
            this.authors = new EObjectContainmentEList.Resolving<Author> ( Author.class, this, BuilderPackage.BOOK_BUILDER__AUTHORS );
        }
        return this.authors;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLicense ()
    {
        return this.license;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setLicense ( final String newLicense )
    {
        final String oldLicense = this.license;
        this.license = newLicense;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BuilderPackage.BOOK_BUILDER__LICENSE, oldLicense, this.license ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Copyright> getCopyright ()
    {
        if ( this.copyright == null )
        {
            this.copyright = new EObjectContainmentEList.Resolving<Copyright> ( Copyright.class, this, BuilderPackage.BOOK_BUILDER__COPYRIGHT );
        }
        return this.copyright;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCopyrightMarker ()
    {
        return this.copyrightMarker;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCopyrightMarker ( final String newCopyrightMarker )
    {
        final String oldCopyrightMarker = this.copyrightMarker;
        this.copyrightMarker = newCopyrightMarker;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BuilderPackage.BOOK_BUILDER__COPYRIGHT_MARKER, oldCopyrightMarker, this.copyrightMarker ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<PropertyEntry> getProperties ()
    {
        if ( this.properties == null )
        {
            this.properties = new EObjectContainmentEList.Resolving<PropertyEntry> ( PropertyEntry.class, this, BuilderPackage.BOOK_BUILDER__PROPERTIES );
        }
        return this.properties;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case BuilderPackage.BOOK_BUILDER__AUTHORS:
                return ( (InternalEList<?>)getAuthors () ).basicRemove ( otherEnd, msgs );
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT:
                return ( (InternalEList<?>)getCopyright () ).basicRemove ( otherEnd, msgs );
            case BuilderPackage.BOOK_BUILDER__PROPERTIES:
                return ( (InternalEList<?>)getProperties () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case BuilderPackage.BOOK_BUILDER__TITLE:
                return getTitle ();
            case BuilderPackage.BOOK_BUILDER__VERSION:
                return getVersion ();
            case BuilderPackage.BOOK_BUILDER__AUTHORS:
                return getAuthors ();
            case BuilderPackage.BOOK_BUILDER__LICENSE:
                return getLicense ();
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT:
                return getCopyright ();
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT_MARKER:
                return getCopyrightMarker ();
            case BuilderPackage.BOOK_BUILDER__PROPERTIES:
                return getProperties ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case BuilderPackage.BOOK_BUILDER__TITLE:
                setTitle ( (String)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__VERSION:
                setVersion ( (String)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__AUTHORS:
                getAuthors ().clear ();
                getAuthors ().addAll ( (Collection<? extends Author>)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__LICENSE:
                setLicense ( (String)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT:
                getCopyright ().clear ();
                getCopyright ().addAll ( (Collection<? extends Copyright>)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT_MARKER:
                setCopyrightMarker ( (String)newValue );
                return;
            case BuilderPackage.BOOK_BUILDER__PROPERTIES:
                getProperties ().clear ();
                getProperties ().addAll ( (Collection<? extends PropertyEntry>)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case BuilderPackage.BOOK_BUILDER__TITLE:
                setTitle ( TITLE_EDEFAULT );
                return;
            case BuilderPackage.BOOK_BUILDER__VERSION:
                setVersion ( VERSION_EDEFAULT );
                return;
            case BuilderPackage.BOOK_BUILDER__AUTHORS:
                getAuthors ().clear ();
                return;
            case BuilderPackage.BOOK_BUILDER__LICENSE:
                setLicense ( LICENSE_EDEFAULT );
                return;
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT:
                getCopyright ().clear ();
                return;
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT_MARKER:
                setCopyrightMarker ( COPYRIGHT_MARKER_EDEFAULT );
                return;
            case BuilderPackage.BOOK_BUILDER__PROPERTIES:
                getProperties ().clear ();
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case BuilderPackage.BOOK_BUILDER__TITLE:
                return TITLE_EDEFAULT == null ? this.title != null : !TITLE_EDEFAULT.equals ( this.title );
            case BuilderPackage.BOOK_BUILDER__VERSION:
                return VERSION_EDEFAULT == null ? this.version != null : !VERSION_EDEFAULT.equals ( this.version );
            case BuilderPackage.BOOK_BUILDER__AUTHORS:
                return this.authors != null && !this.authors.isEmpty ();
            case BuilderPackage.BOOK_BUILDER__LICENSE:
                return LICENSE_EDEFAULT == null ? this.license != null : !LICENSE_EDEFAULT.equals ( this.license );
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT:
                return this.copyright != null && !this.copyright.isEmpty ();
            case BuilderPackage.BOOK_BUILDER__COPYRIGHT_MARKER:
                return COPYRIGHT_MARKER_EDEFAULT == null ? this.copyrightMarker != null : !COPYRIGHT_MARKER_EDEFAULT.equals ( this.copyrightMarker );
            case BuilderPackage.BOOK_BUILDER__PROPERTIES:
                return this.properties != null && !this.properties.isEmpty ();
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (title: " );
        result.append ( this.title );
        result.append ( ", version: " );
        result.append ( this.version );
        result.append ( ", license: " );
        result.append ( this.license );
        result.append ( ", copyrightMarker: " );
        result.append ( this.copyrightMarker );
        result.append ( ')' );
        return result.toString ();
    }

} //BookBuilderImpl
