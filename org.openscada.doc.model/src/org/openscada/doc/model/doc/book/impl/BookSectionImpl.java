/**
 */
package org.openscada.doc.model.doc.book.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getContent
 * <em>Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getSections
 * <em>Sections</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getId <em>Id
 * </em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getNumber
 * <em>Number</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getFullNumber
 * <em>Full Number</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookSectionImpl#getTitle
 * <em>Title</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class BookSectionImpl extends EObjectImpl implements BookSection
{
    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected EList<Content> content;

    /**
     * The cached value of the '{@link #getSections() <em>Sections</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSections()
     * @generated
     * @ordered
     */
    protected EList<BookSection> sections;

    /**
     * The default value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected static final NumberingStyle NUMBERING_STYLE_EDEFAULT = NumberingStyle.ARABIC;

    /**
     * The cached value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected NumberingStyle numberingStyle = NUMBERING_STYLE_EDEFAULT;

    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The default value of the '{@link #getNumber() <em>Number</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumber()
     * @generated
     * @ordered
     */
    protected static final int NUMBER_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumber()
     * @generated
     * @ordered
     */
    protected int number = NUMBER_EDEFAULT;

    /**
     * The default value of the '{@link #getFullNumber() <em>Full Number</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFullNumber()
     * @generated
     * @ordered
     */
    protected static final String FULL_NUMBER_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getFullNumber() <em>Full Number</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFullNumber()
     * @generated
     * @ordered
     */
    protected String fullNumber = FULL_NUMBER_EDEFAULT;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookSectionImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return BookPackage.Literals.BOOK_SECTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Content> getContent ()
    {
        if ( this.content == null )
        {
            this.content = new EObjectContainmentEList.Resolving<Content> ( Content.class, this, BookPackage.BOOK_SECTION__CONTENT );
        }
        return this.content;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<BookSection> getSections ()
    {
        if ( this.sections == null )
        {
            this.sections = new EObjectContainmentEList.Resolving<BookSection> ( BookSection.class, this, BookPackage.BOOK_SECTION__SECTIONS );
        }
        return this.sections;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NumberingStyle getNumberingStyle ()
    {
        return this.numberingStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumberingStyle ( final NumberingStyle newNumberingStyle )
    {
        final NumberingStyle oldNumberingStyle = this.numberingStyle;
        this.numberingStyle = newNumberingStyle == null ? NUMBERING_STYLE_EDEFAULT : newNumberingStyle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK_SECTION__NUMBERING_STYLE, oldNumberingStyle, this.numberingStyle ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId ()
    {
        return this.id;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId ( final String newId )
    {
        final String oldId = this.id;
        this.id = newId;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK_SECTION__ID, oldId, this.id ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getNumber ()
    {
        return this.number;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumber ( final int newNumber )
    {
        final int oldNumber = this.number;
        this.number = newNumber;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK_SECTION__NUMBER, oldNumber, this.number ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFullNumber ()
    {
        return this.fullNumber;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFullNumber ( final String newFullNumber )
    {
        final String oldFullNumber = this.fullNumber;
        this.fullNumber = newFullNumber;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK_SECTION__FULL_NUMBER, oldFullNumber, this.fullNumber ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return this.title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle ( final String newTitle )
    {
        final String oldTitle = this.title;
        this.title = newTitle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK_SECTION__TITLE, oldTitle, this.title ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK_SECTION__CONTENT:
                return ( (InternalEList<?>)getContent () ).basicRemove ( otherEnd, msgs );
            case BookPackage.BOOK_SECTION__SECTIONS:
                return ( (InternalEList<?>)getSections () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK_SECTION__CONTENT:
                return getContent ();
            case BookPackage.BOOK_SECTION__SECTIONS:
                return getSections ();
            case BookPackage.BOOK_SECTION__NUMBERING_STYLE:
                return getNumberingStyle ();
            case BookPackage.BOOK_SECTION__ID:
                return getId ();
            case BookPackage.BOOK_SECTION__NUMBER:
                return getNumber ();
            case BookPackage.BOOK_SECTION__FULL_NUMBER:
                return getFullNumber ();
            case BookPackage.BOOK_SECTION__TITLE:
                return getTitle ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK_SECTION__CONTENT:
                getContent ().clear ();
                getContent ().addAll ( (Collection<? extends Content>)newValue );
                return;
            case BookPackage.BOOK_SECTION__SECTIONS:
                getSections ().clear ();
                getSections ().addAll ( (Collection<? extends BookSection>)newValue );
                return;
            case BookPackage.BOOK_SECTION__NUMBERING_STYLE:
                setNumberingStyle ( (NumberingStyle)newValue );
                return;
            case BookPackage.BOOK_SECTION__ID:
                setId ( (String)newValue );
                return;
            case BookPackage.BOOK_SECTION__NUMBER:
                setNumber ( (Integer)newValue );
                return;
            case BookPackage.BOOK_SECTION__FULL_NUMBER:
                setFullNumber ( (String)newValue );
                return;
            case BookPackage.BOOK_SECTION__TITLE:
                setTitle ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK_SECTION__CONTENT:
                getContent ().clear ();
                return;
            case BookPackage.BOOK_SECTION__SECTIONS:
                getSections ().clear ();
                return;
            case BookPackage.BOOK_SECTION__NUMBERING_STYLE:
                setNumberingStyle ( NUMBERING_STYLE_EDEFAULT );
                return;
            case BookPackage.BOOK_SECTION__ID:
                setId ( ID_EDEFAULT );
                return;
            case BookPackage.BOOK_SECTION__NUMBER:
                setNumber ( NUMBER_EDEFAULT );
                return;
            case BookPackage.BOOK_SECTION__FULL_NUMBER:
                setFullNumber ( FULL_NUMBER_EDEFAULT );
                return;
            case BookPackage.BOOK_SECTION__TITLE:
                setTitle ( TITLE_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK_SECTION__CONTENT:
                return this.content != null && !this.content.isEmpty ();
            case BookPackage.BOOK_SECTION__SECTIONS:
                return this.sections != null && !this.sections.isEmpty ();
            case BookPackage.BOOK_SECTION__NUMBERING_STYLE:
                return this.numberingStyle != NUMBERING_STYLE_EDEFAULT;
            case BookPackage.BOOK_SECTION__ID:
                return ID_EDEFAULT == null ? this.id != null : !ID_EDEFAULT.equals ( this.id );
            case BookPackage.BOOK_SECTION__NUMBER:
                return this.number != NUMBER_EDEFAULT;
            case BookPackage.BOOK_SECTION__FULL_NUMBER:
                return FULL_NUMBER_EDEFAULT == null ? this.fullNumber != null : !FULL_NUMBER_EDEFAULT.equals ( this.fullNumber );
            case BookPackage.BOOK_SECTION__TITLE:
                return TITLE_EDEFAULT == null ? this.title != null : !TITLE_EDEFAULT.equals ( this.title );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (numberingStyle: " );
        result.append ( this.numberingStyle );
        result.append ( ", id: " );
        result.append ( this.id );
        result.append ( ", number: " );
        result.append ( this.number );
        result.append ( ", fullNumber: " );
        result.append ( this.fullNumber );
        result.append ( ", title: " );
        result.append ( this.title );
        result.append ( ')' );
        return result.toString ();
    }

} //BookSectionImpl
