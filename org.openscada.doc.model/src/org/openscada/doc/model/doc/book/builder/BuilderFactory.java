/**
 */
package org.openscada.doc.model.doc.book.builder;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.book.builder.BuilderPackage
 * @generated
 */
public interface BuilderFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    BuilderFactory eINSTANCE = org.openscada.doc.model.doc.book.builder.impl.BuilderFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Book Builder</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Book Builder</em>'.
     * @generated
     */
    BookBuilder createBookBuilder ();

    /**
     * Returns a new object of class '<em>Property Entry</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Property Entry</em>'.
     * @generated
     */
    PropertyEntry createPropertyEntry ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    BuilderPackage getBuilderPackage ();

} //BuilderFactory
