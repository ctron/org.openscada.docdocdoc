/**
 */
package org.openscada.doc.model.doc.book.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.Copyright;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getContent <em>
 * Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getSections <em>
 * Sections</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getNumberingStyle
 * <em>Numbering Style</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getAuthors <em>
 * Authors</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getTitle <em>Title
 * </em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getVersion <em>
 * Version</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getCopyright <em>
 * Copyright</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getCopyrightMarker
 * <em>Copyright Marker</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.book.impl.BookImpl#getCopyrightText
 * <em>Copyright Text</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class BookImpl extends EObjectImpl implements Book
{
    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected EList<Content> content;

    /**
     * The cached value of the '{@link #getSections() <em>Sections</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSections()
     * @generated
     * @ordered
     */
    protected EList<BookSection> sections;

    /**
     * The default value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected static final NumberingStyle NUMBERING_STYLE_EDEFAULT = NumberingStyle.ARABIC;

    /**
     * The cached value of the '{@link #getNumberingStyle()
     * <em>Numbering Style</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumberingStyle()
     * @generated
     * @ordered
     */
    protected NumberingStyle numberingStyle = NUMBERING_STYLE_EDEFAULT;

    /**
     * The cached value of the '{@link #getAuthors() <em>Authors</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAuthors()
     * @generated
     * @ordered
     */
    protected EList<Author> authors;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getVersion() <em>Version</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVersion()
     * @generated
     * @ordered
     */
    protected static final String VERSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getVersion() <em>Version</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVersion()
     * @generated
     * @ordered
     */
    protected String version = VERSION_EDEFAULT;

    /**
     * The cached value of the '{@link #getCopyright() <em>Copyright</em>}'
     * containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyright()
     * @generated
     * @ordered
     */
    protected EList<Copyright> copyright;

    /**
     * The default value of the '{@link #getCopyrightMarker()
     * <em>Copyright Marker</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightMarker()
     * @generated
     * @ordered
     */
    protected static final String COPYRIGHT_MARKER_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCopyrightMarker()
     * <em>Copyright Marker</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightMarker()
     * @generated
     * @ordered
     */
    protected String copyrightMarker = COPYRIGHT_MARKER_EDEFAULT;

    /**
     * The default value of the '{@link #getCopyrightText()
     * <em>Copyright Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightText()
     * @generated
     * @ordered
     */
    protected static final String COPYRIGHT_TEXT_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCopyrightText()
     * <em>Copyright Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCopyrightText()
     * @generated
     * @ordered
     */
    protected String copyrightText = COPYRIGHT_TEXT_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return BookPackage.Literals.BOOK;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<BookSection> getSections ()
    {
        if ( this.sections == null )
        {
            this.sections = new EObjectContainmentEList.Resolving<BookSection> ( BookSection.class, this, BookPackage.BOOK__SECTIONS );
        }
        return this.sections;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NumberingStyle getNumberingStyle ()
    {
        return this.numberingStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumberingStyle ( final NumberingStyle newNumberingStyle )
    {
        final NumberingStyle oldNumberingStyle = this.numberingStyle;
        this.numberingStyle = newNumberingStyle == null ? NUMBERING_STYLE_EDEFAULT : newNumberingStyle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK__NUMBERING_STYLE, oldNumberingStyle, this.numberingStyle ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Author> getAuthors ()
    {
        if ( this.authors == null )
        {
            this.authors = new EObjectContainmentEList.Resolving<Author> ( Author.class, this, BookPackage.BOOK__AUTHORS );
        }
        return this.authors;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return this.title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle ( final String newTitle )
    {
        final String oldTitle = this.title;
        this.title = newTitle;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK__TITLE, oldTitle, this.title ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getVersion ()
    {
        return this.version;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setVersion ( final String newVersion )
    {
        final String oldVersion = this.version;
        this.version = newVersion;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK__VERSION, oldVersion, this.version ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Copyright> getCopyright ()
    {
        if ( this.copyright == null )
        {
            this.copyright = new EObjectContainmentEList.Resolving<Copyright> ( Copyright.class, this, BookPackage.BOOK__COPYRIGHT );
        }
        return this.copyright;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCopyrightMarker ()
    {
        return this.copyrightMarker;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCopyrightMarker ( final String newCopyrightMarker )
    {
        final String oldCopyrightMarker = this.copyrightMarker;
        this.copyrightMarker = newCopyrightMarker;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK__COPYRIGHT_MARKER, oldCopyrightMarker, this.copyrightMarker ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCopyrightText ()
    {
        return this.copyrightText;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCopyrightText ( final String newCopyrightText )
    {
        final String oldCopyrightText = this.copyrightText;
        this.copyrightText = newCopyrightText;
        if ( eNotificationRequired () )
        {
            eNotify ( new ENotificationImpl ( this, Notification.SET, BookPackage.BOOK__COPYRIGHT_TEXT, oldCopyrightText, this.copyrightText ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Content> getContent ()
    {
        if ( this.content == null )
        {
            this.content = new EObjectContainmentEList.Resolving<Content> ( Content.class, this, BookPackage.BOOK__CONTENT );
        }
        return this.content;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( final InternalEObject otherEnd, final int featureID, final NotificationChain msgs )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK__CONTENT:
                return ( (InternalEList<?>)getContent () ).basicRemove ( otherEnd, msgs );
            case BookPackage.BOOK__SECTIONS:
                return ( (InternalEList<?>)getSections () ).basicRemove ( otherEnd, msgs );
            case BookPackage.BOOK__AUTHORS:
                return ( (InternalEList<?>)getAuthors () ).basicRemove ( otherEnd, msgs );
            case BookPackage.BOOK__COPYRIGHT:
                return ( (InternalEList<?>)getCopyright () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet ( final int featureID, final boolean resolve, final boolean coreType )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK__CONTENT:
                return getContent ();
            case BookPackage.BOOK__SECTIONS:
                return getSections ();
            case BookPackage.BOOK__NUMBERING_STYLE:
                return getNumberingStyle ();
            case BookPackage.BOOK__AUTHORS:
                return getAuthors ();
            case BookPackage.BOOK__TITLE:
                return getTitle ();
            case BookPackage.BOOK__VERSION:
                return getVersion ();
            case BookPackage.BOOK__COPYRIGHT:
                return getCopyright ();
            case BookPackage.BOOK__COPYRIGHT_MARKER:
                return getCopyrightMarker ();
            case BookPackage.BOOK__COPYRIGHT_TEXT:
                return getCopyrightText ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( final int featureID, final Object newValue )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK__CONTENT:
                getContent ().clear ();
                getContent ().addAll ( (Collection<? extends Content>)newValue );
                return;
            case BookPackage.BOOK__SECTIONS:
                getSections ().clear ();
                getSections ().addAll ( (Collection<? extends BookSection>)newValue );
                return;
            case BookPackage.BOOK__NUMBERING_STYLE:
                setNumberingStyle ( (NumberingStyle)newValue );
                return;
            case BookPackage.BOOK__AUTHORS:
                getAuthors ().clear ();
                getAuthors ().addAll ( (Collection<? extends Author>)newValue );
                return;
            case BookPackage.BOOK__TITLE:
                setTitle ( (String)newValue );
                return;
            case BookPackage.BOOK__VERSION:
                setVersion ( (String)newValue );
                return;
            case BookPackage.BOOK__COPYRIGHT:
                getCopyright ().clear ();
                getCopyright ().addAll ( (Collection<? extends Copyright>)newValue );
                return;
            case BookPackage.BOOK__COPYRIGHT_MARKER:
                setCopyrightMarker ( (String)newValue );
                return;
            case BookPackage.BOOK__COPYRIGHT_TEXT:
                setCopyrightText ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset ( final int featureID )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK__CONTENT:
                getContent ().clear ();
                return;
            case BookPackage.BOOK__SECTIONS:
                getSections ().clear ();
                return;
            case BookPackage.BOOK__NUMBERING_STYLE:
                setNumberingStyle ( NUMBERING_STYLE_EDEFAULT );
                return;
            case BookPackage.BOOK__AUTHORS:
                getAuthors ().clear ();
                return;
            case BookPackage.BOOK__TITLE:
                setTitle ( TITLE_EDEFAULT );
                return;
            case BookPackage.BOOK__VERSION:
                setVersion ( VERSION_EDEFAULT );
                return;
            case BookPackage.BOOK__COPYRIGHT:
                getCopyright ().clear ();
                return;
            case BookPackage.BOOK__COPYRIGHT_MARKER:
                setCopyrightMarker ( COPYRIGHT_MARKER_EDEFAULT );
                return;
            case BookPackage.BOOK__COPYRIGHT_TEXT:
                setCopyrightText ( COPYRIGHT_TEXT_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet ( final int featureID )
    {
        switch ( featureID )
        {
            case BookPackage.BOOK__CONTENT:
                return this.content != null && !this.content.isEmpty ();
            case BookPackage.BOOK__SECTIONS:
                return this.sections != null && !this.sections.isEmpty ();
            case BookPackage.BOOK__NUMBERING_STYLE:
                return this.numberingStyle != NUMBERING_STYLE_EDEFAULT;
            case BookPackage.BOOK__AUTHORS:
                return this.authors != null && !this.authors.isEmpty ();
            case BookPackage.BOOK__TITLE:
                return TITLE_EDEFAULT == null ? this.title != null : !TITLE_EDEFAULT.equals ( this.title );
            case BookPackage.BOOK__VERSION:
                return VERSION_EDEFAULT == null ? this.version != null : !VERSION_EDEFAULT.equals ( this.version );
            case BookPackage.BOOK__COPYRIGHT:
                return this.copyright != null && !this.copyright.isEmpty ();
            case BookPackage.BOOK__COPYRIGHT_MARKER:
                return COPYRIGHT_MARKER_EDEFAULT == null ? this.copyrightMarker != null : !COPYRIGHT_MARKER_EDEFAULT.equals ( this.copyrightMarker );
            case BookPackage.BOOK__COPYRIGHT_TEXT:
                return COPYRIGHT_TEXT_EDEFAULT == null ? this.copyrightText != null : !COPYRIGHT_TEXT_EDEFAULT.equals ( this.copyrightText );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
        {
            return super.toString ();
        }

        final StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (numberingStyle: " );
        result.append ( this.numberingStyle );
        result.append ( ", title: " );
        result.append ( this.title );
        result.append ( ", version: " );
        result.append ( this.version );
        result.append ( ", copyrightMarker: " );
        result.append ( this.copyrightMarker );
        result.append ( ", copyrightText: " );
        result.append ( this.copyrightText );
        result.append ( ')' );
        return result.toString ();
    }

} //BookImpl
