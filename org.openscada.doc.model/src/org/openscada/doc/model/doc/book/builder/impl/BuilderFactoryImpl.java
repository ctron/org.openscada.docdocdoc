/**
 */
package org.openscada.doc.model.doc.book.builder.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openscada.doc.model.doc.book.builder.BookBuilder;
import org.openscada.doc.model.doc.book.builder.BuilderFactory;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BuilderFactoryImpl extends EFactoryImpl implements BuilderFactory
{
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static BuilderFactory init ()
    {
        try
        {
            final BuilderFactory theBuilderFactory = (BuilderFactory)EPackage.Registry.INSTANCE.getEFactory ( "urn:openscada:doc:book:builder" );
            if ( theBuilderFactory != null )
            {
                return theBuilderFactory;
            }
        }
        catch ( final Exception exception )
        {
            EcorePlugin.INSTANCE.log ( exception );
        }
        return new BuilderFactoryImpl ();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BuilderFactoryImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create ( final EClass eClass )
    {
        switch ( eClass.getClassifierID () )
        {
            case BuilderPackage.BOOK_BUILDER:
                return createBookBuilder ();
            case BuilderPackage.PROPERTY_ENTRY:
                return createPropertyEntry ();
            default:
                throw new IllegalArgumentException ( "The class '" + eClass.getName () + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BookBuilder createBookBuilder ()
    {
        final BookBuilderImpl bookBuilder = new BookBuilderImpl ();
        return bookBuilder;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public PropertyEntry createPropertyEntry ()
    {
        final PropertyEntryImpl propertyEntry = new PropertyEntryImpl ();
        return propertyEntry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BuilderPackage getBuilderPackage ()
    {
        return (BuilderPackage)getEPackage ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static BuilderPackage getPackage ()
    {
        return BuilderPackage.eINSTANCE;
    }

} //BuilderFactoryImpl
