/**
 */
package org.openscada.doc.model.doc;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.openscada.doc.model.doc.DocPackage
 * @generated
 */
public interface DocFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    DocFactory eINSTANCE = org.openscada.doc.model.doc.impl.DocFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Test</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Test</em>'.
     * @generated
     */
    Test createTest ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    DocPackage getDocPackage ();

} //DocFactory
