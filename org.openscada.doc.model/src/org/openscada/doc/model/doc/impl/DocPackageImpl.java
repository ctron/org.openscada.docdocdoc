/**
 */
package org.openscada.doc.model.doc.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.model.doc.DocFactory;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.Test;
import org.openscada.doc.model.doc.book.BookPackage;
import org.openscada.doc.model.doc.book.builder.BuilderPackage;
import org.openscada.doc.model.doc.book.builder.impl.BuilderPackageImpl;
import org.openscada.doc.model.doc.book.impl.BookPackageImpl;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.impl.FragmentPackageImpl;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.impl.MapPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class DocPackageImpl extends EPackageImpl implements DocPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass testEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum numberingStyleEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.model.doc.DocPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private DocPackageImpl ()
    {
        super ( eNS_URI, DocFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link DocPackage#eINSTANCE} when that
     * field is accessed. Clients should not invoke it directly. Instead, they
     * should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static DocPackage init ()
    {
        if ( isInited )
        {
            return (DocPackage)EPackage.Registry.INSTANCE.getEPackage ( DocPackage.eNS_URI );
        }

        // Obtain or create and register package
        final DocPackageImpl theDocPackage = (DocPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof DocPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new DocPackageImpl () );

        isInited = true;

        // Obtain or create and register interdependencies
        final MapPackageImpl theMapPackage = (MapPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) instanceof MapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI ) : MapPackage.eINSTANCE );
        final FragmentPackageImpl theFragmentPackage = (FragmentPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) instanceof FragmentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI ) : FragmentPackage.eINSTANCE );
        final BookPackageImpl theBookPackage = (BookPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) instanceof BookPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI ) : BookPackage.eINSTANCE );
        final BuilderPackageImpl theBuilderPackage = (BuilderPackageImpl) ( EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) instanceof BuilderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage ( BuilderPackage.eNS_URI ) : BuilderPackage.eINSTANCE );

        // Create package meta-data objects
        theDocPackage.createPackageContents ();
        theMapPackage.createPackageContents ();
        theFragmentPackage.createPackageContents ();
        theBookPackage.createPackageContents ();
        theBuilderPackage.createPackageContents ();

        // Initialize created meta-data
        theDocPackage.initializePackageContents ();
        theMapPackage.initializePackageContents ();
        theFragmentPackage.initializePackageContents ();
        theBookPackage.initializePackageContents ();
        theBuilderPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theDocPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( DocPackage.eNS_URI, theDocPackage );
        return theDocPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTest ()
    {
        return this.testEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getNumberingStyle ()
    {
        return this.numberingStyleEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public DocFactory getDocFactory ()
    {
        return (DocFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents ()
    {
        if ( this.isCreated )
        {
            return;
        }
        this.isCreated = true;

        // Create classes and their features
        this.testEClass = createEClass ( TEST );

        // Create enums
        this.numberingStyleEEnum = createEEnum ( NUMBERING_STYLE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( this.isInitialized )
        {
            return;
        }
        this.isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Obtain other dependent packages
        final MapPackage theMapPackage = (MapPackage)EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI );
        final FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );
        final BookPackage theBookPackage = (BookPackage)EPackage.Registry.INSTANCE.getEPackage ( BookPackage.eNS_URI );

        // Add subpackages
        getESubpackages ().add ( theMapPackage );
        getESubpackages ().add ( theFragmentPackage );
        getESubpackages ().add ( theBookPackage );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes

        // Initialize classes and features; add operations and parameters
        initEClass ( this.testEClass, Test.class, "Test", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        // Initialize enums and add enum literals
        initEEnum ( this.numberingStyleEEnum, NumberingStyle.class, "NumberingStyle" );
        addEEnumLiteral ( this.numberingStyleEEnum, NumberingStyle.ARABIC );
        addEEnumLiteral ( this.numberingStyleEEnum, NumberingStyle.ROMAN );
        addEEnumLiteral ( this.numberingStyleEEnum, NumberingStyle.LATIN );

        // Create resource
        createResource ( eNS_URI );
    }

} //DocPackageImpl
