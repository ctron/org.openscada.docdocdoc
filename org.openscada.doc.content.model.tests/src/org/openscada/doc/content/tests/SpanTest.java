/**
 */
package org.openscada.doc.content.tests;

import org.openscada.doc.content.Span;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Span</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SpanTest extends TextContainerTest
{

    /**
     * Constructs a new Span test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SpanTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Span test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Span getFixture ()
    {
        return (Span)fixture;
    }

} //SpanTest
