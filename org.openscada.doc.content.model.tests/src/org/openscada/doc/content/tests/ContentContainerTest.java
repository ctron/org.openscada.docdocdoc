/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.tests;

import junit.framework.TestCase;

import org.openscada.doc.content.ContentContainer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getP() <em>P</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getCode() <em>Code</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getPlainText() <em>Plain Text</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getUl() <em>Ul</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getSimpleTable() <em>Simple Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getStructureTable() <em>Structure Table</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class ContentContainerTest extends TestCase
{

    /**
     * The fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ContentContainer fixture = null;

    /**
     * Constructs a new Container test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContentContainerTest ( String name )
    {
        super(name);
    }

    /**
     * Sets the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( ContentContainer fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ContentContainer getFixture ()
    {
        return fixture;
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getP() <em>P</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getP()
     * @generated
     */
    public void testGetP ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getCode() <em>Code</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getCode()
     * @generated
     */
    public void testGetCode ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getPlainText() <em>Plain Text</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getPlainText()
     * @generated
     */
    public void testGetPlainText ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getUl() <em>Ul</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getUl()
     * @generated
     */
    public void testGetUl ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getSimpleTable() <em>Simple Table</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getSimpleTable()
     * @generated
     */
    public void testGetSimpleTable ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getStructureTable() <em>Structure Table</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getStructureTable()
     * @generated
     */
    public void testGetStructureTable()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

} //ContentContainerTest
