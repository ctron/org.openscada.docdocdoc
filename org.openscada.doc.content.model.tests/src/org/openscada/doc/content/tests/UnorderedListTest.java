/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.UnorderedList;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Unordered List</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UnorderedListTest extends ListTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(UnorderedListTest.class);
    }

    /**
     * Constructs a new Unordered List test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UnorderedListTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Unordered List test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected UnorderedList getFixture ()
    {
        return (UnorderedList)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createUnorderedList());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //UnorderedListTest
