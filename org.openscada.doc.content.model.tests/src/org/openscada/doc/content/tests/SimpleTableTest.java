/**
 */
package org.openscada.doc.content.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.SimpleTable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple Table</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleTableTest extends TestCase
{

    /**
     * The fixture for this Simple Table test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected SimpleTable fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(SimpleTableTest.class);
    }

    /**
     * Constructs a new Simple Table test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SimpleTableTest ( String name )
    {
        super(name);
    }

    /**
     * Sets the fixture for this Simple Table test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( SimpleTable fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Simple Table test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected SimpleTable getFixture ()
    {
        return fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createSimpleTable());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //SimpleTableTest
