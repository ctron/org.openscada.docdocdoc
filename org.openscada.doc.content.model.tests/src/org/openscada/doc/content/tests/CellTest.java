/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.Cell;
import org.openscada.doc.content.ContentFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cell</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CellTest extends TextContainerTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(CellTest.class);
    }

    /**
     * Constructs a new Cell test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CellTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Cell test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Cell getFixture ()
    {
        return (Cell)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createCell());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //CellTest
