/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.StructureEntryDescription;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Structure Entry Description</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructureEntryDescriptionTest extends PlainTextContainerTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main(String[] args)
    {
        TestRunner.run(StructureEntryDescriptionTest.class);
    }

    /**
     * Constructs a new Structure Entry Description test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntryDescriptionTest(String name)
    {
        super(name);
    }

    /**
     * Returns the fixture for this Structure Entry Description test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected StructureEntryDescription getFixture()
    {
        return (StructureEntryDescription)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createStructureEntryDescription());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception
    {
        setFixture(null);
    }

} //StructureEntryDescriptionTest
