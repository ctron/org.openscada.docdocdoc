/**
 */
package org.openscada.doc.content.tests;

import junit.framework.TestCase;

import org.openscada.doc.content.List;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ListTest extends TestCase
{

    /**
     * The fixture for this List test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected List fixture = null;

    /**
     * Constructs a new List test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ListTest ( String name )
    {
        super(name);
    }

    /**
     * Sets the fixture for this List test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( List fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this List test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected List getFixture ()
    {
        return fixture;
    }

} //ListTest
