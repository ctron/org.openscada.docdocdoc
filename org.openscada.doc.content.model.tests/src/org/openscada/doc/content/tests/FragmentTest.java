/**
 */
package org.openscada.doc.content.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.Fragment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getP() <em>P</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getCode() <em>Code</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getPlainText() <em>Plain Text</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getUl() <em>Ul</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getSimpleTable() <em>Simple Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getStructureTable() <em>Structure Table</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class FragmentTest extends TestCase
{

    /**
     * The fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Fragment fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(FragmentTest.class);
    }

    /**
     * Constructs a new Fragment test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public FragmentTest ( String name )
    {
        super(name);
    }

    /**
     * Sets the fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( Fragment fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Fragment getFixture ()
    {
        return fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createFragment());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getP() <em>P</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getP()
     * @generated
     */
    public void testGetP ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getCode() <em>Code</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getCode()
     * @generated
     */
    public void testGetCode ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getPlainText() <em>Plain Text</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getPlainText()
     * @generated
     */
    public void testGetPlainText ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getUl() <em>Ul</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getUl()
     * @generated
     */
    public void testGetUl ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getSimpleTable() <em>Simple Table</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getSimpleTable()
     * @generated
     */
    public void testGetSimpleTable ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.ContentContainer#getStructureTable() <em>Structure Table</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer#getStructureTable()
     * @generated
     */
    public void testGetStructureTable()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

} //FragmentTest
