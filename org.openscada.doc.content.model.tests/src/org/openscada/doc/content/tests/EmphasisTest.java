/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.Emphasis;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Emphasis</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmphasisTest extends SpanTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(EmphasisTest.class);
    }

    /**
     * Constructs a new Emphasis test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EmphasisTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Emphasis test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Emphasis getFixture ()
    {
        return (Emphasis)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createEmphasis());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //EmphasisTest
