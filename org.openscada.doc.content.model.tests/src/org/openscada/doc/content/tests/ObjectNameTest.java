/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.ObjectName;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Object Name</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectNameTest extends PlainTextContainerTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(ObjectNameTest.class);
    }

    /**
     * Constructs a new Object Name test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ObjectNameTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Object Name test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected ObjectName getFixture ()
    {
        return (ObjectName)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createObjectName());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //ObjectNameTest
