/**
 */
package org.openscada.doc.content.tests;

import junit.framework.TestCase;

import org.openscada.doc.content.PlainTextContainer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Plain Text Container</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class PlainTextContainerTest extends TestCase
{

    /**
     * The fixture for this Plain Text Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PlainTextContainer fixture = null;

    /**
     * Constructs a new Plain Text Container test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PlainTextContainerTest ( String name )
    {
        super(name);
    }

    /**
     * Sets the fixture for this Plain Text Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( PlainTextContainer fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Plain Text Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PlainTextContainer getFixture ()
    {
        return fixture;
    }

} //PlainTextContainerTest
