/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.ContentFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Code Paragraph</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CodeParagraphTest extends PlainTextContainerTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(CodeParagraphTest.class);
    }

    /**
     * Constructs a new Code Paragraph test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CodeParagraphTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Code Paragraph test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected CodeParagraph getFixture ()
    {
        return (CodeParagraph)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createCodeParagraph());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //CodeParagraphTest
