/**
 */
package org.openscada.doc.content.tests;

import junit.textui.TestRunner;

import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.Quote;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Quote</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class QuoteTest extends SpanTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(QuoteTest.class);
    }

    /**
     * Constructs a new Quote test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public QuoteTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Quote test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Quote getFixture ()
    {
        return (Quote)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture(ContentFactory.eINSTANCE.createQuote());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture(null);
    }

} //QuoteTest
