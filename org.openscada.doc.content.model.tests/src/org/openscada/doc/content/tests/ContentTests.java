/**
 */
package org.openscada.doc.content.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>content</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ContentTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run(suite());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Test suite ()
    {
        TestSuite suite = new ContentTests("content Tests");
        suite.addTestSuite(ParagraphTest.class);
        suite.addTestSuite(EmphasisTest.class);
        suite.addTestSuite(QuoteTest.class);
        suite.addTestSuite(ListItemTest.class);
        suite.addTestSuite(CellTest.class);
        suite.addTestSuite(SectionTest.class);
        suite.addTestSuite(FragmentTest.class);
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContentTests ( String name )
    {
        super(name);
    }

} //ContentTests
