/**
 */
package org.openscada.doc.content.tests;

import org.openscada.doc.content.TextContainer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Text Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.openscada.doc.content.TextContainer#getGroupContent() <em>Group Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getEmphasis() <em>Emphasis</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getImage() <em>Image</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getObjectName() <em>Object Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getXref() <em>Xref</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getLink() <em>Link</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getQuote() <em>Quote</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class TextContainerTest extends PlainTextContainerTest
{

    /**
     * Constructs a new Text Container test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public TextContainerTest ( String name )
    {
        super(name);
    }

    /**
     * Returns the fixture for this Text Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected TextContainer getFixture ()
    {
        return (TextContainer)fixture;
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getGroupContent() <em>Group Content</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getGroupContent()
     * @generated
     */
    public void testGetGroupContent ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getEmphasis() <em>Emphasis</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getEmphasis()
     * @generated
     */
    public void testGetEmphasis ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getImage() <em>Image</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getImage()
     * @generated
     */
    public void testGetImage ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getObjectName() <em>Object Name</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getObjectName()
     * @generated
     */
    public void testGetObjectName ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getXref() <em>Xref</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getXref()
     * @generated
     */
    public void testGetXref ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getLink() <em>Link</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getLink()
     * @generated
     */
    public void testGetLink ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link org.openscada.doc.content.TextContainer#getQuote() <em>Quote</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.TextContainer#getQuote()
     * @generated
     */
    public void testGetQuote ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

} //TextContainerTest
