/**
 */
package org.openscada.doc.content.properties.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.content.properties.DataType;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesDefinition;
import org.openscada.doc.content.properties.PropertiesDefinitionGenerator;
import org.openscada.doc.content.properties.PropertiesFactory;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.PropertiesPackage;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.map.MapPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertiesPackageImpl extends EPackageImpl implements PropertiesPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass propertiesEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass propertyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass propertiesGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass propertiesDefinitionGeneratorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass propertiesDefinitionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum dataTypeEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.content.properties.PropertiesPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private PropertiesPackageImpl ()
    {
        super ( eNS_URI, PropertiesFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link PropertiesPackage#eINSTANCE}
     * when that field is accessed. Clients should not invoke it directly.
     * Instead, they should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static PropertiesPackage init ()
    {
        if ( isInited )
            return (PropertiesPackage)EPackage.Registry.INSTANCE.getEPackage ( PropertiesPackage.eNS_URI );

        // Obtain or create and register package
        PropertiesPackageImpl thePropertiesPackage = (PropertiesPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof PropertiesPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new PropertiesPackageImpl () );

        isInited = true;

        // Initialize simple dependencies
        DocPackage.eINSTANCE.eClass ();

        // Create package meta-data objects
        thePropertiesPackage.createPackageContents ();

        // Initialize created meta-data
        thePropertiesPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        thePropertiesPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( PropertiesPackage.eNS_URI, thePropertiesPackage );
        return thePropertiesPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getProperties ()
    {
        return propertiesEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProperties_Properties ()
    {
        return (EReference)propertiesEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProperties_Groups ()
    {
        return (EReference)propertiesEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getProperty ()
    {
        return propertyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_Name ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_DefaultValue ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_DataType ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_CustomDataTypeDescription ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_ShortDescription ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProperty_LongDescription ()
    {
        return (EAttribute)propertyEClass.getEStructuralFeatures ().get ( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPropertiesGroup ()
    {
        return propertiesGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPropertiesGroup_Prefix ()
    {
        return (EAttribute)propertiesGroupEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPropertiesGroup_Properties ()
    {
        return (EReference)propertiesGroupEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPropertiesDefinitionGenerator ()
    {
        return propertiesDefinitionGeneratorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPropertiesDefinition ()
    {
        return propertiesDefinitionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPropertiesDefinition_Properties ()
    {
        return (EReference)propertiesDefinitionEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getDataType ()
    {
        return dataTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PropertiesFactory getPropertiesFactory ()
    {
        return (PropertiesFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents ()
    {
        if ( isCreated )
            return;
        isCreated = true;

        // Create classes and their features
        propertiesEClass = createEClass ( PROPERTIES );
        createEReference ( propertiesEClass, PROPERTIES__PROPERTIES );
        createEReference ( propertiesEClass, PROPERTIES__GROUPS );

        propertyEClass = createEClass ( PROPERTY );
        createEAttribute ( propertyEClass, PROPERTY__NAME );
        createEAttribute ( propertyEClass, PROPERTY__DEFAULT_VALUE );
        createEAttribute ( propertyEClass, PROPERTY__DATA_TYPE );
        createEAttribute ( propertyEClass, PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION );
        createEAttribute ( propertyEClass, PROPERTY__SHORT_DESCRIPTION );
        createEAttribute ( propertyEClass, PROPERTY__LONG_DESCRIPTION );

        propertiesGroupEClass = createEClass ( PROPERTIES_GROUP );
        createEAttribute ( propertiesGroupEClass, PROPERTIES_GROUP__PREFIX );
        createEReference ( propertiesGroupEClass, PROPERTIES_GROUP__PROPERTIES );

        propertiesDefinitionGeneratorEClass = createEClass ( PROPERTIES_DEFINITION_GENERATOR );

        propertiesDefinitionEClass = createEClass ( PROPERTIES_DEFINITION );
        createEReference ( propertiesDefinitionEClass, PROPERTIES_DEFINITION__PROPERTIES );

        // Create enums
        dataTypeEEnum = createEEnum ( DATA_TYPE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( isInitialized )
            return;
        isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Obtain other dependent packages
        FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage ( FragmentPackage.eNS_URI );
        MapPackage theMapPackage = (MapPackage)EPackage.Registry.INSTANCE.getEPackage ( MapPackage.eNS_URI );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        propertiesEClass.getESuperTypes ().add ( theFragmentPackage.getContent () );
        propertiesDefinitionGeneratorEClass.getESuperTypes ().add ( theMapPackage.getContentGenerator () );
        propertiesDefinitionEClass.getESuperTypes ().add ( theFragmentPackage.getContent () );

        // Initialize classes and features; add operations and parameters
        initEClass ( propertiesEClass, Properties.class, "Properties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getProperties_Properties (), this.getProperty (), null, "properties", null, 0, -1, Properties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getProperties_Groups (), this.getPropertiesGroup (), null, "groups", null, 0, -1, Properties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getProperty_Name (), ecorePackage.getEString (), "name", null, 1, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getProperty_DefaultValue (), ecorePackage.getEString (), "defaultValue", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getProperty_DataType (), this.getDataType (), "dataType", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getProperty_CustomDataTypeDescription (), ecorePackage.getEString (), "customDataTypeDescription", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getProperty_ShortDescription (), ecorePackage.getEString (), "shortDescription", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getProperty_LongDescription (), ecorePackage.getEString (), "longDescription", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( propertiesGroupEClass, PropertiesGroup.class, "PropertiesGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getPropertiesGroup_Prefix (), ecorePackage.getEString (), "prefix", null, 1, 1, PropertiesGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEReference ( getPropertiesGroup_Properties (), this.getProperty (), null, "properties", null, 0, -1, PropertiesGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        initEClass ( propertiesDefinitionGeneratorEClass, PropertiesDefinitionGenerator.class, "PropertiesDefinitionGenerator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );

        initEClass ( propertiesDefinitionEClass, PropertiesDefinition.class, "PropertiesDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEReference ( getPropertiesDefinition_Properties (), this.getProperty (), null, "properties", null, 0, -1, PropertiesDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        // Initialize enums and add enum literals
        initEEnum ( dataTypeEEnum, DataType.class, "DataType" );
        addEEnumLiteral ( dataTypeEEnum, DataType.STRING );
        addEEnumLiteral ( dataTypeEEnum, DataType.INTEGER );
        addEEnumLiteral ( dataTypeEEnum, DataType.LONG );
        addEEnumLiteral ( dataTypeEEnum, DataType.BOOLEAN );
        addEEnumLiteral ( dataTypeEEnum, DataType.CUSTOM );

        // Create resource
        createResource ( eNS_URI );

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations ();
    }

    /**
     * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void createExtendedMetaDataAnnotations ()
    {
        String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
        addAnnotation ( propertiesEClass, source, new String[] { "name", "properties" } );
        addAnnotation ( getProperties_Properties (), source, new String[] { "name", "properties", "namespace", "##targetNamespace" } );
        addAnnotation ( getProperties_Groups (), source, new String[] { "name", "groups", "namespace", "##targetNamespace" } );
        addAnnotation ( propertyEClass, source, new String[] { "name", "property" } );
        addAnnotation ( getProperty_Name (), source, new String[] { "namespace", "##targetNamespace" } );
        addAnnotation ( getProperty_DefaultValue (), source, new String[] { "kind", "element", "namespace", "##targetNamespace" } );
        addAnnotation ( getProperty_DataType (), source, new String[] { "namespace", "##targetNamespace" } );
        addAnnotation ( getProperty_CustomDataTypeDescription (), source, new String[] { "kind", "element", "namespace", "##targetNamespace" } );
        addAnnotation ( getProperty_ShortDescription (), source, new String[] { "namespace", "##targetNamespace" } );
        addAnnotation ( getProperty_LongDescription (), source, new String[] { "kind", "element", "namespace", "##targetNamespace" } );
        addAnnotation ( dataTypeEEnum, source, new String[] { "name", "dataType" } );
        addAnnotation ( propertiesGroupEClass, source, new String[] { "name", "group" } );
        addAnnotation ( getPropertiesGroup_Prefix (), source, new String[] { "namespace", "##targetNamespace" } );
        addAnnotation ( getPropertiesGroup_Properties (), source, new String[] { "name", "properties", "namespace", "##targetNamespace" } );
    }

} //PropertiesPackageImpl
