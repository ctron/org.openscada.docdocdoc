/**
 */
package org.openscada.doc.content.properties.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.properties.PropertiesDefinitionGenerator;
import org.openscada.doc.content.properties.PropertiesPackage;
import org.openscada.doc.model.doc.map.impl.ContentGeneratorImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PropertiesDefinitionGeneratorImpl extends ContentGeneratorImpl implements PropertiesDefinitionGenerator
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertiesDefinitionGeneratorImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return PropertiesPackage.Literals.PROPERTIES_DEFINITION_GENERATOR;
    }

} //PropertiesDefinitionGeneratorImpl
