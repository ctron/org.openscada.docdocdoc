/**
 */
package org.openscada.doc.content.properties.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.PropertiesPackage;
import org.openscada.doc.content.properties.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertiesImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertiesImpl#getGroups <em>Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertiesImpl extends EObjectImpl implements Properties
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getProperties()
     * @generated
     * @ordered
     */
    protected EList<Property> properties;

    /**
     * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getGroups()
     * @generated
     * @ordered
     */
    protected EList<PropertiesGroup> groups;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertiesImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return PropertiesPackage.Literals.PROPERTIES;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<Property> getProperties ()
    {
        if ( properties == null )
        {
            properties = new EObjectContainmentEList<Property> ( Property.class, this, PropertiesPackage.PROPERTIES__PROPERTIES );
        }
        return properties;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<PropertiesGroup> getGroups ()
    {
        if ( groups == null )
        {
            groups = new EObjectContainmentEList<PropertiesGroup> ( PropertiesGroup.class, this, PropertiesPackage.PROPERTIES__GROUPS );
        }
        return groups;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTIES__PROPERTIES:
                return ( (InternalEList<?>)getProperties () ).basicRemove ( otherEnd, msgs );
            case PropertiesPackage.PROPERTIES__GROUPS:
                return ( (InternalEList<?>)getGroups () ).basicRemove ( otherEnd, msgs );
        }
        return super.eInverseRemove ( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTIES__PROPERTIES:
                return getProperties ();
            case PropertiesPackage.PROPERTIES__GROUPS:
                return getGroups ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTIES__PROPERTIES:
                getProperties ().clear ();
                getProperties ().addAll ( (Collection<? extends Property>)newValue );
                return;
            case PropertiesPackage.PROPERTIES__GROUPS:
                getGroups ().clear ();
                getGroups ().addAll ( (Collection<? extends PropertiesGroup>)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTIES__PROPERTIES:
                getProperties ().clear ();
                return;
            case PropertiesPackage.PROPERTIES__GROUPS:
                getGroups ().clear ();
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTIES__PROPERTIES:
                return properties != null && !properties.isEmpty ();
            case PropertiesPackage.PROPERTIES__GROUPS:
                return groups != null && !groups.isEmpty ();
        }
        return super.eIsSet ( featureID );
    }

} //PropertiesImpl
