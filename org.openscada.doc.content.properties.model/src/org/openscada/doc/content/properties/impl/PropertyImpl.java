/**
 */
package org.openscada.doc.content.properties.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.doc.content.properties.DataType;
import org.openscada.doc.content.properties.PropertiesPackage;
import org.openscada.doc.content.properties.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getCustomDataTypeDescription <em>Custom Data Type Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getShortDescription <em>Short Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.impl.PropertyImpl#getLongDescription <em>Long Description</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyImpl extends EObjectImpl implements Property
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDefaultValue()
     * @generated
     * @ordered
     */
    protected static final String DEFAULT_VALUE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDefaultValue()
     * @generated
     * @ordered
     */
    protected String defaultValue = DEFAULT_VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDataType()
     * @generated
     * @ordered
     */
    protected static final DataType DATA_TYPE_EDEFAULT = DataType.STRING;

    /**
     * The cached value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDataType()
     * @generated
     * @ordered
     */
    protected DataType dataType = DATA_TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getCustomDataTypeDescription() <em>Custom Data Type Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCustomDataTypeDescription()
     * @generated
     * @ordered
     */
    protected static final String CUSTOM_DATA_TYPE_DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCustomDataTypeDescription() <em>Custom Data Type Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCustomDataTypeDescription()
     * @generated
     * @ordered
     */
    protected String customDataTypeDescription = CUSTOM_DATA_TYPE_DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getShortDescription() <em>Short Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getShortDescription()
     * @generated
     * @ordered
     */
    protected static final String SHORT_DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getShortDescription() <em>Short Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getShortDescription()
     * @generated
     * @ordered
     */
    protected String shortDescription = SHORT_DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getLongDescription() <em>Long Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLongDescription()
     * @generated
     * @ordered
     */
    protected static final String LONG_DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getLongDescription() <em>Long Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLongDescription()
     * @generated
     * @ordered
     */
    protected String longDescription = LONG_DESCRIPTION_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertyImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return PropertiesPackage.Literals.PROPERTY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName ()
    {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setName ( String newName )
    {
        String oldName = name;
        name = newName;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__NAME, oldName, name ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getDefaultValue ()
    {
        return defaultValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDefaultValue ( String newDefaultValue )
    {
        String oldDefaultValue = defaultValue;
        defaultValue = newDefaultValue;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__DEFAULT_VALUE, oldDefaultValue, defaultValue ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DataType getDataType ()
    {
        return dataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDataType ( DataType newDataType )
    {
        DataType oldDataType = dataType;
        dataType = newDataType == null ? DATA_TYPE_EDEFAULT : newDataType;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__DATA_TYPE, oldDataType, dataType ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getCustomDataTypeDescription ()
    {
        return customDataTypeDescription;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setCustomDataTypeDescription ( String newCustomDataTypeDescription )
    {
        String oldCustomDataTypeDescription = customDataTypeDescription;
        customDataTypeDescription = newCustomDataTypeDescription;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION, oldCustomDataTypeDescription, customDataTypeDescription ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getShortDescription ()
    {
        return shortDescription;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setShortDescription ( String newShortDescription )
    {
        String oldShortDescription = shortDescription;
        shortDescription = newShortDescription;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__SHORT_DESCRIPTION, oldShortDescription, shortDescription ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getLongDescription ()
    {
        return longDescription;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setLongDescription ( String newLongDescription )
    {
        String oldLongDescription = longDescription;
        longDescription = newLongDescription;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, PropertiesPackage.PROPERTY__LONG_DESCRIPTION, oldLongDescription, longDescription ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTY__NAME:
                return getName ();
            case PropertiesPackage.PROPERTY__DEFAULT_VALUE:
                return getDefaultValue ();
            case PropertiesPackage.PROPERTY__DATA_TYPE:
                return getDataType ();
            case PropertiesPackage.PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION:
                return getCustomDataTypeDescription ();
            case PropertiesPackage.PROPERTY__SHORT_DESCRIPTION:
                return getShortDescription ();
            case PropertiesPackage.PROPERTY__LONG_DESCRIPTION:
                return getLongDescription ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTY__NAME:
                setName ( (String)newValue );
                return;
            case PropertiesPackage.PROPERTY__DEFAULT_VALUE:
                setDefaultValue ( (String)newValue );
                return;
            case PropertiesPackage.PROPERTY__DATA_TYPE:
                setDataType ( (DataType)newValue );
                return;
            case PropertiesPackage.PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION:
                setCustomDataTypeDescription ( (String)newValue );
                return;
            case PropertiesPackage.PROPERTY__SHORT_DESCRIPTION:
                setShortDescription ( (String)newValue );
                return;
            case PropertiesPackage.PROPERTY__LONG_DESCRIPTION:
                setLongDescription ( (String)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTY__NAME:
                setName ( NAME_EDEFAULT );
                return;
            case PropertiesPackage.PROPERTY__DEFAULT_VALUE:
                setDefaultValue ( DEFAULT_VALUE_EDEFAULT );
                return;
            case PropertiesPackage.PROPERTY__DATA_TYPE:
                setDataType ( DATA_TYPE_EDEFAULT );
                return;
            case PropertiesPackage.PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION:
                setCustomDataTypeDescription ( CUSTOM_DATA_TYPE_DESCRIPTION_EDEFAULT );
                return;
            case PropertiesPackage.PROPERTY__SHORT_DESCRIPTION:
                setShortDescription ( SHORT_DESCRIPTION_EDEFAULT );
                return;
            case PropertiesPackage.PROPERTY__LONG_DESCRIPTION:
                setLongDescription ( LONG_DESCRIPTION_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch ( featureID )
        {
            case PropertiesPackage.PROPERTY__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals ( name );
            case PropertiesPackage.PROPERTY__DEFAULT_VALUE:
                return DEFAULT_VALUE_EDEFAULT == null ? defaultValue != null : !DEFAULT_VALUE_EDEFAULT.equals ( defaultValue );
            case PropertiesPackage.PROPERTY__DATA_TYPE:
                return dataType != DATA_TYPE_EDEFAULT;
            case PropertiesPackage.PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION:
                return CUSTOM_DATA_TYPE_DESCRIPTION_EDEFAULT == null ? customDataTypeDescription != null : !CUSTOM_DATA_TYPE_DESCRIPTION_EDEFAULT.equals ( customDataTypeDescription );
            case PropertiesPackage.PROPERTY__SHORT_DESCRIPTION:
                return SHORT_DESCRIPTION_EDEFAULT == null ? shortDescription != null : !SHORT_DESCRIPTION_EDEFAULT.equals ( shortDescription );
            case PropertiesPackage.PROPERTY__LONG_DESCRIPTION:
                return LONG_DESCRIPTION_EDEFAULT == null ? longDescription != null : !LONG_DESCRIPTION_EDEFAULT.equals ( longDescription );
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
            return super.toString ();

        StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (name: " );
        result.append ( name );
        result.append ( ", defaultValue: " );
        result.append ( defaultValue );
        result.append ( ", dataType: " );
        result.append ( dataType );
        result.append ( ", customDataTypeDescription: " );
        result.append ( customDataTypeDescription );
        result.append ( ", shortDescription: " );
        result.append ( shortDescription );
        result.append ( ", longDescription: " );
        result.append ( longDescription );
        result.append ( ')' );
        return result.toString ();
    }

} //PropertyImpl
