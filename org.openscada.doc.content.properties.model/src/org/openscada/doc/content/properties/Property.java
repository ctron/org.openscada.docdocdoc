/**
 */
package org.openscada.doc.content.properties;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.properties.Property#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Property#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Property#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Property#getCustomDataTypeDescription <em>Custom Data Type Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Property#getShortDescription <em>Short Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Property#getLongDescription <em>Long Description</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty()
 * @model extendedMetaData="name='property'"
 * @generated
 */
public interface Property extends EObject
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_Name()
     * @model id="true" required="true"
     *        extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    String getName ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName ( String value );

    /**
     * Returns the value of the '<em><b>Default Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Default Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Default Value</em>' attribute.
     * @see #setDefaultValue(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_DefaultValue()
     * @model extendedMetaData="kind='element' namespace='##targetNamespace'"
     * @generated
     */
    String getDefaultValue ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getDefaultValue <em>Default Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default Value</em>' attribute.
     * @see #getDefaultValue()
     * @generated
     */
    void setDefaultValue ( String value );

    /**
     * Returns the value of the '<em><b>Data Type</b></em>' attribute.
     * The literals are from the enumeration {@link org.openscada.doc.content.properties.DataType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Data Type</em>' attribute.
     * @see org.openscada.doc.content.properties.DataType
     * @see #setDataType(DataType)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_DataType()
     * @model extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    DataType getDataType ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getDataType <em>Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Data Type</em>' attribute.
     * @see org.openscada.doc.content.properties.DataType
     * @see #getDataType()
     * @generated
     */
    void setDataType ( DataType value );

    /**
     * Returns the value of the '<em><b>Custom Data Type Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Custom Data Type Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Custom Data Type Description</em>' attribute.
     * @see #setCustomDataTypeDescription(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_CustomDataTypeDescription()
     * @model extendedMetaData="kind='element' namespace='##targetNamespace'"
     * @generated
     */
    String getCustomDataTypeDescription ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getCustomDataTypeDescription <em>Custom Data Type Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Custom Data Type Description</em>' attribute.
     * @see #getCustomDataTypeDescription()
     * @generated
     */
    void setCustomDataTypeDescription ( String value );

    /**
     * Returns the value of the '<em><b>Short Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Short Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Short Description</em>' attribute.
     * @see #setShortDescription(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_ShortDescription()
     * @model extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    String getShortDescription ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getShortDescription <em>Short Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Short Description</em>' attribute.
     * @see #getShortDescription()
     * @generated
     */
    void setShortDescription ( String value );

    /**
     * Returns the value of the '<em><b>Long Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Long Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Long Description</em>' attribute.
     * @see #setLongDescription(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperty_LongDescription()
     * @model extendedMetaData="kind='element' namespace='##targetNamespace'"
     * @generated
     */
    String getLongDescription ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.Property#getLongDescription <em>Long Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Long Description</em>' attribute.
     * @see #getLongDescription()
     * @generated
     */
    void setLongDescription ( String value );

} // Property
