/**
 */
package org.openscada.doc.content.properties;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.map.MapPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * A model to document properties (e.g. system properties).
 * <!-- end-model-doc -->
 * @see org.openscada.doc.content.properties.PropertiesFactory
 * @model kind="package"
 * @generated
 */
public interface PropertiesPackage extends EPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "properties";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:content:properties";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "properties";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    PropertiesPackage eINSTANCE = org.openscada.doc.content.properties.impl.PropertiesPackageImpl.init ();

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.impl.PropertiesImpl <em>Properties</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.impl.PropertiesImpl
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getProperties()
     * @generated
     */
    int PROPERTIES = 0;

    /**
     * The feature id for the '<em><b>Properties</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES__PROPERTIES = FragmentPackage.CONTENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Groups</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES__GROUPS = FragmentPackage.CONTENT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Properties</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_FEATURE_COUNT = FragmentPackage.CONTENT_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.impl.PropertyImpl <em>Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.impl.PropertyImpl
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getProperty()
     * @generated
     */
    int PROPERTY = 1;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__NAME = 0;

    /**
     * The feature id for the '<em><b>Default Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__DEFAULT_VALUE = 1;

    /**
     * The feature id for the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__DATA_TYPE = 2;

    /**
     * The feature id for the '<em><b>Custom Data Type Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION = 3;

    /**
     * The feature id for the '<em><b>Short Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__SHORT_DESCRIPTION = 4;

    /**
     * The feature id for the '<em><b>Long Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY__LONG_DESCRIPTION = 5;

    /**
     * The number of structural features of the '<em>Property</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTY_FEATURE_COUNT = 6;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.impl.PropertiesGroupImpl <em>Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.impl.PropertiesGroupImpl
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesGroup()
     * @generated
     */
    int PROPERTIES_GROUP = 2;

    /**
     * The feature id for the '<em><b>Prefix</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_GROUP__PREFIX = 0;

    /**
     * The feature id for the '<em><b>Properties</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_GROUP__PROPERTIES = 1;

    /**
     * The number of structural features of the '<em>Group</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_GROUP_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.impl.PropertiesDefinitionGeneratorImpl <em>Definition Generator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.impl.PropertiesDefinitionGeneratorImpl
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesDefinitionGenerator()
     * @generated
     */
    int PROPERTIES_DEFINITION_GENERATOR = 3;

    /**
     * The number of structural features of the '<em>Definition Generator</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_DEFINITION_GENERATOR_FEATURE_COUNT = MapPackage.CONTENT_GENERATOR_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.impl.PropertiesDefinitionImpl <em>Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.impl.PropertiesDefinitionImpl
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesDefinition()
     * @generated
     */
    int PROPERTIES_DEFINITION = 4;

    /**
     * The feature id for the '<em><b>Properties</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_DEFINITION__PROPERTIES = FragmentPackage.CONTENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Definition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PROPERTIES_DEFINITION_FEATURE_COUNT = FragmentPackage.CONTENT_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.properties.DataType <em>Data Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.properties.DataType
     * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getDataType()
     * @generated
     */
    int DATA_TYPE = 5;

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.properties.Properties <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Properties</em>'.
     * @see org.openscada.doc.content.properties.Properties
     * @generated
     */
    EClass getProperties ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.properties.Properties#getProperties <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Properties</em>'.
     * @see org.openscada.doc.content.properties.Properties#getProperties()
     * @see #getProperties()
     * @generated
     */
    EReference getProperties_Properties ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.properties.Properties#getGroups <em>Groups</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Groups</em>'.
     * @see org.openscada.doc.content.properties.Properties#getGroups()
     * @see #getProperties()
     * @generated
     */
    EReference getProperties_Groups ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.properties.Property <em>Property</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Property</em>'.
     * @see org.openscada.doc.content.properties.Property
     * @generated
     */
    EClass getProperty ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.openscada.doc.content.properties.Property#getName()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_Name ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getDefaultValue <em>Default Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Default Value</em>'.
     * @see org.openscada.doc.content.properties.Property#getDefaultValue()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_DefaultValue ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getDataType <em>Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Data Type</em>'.
     * @see org.openscada.doc.content.properties.Property#getDataType()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_DataType ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getCustomDataTypeDescription <em>Custom Data Type Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Custom Data Type Description</em>'.
     * @see org.openscada.doc.content.properties.Property#getCustomDataTypeDescription()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_CustomDataTypeDescription ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getShortDescription <em>Short Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Short Description</em>'.
     * @see org.openscada.doc.content.properties.Property#getShortDescription()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_ShortDescription ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.Property#getLongDescription <em>Long Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Long Description</em>'.
     * @see org.openscada.doc.content.properties.Property#getLongDescription()
     * @see #getProperty()
     * @generated
     */
    EAttribute getProperty_LongDescription ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.properties.PropertiesGroup <em>Group</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Group</em>'.
     * @see org.openscada.doc.content.properties.PropertiesGroup
     * @generated
     */
    EClass getPropertiesGroup ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.properties.PropertiesGroup#getPrefix <em>Prefix</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Prefix</em>'.
     * @see org.openscada.doc.content.properties.PropertiesGroup#getPrefix()
     * @see #getPropertiesGroup()
     * @generated
     */
    EAttribute getPropertiesGroup_Prefix ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.properties.PropertiesGroup#getProperties <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Properties</em>'.
     * @see org.openscada.doc.content.properties.PropertiesGroup#getProperties()
     * @see #getPropertiesGroup()
     * @generated
     */
    EReference getPropertiesGroup_Properties ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.properties.PropertiesDefinitionGenerator <em>Definition Generator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Definition Generator</em>'.
     * @see org.openscada.doc.content.properties.PropertiesDefinitionGenerator
     * @generated
     */
    EClass getPropertiesDefinitionGenerator ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.properties.PropertiesDefinition <em>Definition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Definition</em>'.
     * @see org.openscada.doc.content.properties.PropertiesDefinition
     * @generated
     */
    EClass getPropertiesDefinition ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.properties.PropertiesDefinition#getProperties <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Properties</em>'.
     * @see org.openscada.doc.content.properties.PropertiesDefinition#getProperties()
     * @see #getPropertiesDefinition()
     * @generated
     */
    EReference getPropertiesDefinition_Properties ();

    /**
     * Returns the meta object for enum '{@link org.openscada.doc.content.properties.DataType <em>Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Data Type</em>'.
     * @see org.openscada.doc.content.properties.DataType
     * @generated
     */
    EEnum getDataType ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    PropertiesFactory getPropertiesFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.impl.PropertiesImpl <em>Properties</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.impl.PropertiesImpl
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getProperties()
         * @generated
         */
        EClass PROPERTIES = eINSTANCE.getProperties ();

        /**
         * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PROPERTIES__PROPERTIES = eINSTANCE.getProperties_Properties ();

        /**
         * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PROPERTIES__GROUPS = eINSTANCE.getProperties_Groups ();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.impl.PropertyImpl <em>Property</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.impl.PropertyImpl
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getProperty()
         * @generated
         */
        EClass PROPERTY = eINSTANCE.getProperty ();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__NAME = eINSTANCE.getProperty_Name ();

        /**
         * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__DEFAULT_VALUE = eINSTANCE.getProperty_DefaultValue ();

        /**
         * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__DATA_TYPE = eINSTANCE.getProperty_DataType ();

        /**
         * The meta object literal for the '<em><b>Custom Data Type Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__CUSTOM_DATA_TYPE_DESCRIPTION = eINSTANCE.getProperty_CustomDataTypeDescription ();

        /**
         * The meta object literal for the '<em><b>Short Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__SHORT_DESCRIPTION = eINSTANCE.getProperty_ShortDescription ();

        /**
         * The meta object literal for the '<em><b>Long Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTY__LONG_DESCRIPTION = eINSTANCE.getProperty_LongDescription ();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.impl.PropertiesGroupImpl <em>Group</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.impl.PropertiesGroupImpl
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesGroup()
         * @generated
         */
        EClass PROPERTIES_GROUP = eINSTANCE.getPropertiesGroup ();

        /**
         * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PROPERTIES_GROUP__PREFIX = eINSTANCE.getPropertiesGroup_Prefix ();

        /**
         * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PROPERTIES_GROUP__PROPERTIES = eINSTANCE.getPropertiesGroup_Properties ();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.impl.PropertiesDefinitionGeneratorImpl <em>Definition Generator</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.impl.PropertiesDefinitionGeneratorImpl
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesDefinitionGenerator()
         * @generated
         */
        EClass PROPERTIES_DEFINITION_GENERATOR = eINSTANCE.getPropertiesDefinitionGenerator ();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.impl.PropertiesDefinitionImpl <em>Definition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.impl.PropertiesDefinitionImpl
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getPropertiesDefinition()
         * @generated
         */
        EClass PROPERTIES_DEFINITION = eINSTANCE.getPropertiesDefinition ();

        /**
         * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference PROPERTIES_DEFINITION__PROPERTIES = eINSTANCE.getPropertiesDefinition_Properties ();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.properties.DataType <em>Data Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.properties.DataType
         * @see org.openscada.doc.content.properties.impl.PropertiesPackageImpl#getDataType()
         * @generated
         */
        EEnum DATA_TYPE = eINSTANCE.getDataType ();

    }

} //PropertiesPackage
