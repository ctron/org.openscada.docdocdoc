/**
 */
package org.openscada.doc.content.properties;

import org.openscada.doc.model.doc.map.ContentGenerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.properties.PropertiesPackage#getPropertiesDefinitionGenerator()
 * @model
 * @generated
 */
public interface PropertiesDefinitionGenerator extends ContentGenerator
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";
} // PropertiesDefinitionGenerator
