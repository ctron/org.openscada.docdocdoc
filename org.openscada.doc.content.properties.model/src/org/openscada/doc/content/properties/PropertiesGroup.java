/**
 */
package org.openscada.doc.content.properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.properties.PropertiesGroup#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.PropertiesGroup#getProperties <em>Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.properties.PropertiesPackage#getPropertiesGroup()
 * @model extendedMetaData="name='group'"
 * @generated
 */
public interface PropertiesGroup extends EObject
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * Returns the value of the '<em><b>Prefix</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Prefix</em>' attribute.
     * @see #setPrefix(String)
     * @see org.openscada.doc.content.properties.PropertiesPackage#getPropertiesGroup_Prefix()
     * @model required="true"
     *        extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    String getPrefix ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.properties.PropertiesGroup#getPrefix <em>Prefix</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Prefix</em>' attribute.
     * @see #getPrefix()
     * @generated
     */
    void setPrefix ( String value );

    /**
     * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.properties.Property}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Properties</em>' containment reference list.
     * @see org.openscada.doc.content.properties.PropertiesPackage#getPropertiesGroup_Properties()
     * @model containment="true"
     *        extendedMetaData="name='properties' namespace='##targetNamespace'"
     * @generated
     */
    EList<Property> getProperties ();

} // PropertiesGroup
