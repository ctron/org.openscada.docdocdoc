/**
 */
package org.openscada.doc.content.properties.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesDefinition;
import org.openscada.doc.content.properties.PropertiesDefinitionGenerator;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.PropertiesPackage;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.map.ContentGenerator;
import org.openscada.doc.model.doc.map.MapElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.openscada.doc.content.properties.PropertiesPackage
 * @generated
 */
public class PropertiesAdapterFactory extends AdapterFactoryImpl
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static PropertiesPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PropertiesAdapterFactory ()
    {
        if ( modelPackage == null )
        {
            modelPackage = PropertiesPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType ( Object object )
    {
        if ( object == modelPackage )
        {
            return true;
        }
        if ( object instanceof EObject )
        {
            return ( (EObject)object ).eClass ().getEPackage () == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertiesSwitch<Adapter> modelSwitch = new PropertiesSwitch<Adapter> () {
        @Override
        public Adapter caseProperties ( Properties object )
        {
            return createPropertiesAdapter ();
        }

        @Override
        public Adapter caseProperty ( Property object )
        {
            return createPropertyAdapter ();
        }

        @Override
        public Adapter casePropertiesGroup ( PropertiesGroup object )
        {
            return createPropertiesGroupAdapter ();
        }

        @Override
        public Adapter casePropertiesDefinitionGenerator ( PropertiesDefinitionGenerator object )
        {
            return createPropertiesDefinitionGeneratorAdapter ();
        }

        @Override
        public Adapter casePropertiesDefinition ( PropertiesDefinition object )
        {
            return createPropertiesDefinitionAdapter ();
        }

        @Override
        public Adapter caseContent ( Content object )
        {
            return createContentAdapter ();
        }

        @Override
        public Adapter caseMapElement ( MapElement object )
        {
            return createMapElementAdapter ();
        }

        @Override
        public Adapter caseContentGenerator ( ContentGenerator object )
        {
            return createContentGeneratorAdapter ();
        }

        @Override
        public Adapter defaultCase ( EObject object )
        {
            return createEObjectAdapter ();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter ( Notifier target )
    {
        return modelSwitch.doSwitch ( (EObject)target );
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.properties.Properties <em>Properties</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.properties.Properties
     * @generated
     */
    public Adapter createPropertiesAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.properties.Property <em>Property</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.properties.Property
     * @generated
     */
    public Adapter createPropertyAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.properties.PropertiesGroup <em>Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.properties.PropertiesGroup
     * @generated
     */
    public Adapter createPropertiesGroupAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.properties.PropertiesDefinitionGenerator <em>Definition Generator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.properties.PropertiesDefinitionGenerator
     * @generated
     */
    public Adapter createPropertiesDefinitionGeneratorAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.properties.PropertiesDefinition <em>Definition</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.properties.PropertiesDefinition
     * @generated
     */
    public Adapter createPropertiesDefinitionAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Content
     * @generated
     */
    public Adapter createContentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.model.doc.map.MapElement <em>Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.MapElement
     * @generated
     */
    public Adapter createMapElementAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.model.doc.map.ContentGenerator <em>Content Generator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.map.ContentGenerator
     * @generated
     */
    public Adapter createContentGeneratorAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter ()
    {
        return null;
    }

} //PropertiesAdapterFactory
