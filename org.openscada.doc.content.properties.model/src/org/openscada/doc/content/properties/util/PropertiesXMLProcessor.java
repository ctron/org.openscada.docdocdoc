/**
 */
package org.openscada.doc.content.properties.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.openscada.doc.content.properties.PropertiesPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertiesXMLProcessor extends XMLProcessor
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PropertiesXMLProcessor ()
    {
        super ( ( EPackage.Registry.INSTANCE ) );
        PropertiesPackage.eINSTANCE.eClass ();
    }

    /**
     * Register for "*" and "xml" file extensions the PropertiesResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations ()
    {
        if ( registrations == null )
        {
            super.getRegistrations ();
            registrations.put ( XML_EXTENSION, new PropertiesResourceFactoryImpl () );
            registrations.put ( STAR_EXTENSION, new PropertiesResourceFactoryImpl () );
        }
        return registrations;
    }

} //PropertiesXMLProcessor
