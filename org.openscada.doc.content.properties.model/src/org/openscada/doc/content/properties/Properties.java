/**
 */
package org.openscada.doc.content.properties;

import org.eclipse.emf.common.util.EList;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.properties.Properties#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.openscada.doc.content.properties.Properties#getGroups <em>Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.properties.PropertiesPackage#getProperties()
 * @model extendedMetaData="name='properties'"
 * @generated
 */
public interface Properties extends Content
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.properties.Property}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Properties</em>' containment reference list.
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperties_Properties()
     * @model containment="true"
     *        extendedMetaData="name='properties' namespace='##targetNamespace'"
     * @generated
     */
    EList<Property> getProperties ();

    /**
     * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.properties.PropertiesGroup}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Groups</em>' containment reference list.
     * @see org.openscada.doc.content.properties.PropertiesPackage#getProperties_Groups()
     * @model containment="true"
     *        extendedMetaData="name='groups' namespace='##targetNamespace'"
     * @generated
     */
    EList<PropertiesGroup> getGroups ();

} // Properties
