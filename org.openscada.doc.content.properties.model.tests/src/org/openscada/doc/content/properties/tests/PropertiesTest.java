/**
 */
package org.openscada.doc.content.properties.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Properties</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertiesTest extends TestCase
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The fixture for this Properties test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Properties fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run ( PropertiesTest.class );
    }

    /**
     * Constructs a new Properties test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PropertiesTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Properties test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( Properties fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Properties test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Properties getFixture ()
    {
        return fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( PropertiesFactory.eINSTANCE.createProperties () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //PropertiesTest
