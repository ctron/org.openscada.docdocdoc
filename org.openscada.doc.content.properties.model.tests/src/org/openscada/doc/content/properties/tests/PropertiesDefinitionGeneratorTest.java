/**
 */
package org.openscada.doc.content.properties.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.content.properties.PropertiesDefinitionGenerator;
import org.openscada.doc.content.properties.PropertiesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Definition Generator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertiesDefinitionGeneratorTest extends TestCase
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final String copyright = "Copyright (c) 2013 Jens Reimann.\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Jens Reimann - initial API and implementation and/or initial documentation\n";

    /**
     * The fixture for this Definition Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertiesDefinitionGenerator fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run ( PropertiesDefinitionGeneratorTest.class );
    }

    /**
     * Constructs a new Definition Generator test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PropertiesDefinitionGeneratorTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Definition Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( PropertiesDefinitionGenerator fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Definition Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PropertiesDefinitionGenerator getFixture ()
    {
        return fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( PropertiesFactory.eINSTANCE.createPropertiesDefinitionGenerator () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //PropertiesDefinitionGeneratorTest
