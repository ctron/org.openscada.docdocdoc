/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.renderer.element;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.utils.str.StringHelper;

public class Element
{
    private final String name;

    private final Set<String> classes;

    private String id;

    public Element ( final String name )
    {
        this.name = name;
        this.classes = null;
    }

    public Element ( final String name, final Collection<String> classes )
    {
        this.name = name;
        this.classes = new HashSet<String> ( classes );
    }

    public void setId ( final String id )
    {
        this.id = id;
    }

    public String getId ()
    {
        return this.id;
    }

    public void start ( final PrintStream out )
    {
        start ( out, false );
    }

    public void start ( final PrintStream out, final boolean empty )
    {
        out.print ( "<" + this.name );
        if ( this.classes != null && !this.classes.isEmpty () )
        {
            out.print ( " class=\"" + StringHelper.join ( this.classes, " " ) + "\"" );
        }
        if ( this.id != null && !this.id.isEmpty () )
        {
            out.print ( String.format ( " id=\"%s\"", Helper.encodeHtml ( this.id ) ) );
        }
        if ( empty )
        {
            out.print ( " />" );
        }
        else
        {
            out.print ( ">" );
        }
    }

    public void end ( final PrintStream out )
    {
        out.print ( "</" + this.name + ">" );
    }

    public void wrapNotEmpty ( final RenderContext context, final String content )
    {
        if ( content == null || content.isEmpty () )
        {
            return;
        }
        else
        {
            wrap ( context, content );
        }
    }

    public void wrapNotNull ( final RenderContext context, final Object content )
    {
        if ( content == null )
        {
            return;
        }
        else
        {
            wrap ( context, content );
        }
    }

    public void wrap ( final RenderContext context, final Object content )
    {
        if ( content != null )
        {
            start ( context.getCurrentPrintStream () );
            context.render ( content );
            end ( context.getCurrentPrintStream () );
        }
        else
        {
            start ( context.getCurrentPrintStream (), true );
        }
    }
}
