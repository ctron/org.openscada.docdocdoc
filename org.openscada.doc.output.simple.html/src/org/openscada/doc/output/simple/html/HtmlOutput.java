/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.output.simple.AbstractOutput;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.RenderContextImpl;
import org.openscada.doc.output.simple.actions.CopyUrlResource;
import org.openscada.doc.output.simple.html.configuration.Code;
import org.openscada.doc.output.simple.html.configuration.Profile;
import org.openscada.doc.output.simple.html.configuration.Profiles;
import org.openscada.doc.output.simple.html.configuration.Resource;
import org.openscada.doc.output.simple.html.configuration.Styles;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.html.renderer.element.Element;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.ui.databinding.AdapterHelper;

import com.ibm.icu.text.MessageFormat;

public class HtmlOutput extends AbstractOutput
{

    public class RenderContextExtension extends RenderContextImpl
    {
        public RenderContextExtension ( final IContainer container, final Book book, final Locale locale )
        {
            super ( container, book, locale );
        }

        @Override
        public void render ( final Object object )
        {
            processObject ( this, object );
        }
    }

    private final Profile profile;

    private final ResourceBundle bundle;

    public HtmlOutput ( final Book book, final String profileId, final Locale locale ) throws CoreException
    {
        super ( book, locale );

        this.bundle = ResourceBundle.getBundle ( "resources/output", locale );

        final Map<String, Profile> profiles = Profiles.loadProfiles ();
        this.profile = profiles.get ( profileId );

        if ( this.profile == null )
        {
            throw new IllegalArgumentException ( String.format ( "Profile with id '%s' does not exists", profileId ) );
        }

        createActions ();
    }

    private void createActions ()
    {
        for ( final Styles styles : this.profile.getStyles () )
        {
            this.actions.add ( new CopyUrlResource ( new Path ( styles.getFileName () ), styles.getUrl () ) );
        }
        for ( final Code code : this.profile.getCode () )
        {
            this.actions.add ( new CopyUrlResource ( new Path ( code.getFileName () ), code.getUrl () ) );
        }
        for ( final Resource resource : this.profile.getResources () )
        {
            this.actions.add ( new CopyUrlResource ( new Path ( resource.getFileName () ), resource.getUrl () ) );
        }
    }

    @Override
    protected void processContent ( final RenderContext context )
    {
        context.setCurrentPrintStream ( "index.html" );

        start ( context );

        processTableOfContents ( context, this.book );

        processBody ( context );

        end ( context );
    }

    @Override
    protected RenderContextExtension createRenderContext ( final IFolder container )
    {
        return new RenderContextExtension ( container, this.book, this.locale );
    }

    private void processBookHeader ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        final boolean showAuthorRefs = !Boolean.parseBoolean ( this.profile.getProperties ().getProperty ( "org.openscada.doc.output.simple.html.book.hideAuthorRefs" ) );

        new Element ( "div", Arrays.asList ( "book_title" ) ).wrapNotEmpty ( context, this.book.getTitle () );
        new Element ( "div", Arrays.asList ( "book_version" ) ).wrapNotEmpty ( context, this.book.getVersion () );

        if ( this.book.getAuthors () != null && !this.book.getAuthors ().isEmpty () )
        {
            new Element ( "div", Arrays.asList ( "book_authors" ) ).wrap ( context, this.bundle.getString ( "heading.authors.label" ) );
            final Element ele = new Element ( "ul", Arrays.asList ( "book_author_list" ) );
            ele.start ( out );
            for ( final Author author : this.book.getAuthors () )
            {
                final Element aele = new Element ( "li" );
                aele.start ( out );
                out.print ( Helper.encodeHtml ( author.getName () ) );
                if ( showAuthorRefs && author.getRef () != null && !author.getRef ().isEmpty () )
                {
                    out.print ( " (" + Helper.encodeHtml ( author.getRef () ) + ")" );
                }
                aele.end ( out );
            }
            ele.end ( out );
        }

        new Element ( "div", Arrays.asList ( "book_copyright" ) ).wrapNotEmpty ( context, makeCopyright ( this.book.getCopyright () ) );
        new Element ( "p", Arrays.asList ( "book_copyrightMarker" ) ).wrapNotEmpty ( context, this.book.getCopyrightMarker () );
        new Element ( "p", Arrays.asList ( "book_copyrightText" ) ).wrapNotEmpty ( context, this.book.getCopyrightText () );
    }

    private String makeCopyright ( final List<Copyright> copyright )
    {
        if ( copyright.isEmpty () )
        {
            return this.bundle.getString ( "copyright.empty" );
        }

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for ( final Copyright c : copyright )
        {
            min = Math.min ( min, c.getYear () );
            max = Math.max ( max, c.getYear () );
        }

        String text;
        if ( min == max )
        {
            text = String.format ( "%s", min );
        }
        else
        {
            text = String.format ( "%s-%s", min, max );
        }

        return new MessageFormat ( this.bundle.getString ( "copyright.format" ), this.locale ).format ( new Object[] { text } );
    }

    private void start ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( "<!DOCTYPE html>" );
        out.println ( "<html>" );
        out.println ( "<head>" );

        out.println ( String.format ( "<meta name=\"generator\" content=\"openSCADA DocDocDoc HTML Book Renderer v%s\" />", Activator.getDefault ().getBundle ().getVersion () ) );
        out.println ( "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" );

        if ( this.book.getTitle () != null )
        {
            out.println ( String.format ( "<title>%s</title>", Helper.encodeHtml ( this.book.getTitle () ) ) );
        }

        writeStyleImports ( out );
        writeCodeImports ( out );

        out.println ( "</head>" );
        out.println ( "<body>" );
    }

    private void writeStyleImports ( final PrintStream out )
    {
        final Set<String> fileName = new HashSet<String> ();
        for ( final Styles styles : this.profile.getStyles () )
        {
            if ( !fileName.contains ( styles.getFileName () ) )
            {
                fileName.add ( styles.getFileName () );
                out.println ( String.format ( "<link href=\"%s\" rel=\"stylesheet\">", styles.getFileName () ) );
            }
        }
    }

    private void writeCodeImports ( final PrintStream out )
    {
        final Set<String> fileName = new HashSet<String> ();
        for ( final Code code : this.profile.getCode () )
        {
            if ( !fileName.contains ( code.getFileName () ) )
            {
                fileName.add ( code.getFileName () );
                if ( code.getType () != null && !code.getType ().isEmpty () )
                {
                    out.println ( String.format ( "<script src=\"%s\" type=\"%s\"></script>", code.getFileName (), code.getType () ) );
                }
                else
                {
                    out.println ( String.format ( "<script src=\"%s\"></script>", code.getFileName () ) );
                }
            }
        }
    }

    private void end ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( "</body></html>" );
    }

    protected void processObject ( final RenderContext context, final Object content )
    {
        if ( content instanceof String )
        {
            final PrintStream out = context.getCurrentPrintStream ();
            out.print ( org.openscada.doc.output.simple.html.renderer.Helper.encodeHtml ( (String)content ) );
        }
        else if ( content instanceof FeatureMap )
        {
            for ( final FeatureMap.Entry entry : (FeatureMap)content )
            {
                processObject ( context, entry.getValue () );
            }
        }
        else
        {
            render ( context, content );
        }

    }

    private void render ( final RenderContext context, final Object content )
    {
        final BookRenderer renderer = findRenderer ( "html", content.getClass () );

        if ( renderer != null )
        {
            renderer.render ( context, content );
        }
        else
        {
            throw new IllegalStateException ( "Unable to render object type: " + content.getClass () );
        }
    }

    private List<Integer> sectionNumber ( final BookSection section )
    {
        final List<Integer> nrs = new LinkedList<Integer> ();

        BookSection currentSection = section;
        do
        {
            nrs.add ( 0, currentSection.getNumber () );
            currentSection = AdapterHelper.adapt ( currentSection.eContainer (), BookSection.class );
        } while ( currentSection != null );

        return nrs;
    }

    private void processBody ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( "<div class=\"bodybox\">" );
        processBookHeader ( context );
        processContainer ( context, this.book );
        out.println ( "</div>" );
    }

    private void processTableOfContents ( final RenderContext context, final Book book )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( "<div class=\"tocbox\">" );
        out.println ( String.format ( "<div class=\"heading toc\">%s</div>", this.bundle.getString ( "toc.label" ) ) );

        final TreeIterator<EObject> i = book.eAllContents ();
        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( ! ( o instanceof BookSection ) )
            {
                continue;
            }

            final BookSection section = (BookSection)o;

            final String title = section.getTitle ();

            final List<Integer> list = sectionNumber ( section );
            final int level = list.size ();

            final String xrefId = context.createCrossReference ( "section", section.getId () );

            out.println ( String.format ( "<div class=\"tocentry level_%1$s\"><a href=\"#%4$s\"><span class=\"number\">%2$s</span><span class=\"title\">%3$s</span></a></div>", level, section.getFullNumber (), title, xrefId ) );
        }
        out.println ( "</div>" );
    }

    private void processContainer ( final RenderContext context, final BookContainer container )
    {
        for ( final Content content : container.getContent () )
        {
            processContent ( context, content );
        }
        for ( final BookSection section : container.getSections () )
        {
            processSection ( context, section );
        }
    }

    private void processSection ( final RenderContext context, final BookSection section )
    {
        final String title = section.getTitle ();
        final List<Integer> list = sectionNumber ( section );
        final int level = list.size ();

        final PrintStream out = context.getCurrentPrintStream ();

        final String xrefId = context.createLinkTarget ( "section", section.getId () );

        out.println ( String.format ( "<div id=\"%4$s\" class=\"heading level_%1$s\"><span class=\"number\">%2$s</span><span class=\"title\">%3$s</span></div>", level, section.getFullNumber (), title, xrefId ) );

        processContainer ( context, section );
    }

    private void processContent ( final RenderContext context, final Content content )
    {
        processObject ( context, content );
    }
}
