/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Profile
{
    private final String id;

    private final Profile parentProfile;

    private final List<Styles> styles;

    private final List<Code> code;

    private final List<Resource> resources;

    private final Properties properties;

    public Profile ( final String id, final Profile parentProfile, final List<Styles> styles, final List<Code> code, final List<Resource> resources, final Map<String, String> properties )
    {
        this.id = id;
        this.parentProfile = parentProfile;
        this.styles = makeStyles ( styles );
        this.code = makeCode ( code );
        this.resources = makeResources ( resources );
        this.properties = makeProperties ( parentProfile, properties );
    }

    private static Properties makeProperties ( final Profile parentProfile, final Map<String, String> properties )
    {
        if ( properties == null )
        {
            if ( parentProfile != null )
            {
                return parentProfile.getProperties ();
            }
            else
            {
                return System.getProperties ();
            }
        }

        final Properties p = new Properties ( parentProfile != null ? parentProfile.getProperties () : System.getProperties () );
        p.putAll ( properties );
        return p;
    }

    public Properties getProperties ()
    {
        return this.properties;
    }

    private static List<Resource> makeResources ( final List<Resource> resource )
    {
        if ( resource == null )
        {
            return Collections.emptyList ();
        }

        return new ArrayList<Resource> ( resource );
    }

    private static List<Styles> makeStyles ( final List<Styles> styles )
    {
        if ( styles == null )
        {
            return Collections.emptyList ();
        }

        final List<Styles> result = new ArrayList<Styles> ( styles );

        Collections.sort ( result, new Comparator<Styles> () {
            @Override
            public int compare ( final Styles o1, final Styles o2 )
            {
                return Integer.valueOf ( o1.getPriority () ).compareTo ( o2.getPriority () );
            }
        } );

        return result;
    }

    private static List<Code> makeCode ( final List<Code> code )
    {
        if ( code == null )
        {
            return Collections.emptyList ();
        }

        final List<Code> result = new ArrayList<Code> ( code );

        Collections.sort ( result, new Comparator<Code> () {
            @Override
            public int compare ( final Code o1, final Code o2 )
            {
                return Integer.valueOf ( o1.getPriority () ).compareTo ( o2.getPriority () );
            }
        } );

        return result;
    }

    public String getId ()
    {
        return this.id;
    }

    public List<Styles> getStyles ()
    {
        return this.styles;
    }

    public List<Code> getCode ()
    {
        return this.code;
    }

    public List<Resource> getResources ()
    {
        return this.resources;
    }

    public Profile getParentProfile ()
    {
        return this.parentProfile;
    }
}
