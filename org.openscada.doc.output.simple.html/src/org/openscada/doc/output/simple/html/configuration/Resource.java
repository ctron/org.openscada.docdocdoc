/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.configuration;

import java.net.URL;

public class Resource
{
    private final URL url;

    private final String fileName;

    public Resource ( final URL url, final String fileName )
    {
        this.url = url;
        this.fileName = fileName;
    }

    public String getFileName ()
    {
        return this.fileName;
    }

    public URL getUrl ()
    {
        return this.url;
    }

}
