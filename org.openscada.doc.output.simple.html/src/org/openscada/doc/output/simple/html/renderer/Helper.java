/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.renderer;

import org.apache.commons.lang.StringEscapeUtils;

public final class Helper
{
    public static String encodeHtml ( final String string )
    {
        if ( string == null )
        {
            return "";
        }

        return StringEscapeUtils.escapeHtml ( string );
    }

    public static String convertBreaks ( final String string )
    {
        return string.replace ( "\n", "<br/>" );
    }
}
