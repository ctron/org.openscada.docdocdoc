/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.configuration;

import java.net.URL;

public class Code
{
    private final URL url;

    private final int priority;

    private final String fileName;

    private final String type;

    public Code ( final URL url, final String fileName, final String type, final int priority )
    {
        this.url = url;
        this.fileName = fileName;
        this.type = type;
        this.priority = priority;
    }

    public String getType ()
    {
        return this.type;
    }

    public String getFileName ()
    {
        return this.fileName;
    }

    public URL getUrl ()
    {
        return this.url;
    }

    public int getPriority ()
    {
        return this.priority;
    }
}
