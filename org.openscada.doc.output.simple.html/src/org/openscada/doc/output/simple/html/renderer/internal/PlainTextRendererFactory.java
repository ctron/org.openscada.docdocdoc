/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.renderer.internal;

import java.util.HashMap;
import java.util.Map;

import org.openscada.doc.model.doc.fragment.PlainTextContent;
import org.openscada.doc.output.simple.renderer.AbstractCacheFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PlainTextRendererFactory extends AbstractCacheFactory
{

    private final Map<Entry, BookRenderer> cache = new HashMap<Entry, BookRenderer> ();

    public PlainTextRendererFactory ()
    {
        this.cache.put ( new Entry ( "html", PlainTextContent.class ), new PlainTextRenderer () );
    }

    @Override
    protected Map<Entry, BookRenderer> getCache ()
    {
        return this.cache;
    }

}
