/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.ui.databinding.SelectionHelper;
import org.openscada.ui.utils.status.StatusHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateAction implements IObjectActionDelegate
{

    private final static Logger logger = LoggerFactory.getLogger ( GenerateAction.class );

    private IFile file;

    private Shell shell;

    public GenerateAction ()
    {

    }

    @Override
    public void run ( final IAction action )
    {
        try
        {
            // emf
            final Map<Object, Object> options = new HashMap<Object, Object> ();

            final ResourceSet resourceSet = new ResourceSetImpl ();
            final Resource resource = resourceSet.createResource ( URI.createFileURI ( this.file.getLocation ().toString () ) );
            resource.load ( options );

            final ProgressMonitorDialog dlg = new ProgressMonitorDialog ( this.shell );

            dlg.run ( true, false, new IRunnableWithProgress () {

                @Override
                public void run ( final IProgressMonitor monitor ) throws InvocationTargetException, InterruptedException
                {
                    try
                    {
                        new HtmlOutput ( (Book)resource.getContents ().get ( 0 ), "default", Locale.getDefault () ).output ( monitor, GenerateAction.this.file.getParent () );
                    }
                    catch ( final Exception e )
                    {
                        throw new InvocationTargetException ( e );
                    }
                }
            } );
        }
        catch ( final Exception e )
        {
            logger.warn ( "Failed to generate", e );
            ErrorDialog.openError ( this.shell, "Generate plaintext", "Failed to generate", StatusHelper.convertStatus ( "org.openscada.doc.output.xpand.plaintext", e ) );
        }
    }

    @Override
    public void selectionChanged ( final IAction action, final ISelection selection )
    {
        this.file = SelectionHelper.first ( selection, IFile.class );
    }

    @Override
    public void setActivePart ( final IAction action, final IWorkbenchPart targetPart )
    {
        this.shell = targetPart.getSite ().getShell ();
    }

}
