/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.html.configuration;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public final class Profiles
{
    private static final String POINT_HTML_BOOK_RENDERER = "org.openscada.doc.output.simple.html.bookRenderer";

    private Profiles ()
    {
    }

    public static Map<String, Profile> loadProfiles ()
    {
        final Map<String, Profile> result = new HashMap<String, Profile> ();

        final Map<String, IConfigurationElement> parentBuilderMap = new HashMap<String, IConfigurationElement> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( POINT_HTML_BOOK_RENDERER ) )
        {
            if ( "htmlProfile".equals ( ele.getName () ) )
            {
                final String id = ele.getAttribute ( "id" );
                if ( id != null && !id.isEmpty () )
                {
                    parentBuilderMap.put ( id, ele );
                }
            }
        }

        for ( final String id : parentBuilderMap.keySet () )
        {
            findParent ( id, parentBuilderMap, result );
        }

        return result;
    }

    private static Profile findParent ( final String id, final Map<String, IConfigurationElement> parentBuilderMap, final Map<String, Profile> result )
    {
        Profile p = result.get ( id );
        if ( p != null )
        {
            return p;
        }

        final IConfigurationElement ele = parentBuilderMap.get ( id );

        if ( ele == null )
        {
            throw new IllegalStateException ( String.format ( "Parent profile '%s' does not exists", id ) );
        }

        p = loadProfile ( ele, parentBuilderMap, result );
        result.put ( id, p );
        return p;
    }

    private static Profile loadProfile ( final IConfigurationElement ele, final Map<String, IConfigurationElement> parentBuilderMap, final Map<String, Profile> result )
    {
        final String parentId = ele.getAttribute ( "parentId" );

        final Profile parentProfile;

        if ( parentId != null )
        {
            parentProfile = findParent ( parentId, parentBuilderMap, result );
        }
        else
        {
            parentProfile = null;
        }

        final String id = ele.getAttribute ( "id" );

        if ( id == null || id.isEmpty () )
        {
            return null;
        }

        final Map<String, String> properties = new HashMap<String, String> ();

        for ( final IConfigurationElement child : ele.getChildren ( "property" ) )
        {
            final String key = child.getAttribute ( "key" );
            final String value = child.getAttribute ( "value" );
            properties.put ( key, value );
        }

        final List<Styles> styles = loadStylesList ( ele );
        styles.addAll ( findExtensionStyles ( id ) );

        final List<Code> code = loadCodeList ( ele );
        code.addAll ( findExtensionCode ( id ) );

        final List<Resource> resources = loadResourcesList ( ele );
        resources.addAll ( findExtensionResources ( id ) );

        return new Profile ( id, parentProfile, styles, code, resources, properties );
    }

    private static Collection<? extends Resource> findExtensionResources ( final String profileId )
    {
        if ( profileId == null )
        {
            return Collections.emptyList ();
        }

        final Collection<Resource> result = new LinkedList<Resource> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( POINT_HTML_BOOK_RENDERER ) )
        {
            if ( !"htmlProfileExtension".equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "profileId" );
            if ( !profileId.equals ( id ) )
            {
                continue;
            }

            result.addAll ( loadResourcesList ( ele ) );
        }

        return result;
    }

    private static List<Resource> loadResourcesList ( final IConfigurationElement ele )
    {
        final List<Resource> result = new LinkedList<Resource> ();

        for ( final IConfigurationElement child : ele.getChildren ( "resource" ) )
        {
            final Resource resource = loadResource ( child );
            if ( resource != null )
            {
                result.add ( resource );
            }
        }

        return result;
    }

    private static Resource loadResource ( final IConfigurationElement ele )
    {
        final String file = ele.getAttribute ( "file" );

        final Bundle bundle = Platform.getBundle ( ele.getContributor ().getName () );

        final URL url = FileLocator.find ( bundle, new Path ( file ), null );

        if ( url == null )
        {
            throw new IllegalStateException ( String.format ( "Resource file '%s' could not be found in bundle '%s'", file, bundle.getSymbolicName () ) );
        }

        return new Resource ( url, file );
    }

    private static Collection<? extends Code> findExtensionCode ( final String profileId )
    {
        if ( profileId == null )
        {
            return Collections.emptyList ();
        }

        final Collection<Code> result = new LinkedList<Code> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( POINT_HTML_BOOK_RENDERER ) )
        {
            if ( !"htmlProfileExtension".equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "profileId" );
            if ( !profileId.equals ( id ) )
            {
                continue;
            }

            result.addAll ( loadCodeList ( ele ) );
        }

        return result;
    }

    private static List<Code> loadCodeList ( final IConfigurationElement ele )
    {
        final List<Code> result = new LinkedList<Code> ();

        for ( final IConfigurationElement child : ele.getChildren ( "htmlCode" ) )
        {
            final Code code = loadCode ( child );
            if ( code != null )
            {
                result.add ( code );
            }
        }

        return result;
    }

    private static Code loadCode ( final IConfigurationElement ele )
    {
        final int priority = makeInt ( ele.getAttribute ( "priority" ), Integer.MAX_VALUE );

        final String cssFileName = ele.getAttribute ( "codeFile" );
        final String language = ele.getAttribute ( "type" );

        final Bundle bundle = Platform.getBundle ( ele.getContributor ().getName () );

        final URL url = FileLocator.find ( bundle, new Path ( cssFileName ), null );

        if ( url == null )
        {
            throw new IllegalStateException ( String.format ( "Code file '%s' could not be found in bundle '%s'", cssFileName, bundle.getSymbolicName () ) );
        }

        return new Code ( url, cssFileName, language, priority );
    }

    private static Collection<? extends Styles> findExtensionStyles ( final String profileId )
    {
        if ( profileId == null )
        {
            return Collections.emptyList ();
        }

        final Collection<Styles> result = new LinkedList<Styles> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( POINT_HTML_BOOK_RENDERER ) )
        {
            if ( !"htmlProfileExtension".equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "profileId" );
            if ( !profileId.equals ( id ) )
            {
                continue;
            }

            result.addAll ( loadStylesList ( ele ) );
        }

        return result;
    }

    private static List<Styles> loadStylesList ( final IConfigurationElement ele )
    {
        final List<Styles> result = new LinkedList<Styles> ();

        for ( final IConfigurationElement child : ele.getChildren ( "htmlStyles" ) )
        {
            final Styles styles = loadStyles ( child );
            if ( styles != null )
            {
                result.add ( styles );
            }
        }

        return result;
    }

    private static Styles loadStyles ( final IConfigurationElement ele )
    {
        final int priority = makeInt ( ele.getAttribute ( "priority" ), Integer.MAX_VALUE );

        final String cssFileName = ele.getAttribute ( "cssFile" );
        final Bundle bundle = Platform.getBundle ( ele.getContributor ().getName () );

        final URL url = FileLocator.find ( bundle, new Path ( cssFileName ), null );

        if ( url == null )
        {
            throw new IllegalStateException ( String.format ( "CSS file '%s' could not be found in bundle '%s'", cssFileName, bundle.getSymbolicName () ) );
        }

        return new Styles ( url, cssFileName, priority );
    }

    private static int makeInt ( final String value, final int defaultValue )
    {
        try
        {
            return Integer.parseInt ( value );
        }
        catch ( final Exception e )
        {
            return defaultValue;
        }
    }
}
