package org.openscada.doc.output.simple.plaintext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.output.simple.plaintext.render.BookPlaintextRenderer;
import org.openscada.doc.output.simple.plaintext.render.RenderContext;
import org.openscada.doc.output.simple.plaintext.render.RenderParagraph;
import org.openscada.doc.output.simple.plaintext.render.RenderPlainTextFragment;
import org.openscada.ui.databinding.AdapterHelper;
import org.openscada.utils.str.StringHelper;

public class PlainTextOutput
{

    public static class RenderContextImpl implements RenderContext
    {
        private final PrintStream pos;

        public RenderContextImpl ( final PrintStream pos )
        {
            this.pos = pos;
        }

        @Override
        public PrintStream getPrintStream ()
        {
            return this.pos;
        }
    }

    private final Book book;

    private final List<BookPlaintextRenderer> renderer = new LinkedList<BookPlaintextRenderer> ();

    public PlainTextOutput ( final Book book )
    {
        this.book = book;
        this.renderer.add ( new RenderPlainTextFragment () );
        this.renderer.add ( new RenderParagraph () );
    }

    public void output ( final IProgressMonitor monitor, final IContainer parent ) throws Exception
    {
        try
        {
            try
            {
                final IFile file = parent.getFile ( new Path ( "book.txt" ) );
                processOutput ( monitor, file );
            }
            finally
            {
                parent.refreshLocal ( IResource.DEPTH_INFINITE, monitor );
            }
        }
        finally
        {

            monitor.done ();
        }
    }

    private InputStream getContent ( final IProgressMonitor monitor ) throws Exception
    {
        final File temp = File.createTempFile ( "book", "txt" );
        final FileOutputStream fos = new FileOutputStream ( temp );

        final PrintStream pos = new PrintStream ( fos, true, "UTF-8" );
        try
        {
            final RenderContext context = new RenderContextImpl ( pos );
            renderBook ( monitor, context );
        }
        finally
        {
            fos.close ();
        }

        return new FileInputStream ( temp ) {
            @Override
            public void close () throws IOException
            {
                super.close ();
                temp.delete ();
            }
        };
    }

    private void renderBook ( final IProgressMonitor monitor, final RenderContext context )
    {
        renderAllContent ( monitor, context, this.book.getContent () );
        renderAllSections ( monitor, context, this.book.getSections (), 1 );
    }

    private void renderAllSections ( final IProgressMonitor monitor, final RenderContext context, final EList<BookSection> sections, final int depth )
    {
        for ( final BookSection section : sections )
        {
            renderSection ( monitor, context, section, depth );
        }
    }

    String sectionNumber ( final BookSection section )
    {
        final List<Integer> nrs = new LinkedList<Integer> ();

        BookSection currentSection = section;
        do
        {
            nrs.add ( 0, currentSection.getNumber () );
            currentSection = AdapterHelper.adapt ( currentSection.eContainer (), BookSection.class );
        } while ( currentSection != null );

        return StringHelper.join ( nrs, "." );
    }

    private void renderSection ( final IProgressMonitor monitor, final RenderContext context, final BookSection section, final int depth )
    {
        final PrintStream ps = context.getPrintStream ();

        ps.println ( String.format ( "%s - %s", sectionNumber ( section ), section.getTitle () ) );

        switch ( depth )
        {
            case 1:
                ps.println ( "==============================" );
                break;
            case 2:
                ps.println ( "------------------------------" );
                break;
            case 3:
                ps.println ( ".............................." );
                break;
        }
        ps.println ();

        renderAllContent ( monitor, context, section.getContent () );

        ps.println ();
        renderAllSections ( monitor, context, section.getSections (), depth + 1 );
    }

    private void renderAllContent ( final IProgressMonitor monitor, final RenderContext context, final List<Content> contentList )
    {
        for ( final Content content : contentList )
        {
            renderContent ( monitor, context, content );
        }
    }

    private void renderContent ( final IProgressMonitor monitor, final RenderContext context, final Content content )
    {
        for ( final BookPlaintextRenderer renderer : this.renderer )
        {
            if ( renderer.canRender ( content ) )
            {
                renderer.render ( context, content );
                return;
            }
        }
        throw new IllegalStateException ( String.format ( "Unable to render content type: %s", content.eClass ().getName () ) );
    }

    private void processOutput ( final IProgressMonitor monitor, final IFile file ) throws Exception
    {
        if ( !file.exists () )
        {
            file.create ( getContent ( monitor ), true, monitor );
        }
        else
        {
            file.setContents ( getContent ( monitor ), IResource.FORCE | IResource.KEEP_HISTORY, monitor );
        }

    }

}
