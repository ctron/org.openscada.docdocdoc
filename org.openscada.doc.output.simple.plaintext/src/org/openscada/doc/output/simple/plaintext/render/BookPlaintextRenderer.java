package org.openscada.doc.output.simple.plaintext.render;

import org.openscada.doc.model.doc.fragment.Content;

public interface BookPlaintextRenderer
{
    public boolean canRender ( Content fragment );

    public void render ( RenderContext context, Content fragment );
}
