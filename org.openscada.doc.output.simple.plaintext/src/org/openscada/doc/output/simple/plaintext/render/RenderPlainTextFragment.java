package org.openscada.doc.output.simple.plaintext.render;

import java.io.PrintStream;

import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

public class RenderPlainTextFragment implements BookPlaintextRenderer
{

    @Override
    public boolean canRender ( final Content fragment )
    {
        return fragment instanceof PlainTextContent;
    }

    @Override
    public void render ( final RenderContext context, final Content fragment )
    {
        final PrintStream ps = context.getPrintStream ();
        final PlainTextContent frag = (PlainTextContent)fragment;

        ps.print ( frag.getValue () );
    }
}
