package org.openscada.doc.output.simple.plaintext.render;

import java.io.PrintStream;

public interface RenderContext
{
    public PrintStream getPrintStream ();
}
