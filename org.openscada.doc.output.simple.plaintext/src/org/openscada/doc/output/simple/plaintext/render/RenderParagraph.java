package org.openscada.doc.output.simple.plaintext.render;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.model.doc.fragment.Content;

public class RenderParagraph implements BookPlaintextRenderer
{

    @Override
    public boolean canRender ( final Content fragment )
    {
        return fragment instanceof Paragraph;
    }

    @Override
    public void render ( final RenderContext context, final Content fragment )
    {
        final Paragraph c = (Paragraph)fragment;

        context.getPrintStream ().println ();
        renderFeatureMap ( context, c.getContent () );
        context.getPrintStream ().println ();
    }

    private void renderFeatureMap ( final RenderContext context, final FeatureMap map )
    {
        for ( final FeatureMap.Entry entry : map )
        {
            render ( context, entry.getValue () );
        }
    }

    private void render ( final RenderContext context, final Object value )
    {
        if ( value instanceof String )
        {
            context.getPrintStream ().print ( value );
        }
        else if ( value instanceof Emphasis )
        {
            renderEmphasis ( context, (Emphasis)value );
        }
        else
        {
            throw new IllegalStateException ( "Unable to render object type: " + value.getClass () + " -> " + value );
        }
    }

    private void renderEmphasis ( final RenderContext context, final Emphasis value )
    {
        context.getPrintStream ().print ( "_" );
        renderFeatureMap ( context, value.getContent () );
        context.getPrintStream ().print ( "_" );
    }
}
