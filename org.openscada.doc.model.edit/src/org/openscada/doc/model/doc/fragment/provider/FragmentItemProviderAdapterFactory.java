/**
 */
package org.openscada.doc.model.doc.fragment.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ChildCreationExtenderManager;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemFontProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITableItemColorProvider;
import org.eclipse.emf.edit.provider.ITableItemFontProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.openscada.doc.model.doc.fragment.FragmentPackage;
import org.openscada.doc.model.doc.fragment.util.FragmentAdapterFactory;
import org.openscada.doc.model.doc.provider.DocEditPlugin;

/**
 * This is the factory that is used to provide the interfaces needed to support
 * Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into
 * calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class FragmentItemProviderAdapterFactory extends FragmentAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable, IChildCreationExtender
{
    /**
     * This keeps track of the root adapter factory that delegates to this
     * adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ComposedAdapterFactory parentAdapterFactory;

    /**
     * This is used to implement
     * {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IChangeNotifier changeNotifier = new ChangeNotifier ();

    /**
     * This helps manage the child creation extenders.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ChildCreationExtenderManager childCreationExtenderManager = new ChildCreationExtenderManager ( DocEditPlugin.INSTANCE, FragmentPackage.eNS_URI );

    /**
     * This keeps track of all the supported types checked by
     * {@link #isFactoryForType isFactoryForType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Collection<Object> supportedTypes = new ArrayList<Object> ();

    /**
     * This constructs an instance.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentItemProviderAdapterFactory ()
    {
        this.supportedTypes.add ( IEditingDomainItemProvider.class );
        this.supportedTypes.add ( IStructuredItemContentProvider.class );
        this.supportedTypes.add ( ITreeItemContentProvider.class );
        this.supportedTypes.add ( IItemLabelProvider.class );
        this.supportedTypes.add ( IItemPropertySource.class );
        this.supportedTypes.add ( ITableItemLabelProvider.class );
        this.supportedTypes.add ( ITableItemColorProvider.class );
        this.supportedTypes.add ( ITableItemFontProvider.class );
        this.supportedTypes.add ( IItemColorProvider.class );
        this.supportedTypes.add ( IItemFontProvider.class );
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.fragment.Fragment} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FragmentItemProvider fragmentItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.fragment.Fragment}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createFragmentAdapter ()
    {
        if ( this.fragmentItemProvider == null )
        {
            this.fragmentItemProvider = new FragmentItemProvider ( this );
        }

        return this.fragmentItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PlainTextContentItemProvider plainTextContentItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.fragment.PlainTextContent}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createPlainTextContentAdapter ()
    {
        if ( this.plainTextContentItemProvider == null )
        {
            this.plainTextContentItemProvider = new PlainTextContentItemProvider ( this );
        }

        return this.plainTextContentItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.fragment.Author} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AuthorItemProvider authorItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.fragment.Author}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createAuthorAdapter ()
    {
        if ( this.authorItemProvider == null )
        {
            this.authorItemProvider = new AuthorItemProvider ( this );
        }

        return this.authorItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.fragment.Copyright} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CopyrightItemProvider copyrightItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.fragment.Copyright}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createCopyrightAdapter ()
    {
        if ( this.copyrightItemProvider == null )
        {
            this.copyrightItemProvider = new CopyrightItemProvider ( this );
        }

        return this.copyrightItemProvider;
    }

    /**
     * This returns the root adapter factory that contains this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ComposeableAdapterFactory getRootAdapterFactory ()
    {
        return this.parentAdapterFactory == null ? this : this.parentAdapterFactory.getRootAdapterFactory ();
    }

    /**
     * This sets the composed adapter factory that contains this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setParentAdapterFactory ( final ComposedAdapterFactory parentAdapterFactory )
    {
        this.parentAdapterFactory = parentAdapterFactory;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isFactoryForType ( final Object type )
    {
        return this.supportedTypes.contains ( type ) || super.isFactoryForType ( type );
    }

    /**
     * This implementation substitutes the factory itself as the key for the
     * adapter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter adapt ( final Notifier notifier, final Object type )
    {
        return super.adapt ( notifier, this );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object adapt ( final Object object, final Object type )
    {
        if ( isFactoryForType ( type ) )
        {
            final Object adapter = super.adapt ( object, type );
            if ( ! ( type instanceof Class<?> ) || ( (Class<?>)type ).isInstance ( adapter ) )
            {
                return adapter;
            }
        }

        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public List<IChildCreationExtender> getChildCreationExtenders ()
    {
        return this.childCreationExtenderManager.getChildCreationExtenders ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Collection<?> getNewChildDescriptors ( final Object object, final EditingDomain editingDomain )
    {
        return this.childCreationExtenderManager.getNewChildDescriptors ( object, editingDomain );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator ()
    {
        return this.childCreationExtenderManager;
    }

    /**
     * This adds a listener.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void addListener ( final INotifyChangedListener notifyChangedListener )
    {
        this.changeNotifier.addListener ( notifyChangedListener );
    }

    /**
     * This removes a listener.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void removeListener ( final INotifyChangedListener notifyChangedListener )
    {
        this.changeNotifier.removeListener ( notifyChangedListener );
    }

    /**
     * This delegates to {@link #changeNotifier} and to
     * {@link #parentAdapterFactory}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void fireNotifyChanged ( final Notification notification )
    {
        this.changeNotifier.fireNotifyChanged ( notification );

        if ( this.parentAdapterFactory != null )
        {
            this.parentAdapterFactory.fireNotifyChanged ( notification );
        }
    }

    /**
     * This disposes all of the item providers created by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void dispose ()
    {
        if ( this.fragmentItemProvider != null )
        {
            this.fragmentItemProvider.dispose ();
        }
        if ( this.plainTextContentItemProvider != null )
        {
            this.plainTextContentItemProvider.dispose ();
        }
        if ( this.authorItemProvider != null )
        {
            this.authorItemProvider.dispose ();
        }
        if ( this.copyrightItemProvider != null )
        {
            this.copyrightItemProvider.dispose ();
        }
    }

}
