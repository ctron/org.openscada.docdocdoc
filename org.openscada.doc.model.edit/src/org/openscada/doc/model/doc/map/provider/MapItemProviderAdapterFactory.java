/**
 */
package org.openscada.doc.model.doc.map.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ChildCreationExtenderManager;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemFontProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITableItemColorProvider;
import org.eclipse.emf.edit.provider.ITableItemFontProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.openscada.doc.model.doc.map.MapPackage;
import org.openscada.doc.model.doc.map.util.MapAdapterFactory;
import org.openscada.doc.model.doc.provider.DocEditPlugin;

/**
 * This is the factory that is used to provide the interfaces needed to support
 * Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into
 * calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MapItemProviderAdapterFactory extends MapAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable, IChildCreationExtender
{
    /**
     * This keeps track of the root adapter factory that delegates to this
     * adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ComposedAdapterFactory parentAdapterFactory;

    /**
     * This is used to implement
     * {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IChangeNotifier changeNotifier = new ChangeNotifier ();

    /**
     * This helps manage the child creation extenders.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ChildCreationExtenderManager childCreationExtenderManager = new ChildCreationExtenderManager ( DocEditPlugin.INSTANCE, MapPackage.eNS_URI );

    /**
     * This keeps track of all the supported types checked by
     * {@link #isFactoryForType isFactoryForType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Collection<Object> supportedTypes = new ArrayList<Object> ();

    /**
     * This constructs an instance.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapItemProviderAdapterFactory ()
    {
        this.supportedTypes.add ( IEditingDomainItemProvider.class );
        this.supportedTypes.add ( IStructuredItemContentProvider.class );
        this.supportedTypes.add ( ITreeItemContentProvider.class );
        this.supportedTypes.add ( IItemLabelProvider.class );
        this.supportedTypes.add ( IItemPropertySource.class );
        this.supportedTypes.add ( ITableItemLabelProvider.class );
        this.supportedTypes.add ( ITableItemColorProvider.class );
        this.supportedTypes.add ( ITableItemFontProvider.class );
        this.supportedTypes.add ( IItemColorProvider.class );
        this.supportedTypes.add ( IItemFontProvider.class );
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.Map} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapItemProvider mapItemProvider;

    /**
     * This creates an adapter for a {@link org.openscada.doc.model.doc.map.Map}
     * .
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createMapAdapter ()
    {
        if ( this.mapItemProvider == null )
        {
            this.mapItemProvider = new MapItemProvider ( this );
        }

        return this.mapItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.File} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FileItemProvider fileItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.File}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createFileAdapter ()
    {
        if ( this.fileItemProvider == null )
        {
            this.fileItemProvider = new FileItemProvider ( this );
        }

        return this.fileItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.MapSection} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapSectionItemProvider mapSectionItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.MapSection}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createMapSectionAdapter ()
    {
        if ( this.mapSectionItemProvider == null )
        {
            this.mapSectionItemProvider = new MapSectionItemProvider ( this );
        }

        return this.mapSectionItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.Feature} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FeatureItemProvider featureItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.Feature}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createFeatureAdapter ()
    {
        if ( this.featureItemProvider == null )
        {
            this.featureItemProvider = new FeatureItemProvider ( this );
        }

        return this.featureItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.IncludePatternRule} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IncludePatternRuleItemProvider includePatternRuleItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.IncludePatternRule}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createIncludePatternRuleAdapter ()
    {
        if ( this.includePatternRuleItemProvider == null )
        {
            this.includePatternRuleItemProvider = new IncludePatternRuleItemProvider ( this );
        }

        return this.includePatternRuleItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.ExcludePatternRule} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExcludePatternRuleItemProvider excludePatternRuleItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.ExcludePatternRule}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createExcludePatternRuleAdapter ()
    {
        if ( this.excludePatternRuleItemProvider == null )
        {
            this.excludePatternRuleItemProvider = new ExcludePatternRuleItemProvider ( this );
        }

        return this.excludePatternRuleItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.ResourceFactory} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ResourceFactoryItemProvider resourceFactoryItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.ResourceFactory}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createResourceFactoryAdapter ()
    {
        if ( this.resourceFactoryItemProvider == null )
        {
            this.resourceFactoryItemProvider = new ResourceFactoryItemProvider ( this );
        }

        return this.resourceFactoryItemProvider;
    }

    /**
     * This keeps track of the one adapter used for all
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry} instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtensionMappingEntryItemProvider extensionMappingEntryItemProvider;

    /**
     * This creates an adapter for a
     * {@link org.openscada.doc.model.doc.map.ExtensionMappingEntry}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter createExtensionMappingEntryAdapter ()
    {
        if ( this.extensionMappingEntryItemProvider == null )
        {
            this.extensionMappingEntryItemProvider = new ExtensionMappingEntryItemProvider ( this );
        }

        return this.extensionMappingEntryItemProvider;
    }

    /**
     * This returns the root adapter factory that contains this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ComposeableAdapterFactory getRootAdapterFactory ()
    {
        return this.parentAdapterFactory == null ? this : this.parentAdapterFactory.getRootAdapterFactory ();
    }

    /**
     * This sets the composed adapter factory that contains this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setParentAdapterFactory ( final ComposedAdapterFactory parentAdapterFactory )
    {
        this.parentAdapterFactory = parentAdapterFactory;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isFactoryForType ( final Object type )
    {
        return this.supportedTypes.contains ( type ) || super.isFactoryForType ( type );
    }

    /**
     * This implementation substitutes the factory itself as the key for the
     * adapter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Adapter adapt ( final Notifier notifier, final Object type )
    {
        return super.adapt ( notifier, this );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object adapt ( final Object object, final Object type )
    {
        if ( isFactoryForType ( type ) )
        {
            final Object adapter = super.adapt ( object, type );
            if ( ! ( type instanceof Class<?> ) || ( (Class<?>)type ).isInstance ( adapter ) )
            {
                return adapter;
            }
        }

        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public List<IChildCreationExtender> getChildCreationExtenders ()
    {
        return this.childCreationExtenderManager.getChildCreationExtenders ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Collection<?> getNewChildDescriptors ( final Object object, final EditingDomain editingDomain )
    {
        return this.childCreationExtenderManager.getNewChildDescriptors ( object, editingDomain );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator ()
    {
        return this.childCreationExtenderManager;
    }

    /**
     * This adds a listener.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void addListener ( final INotifyChangedListener notifyChangedListener )
    {
        this.changeNotifier.addListener ( notifyChangedListener );
    }

    /**
     * This removes a listener.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void removeListener ( final INotifyChangedListener notifyChangedListener )
    {
        this.changeNotifier.removeListener ( notifyChangedListener );
    }

    /**
     * This delegates to {@link #changeNotifier} and to
     * {@link #parentAdapterFactory}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void fireNotifyChanged ( final Notification notification )
    {
        this.changeNotifier.fireNotifyChanged ( notification );

        if ( this.parentAdapterFactory != null )
        {
            this.parentAdapterFactory.fireNotifyChanged ( notification );
        }
    }

    /**
     * This disposes all of the item providers created by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void dispose ()
    {
        if ( this.mapItemProvider != null )
        {
            this.mapItemProvider.dispose ();
        }
        if ( this.fileItemProvider != null )
        {
            this.fileItemProvider.dispose ();
        }
        if ( this.mapSectionItemProvider != null )
        {
            this.mapSectionItemProvider.dispose ();
        }
        if ( this.featureItemProvider != null )
        {
            this.featureItemProvider.dispose ();
        }
        if ( this.includePatternRuleItemProvider != null )
        {
            this.includePatternRuleItemProvider.dispose ();
        }
        if ( this.excludePatternRuleItemProvider != null )
        {
            this.excludePatternRuleItemProvider.dispose ();
        }
        if ( this.resourceFactoryItemProvider != null )
        {
            this.resourceFactoryItemProvider.dispose ();
        }
        if ( this.extensionMappingEntryItemProvider != null )
        {
            this.extensionMappingEntryItemProvider.dispose ();
        }
    }

}
