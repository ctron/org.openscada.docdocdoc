package org.openscada.doc.extractor.jars;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.CanReadFileFilter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;

public class JarFileVisitor
{
    private final File basePath;

    private final boolean recurse;

    private final AndFileFilter jarFilter;

    private final AndFileFilter dirFilter;

    public JarFileVisitor ( final File basePath, final boolean recurse )
    {
        this.basePath = basePath;
        this.recurse = recurse;

        this.jarFilter = new AndFileFilter ();

        this.jarFilter.addFileFilter ( FileFileFilter.FILE );
        this.jarFilter.addFileFilter ( CanReadFileFilter.CAN_READ );
        this.jarFilter.addFileFilter ( new SuffixFileFilter ( "jar", IOCase.INSENSITIVE ) );

        this.dirFilter = new AndFileFilter ();
        this.dirFilter.addFileFilter ( DirectoryFileFilter.DIRECTORY );
        this.dirFilter.addFileFilter ( CanReadFileFilter.CAN_READ );
    }

    public void visit ( final Visitor visitor ) throws Exception
    {
        visit ( visitor, this.basePath );
    }

    private void visit ( final Visitor visitor, final File path ) throws Exception
    {
        if ( !path.isDirectory () )
        {
            throw new IllegalArgumentException ( String.format ( "'%s' is not a directory", path ) );
        }

        for ( final File file : path.listFiles ( (FileFilter)this.jarFilter ) )
        {
            processJarFile ( visitor, file );
        }
        if ( this.recurse )
        {
            for ( final File file : path.listFiles ( (FileFilter)this.dirFilter ) )
            {
                visit ( visitor, file );
            }
        }
    }

    private void processJarFile ( final Visitor visitor, final File file ) throws IOException
    {
        final JarFile jarFile = new JarFile ( file );
        try
        {
            final Manifest m = jarFile.getManifest ();

            final String version = m.getMainAttributes ().getValue ( Attributes.Name.IMPLEMENTATION_VERSION );
            visitor.visit ( file, jarFile, new JarInformation ( version ) );
        }
        finally
        {
            jarFile.close ();
        }
    }
}
