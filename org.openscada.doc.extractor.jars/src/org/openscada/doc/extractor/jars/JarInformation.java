package org.openscada.doc.extractor.jars;

public class JarInformation
{
    private final String version;

    public JarInformation ( final String version )
    {
        this.version = version;
    }

    public String getVersion ()
    {
        return this.version;
    }

    private static final String NL = System.getProperty ( "line.separator", "\n" );

    @Override
    public String toString ()
    {
        final StringBuilder sb = new StringBuilder ();

        sb.append ( "[" );
        if ( this.version != null )
        {
            sb.append ( NL + "version: " + this.version );
        }
        sb.append ( "]" );

        return sb.toString ();
    }
}
