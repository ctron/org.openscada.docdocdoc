package org.openscada.doc.extractor.jars;

import java.io.File;
import java.util.jar.JarFile;

public class Application
{
    public static void main ( final String[] args ) throws Exception
    {
        final JarFileVisitor visitor = new JarFileVisitor ( new File ( args[0] ), false );
        visitor.visit ( new Visitor () {

            @Override
            public void visit ( final File file, final JarFile jarFile, final JarInformation jarInformation )
            {
                System.out.println ( String.format ( "%s - %s", file, jarFile.getName () ) );
                System.out.println ( jarInformation );
            }
        } );
    }
}
