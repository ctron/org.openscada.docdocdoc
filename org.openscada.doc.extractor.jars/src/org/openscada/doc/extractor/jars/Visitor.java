package org.openscada.doc.extractor.jars;

import java.io.File;
import java.util.jar.JarFile;

public interface Visitor
{
    public void visit ( File file, JarFile jarFile, JarInformation jarInformation );
}
