/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder.license;

import java.util.Locale;
import java.util.Properties;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * A license processor for a custom license
 * <p>
 * The following system properties must be set:
 * </p>
 * <ul>
 * <li>custom.license.copyrightText</li>
 * <li>custom.license.sectionTitle</li>
 * <li>custom.license.text</li>
 * </ul>
 */
public class Custom extends AbstractLicenseProcessor
{

    @Override
    public void applyLicense ( final Book book, final Locale locale, final Properties properties )
    {
        final BookSection section = getSection ( book, locale );

        final String copyrightText = properties.getProperty ( "custom.license.copyrightText" ); //$NON-NLS-1$
        final String sectionTitle = properties.getProperty ( "custom.license.sectionTitle" ); //$NON-NLS-1$
        final String text = properties.getProperty ( "custom.license.text" ); //$NON-NLS-1$

        book.setCopyrightText ( copyrightText );

        if ( text == null || text.isEmpty () )
        {
            // remove extra section
            EcoreUtil.remove ( section );
        }
        else
        {
            if ( sectionTitle != null && !sectionTitle.isEmpty () )
            {
                section.setTitle ( sectionTitle );
            }

            // fill section

            final PlainTextContent content = FragmentFactory.eINSTANCE.createPlainTextContent ();
            content.setValue ( text );
            section.getContent ().add ( content );
        }
    }

}
