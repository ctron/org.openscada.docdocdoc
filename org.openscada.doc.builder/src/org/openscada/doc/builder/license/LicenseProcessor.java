/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder.license;

import java.util.Locale;
import java.util.Properties;

import org.openscada.doc.model.doc.book.Book;

/**
 * The license processor applies a license to a book
 * <p>
 * The implementation should re-use a section with id
 * <q>license</q> for placing the license text. If a license processor does not
 * add a specific license chapter it still should remove that section. If the
 * section does not exist, it should be added as last section to be book with id
 * <q>license</q>.
 * </p>
 * 
 * @author Jens Reimann
 */
public interface LicenseProcessor
{
    public void applyLicense ( Book book, Locale locale, Properties properties );
}
