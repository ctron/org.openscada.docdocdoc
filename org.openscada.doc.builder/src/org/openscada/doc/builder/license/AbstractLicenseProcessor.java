/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder.license;

import java.util.Locale;
import java.util.ResourceBundle;

import org.openscada.doc.builder.Books;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.BookSection;

public abstract class AbstractLicenseProcessor implements LicenseProcessor
{

    protected BookSection getSection ( final Book book, final Locale locale )
    {
        final ResourceBundle defaultBundle = ResourceBundle.getBundle ( "org.openscada.doc.builder.license.resource.default", locale );

        BookSection section = Books.findSectionById ( book, "license" ); //$NON-NLS-1$
        if ( section == null )
        {
            section = BookFactory.eINSTANCE.createBookSection ();
            section.setId ( "license" ); //$NON-NLS-1$
            section.setTitle ( defaultBundle.getString ( "sectionTitle" ) ); //$NON-NLS-1$
            section.setNumber ( Books.lastSectionNumber ( book ) + 1 );
            book.getSections ().add ( section );
        }

        return section;
    }
}