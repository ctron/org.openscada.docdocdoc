/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder.license;

import java.util.Locale;
import java.util.ResourceBundle;

public class FDLv13 extends AbstractBundleLicenseProcessor
{
    @Override
    protected ResourceBundle getBundle ( final Locale locale )
    {
        return ResourceBundle.getBundle ( "org.openscada.doc.builder.license.resource.fdlv13", locale );
    }

}
