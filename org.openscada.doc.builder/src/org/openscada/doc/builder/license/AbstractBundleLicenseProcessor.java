/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder.license;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

public abstract class AbstractBundleLicenseProcessor extends AbstractLicenseProcessor implements LicenseProcessor
{

    protected abstract ResourceBundle getBundle ( Locale locale );

    @Override
    public void applyLicense ( final Book book, final Locale locale, final Properties properties )
    {
        final ResourceBundle bundle = getBundle ( locale );

        final BookSection section = getSection ( book, locale );

        book.setCopyrightText ( bundle.getString ( "copyright" ) ); //$NON-NLS-1$
        section.setTitle ( bundle.getString ( "sectionTitle" ) ); //$NON-NLS-1$

        final PlainTextContent content = FragmentFactory.eINSTANCE.createPlainTextContent ();
        content.setValue ( bundle.getString ( "license" ) ); //$NON-NLS-1$
        section.getContent ().add ( content );
    }
}
