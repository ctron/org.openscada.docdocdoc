/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.openscada.doc.builder.license.LicenseProcessor;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;
import org.openscada.doc.model.doc.book.util.BookResourceFactoryImpl;
import org.openscada.doc.model.doc.map.Visitor;
import org.openscada.utils.str.StringHelper;
import org.openscada.utils.str.StringReplacer;

public class BookBuilder
{
    private final org.openscada.doc.model.doc.book.builder.BookBuilder builder;

    private final Properties properties;

    public BookBuilder ( final org.openscada.doc.model.doc.book.builder.BookBuilder builder, final Properties properties )
    {
        this.builder = builder;

        this.properties = new Properties ( properties );
        for ( final PropertyEntry entry : builder.getProperties () )
        {
            this.properties.put ( entry.getKey (), entry.getValue () );
        }
    }

    public Book build ( final IProgressMonitor monitor, final URI uri, final Locale locale ) throws Exception
    {
        monitor.beginTask ( "Building book", 10 );

        final Book book = BookFactory.eINSTANCE.createBook ();

        final List<VisitorProcessor> visitorProcessors = findVisitorProcessors ();

        final Visitor visitor = new VisitorImplementation ( book, visitorProcessors, this.properties );

        book.setTitle ( this.builder.getTitle () );
        book.setVersion ( makeVersion () );
        book.setNumberingStyle ( this.builder.getNumberingStyle () );
        monitor.worked ( 1 );

        monitor.setTaskName ( "Creating legal section" );
        makeLegalSection ( book );
        monitor.worked ( 1 );

        monitor.setTaskName ( "Processing" );
        this.builder.visit ( visitor );
        monitor.worked ( 1 );

        applyLicense ( this.builder.getLicense (), book, locale );
        monitor.worked ( 1 );

        // save

        final ResourceSet resourceSet = new ResourceSetImpl ();
        final Resource resource = new BookResourceFactoryImpl ().createResource ( uri );
        resourceSet.getResources ().add ( resource );

        runPostProcess ( new SubProgressMonitor ( monitor, 1 ), book );

        generateSectionNumbers ( new SubProgressMonitor ( monitor, 1 ), book );

        resource.getContents ().add ( book );

        monitor.setTaskName ( "Saving content" );
        resource.save ( null );

        return book;
    }

    private void generateSectionNumbers ( final IProgressMonitor monitor, final Book book )
    {
        try
        {
            monitor.beginTask ( "Creating section numbers", IProgressMonitor.UNKNOWN );
            new BookVisitor ( book ).visitSections ( new SectionNumberGenerator ( this.properties ) );
        }
        finally
        {
            monitor.done ();
        }
    }

    private void makeLegalSection ( final Book book )
    {
        book.setCopyrightMarker ( this.builder.getCopyrightMarker () );

        // for now, use what we got
        final Copier copier = new Copier ();
        book.getAuthors ().addAll ( copier.copyAll ( this.builder.getAuthors () ) );
        book.getCopyright ().addAll ( copier.copyAll ( this.builder.getCopyright () ) );
        copier.copyReferences ();
    }

    private void applyLicense ( String licenseId, final Book book, final Locale locale ) throws CoreException
    {
        if ( licenseId == null )
        {
            licenseId = "none";
        }

        final Set<String> ids = new HashSet<String> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( "org.openscada.doc.builder.bookBuilder" ) )
        {
            if ( !"licenseProcessor".equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "id" );

            ids.add ( id );

            if ( !id.equalsIgnoreCase ( licenseId ) )
            {
                continue;
            }

            final LicenseProcessor processor = (LicenseProcessor)ele.createExecutableExtension ( "class" );
            processor.applyLicense ( book, locale, this.properties );
            return;
        }

        throw new IllegalStateException ( String.format ( "License with id '%s' was not defined. Known licenses are: %s", licenseId, StringHelper.join ( ids, ", " ) ) );
    }

    private String makeVersion ()
    {
        final String str = this.builder.getVersion ();

        if ( str == null )
        {
            return null;
        }

        final String q = String.format ( "%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", new Date () );

        final Properties properties = new Properties ( this.properties );
        properties.put ( "qualifier", q );

        return StringReplacer.replace ( str, properties );
    }

    protected void runPostProcess ( final IProgressMonitor monitor, final Book book ) throws Exception
    {
        try
        {
            final List<PostProcessorEntry> procs = findPostProcessors ();

            monitor.beginTask ( "Running post processors", procs.size () );

            for ( final PostProcessorEntry proc : procs )
            {
                proc.postProcessor.postProcess ( monitor, book );
                monitor.worked ( 1 );
            }
        }
        finally
        {
            monitor.done ();
        }
    }

    private static final class PostProcessorEntry implements Comparable<PostProcessorEntry>
    {
        private final PostProcessor postProcessor;

        private final int priority;

        public PostProcessorEntry ( final PostProcessor postProcessor, final int priority )
        {
            this.postProcessor = postProcessor;
            this.priority = priority;
        }

        @Override
        public int compareTo ( final PostProcessorEntry o )
        {
            return this.priority < o.priority ? -1 : this.priority == o.priority ? 0 : 1;
        }
    }

    private List<PostProcessorEntry> findPostProcessors () throws CoreException
    {
        final LinkedList<PostProcessorEntry> result = new LinkedList<PostProcessorEntry> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( "org.openscada.doc.builder.bookBuilder" ) )
        {
            if ( !"postProcessor".equals ( ele.getName () ) )
            {
                continue;
            }

            int priority = Integer.MAX_VALUE;
            try
            {
                priority = Integer.parseInt ( ele.getAttribute ( "priority" ) );
            }
            catch ( final Exception e )
            {
            }

            final Object o = ele.createExecutableExtension ( "class" );
            if ( o instanceof PostProcessor )
            {
                result.add ( new PostProcessorEntry ( (PostProcessor)o, priority ) );
            }
        }

        Collections.sort ( result );

        return result;
    }

    private List<VisitorProcessor> findVisitorProcessors () throws CoreException
    {
        final LinkedList<VisitorProcessor> result = new LinkedList<VisitorProcessor> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( "org.openscada.doc.builder.bookBuilder" ) )
        {
            if ( !"visitProcessor".equals ( ele.getName () ) )
            {
                continue;
            }

            final Object o = ele.createExecutableExtension ( "class" );
            if ( o instanceof VisitorProcessor )
            {
                result.add ( (VisitorProcessor)o );
            }
        }

        return result;
    }
}
