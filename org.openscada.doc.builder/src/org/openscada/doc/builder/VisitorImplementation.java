/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import java.util.List;
import java.util.Properties;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.map.MapSection;
import org.openscada.doc.model.doc.map.Visitor;

final class VisitorImplementation implements Visitor
{
    private final Stack<BookContainer> sectionContainer = new Stack<BookContainer> ();

    private final Stack<AtomicInteger> sectionNumberStack = new Stack<AtomicInteger> ();

    private final List<VisitorProcessor> processors;

    public VisitorImplementation ( final Book book, final List<VisitorProcessor> processors, final Properties props )
    {
        this.processors = processors;

        this.sectionNumberStack.push ( new AtomicInteger ( 0 ) );
        this.sectionContainer.push ( book );
    }

    @Override
    public void openSection ( final MapSection mapSection )
    {
        final BookSection section = BookFactory.eINSTANCE.createBookSection ();

        section.setNumber ( this.sectionNumberStack.peek ().incrementAndGet () );
        section.setTitle ( mapSection.getTitle () );
        section.setNumberingStyle ( mapSection.getNumberingStyle () );
        section.setId ( mapSection.getId () );

        this.sectionContainer.peek ().getSections ().add ( section );
        this.sectionContainer.push ( section );

        this.sectionNumberStack.push ( new AtomicInteger ( 0 ) );
    }

    @Override
    public void closeSection ( final MapSection mapSection )
    {
        this.sectionContainer.pop ();
        this.sectionNumberStack.pop ();
    }

    @Override
    public void visit ( final URI uri, final Content content ) throws Exception
    {
        for ( final VisitorProcessor proc : this.processors )
        {
            proc.visit ( uri, content );
        }

        this.sectionContainer.peek ().getContent ().add ( EcoreUtil.copy ( content ) );
    }
}