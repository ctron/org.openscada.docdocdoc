package org.openscada.doc.builder;

import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookSection;

public final class Books
{
    public static class SectionFinder implements BookVisitor.SectionVisitor
    {
        private final String sectionId;

        private BookSection section;

        public SectionFinder ( final String sectionId )
        {
            this.sectionId = sectionId;
        }

        @Override
        public void visitSection ( final BookSection section )
        {
            if ( this.sectionId == null )
            {
                return;
            }

            if ( this.sectionId.equals ( section.getId () ) )
            {
                this.section = section;
            }
        }

        public BookSection getSection ()
        {
            return this.section;
        }
    }

    private Books ()
    {
    }

    /**
     * Find the last section number of 0 if the book is empty
     * 
     * @param book
     *            the book to check
     * @return the last section number or zero, never a negative number
     */
    public static int lastSectionNumber ( final Book book )
    {
        int n = 0;

        // we need to check all since they might not be ordered, although they should be
        for ( final BookSection section : book.getSections () )
        {
            n = Math.max ( section.getNumber (), n );
        }

        return n;
    }

    public static BookSection findSectionById ( final Book book, final String id )
    {
        final SectionFinder finder = new SectionFinder ( id );
        new BookVisitor ( book ).visitSections ( finder );
        return finder.getSection ();
    }
}
