/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import org.eclipse.core.runtime.IProgressMonitor;
import org.openscada.doc.model.doc.book.Book;

public interface PostProcessor
{
    public void postProcess ( IProgressMonitor monitor, Book book ) throws Exception;
}
