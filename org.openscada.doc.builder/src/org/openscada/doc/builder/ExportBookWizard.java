/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ISetSelectionTarget;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.ui.databinding.SelectionHelper;
import org.openscada.ui.utils.status.StatusHelper;

public class ExportBookWizard extends Wizard implements IExportWizard
{

    private org.openscada.doc.model.doc.book.builder.BookBuilder builder;

    private FilePage filePage;

    private IWorkbench workbench;

    public ExportBookWizard ()
    {
        setNeedsProgressMonitor ( true );
    }

    @Override
    public void init ( final IWorkbench workbench, final IStructuredSelection selection )
    {
        this.workbench = workbench;
        this.builder = load ( SelectionHelper.first ( selection, IResource.class ) );
        addPage ( this.filePage = new FilePage ( "filePage", selection ) );
    }

    private org.openscada.doc.model.doc.book.builder.BookBuilder load ( final IResource first )
    {
        final ResourceSet resourceSet = new ResourceSetImpl ();

        final URI fileURI = URI.createPlatformResourceURI ( first.getFullPath ().toString (), true );
        final Resource resource = resourceSet.createResource ( fileURI );

        final java.util.Map<Object, Object> options = new HashMap<Object, Object> ();
        try
        {
            resource.load ( options );
        }
        catch ( final IOException e )
        {
            final IStatus status = StatusHelper.convertStatus ( Activator.PLUGIN_ID, e );
            StatusManager.getManager ().handle ( status, StatusManager.LOG );
            ErrorDialog.openError ( getShell (), "Error", "Failed to load book builder", status );
            return null;
        }

        return (org.openscada.doc.model.doc.book.builder.BookBuilder)resource.getContents ().get ( 0 );
    }

    @Override
    public boolean canFinish ()
    {
        return super.canFinish () && this.builder != null;
    }

    @Override
    public boolean performFinish ()
    {
        try
        {
            doFinish ();
        }
        catch ( final Exception e )
        {
            final IStatus status = StatusHelper.convertStatus ( Activator.PLUGIN_ID, e );
            StatusManager.getManager ().handle ( status, StatusManager.LOG );
            ErrorDialog.openError ( getShell (), "Error", "Failed to generate book", status );
            return false;
        }

        return true;
    }

    protected Properties getProperties ()
    {
        return System.getProperties ();
    }

    private void doFinish () throws Exception
    {
        final IFile path = this.filePage.getExportFile ();

        final URI uri = URI.createPlatformResourceURI ( path.getFullPath ().toString (), true );

        final WorkspaceModifyOperation operation = new WorkspaceModifyOperation () {
            @Override
            protected void execute ( final IProgressMonitor monitor ) throws CoreException, InvocationTargetException, InterruptedException
            {
                try
                {
                    new BookBuilder ( ExportBookWizard.this.builder, getProperties () ).build ( monitor, uri, Locale.getDefault () );
                }
                catch ( final CoreException e )
                {
                    throw e;
                }
                catch ( final InvocationTargetException e )
                {
                    throw e;
                }
                catch ( final Exception e )
                {
                    throw new InvocationTargetException ( e );
                }
                finally
                {
                    monitor.done ();
                }
            }
        };

        getContainer ().run ( false, false, operation );

        // Select the new file resource in the current view.
        //
        final IWorkbenchWindow workbenchWindow = this.workbench.getActiveWorkbenchWindow ();
        final IWorkbenchPage page = workbenchWindow.getActivePage ();
        final IWorkbenchPart activePart = page.getActivePart ();
        if ( activePart instanceof ISetSelectionTarget )
        {
            final ISelection targetSelection = new StructuredSelection ( path );
            getShell ().getDisplay ().asyncExec ( new Runnable () {
                @Override
                public void run ()
                {
                    ( (ISetSelectionTarget)activePart ).selectReveal ( targetSelection );
                }
            } );
        }

        page.openEditor ( new FileEditorInput ( path ), this.workbench.getEditorRegistry ().getDefaultEditor ( path.getFullPath ().toString () ).getId () );
    }

    private class FilePage extends org.eclipse.ui.dialogs.WizardNewFileCreationPage
    {

        protected FilePage ( final String pageName, final IStructuredSelection selection )
        {
            super ( pageName, selection );
            setFileName ( "merged.book" );
            setAllowExistingResources ( true );
            setFileExtension ( "book" );
            setTitle ( "Select the file into which to generate to book" );
        }

        public IFile getExportFile ()
        {
            return ResourcesPlugin.getWorkspace ().getRoot ().getFile ( getContainerFullPath ().append ( getFileName () ) );
        }
    }
}
