/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.openscada.doc.builder.BookVisitor.SectionVisitor;
import org.openscada.doc.builder.internal.Latin;
import org.openscada.doc.builder.internal.Roman;
import org.openscada.doc.model.doc.NumberingStyle;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.ui.databinding.AdapterHelper;
import org.openscada.utils.str.StringHelper;

public class SectionNumberGenerator implements SectionVisitor
{

    private final Properties properties;

    private final String delimiter;

    public SectionNumberGenerator ( final Properties properties )
    {
        this.properties = properties;
        this.delimiter = this.properties.getProperty ( "org.openscada.doc.builder.sectionNumberDelimiter", "." );
    }

    @Override
    public void visitSection ( final BookSection section )
    {
        final List<String> toks = new LinkedList<String> ();

        BookSection currentSection = section;
        while ( currentSection != null )
        {
            toks.add ( 0, generateNumber ( findNumberingStyle ( currentSection ), currentSection.getNumber () ) );
            currentSection = AdapterHelper.adapt ( currentSection.eContainer (), BookSection.class );
        }

        section.setFullNumber ( StringHelper.join ( toks, this.delimiter ) );
        if ( section.getId () == null || section.getId ().isEmpty () )
        {
            section.setId ( section.getFullNumber () );
        }
    }

    /**
     * Find the numbering style to use for this section
     * <p>
     * Note that the numbering style for this section is not stored in the field
     * {@link BookSection#getNumberingStyle()} since this only defines the
     * numbering style for contained sections!
     * </p>
     * 
     * @param section
     *            the section for which to find the numbering style
     * @return the numbering style, may be <code>null</code> if none was defined
     *         by any parent
     */
    public static NumberingStyle findNumberingStyle ( final BookSection section )
    {
        if ( section == null )
        {
            return null;
        }

        BookContainer currentSection = section;
        do
        {
            currentSection = AdapterHelper.adapt ( currentSection.eContainer (), BookContainer.class );
            if ( currentSection == null )
            {
                // we reached the top
                return null;
            }
        } while ( currentSection.getNumberingStyle () == null );

        return currentSection.getNumberingStyle ();
    }

    /**
     * Generate a number string from the section number and the numbering style
     * 
     * @param style
     *            the style to use
     * @param number
     *            the number to convert
     * @return the result, must never be <code>null</code>
     */
    public static String generateNumber ( final NumberingStyle style, final int number )
    {
        if ( style == null )
        {
            return String.format ( "%d", number );
        }

        switch ( style )
        {
            case ARABIC:
                return String.format ( "%d", number );
            case LATIN:
                return Latin.toUpperLatinNumber ( number );
            case ROMAN:
                return Roman.int2roman ( number );
        }

        // default, not in case to trigger warning in case
        return String.format ( "%d", number );
    }

}
