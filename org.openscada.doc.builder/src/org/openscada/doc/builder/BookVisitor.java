/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.builder;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookSection;

public class BookVisitor
{
    private final Book book;

    public interface SectionVisitor
    {
        public void visitSection ( BookSection section );
    }

    public BookVisitor ( final Book book )
    {
        this.book = book;
    }

    public void visitSections ( final SectionVisitor visitor )
    {
        final TreeIterator<EObject> i = this.book.eAllContents ();
        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( o instanceof BookSection )
            {
                visitor.visitSection ( (BookSection)o );
            }
        }
    }
}
