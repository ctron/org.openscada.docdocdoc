package org.openscada.doc.extractor.jdt.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.openscada.doc.content.properties.DataType;
import org.openscada.doc.content.properties.PropertiesFactory;
import org.openscada.doc.content.properties.Property;
import org.openscada.ui.databinding.SelectionHelper;

public class GeneratePropertiesAction implements IObjectActionDelegate
{
    private List<IJavaProject> projects;

    @Override
    public void run ( final IAction action )
    {
        final Job job = new Job ( "Extracting system properties information" ) {

            @Override
            protected IStatus run ( final IProgressMonitor monitor )
            {
                try
                {
                    GeneratePropertiesAction.this.run ( monitor );
                }
                finally
                {
                    monitor.done ();
                }
                return Status.OK_STATUS;
            }
        };

        job.setSystem ( false );
        job.setUser ( true );
        job.schedule ();
    }

    protected void run ( final IProgressMonitor monitor )
    {
        monitor.beginTask ( "Extracting system properties", this.projects.size () );

        for ( final IJavaProject project : this.projects )
        {
            SafeRunner.run ( new SafeRunnable () {

                @Override
                public void run () throws Exception
                {
                    monitor.setTaskName ( "Scanning project: " + project.getProject ().getName () );
                    runProject ( project );
                    monitor.worked ( 1 );
                }
            } );
        }
    }

    private void runProject ( final IJavaProject project ) throws Exception
    {
        final Collection<Property> properties = new LinkedList<Property> ();

        for ( final IPackageFragment frag : project.getPackageFragments () )
        {
            for ( final ICompilationUnit cu : frag.getCompilationUnits () )
            {
                final ASTParser c = ASTParser.newParser ( AST.JLS4 );
                c.setSource ( cu );
                c.setResolveBindings ( true );

                final CompilationUnit result = (CompilationUnit)c.createAST ( null );

                result.accept ( new ASTVisitor () {
                    @Override
                    public boolean visit ( final MethodInvocation node )
                    {
                        visitMethodInvocation ( node, properties );

                        return true;
                    }
                } );
            }
        }

        // store
        if ( !properties.isEmpty () )
        {
            final PropertiesRecorder recorder = new PropertiesRecorder ( Activator.getDefault ().getBundle ().getBundleContext (), project.getProject () );
            try
            {
                recorder.store ( properties );
            }
            finally
            {
                recorder.dispose ();
            }
        }
    }

    @Override
    public void selectionChanged ( final IAction action, final ISelection selection )
    {
        this.projects = SelectionHelper.list ( selection, IJavaProject.class );
    }

    @Override
    public void setActivePart ( final IAction action, final IWorkbenchPart targetPart )
    {
    }

    private void visitMethodInvocation ( final MethodInvocation node, final Collection<Property> properties )
    {
        final Property property = makeProperty ( node );
        if ( property != null )
        {
            properties.add ( property );
        }
    }

    private Property makeProperty ( final MethodInvocation node )
    {
        final SimpleName name = node.getName ();

        final IBinding binding = name.resolveBinding ();

        String typeName = null;

        if ( binding == null )
        {
            return null;
        }

        if ( ! ( binding instanceof IMethodBinding ) )
        {
            return null;
        }

        typeName = ( (IMethodBinding)binding ).getDeclaringClass ().getQualifiedName ();

        final Object para1 = node.arguments ().size () > 0 ? makeLiteral ( node.arguments ().get ( 0 ) ) : null;
        final Object literalDefault = node.arguments ().size () > 1 ? makeLiteral ( node.arguments ().get ( 1 ) ) : null;

        final String methodName = name.getFullyQualifiedName ();
        if ( methodName == null )
        {
            return null;
        }

        if ( "java.lang.System".equals ( typeName ) && "getProperty".equals ( methodName ) )
        {
            if ( para1 instanceof String )
            {
                return createProperty ( (String)para1, DataType.STRING, literalDefault );
            }
        }
        else if ( "java.lang.Integer".equals ( typeName ) && "getInteger".equals ( methodName ) )
        {
            if ( para1 instanceof String )
            {
                return createProperty ( (String)para1, DataType.INTEGER, literalDefault );
            }
        }
        else if ( "java.lang.Long".equals ( typeName ) && "getLong".equals ( methodName ) )
        {
            if ( para1 instanceof String )
            {
                return createProperty ( (String)para1, DataType.LONG, literalDefault );
            }
        }
        else if ( "java.lang.Boolean".equals ( typeName ) && "getBoolean".equals ( methodName ) )
        {
            if ( para1 instanceof String )
            {
                return createProperty ( (String)para1, DataType.BOOLEAN, Boolean.FALSE );
            }
        }

        // finally
        return null;
    }

    private Object makeLiteral ( final Object value )
    {
        if ( value instanceof Expression )
        {
            // check if we have a constant expression value
            final Object cev = ( (Expression)value ).resolveConstantExpressionValue ();
            if ( cev != null )
            {
                return cev;
            }
        }

        if ( value instanceof Name )
        {
            final IBinding b = ( (Name)value ).resolveBinding ();
            if ( b instanceof IVariableBinding )
            {
                final Object cv = ( (IVariableBinding)b ).getConstantValue ();
                if ( cv != null )
                {
                    return cv;
                }
            }
        }

        if ( value instanceof MethodInvocation )
        {
            final Property property = makeProperty ( (MethodInvocation)value );
            if ( property != null )
            {
                return property;
            }
        }

        return null;
    }

    private Property createProperty ( final String name, final DataType dataType, final Object literalDefault )
    {
        final Property p = PropertiesFactory.eINSTANCE.createProperty ();

        p.setName ( name );
        p.setDataType ( dataType );

        if ( literalDefault instanceof Property )
        {
            p.setDefaultValue ( "Defaults to system property: " + ( (Property)literalDefault ).getName () );
        }
        else if ( literalDefault != null )
        {
            p.setDefaultValue ( "" + literalDefault );
        }

        return p;
    }

}
