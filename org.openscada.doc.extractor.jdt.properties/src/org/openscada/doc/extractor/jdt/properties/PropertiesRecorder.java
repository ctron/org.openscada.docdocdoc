package org.openscada.doc.extractor.jdt.properties;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.pde.core.project.IBundleProjectDescription;
import org.eclipse.pde.core.project.IBundleProjectService;
import org.openscada.doc.content.properties.DataType;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesFactory;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.Property;
import org.openscada.ui.utils.status.StatusHelper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class PropertiesRecorder
{
    private final IProject project;

    private final BundleContext context;

    private ServiceReference<IBundleProjectService> ref;

    private IBundleProjectService service;

    public PropertiesRecorder ( final BundleContext context, final IProject project )
    {
        this.context = context;
        this.project = project;

        initializeServices ();
    }

    public void dispose ()
    {
        this.service = null;
        this.context.ungetService ( this.ref );
    }

    public void store ( final Collection<Property> properties ) throws CoreException
    {

        try
        {
            processStore ( properties );
        }
        catch ( final CoreException e )
        {
            throw e;
        }
        catch ( final Exception e )
        {
            throw new CoreException ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
        }
    }

    private void initializeServices ()
    {
        this.ref = this.context.getServiceReference ( IBundleProjectService.class );
        this.service = this.context.getService ( this.ref );
    }

    private void processStore ( final Collection<Property> properties ) throws Exception
    {
        if ( properties.isEmpty () )
        {
            return;
        }

        final ResourceSet rs = new ResourceSetImpl ();

        final IFile file = this.project.getFile ( "properties.propdocx" );

        final Resource r = rs.createResource ( URI.createURI ( file.getLocationURI ().toString () ) );

        if ( file.exists () )
        {
            r.load ( null );
        }

        if ( r.getContents ().isEmpty () )
        {
            // create new
            final Properties p = PropertiesFactory.eINSTANCE.createProperties ();
            merge ( properties, p );
            r.getContents ().add ( p );
        }
        else
        {
            for ( final EObject o : r.getContents () )
            {
                if ( o instanceof Properties )
                {
                    merge ( properties, (Properties)o );
                }
            }
        }

        final Map<Object, Object> options = new HashMap<Object, Object> ();
        options.put ( Resource.OPTION_SAVE_ONLY_IF_CHANGED, Boolean.TRUE );
        r.save ( options );

        file.refreshLocal ( IResource.DEPTH_INFINITE, null );
    }

    private void merge ( final Collection<Property> properties, final Properties currentProperties )
    {
        String group = null;
        try
        {
            final IBundleProjectDescription desc = this.service.getDescription ( this.project );

            if ( desc != null )
            {
                group = desc.getSymbolicName ();
            }
        }
        catch ( final CoreException e )
        {
            // failed to load group id
        }

        for ( final Property property : properties )
        {
            mergeProperty ( group, property, currentProperties );
        }
    }

    private void mergeProperty ( final String group, final Property property, final Properties currentProperties )
    {
        if ( group != null )
        {
            for ( final PropertiesGroup pg : currentProperties.getGroups () )
            {
                if ( pg.getPrefix ().equals ( group ) )
                {
                    mergeInto ( property, pg.getProperties (), currentProperties.getProperties () );
                    return;
                }
            }

            // create new group
            final PropertiesGroup pg = PropertiesFactory.eINSTANCE.createPropertiesGroup ();
            pg.setPrefix ( group );

            mergeInto ( property, pg.getProperties (), currentProperties.getProperties () );

            if ( !pg.getProperties ().isEmpty () )
            {
                // only add if we really hold an entry
                currentProperties.getGroups ().add ( pg );
            }
        }
        else
        {
            mergeInto ( property, currentProperties.getProperties (), null );
        }
    }

    private void mergeInto ( final Property property, final EList<Property> properties, final EList<Property> gobalProperties )
    {
        final String name = property.getName ();

        final Property globalProperty = findProperty ( name, gobalProperties );
        if ( globalProperty != null )
        {
            // merge into global
            mergeInto ( globalProperty, property );
            return;
        }

        for ( final Property cp : properties )
        {
            // merge into group
            if ( cp.getName ().equals ( name ) )
            {
                mergeInto ( cp, property );
                return;
            }
        }

        // add new
        properties.add ( EcoreUtil.copy ( property ) );
    }

    private void mergeInto ( final Property currentProperty, final Property property )
    {
        if ( currentProperty.getDataType () != DataType.CUSTOM && property.getDataType () != null )
        {
            currentProperty.setDataType ( property.getDataType () );
        }
    }

    private Property findProperty ( final String name, final List<Property> properties )
    {
        for ( final Property cp : properties )
        {
            return cp;
        }
        return null;
    }
}
