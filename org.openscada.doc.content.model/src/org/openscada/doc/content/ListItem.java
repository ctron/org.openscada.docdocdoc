/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List Item</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getListItem()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface ListItem extends TextContainer
{
} // ListItem
