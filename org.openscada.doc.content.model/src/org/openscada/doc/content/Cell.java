/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getCell()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface Cell extends TextContainer
{
} // Cell
