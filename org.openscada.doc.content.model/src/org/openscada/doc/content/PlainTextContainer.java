/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plain Text Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The plain text container can only hold plain text elements and no more sub-elements.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.PlainTextContainer#getContent <em>Content</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getPlainTextContainer()
 * @model abstract="true"
 *        extendedMetaData="kind='mixed'"
 * @generated
 */
public interface PlainTextContainer extends Content
{
    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute list.
     * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Content</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Content</em>' attribute list.
     * @see org.openscada.doc.content.ContentPackage#getPlainTextContainer_Content()
     * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
     *        extendedMetaData="name=':mixed' kind='elementWildcard'"
     * @generated
     */
    FeatureMap getContent ();

} // PlainTextContainer
