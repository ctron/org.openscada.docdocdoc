/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.openscada.doc.model.doc.fragment.FragmentPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.openscada.doc.content.ContentFactory
 * @model kind="package"
 * @generated
 */
public interface ContentPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "content";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "urn:openscada:doc:content";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "content";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    ContentPackage eINSTANCE = org.openscada.doc.content.impl.ContentPackageImpl.init();

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.TextContainerImpl <em>Text Container</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.TextContainerImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getTextContainer()
     * @generated
     */
    int TEXT_CONTAINER = 4;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ParagraphImpl <em>Paragraph</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ParagraphImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getParagraph()
     * @generated
     */
    int PARAGRAPH = 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.FragmentImpl <em>Fragment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.FragmentImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getFragment()
     * @generated
     */
    int FRAGMENT = 20;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.SpanImpl <em>Span</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.SpanImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getSpan()
     * @generated
     */
    int SPAN = 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.EmphasisImpl <em>Emphasis</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.EmphasisImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getEmphasis()
     * @generated
     */
    int EMPHASIS = 3;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ImageImpl <em>Image</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ImageImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getImage()
     * @generated
     */
    int IMAGE = 5;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.PlainTextContainerImpl <em>Plain Text Container</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.PlainTextContainerImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getPlainTextContainer()
     * @generated
     */
    int PLAIN_TEXT_CONTAINER = 7;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PLAIN_TEXT_CONTAINER__CONTENT = FragmentPackage.CONTENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Plain Text Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PLAIN_TEXT_CONTAINER_FEATURE_COUNT = FragmentPackage.CONTENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__CONTENT = PLAIN_TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__GROUP_CONTENT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__EMPHASIS = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__IMAGE = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__OBJECT_NAME = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__XREF = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__LINK = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER__QUOTE = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>Text Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TEXT_CONTAINER_FEATURE_COUNT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__CONTENT = TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__GROUP_CONTENT = TEXT_CONTAINER__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__EMPHASIS = TEXT_CONTAINER__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__IMAGE = TEXT_CONTAINER__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__OBJECT_NAME = TEXT_CONTAINER__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__XREF = TEXT_CONTAINER__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__LINK = TEXT_CONTAINER__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH__QUOTE = TEXT_CONTAINER__QUOTE;

    /**
     * The number of structural features of the '<em>Paragraph</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int PARAGRAPH_FEATURE_COUNT = TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.ContentContainer <em>Container</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.ContentContainer
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getContentContainer()
     * @generated
     */
    int CONTENT_CONTAINER = 1;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__CONTENT = 0;

    /**
     * The feature id for the '<em><b>P</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__P = 1;

    /**
     * The feature id for the '<em><b>Code</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__CODE = 2;

    /**
     * The feature id for the '<em><b>Plain Text</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__PLAIN_TEXT = 3;

    /**
     * The feature id for the '<em><b>Ul</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__UL = 4;

    /**
     * The feature id for the '<em><b>Simple Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__SIMPLE_TABLE = 5;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__SECTION = 6;

    /**
     * The feature id for the '<em><b>Structure Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER__STRUCTURE_TABLE = 7;

    /**
     * The number of structural features of the '<em>Container</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTENT_CONTAINER_FEATURE_COUNT = 8;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__CONTENT = TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__GROUP_CONTENT = TEXT_CONTAINER__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__EMPHASIS = TEXT_CONTAINER__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__IMAGE = TEXT_CONTAINER__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__OBJECT_NAME = TEXT_CONTAINER__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__XREF = TEXT_CONTAINER__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__LINK = TEXT_CONTAINER__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN__QUOTE = TEXT_CONTAINER__QUOTE;

    /**
     * The number of structural features of the '<em>Span</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SPAN_FEATURE_COUNT = TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__CONTENT = SPAN__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__GROUP_CONTENT = SPAN__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__EMPHASIS = SPAN__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__IMAGE = SPAN__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__OBJECT_NAME = SPAN__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__XREF = SPAN__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__LINK = SPAN__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS__QUOTE = SPAN__QUOTE;

    /**
     * The number of structural features of the '<em>Emphasis</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EMPHASIS_FEATURE_COUNT = SPAN_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int IMAGE__SOURCE = 0;

    /**
     * The feature id for the '<em><b>Data</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int IMAGE__DATA = 1;

    /**
     * The number of structural features of the '<em>Image</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int IMAGE_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ObjectNameImpl <em>Object Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ObjectNameImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getObjectName()
     * @generated
     */
    int OBJECT_NAME = 6;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OBJECT_NAME__CONTENT = PLAIN_TEXT_CONTAINER__CONTENT;

    /**
     * The number of structural features of the '<em>Object Name</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int OBJECT_NAME_FEATURE_COUNT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.CodeParagraphImpl <em>Code Paragraph</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.CodeParagraphImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getCodeParagraph()
     * @generated
     */
    int CODE_PARAGRAPH = 8;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CODE_PARAGRAPH__CONTENT = PLAIN_TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Language</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CODE_PARAGRAPH__LANGUAGE = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Code Paragraph</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CODE_PARAGRAPH_FEATURE_COUNT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.CrossReferenceImpl <em>Cross Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.CrossReferenceImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getCrossReference()
     * @generated
     */
    int CROSS_REFERENCE = 9;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CROSS_REFERENCE__CONTENT = PLAIN_TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CROSS_REFERENCE__TYPE = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CROSS_REFERENCE__ID = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Cross Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CROSS_REFERENCE_FEATURE_COUNT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.LinkImpl <em>Link</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.LinkImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getLink()
     * @generated
     */
    int LINK = 10;

    /**
     * The feature id for the '<em><b>Ref</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LINK__REF = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LINK__NAME = 1;

    /**
     * The number of structural features of the '<em>Link</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LINK_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.QuoteImpl <em>Quote</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.QuoteImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getQuote()
     * @generated
     */
    int QUOTE = 11;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__CONTENT = SPAN__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__GROUP_CONTENT = SPAN__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__EMPHASIS = SPAN__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__IMAGE = SPAN__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__OBJECT_NAME = SPAN__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__XREF = SPAN__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__LINK = SPAN__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE__QUOTE = SPAN__QUOTE;

    /**
     * The number of structural features of the '<em>Quote</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int QUOTE_FEATURE_COUNT = SPAN_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ListImpl <em>List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ListImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getList()
     * @generated
     */
    int LIST = 12;

    /**
     * The feature id for the '<em><b>Item</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST__ITEM = 0;

    /**
     * The number of structural features of the '<em>List</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ListItemImpl <em>List Item</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ListItemImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getListItem()
     * @generated
     */
    int LIST_ITEM = 13;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__CONTENT = TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__GROUP_CONTENT = TEXT_CONTAINER__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__EMPHASIS = TEXT_CONTAINER__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__IMAGE = TEXT_CONTAINER__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__OBJECT_NAME = TEXT_CONTAINER__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__XREF = TEXT_CONTAINER__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__LINK = TEXT_CONTAINER__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM__QUOTE = TEXT_CONTAINER__QUOTE;

    /**
     * The number of structural features of the '<em>List Item</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LIST_ITEM_FEATURE_COUNT = TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.UnorderedListImpl <em>Unordered List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.UnorderedListImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getUnorderedList()
     * @generated
     */
    int UNORDERED_LIST = 14;

    /**
     * The feature id for the '<em><b>Item</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UNORDERED_LIST__ITEM = LIST__ITEM;

    /**
     * The number of structural features of the '<em>Unordered List</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int UNORDERED_LIST_FEATURE_COUNT = LIST_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.Table <em>Table</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.Table
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getTable()
     * @generated
     */
    int TABLE = 22;

    /**
     * The feature id for the '<em><b>Maximize</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TABLE__MAXIMIZE = FragmentPackage.CONTENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TABLE__CAPTION = FragmentPackage.CONTENT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Table</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int TABLE_FEATURE_COUNT = FragmentPackage.CONTENT_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.SimpleTableImpl <em>Simple Table</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.SimpleTableImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getSimpleTable()
     * @generated
     */
    int SIMPLE_TABLE = 15;

    /**
     * The feature id for the '<em><b>Maximize</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SIMPLE_TABLE__MAXIMIZE = TABLE__MAXIMIZE;

    /**
     * The feature id for the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SIMPLE_TABLE__CAPTION = TABLE__CAPTION;

    /**
     * The feature id for the '<em><b>Column</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SIMPLE_TABLE__COLUMN = TABLE_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Row</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SIMPLE_TABLE__ROW = TABLE_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Simple Table</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SIMPLE_TABLE_FEATURE_COUNT = TABLE_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.ColumnImpl <em>Column</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.ColumnImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getColumn()
     * @generated
     */
    int COLUMN = 16;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COLUMN__TITLE = 0;

    /**
     * The feature id for the '<em><b>Horizontal Align</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COLUMN__HORIZONTAL_ALIGN = 1;

    /**
     * The number of structural features of the '<em>Column</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COLUMN_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.RowImpl <em>Row</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.RowImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getRow()
     * @generated
     */
    int ROW = 17;

    /**
     * The feature id for the '<em><b>Cell</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ROW__CELL = 0;

    /**
     * The number of structural features of the '<em>Row</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ROW_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.CellImpl <em>Cell</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.CellImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getCell()
     * @generated
     */
    int CELL = 18;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__CONTENT = TEXT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>Group Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__GROUP_CONTENT = TEXT_CONTAINER__GROUP_CONTENT;

    /**
     * The feature id for the '<em><b>Emphasis</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__EMPHASIS = TEXT_CONTAINER__EMPHASIS;

    /**
     * The feature id for the '<em><b>Image</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__IMAGE = TEXT_CONTAINER__IMAGE;

    /**
     * The feature id for the '<em><b>Object Name</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__OBJECT_NAME = TEXT_CONTAINER__OBJECT_NAME;

    /**
     * The feature id for the '<em><b>Xref</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__XREF = TEXT_CONTAINER__XREF;

    /**
     * The feature id for the '<em><b>Link</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__LINK = TEXT_CONTAINER__LINK;

    /**
     * The feature id for the '<em><b>Quote</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL__QUOTE = TEXT_CONTAINER__QUOTE;

    /**
     * The number of structural features of the '<em>Cell</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CELL_FEATURE_COUNT = TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.SectionImpl <em>Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.SectionImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getSection()
     * @generated
     */
    int SECTION = 19;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__CONTENT = CONTENT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>P</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__P = CONTENT_CONTAINER__P;

    /**
     * The feature id for the '<em><b>Code</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__CODE = CONTENT_CONTAINER__CODE;

    /**
     * The feature id for the '<em><b>Plain Text</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__PLAIN_TEXT = CONTENT_CONTAINER__PLAIN_TEXT;

    /**
     * The feature id for the '<em><b>Ul</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__UL = CONTENT_CONTAINER__UL;

    /**
     * The feature id for the '<em><b>Simple Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__SIMPLE_TABLE = CONTENT_CONTAINER__SIMPLE_TABLE;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__SECTION = CONTENT_CONTAINER__SECTION;

    /**
     * The feature id for the '<em><b>Structure Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__STRUCTURE_TABLE = CONTENT_CONTAINER__STRUCTURE_TABLE;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__ID = CONTENT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION__TITLE = CONTENT_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Section</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int SECTION_FEATURE_COUNT = CONTENT_CONTAINER_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__CONTENT = CONTENT_CONTAINER__CONTENT;

    /**
     * The feature id for the '<em><b>P</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__P = CONTENT_CONTAINER__P;

    /**
     * The feature id for the '<em><b>Code</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__CODE = CONTENT_CONTAINER__CODE;

    /**
     * The feature id for the '<em><b>Plain Text</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__PLAIN_TEXT = CONTENT_CONTAINER__PLAIN_TEXT;

    /**
     * The feature id for the '<em><b>Ul</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__UL = CONTENT_CONTAINER__UL;

    /**
     * The feature id for the '<em><b>Simple Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__SIMPLE_TABLE = CONTENT_CONTAINER__SIMPLE_TABLE;

    /**
     * The feature id for the '<em><b>Section</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__SECTION = CONTENT_CONTAINER__SECTION;

    /**
     * The feature id for the '<em><b>Structure Table</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT__STRUCTURE_TABLE = CONTENT_CONTAINER__STRUCTURE_TABLE;

    /**
     * The number of structural features of the '<em>Fragment</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FRAGMENT_FEATURE_COUNT = CONTENT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.StructureTableImpl <em>Structure Table</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.StructureTableImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureTable()
     * @generated
     */
    int STRUCTURE_TABLE = 21;

    /**
     * The feature id for the '<em><b>Maximize</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE__MAXIMIZE = TABLE__MAXIMIZE;

    /**
     * The feature id for the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE__CAPTION = TABLE__CAPTION;

    /**
     * The feature id for the '<em><b>Entry</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE__ENTRY = TABLE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Structure Table</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_TABLE_FEATURE_COUNT = TABLE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.StructureEntryImpl <em>Structure Entry</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.StructureEntryImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureEntry()
     * @generated
     */
    int STRUCTURE_ENTRY = 23;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY__NAME = 0;

    /**
     * The feature id for the '<em><b>Size</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY__SIZE = 1;

    /**
     * The feature id for the '<em><b>Type Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY__TYPE_NAME = 2;

    /**
     * The feature id for the '<em><b>Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY__DESCRIPTION = 3;

    /**
     * The feature id for the '<em><b>Size Field</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY__SIZE_FIELD = 4;

    /**
     * The number of structural features of the '<em>Structure Entry</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.impl.StructureEntryDescriptionImpl <em>Structure Entry Description</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.impl.StructureEntryDescriptionImpl
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureEntryDescription()
     * @generated
     */
    int STRUCTURE_ENTRY_DESCRIPTION = 24;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY_DESCRIPTION__CONTENT = PLAIN_TEXT_CONTAINER__CONTENT;

    /**
     * The number of structural features of the '<em>Structure Entry Description</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STRUCTURE_ENTRY_DESCRIPTION_FEATURE_COUNT = PLAIN_TEXT_CONTAINER_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link org.openscada.doc.content.HorizontalAlign <em>Horizontal Align</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.doc.content.HorizontalAlign
     * @see org.openscada.doc.content.impl.ContentPackageImpl#getHorizontalAlign()
     * @generated
     */
    int HORIZONTAL_ALIGN = 25;

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Paragraph <em>Paragraph</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Paragraph</em>'.
     * @see org.openscada.doc.content.Paragraph
     * @generated
     */
    EClass getParagraph ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.ContentContainer <em>Container</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Container</em>'.
     * @see org.openscada.doc.content.ContentContainer
     * @generated
     */
    EClass getContentContainer ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getP <em>P</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>P</em>'.
     * @see org.openscada.doc.content.ContentContainer#getP()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_P ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getCode <em>Code</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Code</em>'.
     * @see org.openscada.doc.content.ContentContainer#getCode()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_Code ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getPlainText <em>Plain Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Plain Text</em>'.
     * @see org.openscada.doc.content.ContentContainer#getPlainText()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_PlainText ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getUl <em>Ul</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Ul</em>'.
     * @see org.openscada.doc.content.ContentContainer#getUl()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_Ul ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getSimpleTable <em>Simple Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Simple Table</em>'.
     * @see org.openscada.doc.content.ContentContainer#getSimpleTable()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_SimpleTable ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getSection <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Section</em>'.
     * @see org.openscada.doc.content.ContentContainer#getSection()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_Section ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.ContentContainer#getStructureTable <em>Structure Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Structure Table</em>'.
     * @see org.openscada.doc.content.ContentContainer#getStructureTable()
     * @see #getContentContainer()
     * @generated
     */
    EReference getContentContainer_StructureTable();

    /**
     * Returns the meta object for the attribute list '{@link org.openscada.doc.content.ContentContainer#getContent <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Content</em>'.
     * @see org.openscada.doc.content.ContentContainer#getContent()
     * @see #getContentContainer()
     * @generated
     */
    EAttribute getContentContainer_Content ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Fragment <em>Fragment</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Fragment</em>'.
     * @see org.openscada.doc.content.Fragment
     * @generated
     */
    EClass getFragment ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.StructureTable <em>Structure Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Structure Table</em>'.
     * @see org.openscada.doc.content.StructureTable
     * @generated
     */
    EClass getStructureTable();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.StructureTable#getEntry <em>Entry</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Entry</em>'.
     * @see org.openscada.doc.content.StructureTable#getEntry()
     * @see #getStructureTable()
     * @generated
     */
    EReference getStructureTable_Entry();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Table <em>Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Table</em>'.
     * @see org.openscada.doc.content.Table
     * @generated
     */
    EClass getTable();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Table#isMaximize <em>Maximize</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Maximize</em>'.
     * @see org.openscada.doc.content.Table#isMaximize()
     * @see #getTable()
     * @generated
     */
    EAttribute getTable_Maximize();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Table#getCaption <em>Caption</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Caption</em>'.
     * @see org.openscada.doc.content.Table#getCaption()
     * @see #getTable()
     * @generated
     */
    EAttribute getTable_Caption();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.StructureEntry <em>Structure Entry</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Structure Entry</em>'.
     * @see org.openscada.doc.content.StructureEntry
     * @generated
     */
    EClass getStructureEntry();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.StructureEntry#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.openscada.doc.content.StructureEntry#getName()
     * @see #getStructureEntry()
     * @generated
     */
    EAttribute getStructureEntry_Name();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.StructureEntry#getSize <em>Size</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Size</em>'.
     * @see org.openscada.doc.content.StructureEntry#getSize()
     * @see #getStructureEntry()
     * @generated
     */
    EAttribute getStructureEntry_Size();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.StructureEntry#getTypeName <em>Type Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type Name</em>'.
     * @see org.openscada.doc.content.StructureEntry#getTypeName()
     * @see #getStructureEntry()
     * @generated
     */
    EAttribute getStructureEntry_TypeName();

    /**
     * Returns the meta object for the containment reference '{@link org.openscada.doc.content.StructureEntry#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Description</em>'.
     * @see org.openscada.doc.content.StructureEntry#getDescription()
     * @see #getStructureEntry()
     * @generated
     */
    EReference getStructureEntry_Description();

    /**
     * Returns the meta object for the reference '{@link org.openscada.doc.content.StructureEntry#getSizeField <em>Size Field</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Size Field</em>'.
     * @see org.openscada.doc.content.StructureEntry#getSizeField()
     * @see #getStructureEntry()
     * @generated
     */
    EReference getStructureEntry_SizeField();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.StructureEntryDescription <em>Structure Entry Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Structure Entry Description</em>'.
     * @see org.openscada.doc.content.StructureEntryDescription
     * @generated
     */
    EClass getStructureEntryDescription();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Span <em>Span</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Span</em>'.
     * @see org.openscada.doc.content.Span
     * @generated
     */
    EClass getSpan ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Emphasis <em>Emphasis</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Emphasis</em>'.
     * @see org.openscada.doc.content.Emphasis
     * @generated
     */
    EClass getEmphasis ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.TextContainer <em>Text Container</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Text Container</em>'.
     * @see org.openscada.doc.content.TextContainer
     * @generated
     */
    EClass getTextContainer ();

    /**
     * Returns the meta object for the attribute list '{@link org.openscada.doc.content.TextContainer#getGroupContent <em>Group Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Group Content</em>'.
     * @see org.openscada.doc.content.TextContainer#getGroupContent()
     * @see #getTextContainer()
     * @generated
     */
    EAttribute getTextContainer_GroupContent ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getEmphasis <em>Emphasis</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Emphasis</em>'.
     * @see org.openscada.doc.content.TextContainer#getEmphasis()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_Emphasis ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getImage <em>Image</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Image</em>'.
     * @see org.openscada.doc.content.TextContainer#getImage()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_Image ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getObjectName <em>Object Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Object Name</em>'.
     * @see org.openscada.doc.content.TextContainer#getObjectName()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_ObjectName ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getXref <em>Xref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Xref</em>'.
     * @see org.openscada.doc.content.TextContainer#getXref()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_Xref ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getLink <em>Link</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Link</em>'.
     * @see org.openscada.doc.content.TextContainer#getLink()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_Link ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.TextContainer#getQuote <em>Quote</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Quote</em>'.
     * @see org.openscada.doc.content.TextContainer#getQuote()
     * @see #getTextContainer()
     * @generated
     */
    EReference getTextContainer_Quote ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Image <em>Image</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Image</em>'.
     * @see org.openscada.doc.content.Image
     * @generated
     */
    EClass getImage ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Image#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Source</em>'.
     * @see org.openscada.doc.content.Image#getSource()
     * @see #getImage()
     * @generated
     */
    EAttribute getImage_Source ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Image#getData <em>Data</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Data</em>'.
     * @see org.openscada.doc.content.Image#getData()
     * @see #getImage()
     * @generated
     */
    EAttribute getImage_Data ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.ObjectName <em>Object Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Object Name</em>'.
     * @see org.openscada.doc.content.ObjectName
     * @generated
     */
    EClass getObjectName ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.PlainTextContainer <em>Plain Text Container</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Plain Text Container</em>'.
     * @see org.openscada.doc.content.PlainTextContainer
     * @generated
     */
    EClass getPlainTextContainer ();

    /**
     * Returns the meta object for the attribute list '{@link org.openscada.doc.content.PlainTextContainer#getContent <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Content</em>'.
     * @see org.openscada.doc.content.PlainTextContainer#getContent()
     * @see #getPlainTextContainer()
     * @generated
     */
    EAttribute getPlainTextContainer_Content ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.CodeParagraph <em>Code Paragraph</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Code Paragraph</em>'.
     * @see org.openscada.doc.content.CodeParagraph
     * @generated
     */
    EClass getCodeParagraph ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.CodeParagraph#getLanguage <em>Language</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Language</em>'.
     * @see org.openscada.doc.content.CodeParagraph#getLanguage()
     * @see #getCodeParagraph()
     * @generated
     */
    EAttribute getCodeParagraph_Language ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.CrossReference <em>Cross Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Cross Reference</em>'.
     * @see org.openscada.doc.content.CrossReference
     * @generated
     */
    EClass getCrossReference ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.CrossReference#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see org.openscada.doc.content.CrossReference#getType()
     * @see #getCrossReference()
     * @generated
     */
    EAttribute getCrossReference_Type ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.CrossReference#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see org.openscada.doc.content.CrossReference#getId()
     * @see #getCrossReference()
     * @generated
     */
    EAttribute getCrossReference_Id ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Link <em>Link</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Link</em>'.
     * @see org.openscada.doc.content.Link
     * @generated
     */
    EClass getLink ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Link#getRef <em>Ref</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Ref</em>'.
     * @see org.openscada.doc.content.Link#getRef()
     * @see #getLink()
     * @generated
     */
    EAttribute getLink_Ref ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Link#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.openscada.doc.content.Link#getName()
     * @see #getLink()
     * @generated
     */
    EAttribute getLink_Name ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Quote <em>Quote</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Quote</em>'.
     * @see org.openscada.doc.content.Quote
     * @generated
     */
    EClass getQuote ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.List <em>List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>List</em>'.
     * @see org.openscada.doc.content.List
     * @generated
     */
    EClass getList ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.List#getItem <em>Item</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Item</em>'.
     * @see org.openscada.doc.content.List#getItem()
     * @see #getList()
     * @generated
     */
    EReference getList_Item ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.ListItem <em>List Item</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>List Item</em>'.
     * @see org.openscada.doc.content.ListItem
     * @generated
     */
    EClass getListItem ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.UnorderedList <em>Unordered List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Unordered List</em>'.
     * @see org.openscada.doc.content.UnorderedList
     * @generated
     */
    EClass getUnorderedList ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.SimpleTable <em>Simple Table</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Simple Table</em>'.
     * @see org.openscada.doc.content.SimpleTable
     * @generated
     */
    EClass getSimpleTable ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.SimpleTable#getRow <em>Row</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Row</em>'.
     * @see org.openscada.doc.content.SimpleTable#getRow()
     * @see #getSimpleTable()
     * @generated
     */
    EReference getSimpleTable_Row ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.SimpleTable#getColumn <em>Column</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Column</em>'.
     * @see org.openscada.doc.content.SimpleTable#getColumn()
     * @see #getSimpleTable()
     * @generated
     */
    EReference getSimpleTable_Column ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Column <em>Column</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Column</em>'.
     * @see org.openscada.doc.content.Column
     * @generated
     */
    EClass getColumn ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Column#getTitle <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.content.Column#getTitle()
     * @see #getColumn()
     * @generated
     */
    EAttribute getColumn_Title ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Column#getHorizontalAlign <em>Horizontal Align</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Horizontal Align</em>'.
     * @see org.openscada.doc.content.Column#getHorizontalAlign()
     * @see #getColumn()
     * @generated
     */
    EAttribute getColumn_HorizontalAlign ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Row <em>Row</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Row</em>'.
     * @see org.openscada.doc.content.Row
     * @generated
     */
    EClass getRow ();

    /**
     * Returns the meta object for the containment reference list '{@link org.openscada.doc.content.Row#getCell <em>Cell</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Cell</em>'.
     * @see org.openscada.doc.content.Row#getCell()
     * @see #getRow()
     * @generated
     */
    EReference getRow_Cell ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Cell <em>Cell</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Cell</em>'.
     * @see org.openscada.doc.content.Cell
     * @generated
     */
    EClass getCell ();

    /**
     * Returns the meta object for class '{@link org.openscada.doc.content.Section <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Section</em>'.
     * @see org.openscada.doc.content.Section
     * @generated
     */
    EClass getSection ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Section#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see org.openscada.doc.content.Section#getId()
     * @see #getSection()
     * @generated
     */
    EAttribute getSection_Id();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.doc.content.Section#getTitle <em>Title</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see org.openscada.doc.content.Section#getTitle()
     * @see #getSection()
     * @generated
     */
    EAttribute getSection_Title ();

    /**
     * Returns the meta object for enum '{@link org.openscada.doc.content.HorizontalAlign <em>Horizontal Align</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Horizontal Align</em>'.
     * @see org.openscada.doc.content.HorizontalAlign
     * @generated
     */
    EEnum getHorizontalAlign ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ContentFactory getContentFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ParagraphImpl <em>Paragraph</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ParagraphImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getParagraph()
         * @generated
         */
        EClass PARAGRAPH = eINSTANCE.getParagraph();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.ContentContainer <em>Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.ContentContainer
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getContentContainer()
         * @generated
         */
        EClass CONTENT_CONTAINER = eINSTANCE.getContentContainer();

        /**
         * The meta object literal for the '<em><b>P</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__P = eINSTANCE.getContentContainer_P();

        /**
         * The meta object literal for the '<em><b>Code</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__CODE = eINSTANCE.getContentContainer_Code();

        /**
         * The meta object literal for the '<em><b>Plain Text</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__PLAIN_TEXT = eINSTANCE.getContentContainer_PlainText();

        /**
         * The meta object literal for the '<em><b>Ul</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__UL = eINSTANCE.getContentContainer_Ul();

        /**
         * The meta object literal for the '<em><b>Simple Table</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__SIMPLE_TABLE = eINSTANCE.getContentContainer_SimpleTable();

        /**
         * The meta object literal for the '<em><b>Section</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__SECTION = eINSTANCE.getContentContainer_Section();

        /**
         * The meta object literal for the '<em><b>Structure Table</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTENT_CONTAINER__STRUCTURE_TABLE = eINSTANCE.getContentContainer_StructureTable();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTENT_CONTAINER__CONTENT = eINSTANCE.getContentContainer_Content();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.FragmentImpl <em>Fragment</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.FragmentImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getFragment()
         * @generated
         */
        EClass FRAGMENT = eINSTANCE.getFragment();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.StructureTableImpl <em>Structure Table</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.StructureTableImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureTable()
         * @generated
         */
        EClass STRUCTURE_TABLE = eINSTANCE.getStructureTable();

        /**
         * The meta object literal for the '<em><b>Entry</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference STRUCTURE_TABLE__ENTRY = eINSTANCE.getStructureTable_Entry();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.Table <em>Table</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.Table
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getTable()
         * @generated
         */
        EClass TABLE = eINSTANCE.getTable();

        /**
         * The meta object literal for the '<em><b>Maximize</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute TABLE__MAXIMIZE = eINSTANCE.getTable_Maximize();

        /**
         * The meta object literal for the '<em><b>Caption</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute TABLE__CAPTION = eINSTANCE.getTable_Caption();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.StructureEntryImpl <em>Structure Entry</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.StructureEntryImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureEntry()
         * @generated
         */
        EClass STRUCTURE_ENTRY = eINSTANCE.getStructureEntry();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STRUCTURE_ENTRY__NAME = eINSTANCE.getStructureEntry_Name();

        /**
         * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STRUCTURE_ENTRY__SIZE = eINSTANCE.getStructureEntry_Size();

        /**
         * The meta object literal for the '<em><b>Type Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STRUCTURE_ENTRY__TYPE_NAME = eINSTANCE.getStructureEntry_TypeName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference STRUCTURE_ENTRY__DESCRIPTION = eINSTANCE.getStructureEntry_Description();

        /**
         * The meta object literal for the '<em><b>Size Field</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference STRUCTURE_ENTRY__SIZE_FIELD = eINSTANCE.getStructureEntry_SizeField();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.StructureEntryDescriptionImpl <em>Structure Entry Description</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.StructureEntryDescriptionImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getStructureEntryDescription()
         * @generated
         */
        EClass STRUCTURE_ENTRY_DESCRIPTION = eINSTANCE.getStructureEntryDescription();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.SpanImpl <em>Span</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.SpanImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getSpan()
         * @generated
         */
        EClass SPAN = eINSTANCE.getSpan();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.EmphasisImpl <em>Emphasis</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.EmphasisImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getEmphasis()
         * @generated
         */
        EClass EMPHASIS = eINSTANCE.getEmphasis();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.TextContainerImpl <em>Text Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.TextContainerImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getTextContainer()
         * @generated
         */
        EClass TEXT_CONTAINER = eINSTANCE.getTextContainer();

        /**
         * The meta object literal for the '<em><b>Group Content</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute TEXT_CONTAINER__GROUP_CONTENT = eINSTANCE.getTextContainer_GroupContent();

        /**
         * The meta object literal for the '<em><b>Emphasis</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__EMPHASIS = eINSTANCE.getTextContainer_Emphasis();

        /**
         * The meta object literal for the '<em><b>Image</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__IMAGE = eINSTANCE.getTextContainer_Image();

        /**
         * The meta object literal for the '<em><b>Object Name</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__OBJECT_NAME = eINSTANCE.getTextContainer_ObjectName();

        /**
         * The meta object literal for the '<em><b>Xref</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__XREF = eINSTANCE.getTextContainer_Xref();

        /**
         * The meta object literal for the '<em><b>Link</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__LINK = eINSTANCE.getTextContainer_Link();

        /**
         * The meta object literal for the '<em><b>Quote</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference TEXT_CONTAINER__QUOTE = eINSTANCE.getTextContainer_Quote();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ImageImpl <em>Image</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ImageImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getImage()
         * @generated
         */
        EClass IMAGE = eINSTANCE.getImage();

        /**
         * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute IMAGE__SOURCE = eINSTANCE.getImage_Source();

        /**
         * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute IMAGE__DATA = eINSTANCE.getImage_Data();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ObjectNameImpl <em>Object Name</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ObjectNameImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getObjectName()
         * @generated
         */
        EClass OBJECT_NAME = eINSTANCE.getObjectName();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.PlainTextContainerImpl <em>Plain Text Container</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.PlainTextContainerImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getPlainTextContainer()
         * @generated
         */
        EClass PLAIN_TEXT_CONTAINER = eINSTANCE.getPlainTextContainer();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute PLAIN_TEXT_CONTAINER__CONTENT = eINSTANCE.getPlainTextContainer_Content();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.CodeParagraphImpl <em>Code Paragraph</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.CodeParagraphImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getCodeParagraph()
         * @generated
         */
        EClass CODE_PARAGRAPH = eINSTANCE.getCodeParagraph();

        /**
         * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CODE_PARAGRAPH__LANGUAGE = eINSTANCE.getCodeParagraph_Language();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.CrossReferenceImpl <em>Cross Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.CrossReferenceImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getCrossReference()
         * @generated
         */
        EClass CROSS_REFERENCE = eINSTANCE.getCrossReference();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CROSS_REFERENCE__TYPE = eINSTANCE.getCrossReference_Type();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CROSS_REFERENCE__ID = eINSTANCE.getCrossReference_Id();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.LinkImpl <em>Link</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.LinkImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getLink()
         * @generated
         */
        EClass LINK = eINSTANCE.getLink();

        /**
         * The meta object literal for the '<em><b>Ref</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LINK__REF = eINSTANCE.getLink_Ref();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LINK__NAME = eINSTANCE.getLink_Name();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.QuoteImpl <em>Quote</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.QuoteImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getQuote()
         * @generated
         */
        EClass QUOTE = eINSTANCE.getQuote();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ListImpl <em>List</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ListImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getList()
         * @generated
         */
        EClass LIST = eINSTANCE.getList();

        /**
         * The meta object literal for the '<em><b>Item</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference LIST__ITEM = eINSTANCE.getList_Item();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ListItemImpl <em>List Item</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ListItemImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getListItem()
         * @generated
         */
        EClass LIST_ITEM = eINSTANCE.getListItem();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.UnorderedListImpl <em>Unordered List</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.UnorderedListImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getUnorderedList()
         * @generated
         */
        EClass UNORDERED_LIST = eINSTANCE.getUnorderedList();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.SimpleTableImpl <em>Simple Table</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.SimpleTableImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getSimpleTable()
         * @generated
         */
        EClass SIMPLE_TABLE = eINSTANCE.getSimpleTable();

        /**
         * The meta object literal for the '<em><b>Row</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference SIMPLE_TABLE__ROW = eINSTANCE.getSimpleTable_Row();

        /**
         * The meta object literal for the '<em><b>Column</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference SIMPLE_TABLE__COLUMN = eINSTANCE.getSimpleTable_Column();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.ColumnImpl <em>Column</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.ColumnImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getColumn()
         * @generated
         */
        EClass COLUMN = eINSTANCE.getColumn();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COLUMN__TITLE = eINSTANCE.getColumn_Title();

        /**
         * The meta object literal for the '<em><b>Horizontal Align</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COLUMN__HORIZONTAL_ALIGN = eINSTANCE.getColumn_HorizontalAlign();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.RowImpl <em>Row</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.RowImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getRow()
         * @generated
         */
        EClass ROW = eINSTANCE.getRow();

        /**
         * The meta object literal for the '<em><b>Cell</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ROW__CELL = eINSTANCE.getRow_Cell();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.CellImpl <em>Cell</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.CellImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getCell()
         * @generated
         */
        EClass CELL = eINSTANCE.getCell();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.impl.SectionImpl <em>Section</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.impl.SectionImpl
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getSection()
         * @generated
         */
        EClass SECTION = eINSTANCE.getSection();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute SECTION__ID = eINSTANCE.getSection_Id();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute SECTION__TITLE = eINSTANCE.getSection_Title();

        /**
         * The meta object literal for the '{@link org.openscada.doc.content.HorizontalAlign <em>Horizontal Align</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.doc.content.HorizontalAlign
         * @see org.openscada.doc.content.impl.ContentPackageImpl#getHorizontalAlign()
         * @generated
         */
        EEnum HORIZONTAL_ALIGN = eINSTANCE.getHorizontalAlign();

    }

} //ContentPackage
