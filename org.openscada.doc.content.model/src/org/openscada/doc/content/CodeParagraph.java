/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Paragraph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.CodeParagraph#getLanguage <em>Language</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getCodeParagraph()
 * @model extendedMetaData="name='CodeParagraph' kind='mixed'"
 * @generated
 */
public interface CodeParagraph extends PlainTextContainer
{
    /**
     * Returns the value of the '<em><b>Language</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Language</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Language</em>' attribute.
     * @see #setLanguage(String)
     * @see org.openscada.doc.content.ContentPackage#getCodeParagraph_Language()
     * @model extendedMetaData="name='language' kind='attribute' namespace='##targetNamespace'"
     * @generated
     */
    String getLanguage ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.CodeParagraph#getLanguage <em>Language</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language</em>' attribute.
     * @see #getLanguage()
     * @generated
     */
    void setLanguage ( String value );

} // CodeParagraph
