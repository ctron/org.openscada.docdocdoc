/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A simple table with columns that have headings on top and rows which contain data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.SimpleTable#getColumn <em>Column</em>}</li>
 *   <li>{@link org.openscada.doc.content.SimpleTable#getRow <em>Row</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getSimpleTable()
 * @model
 * @generated
 */
public interface SimpleTable extends Table
{
    /**
     * Returns the value of the '<em><b>Row</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Row}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Row</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Row</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getSimpleTable_Row()
     * @model containment="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element'"
     * @generated
     */
    EList<Row> getRow ();

    /**
     * Returns the value of the '<em><b>Column</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Column}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Column</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Column</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getSimpleTable_Column()
     * @model containment="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element'"
     * @generated
     */
    EList<Column> getColumn ();

} // SimpleTable
