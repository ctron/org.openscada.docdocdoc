/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Span</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getSpan()
 * @model abstract="true"
 *        extendedMetaData="kind='mixed'"
 * @generated
 */
public interface Span extends TextContainer
{
} // Span
