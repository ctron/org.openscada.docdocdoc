/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentContainer;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.List;
import org.openscada.doc.content.ListItem;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.Span;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureEntryDescription;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.Table;
import org.openscada.doc.content.TextContainer;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the
 * model.
 * <!-- end-user-doc -->
 * @see org.openscada.doc.content.ContentPackage
 * @generated
 */
public class ContentAdapterFactory extends AdapterFactoryImpl
{
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static ContentPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContentAdapterFactory ()
    {
        if (modelPackage == null)
        {
            modelPackage = ContentPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the
     * model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType ( Object object )
    {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ContentSwitch<Adapter> modelSwitch = new ContentSwitch<Adapter>()
        {
            @Override
            public Adapter caseParagraph(Paragraph object)
            {
                return createParagraphAdapter();
            }
            @Override
            public Adapter caseContentContainer(ContentContainer object)
            {
                return createContentContainerAdapter();
            }
            @Override
            public Adapter caseSpan(Span object)
            {
                return createSpanAdapter();
            }
            @Override
            public Adapter caseEmphasis(Emphasis object)
            {
                return createEmphasisAdapter();
            }
            @Override
            public Adapter caseTextContainer(TextContainer object)
            {
                return createTextContainerAdapter();
            }
            @Override
            public Adapter caseImage(Image object)
            {
                return createImageAdapter();
            }
            @Override
            public Adapter caseObjectName(ObjectName object)
            {
                return createObjectNameAdapter();
            }
            @Override
            public Adapter casePlainTextContainer(PlainTextContainer object)
            {
                return createPlainTextContainerAdapter();
            }
            @Override
            public Adapter caseCodeParagraph(CodeParagraph object)
            {
                return createCodeParagraphAdapter();
            }
            @Override
            public Adapter caseCrossReference(CrossReference object)
            {
                return createCrossReferenceAdapter();
            }
            @Override
            public Adapter caseLink(Link object)
            {
                return createLinkAdapter();
            }
            @Override
            public Adapter caseQuote(Quote object)
            {
                return createQuoteAdapter();
            }
            @Override
            public Adapter caseList(List object)
            {
                return createListAdapter();
            }
            @Override
            public Adapter caseListItem(ListItem object)
            {
                return createListItemAdapter();
            }
            @Override
            public Adapter caseUnorderedList(UnorderedList object)
            {
                return createUnorderedListAdapter();
            }
            @Override
            public Adapter caseSimpleTable(SimpleTable object)
            {
                return createSimpleTableAdapter();
            }
            @Override
            public Adapter caseColumn(Column object)
            {
                return createColumnAdapter();
            }
            @Override
            public Adapter caseRow(Row object)
            {
                return createRowAdapter();
            }
            @Override
            public Adapter caseCell(Cell object)
            {
                return createCellAdapter();
            }
            @Override
            public Adapter caseSection(Section object)
            {
                return createSectionAdapter();
            }
            @Override
            public Adapter caseFragment(Fragment object)
            {
                return createFragmentAdapter();
            }
            @Override
            public Adapter caseStructureTable(StructureTable object)
            {
                return createStructureTableAdapter();
            }
            @Override
            public Adapter caseTable(Table object)
            {
                return createTableAdapter();
            }
            @Override
            public Adapter caseStructureEntry(StructureEntry object)
            {
                return createStructureEntryAdapter();
            }
            @Override
            public Adapter caseStructureEntryDescription(StructureEntryDescription object)
            {
                return createStructureEntryDescriptionAdapter();
            }
            @Override
            public Adapter caseContent(Content object)
            {
                return createContentAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter ( Notifier target )
    {
        return modelSwitch.doSwitch((EObject)target);
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Paragraph <em>Paragraph</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Paragraph
     * @generated
     */
    public Adapter createParagraphAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.ContentContainer <em>Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.ContentContainer
     * @generated
     */
    public Adapter createContentContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Span <em>Span</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Span
     * @generated
     */
    public Adapter createSpanAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Emphasis <em>Emphasis</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Emphasis
     * @generated
     */
    public Adapter createEmphasisAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.TextContainer <em>Text Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.TextContainer
     * @generated
     */
    public Adapter createTextContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Image <em>Image</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Image
     * @generated
     */
    public Adapter createImageAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.ObjectName <em>Object Name</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.ObjectName
     * @generated
     */
    public Adapter createObjectNameAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.PlainTextContainer <em>Plain Text Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.PlainTextContainer
     * @generated
     */
    public Adapter createPlainTextContainerAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.CodeParagraph <em>Code Paragraph</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.CodeParagraph
     * @generated
     */
    public Adapter createCodeParagraphAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.CrossReference <em>Cross Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.CrossReference
     * @generated
     */
    public Adapter createCrossReferenceAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Link <em>Link</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Link
     * @generated
     */
    public Adapter createLinkAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Quote <em>Quote</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Quote
     * @generated
     */
    public Adapter createQuoteAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.List <em>List</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.List
     * @generated
     */
    public Adapter createListAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.ListItem <em>List Item</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.ListItem
     * @generated
     */
    public Adapter createListItemAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.UnorderedList <em>Unordered List</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.UnorderedList
     * @generated
     */
    public Adapter createUnorderedListAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.SimpleTable <em>Simple Table</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.SimpleTable
     * @generated
     */
    public Adapter createSimpleTableAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Column <em>Column</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Column
     * @generated
     */
    public Adapter createColumnAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Row <em>Row</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Row
     * @generated
     */
    public Adapter createRowAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Cell <em>Cell</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Cell
     * @generated
     */
    public Adapter createCellAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Section <em>Section</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Section
     * @generated
     */
    public Adapter createSectionAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Fragment <em>Fragment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Fragment
     * @generated
     */
    public Adapter createFragmentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.StructureTable <em>Structure Table</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.StructureTable
     * @generated
     */
    public Adapter createStructureTableAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.Table <em>Table</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.Table
     * @generated
     */
    public Adapter createTableAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.StructureEntry <em>Structure Entry</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.StructureEntry
     * @generated
     */
    public Adapter createStructureEntryAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.content.StructureEntryDescription <em>Structure Entry Description</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.content.StructureEntryDescription
     * @generated
     */
    public Adapter createStructureEntryDescriptionAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.openscada.doc.model.doc.fragment.Content <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore
     * cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.openscada.doc.model.doc.fragment.Content
     * @generated
     */
    public Adapter createContentAdapter ()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter ()
    {
        return null;
    }

} //ContentAdapterFactory
