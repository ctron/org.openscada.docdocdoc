/**
 */
package org.openscada.doc.content.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentContainer;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.List;
import org.openscada.doc.content.ListItem;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.Span;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureEntryDescription;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.Table;
import org.openscada.doc.content.TextContainer;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)} to invoke
 * the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.openscada.doc.content.ContentPackage
 * @generated
 */
public class ContentSwitch<T> extends Switch<T>
{
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static ContentPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContentSwitch ()
    {
        if (modelPackage == null)
        {
            modelPackage = ContentPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @parameter ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor ( EPackage ePackage )
    {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch ( int classifierID, EObject theEObject )
    {
        switch (classifierID)
        {
            case ContentPackage.PARAGRAPH:
            {
                Paragraph paragraph = (Paragraph)theEObject;
                T result = caseParagraph(paragraph);
                if (result == null) result = caseTextContainer(paragraph);
                if (result == null) result = casePlainTextContainer(paragraph);
                if (result == null) result = caseContent(paragraph);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.CONTENT_CONTAINER:
            {
                ContentContainer contentContainer = (ContentContainer)theEObject;
                T result = caseContentContainer(contentContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.SPAN:
            {
                Span span = (Span)theEObject;
                T result = caseSpan(span);
                if (result == null) result = caseTextContainer(span);
                if (result == null) result = casePlainTextContainer(span);
                if (result == null) result = caseContent(span);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.EMPHASIS:
            {
                Emphasis emphasis = (Emphasis)theEObject;
                T result = caseEmphasis(emphasis);
                if (result == null) result = caseSpan(emphasis);
                if (result == null) result = caseTextContainer(emphasis);
                if (result == null) result = casePlainTextContainer(emphasis);
                if (result == null) result = caseContent(emphasis);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.TEXT_CONTAINER:
            {
                TextContainer textContainer = (TextContainer)theEObject;
                T result = caseTextContainer(textContainer);
                if (result == null) result = casePlainTextContainer(textContainer);
                if (result == null) result = caseContent(textContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.IMAGE:
            {
                Image image = (Image)theEObject;
                T result = caseImage(image);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.OBJECT_NAME:
            {
                ObjectName objectName = (ObjectName)theEObject;
                T result = caseObjectName(objectName);
                if (result == null) result = casePlainTextContainer(objectName);
                if (result == null) result = caseContent(objectName);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.PLAIN_TEXT_CONTAINER:
            {
                PlainTextContainer plainTextContainer = (PlainTextContainer)theEObject;
                T result = casePlainTextContainer(plainTextContainer);
                if (result == null) result = caseContent(plainTextContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.CODE_PARAGRAPH:
            {
                CodeParagraph codeParagraph = (CodeParagraph)theEObject;
                T result = caseCodeParagraph(codeParagraph);
                if (result == null) result = casePlainTextContainer(codeParagraph);
                if (result == null) result = caseContent(codeParagraph);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.CROSS_REFERENCE:
            {
                CrossReference crossReference = (CrossReference)theEObject;
                T result = caseCrossReference(crossReference);
                if (result == null) result = casePlainTextContainer(crossReference);
                if (result == null) result = caseContent(crossReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.LINK:
            {
                Link link = (Link)theEObject;
                T result = caseLink(link);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.QUOTE:
            {
                Quote quote = (Quote)theEObject;
                T result = caseQuote(quote);
                if (result == null) result = caseSpan(quote);
                if (result == null) result = caseTextContainer(quote);
                if (result == null) result = casePlainTextContainer(quote);
                if (result == null) result = caseContent(quote);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.LIST:
            {
                List list = (List)theEObject;
                T result = caseList(list);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.LIST_ITEM:
            {
                ListItem listItem = (ListItem)theEObject;
                T result = caseListItem(listItem);
                if (result == null) result = caseTextContainer(listItem);
                if (result == null) result = casePlainTextContainer(listItem);
                if (result == null) result = caseContent(listItem);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.UNORDERED_LIST:
            {
                UnorderedList unorderedList = (UnorderedList)theEObject;
                T result = caseUnorderedList(unorderedList);
                if (result == null) result = caseList(unorderedList);
                if (result == null) result = caseContent(unorderedList);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.SIMPLE_TABLE:
            {
                SimpleTable simpleTable = (SimpleTable)theEObject;
                T result = caseSimpleTable(simpleTable);
                if (result == null) result = caseTable(simpleTable);
                if (result == null) result = caseContent(simpleTable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.COLUMN:
            {
                Column column = (Column)theEObject;
                T result = caseColumn(column);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.ROW:
            {
                Row row = (Row)theEObject;
                T result = caseRow(row);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.CELL:
            {
                Cell cell = (Cell)theEObject;
                T result = caseCell(cell);
                if (result == null) result = caseTextContainer(cell);
                if (result == null) result = casePlainTextContainer(cell);
                if (result == null) result = caseContent(cell);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.SECTION:
            {
                Section section = (Section)theEObject;
                T result = caseSection(section);
                if (result == null) result = caseContentContainer(section);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.FRAGMENT:
            {
                Fragment fragment = (Fragment)theEObject;
                T result = caseFragment(fragment);
                if (result == null) result = caseContentContainer(fragment);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.STRUCTURE_TABLE:
            {
                StructureTable structureTable = (StructureTable)theEObject;
                T result = caseStructureTable(structureTable);
                if (result == null) result = caseTable(structureTable);
                if (result == null) result = caseContent(structureTable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.TABLE:
            {
                Table table = (Table)theEObject;
                T result = caseTable(table);
                if (result == null) result = caseContent(table);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.STRUCTURE_ENTRY:
            {
                StructureEntry structureEntry = (StructureEntry)theEObject;
                T result = caseStructureEntry(structureEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ContentPackage.STRUCTURE_ENTRY_DESCRIPTION:
            {
                StructureEntryDescription structureEntryDescription = (StructureEntryDescription)theEObject;
                T result = caseStructureEntryDescription(structureEntryDescription);
                if (result == null) result = casePlainTextContainer(structureEntryDescription);
                if (result == null) result = caseContent(structureEntryDescription);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Paragraph</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Paragraph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseParagraph ( Paragraph object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContentContainer ( ContentContainer object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fragment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fragment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFragment ( Fragment object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structure Table</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structure Table</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructureTable(StructureTable object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Table</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Table</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTable(Table object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structure Entry</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structure Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructureEntry(StructureEntry object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structure Entry Description</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structure Entry Description</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructureEntryDescription(StructureEntryDescription object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Span</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Span</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSpan ( Span object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Emphasis</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Emphasis</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmphasis ( Emphasis object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Text Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Text Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTextContainer ( TextContainer object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Image</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Image</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseImage ( Image object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Object Name</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Object Name</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseObjectName ( ObjectName object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plain Text Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plain Text Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePlainTextContainer ( PlainTextContainer object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Code Paragraph</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Code Paragraph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCodeParagraph ( CodeParagraph object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cross Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cross Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCrossReference ( CrossReference object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Link</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Link</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLink ( Link object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Quote</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Quote</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseQuote ( Quote object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>List</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>List</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseList ( List object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>List Item</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>List Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseListItem ( ListItem object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Unordered List</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Unordered List</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUnorderedList ( UnorderedList object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Simple Table</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Simple Table</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSimpleTable ( SimpleTable object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Column</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Column</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseColumn ( Column object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Row</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Row</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRow ( Row object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cell</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cell</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCell ( Cell object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Section</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Section</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSection ( Section object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Content</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Content</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContent ( Content object )
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the
     * last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase ( EObject object )
    {
        return null;
    }

} //ContentSwitch
