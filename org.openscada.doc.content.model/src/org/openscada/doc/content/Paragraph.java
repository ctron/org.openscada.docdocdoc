/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paragraph</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getParagraph()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface Paragraph extends TextContainer
{

} // Paragraph
