/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getFragment()
 * @model extendedMetaData="name='fragment'"
 * @generated
 */
public interface Fragment extends ContentContainer
{
} // Fragment
