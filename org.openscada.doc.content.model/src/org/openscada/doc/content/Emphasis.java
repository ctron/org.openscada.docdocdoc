/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Emphasis</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getEmphasis()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface Emphasis extends Span
{
} // Emphasis
