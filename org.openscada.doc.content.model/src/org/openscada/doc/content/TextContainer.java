/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The text container can hold text and sub elements
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.TextContainer#getGroupContent <em>Group Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getEmphasis <em>Emphasis</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getImage <em>Image</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getObjectName <em>Object Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getXref <em>Xref</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getLink <em>Link</em>}</li>
 *   <li>{@link org.openscada.doc.content.TextContainer#getQuote <em>Quote</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getTextContainer()
 * @model abstract="true"
 *        extendedMetaData="kind='mixed'"
 * @generated
 */
public interface TextContainer extends PlainTextContainer
{
    /**
     * Returns the value of the '<em><b>Group Content</b></em>' attribute list.
     * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Group Content</em>' attribute list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Group Content</em>' attribute list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_GroupContent()
     * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="name='groupContent:0' kind='group' group='#:mixed'"
     * @generated
     */
    FeatureMap getGroupContent ();

    /**
     * Returns the value of the '<em><b>Emphasis</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Emphasis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Emphasis</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Emphasis</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_Emphasis()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="kind='element' namespace='##targetNamespace' name='em' group='#groupContent:0'"
     * @generated
     */
    EList<Emphasis> getEmphasis ();

    /**
     * Returns the value of the '<em><b>Image</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Image}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Image</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Image</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_Image()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element' group='#groupContent:0'"
     * @generated
     */
    EList<Image> getImage ();

    /**
     * Returns the value of the '<em><b>Object Name</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.ObjectName}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Object Name</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Object Name</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_ObjectName()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element' group='#groupContent:0'"
     * @generated
     */
    EList<ObjectName> getObjectName ();

    /**
     * Returns the value of the '<em><b>Xref</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.CrossReference}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Xref</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Xref</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_Xref()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element' group='#groupContent:0'"
     * @generated
     */
    EList<CrossReference> getXref ();

    /**
     * Returns the value of the '<em><b>Link</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Link}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Link</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Link</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_Link()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="kind='element' namespace='##targetNamespace' group='#groupContent:0'"
     * @generated
     */
    EList<Link> getLink ();

    /**
     * Returns the value of the '<em><b>Quote</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Quote}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Quote</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Quote</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getTextContainer_Quote()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element' name='q' group='#groupContent:0'"
     * @generated
     */
    EList<Quote> getQuote ();

} // TextContainer
