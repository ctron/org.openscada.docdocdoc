/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.List#getItem <em>Item</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getList()
 * @model abstract="true"
 * @generated
 */
public interface List extends EObject
{
    /**
     * Returns the value of the '<em><b>Item</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.ListItem}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Item</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Item</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getList_Item()
     * @model containment="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='element' name='item'"
     * @generated
     */
    EList<ListItem> getItem ();

} // List
