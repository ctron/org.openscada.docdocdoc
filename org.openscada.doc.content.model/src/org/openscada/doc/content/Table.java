/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content;

import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.Table#isMaximize <em>Maximize</em>}</li>
 *   <li>{@link org.openscada.doc.content.Table#getCaption <em>Caption</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getTable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Table extends Content
{
    /**
     * Returns the value of the '<em><b>Maximize</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Maximize</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Maximize</em>' attribute.
     * @see #setMaximize(boolean)
     * @see org.openscada.doc.content.ContentPackage#getTable_Maximize()
     * @model extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    boolean isMaximize();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Table#isMaximize <em>Maximize</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Maximize</em>' attribute.
     * @see #isMaximize()
     * @generated
     */
    void setMaximize(boolean value);

    /**
     * Returns the value of the '<em><b>Caption</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Caption</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Caption</em>' attribute.
     * @see #setCaption(String)
     * @see org.openscada.doc.content.ContentPackage#getTable_Caption()
     * @model
     * @generated
     */
    String getCaption();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Table#getCaption <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Caption</em>' attribute.
     * @see #getCaption()
     * @generated
     */
    void setCaption(String value);

} // Table
