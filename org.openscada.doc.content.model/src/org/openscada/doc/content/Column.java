/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.Column#getTitle <em>Title</em>}</li>
 *   <li>{@link org.openscada.doc.content.Column#getHorizontalAlign <em>Horizontal Align</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getColumn()
 * @model
 * @generated
 */
public interface Column extends EObject
{
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.doc.content.ContentPackage#getColumn_Title()
     * @model extendedMetaData="name='title' kind='attribute' namespace='##targetNamespace'"
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Column#getTitle <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

    /**
     * Returns the value of the '<em><b>Horizontal Align</b></em>' attribute.
     * The default value is <code>"LEFT"</code>.
     * The literals are from the enumeration {@link org.openscada.doc.content.HorizontalAlign}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Horizontal Align</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Horizontal Align</em>' attribute.
     * @see org.openscada.doc.content.HorizontalAlign
     * @see #setHorizontalAlign(HorizontalAlign)
     * @see org.openscada.doc.content.ContentPackage#getColumn_HorizontalAlign()
     * @model default="LEFT" required="true"
     *        extendedMetaData="namespace='##targetNamespace' kind='attribute'"
     * @generated
     */
    HorizontalAlign getHorizontalAlign ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Column#getHorizontalAlign <em>Horizontal Align</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Horizontal Align</em>' attribute.
     * @see org.openscada.doc.content.HorizontalAlign
     * @see #getHorizontalAlign()
     * @generated
     */
    void setHorizontalAlign ( HorizontalAlign value );

} // Column
