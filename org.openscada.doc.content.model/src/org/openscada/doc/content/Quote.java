/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quote</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getQuote()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface Quote extends Span
{
} // Quote
