package org.openscada.doc.content.model;

import java.util.List;

import org.openscada.doc.model.doc.map.SectionMarker;

public class SectionAdapter implements org.openscada.doc.model.doc.map.SectionAdapter
{

    @Override
    public SectionMarker adaptToSection ( final Object object )
    {
        if ( object instanceof org.openscada.doc.content.Section )
        {
            final org.openscada.doc.content.Section as = (org.openscada.doc.content.Section)object;
            return new SectionMarker () {

                @Override
                public String getTitle ()
                {
                    return as.getTitle ();
                }

                @Override
                public String getId ()
                {
                    return as.getId ();
                }

                @Override
                public List<? extends Object> getContent ()
                {
                    return as.eContents ();
                }
            };

        }
        else
        {
            return null;
        }
    }

}
