/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Entry Description</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getStructureEntryDescription()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface StructureEntryDescription extends PlainTextContainer
{
} // StructureEntryDescription
