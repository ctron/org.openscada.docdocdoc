/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.StructureEntry#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.StructureEntry#getSize <em>Size</em>}</li>
 *   <li>{@link org.openscada.doc.content.StructureEntry#getTypeName <em>Type Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.StructureEntry#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.StructureEntry#getSizeField <em>Size Field</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getStructureEntry()
 * @model
 * @generated
 */
public interface StructureEntry extends EObject
{
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see org.openscada.doc.content.ContentPackage#getStructureEntry_Name()
     * @model id="true" required="true"
     *        extendedMetaData="name='name'"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.StructureEntry#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Size</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Size</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Size</em>' attribute.
     * @see #setSize(Integer)
     * @see org.openscada.doc.content.ContentPackage#getStructureEntry_Size()
     * @model extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    Integer getSize();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.StructureEntry#getSize <em>Size</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Size</em>' attribute.
     * @see #getSize()
     * @generated
     */
    void setSize(Integer value);

    /**
     * Returns the value of the '<em><b>Type Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Type Name</em>' attribute.
     * @see #setTypeName(String)
     * @see org.openscada.doc.content.ContentPackage#getStructureEntry_TypeName()
     * @model required="true"
     *        extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    String getTypeName();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.StructureEntry#getTypeName <em>Type Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type Name</em>' attribute.
     * @see #getTypeName()
     * @generated
     */
    void setTypeName(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Description</em>' containment reference.
     * @see #setDescription(StructureEntryDescription)
     * @see org.openscada.doc.content.ContentPackage#getStructureEntry_Description()
     * @model containment="true" required="true"
     *        extendedMetaData="namespace='##targetNamespace'"
     * @generated
     */
    StructureEntryDescription getDescription();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.StructureEntry#getDescription <em>Description</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Description</em>' containment reference.
     * @see #getDescription()
     * @generated
     */
    void setDescription(StructureEntryDescription value);

    /**
     * Returns the value of the '<em><b>Size Field</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Size Field</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Size Field</em>' reference.
     * @see #setSizeField(StructureEntry)
     * @see org.openscada.doc.content.ContentPackage#getStructureEntry_SizeField()
     * @model
     * @generated
     */
    StructureEntry getSizeField();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.StructureEntry#getSizeField <em>Size Field</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Size Field</em>' reference.
     * @see #getSizeField()
     * @generated
     */
    void setSizeField(StructureEntry value);

} // StructureEntry
