/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getContent <em>Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getP <em>P</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getCode <em>Code</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getPlainText <em>Plain Text</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getUl <em>Ul</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getSimpleTable <em>Simple Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getSection <em>Section</em>}</li>
 *   <li>{@link org.openscada.doc.content.ContentContainer#getStructureTable <em>Structure Table</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getContentContainer()
 * @model interface="true" abstract="true"
 *        extendedMetaData="name='contentContainer'"
 * @generated
 */
public interface ContentContainer extends EObject
{
    /**
     * Returns the value of the '<em><b>P</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Paragraph}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>P</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>P</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_P()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="group='#group:0' namespace='##targetNamespace'"
     * @generated
     */
    EList<Paragraph> getP ();

    /**
     * Returns the value of the '<em><b>Code</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.CodeParagraph}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Code</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Code</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_Code()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="group='#group:0' namespace='##targetNamespace'"
     * @generated
     */
    EList<CodeParagraph> getCode ();

    /**
     * Returns the value of the '<em><b>Plain Text</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.model.doc.fragment.PlainTextContent}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Plain Text</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Plain Text</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_PlainText()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="group='#group:0' namespace='##targetNamespace'"
     * @generated
     */
    EList<PlainTextContent> getPlainText ();

    /**
     * Returns the value of the '<em><b>Ul</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.UnorderedList}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ul</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Ul</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_Ul()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="group='#group:0' namespace='##targetNamespace'"
     * @generated
     */
    EList<UnorderedList> getUl ();

    /**
     * Returns the value of the '<em><b>Simple Table</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.SimpleTable}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Simple Table</em>' containment reference list
     * isn't clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Simple Table</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_SimpleTable()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="group='#group:0' namespace='##targetNamespace'"
     * @generated
     */
    EList<SimpleTable> getSimpleTable ();

    /**
     * Returns the value of the '<em><b>Section</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.Section}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Section</em>' containment reference list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Section</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_Section()
     * @model containment="true"
     *        extendedMetaData="name='section' namespace='##targetNamespace' group='#group:0'"
     * @generated
     */
    EList<Section> getSection ();

    /**
     * Returns the value of the '<em><b>Structure Table</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.doc.content.StructureTable}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Structure Table</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Structure Table</em>' containment reference list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_StructureTable()
     * @model containment="true" transient="true" volatile="true" derived="true"
     *        extendedMetaData="namespace='##targetNamespace' group='#group:0'"
     * @generated
     */
    EList<StructureTable> getStructureTable();

    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute list.
     * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Content</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Content</em>' attribute list.
     * @see org.openscada.doc.content.ContentPackage#getContentContainer_Content()
     * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
     *        extendedMetaData="name='group:0' kind='group'"
     * @generated
     */
    FeatureMap getContent ();

} // ContentContainer
