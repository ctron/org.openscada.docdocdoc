/**
 */
package org.openscada.doc.content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getObjectName()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface ObjectName extends PlainTextContainer
{
} // ObjectName
