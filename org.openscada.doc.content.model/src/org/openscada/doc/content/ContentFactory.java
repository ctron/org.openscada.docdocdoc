/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.openscada.doc.content.ContentPackage
 * @generated
 */
public interface ContentFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    ContentFactory eINSTANCE = org.openscada.doc.content.impl.ContentFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Paragraph</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Paragraph</em>'.
     * @generated
     */
    Paragraph createParagraph ();

    /**
     * Returns a new object of class '<em>Fragment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fragment</em>'.
     * @generated
     */
    Fragment createFragment ();

    /**
     * Returns a new object of class '<em>Structure Table</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Structure Table</em>'.
     * @generated
     */
    StructureTable createStructureTable();

    /**
     * Returns a new object of class '<em>Structure Entry</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Structure Entry</em>'.
     * @generated
     */
    StructureEntry createStructureEntry();

    /**
     * Returns a new object of class '<em>Structure Entry Description</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Structure Entry Description</em>'.
     * @generated
     */
    StructureEntryDescription createStructureEntryDescription();

    /**
     * Returns a new object of class '<em>Emphasis</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Emphasis</em>'.
     * @generated
     */
    Emphasis createEmphasis ();

    /**
     * Returns a new object of class '<em>Image</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Image</em>'.
     * @generated
     */
    Image createImage ();

    /**
     * Returns a new object of class '<em>Object Name</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Object Name</em>'.
     * @generated
     */
    ObjectName createObjectName ();

    /**
     * Returns a new object of class '<em>Code Paragraph</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Code Paragraph</em>'.
     * @generated
     */
    CodeParagraph createCodeParagraph ();

    /**
     * Returns a new object of class '<em>Cross Reference</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Cross Reference</em>'.
     * @generated
     */
    CrossReference createCrossReference ();

    /**
     * Returns a new object of class '<em>Link</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Link</em>'.
     * @generated
     */
    Link createLink ();

    /**
     * Returns a new object of class '<em>Quote</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Quote</em>'.
     * @generated
     */
    Quote createQuote ();

    /**
     * Returns a new object of class '<em>List Item</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>List Item</em>'.
     * @generated
     */
    ListItem createListItem ();

    /**
     * Returns a new object of class '<em>Unordered List</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Unordered List</em>'.
     * @generated
     */
    UnorderedList createUnorderedList ();

    /**
     * Returns a new object of class '<em>Simple Table</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Simple Table</em>'.
     * @generated
     */
    SimpleTable createSimpleTable ();

    /**
     * Returns a new object of class '<em>Column</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Column</em>'.
     * @generated
     */
    Column createColumn ();

    /**
     * Returns a new object of class '<em>Row</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Row</em>'.
     * @generated
     */
    Row createRow ();

    /**
     * Returns a new object of class '<em>Cell</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Cell</em>'.
     * @generated
     */
    Cell createCell ();

    /**
     * Returns a new object of class '<em>Section</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Section</em>'.
     * @generated
     */
    Section createSection ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    ContentPackage getContentPackage ();

} //ContentFactory
