/**
 */
package org.openscada.doc.content;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Image</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.doc.content.Image#getSource <em>Source</em>}</li>
 *   <li>{@link org.openscada.doc.content.Image#getData <em>Data</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.doc.content.ContentPackage#getImage()
 * @model
 * @generated
 */
public interface Image extends EObject
{
    /**
     * Returns the value of the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source</em>' attribute isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Source</em>' attribute.
     * @see #setSource(String)
     * @see org.openscada.doc.content.ContentPackage#getImage_Source()
     * @model extendedMetaData="kind='attribute'"
     * @generated
     */
    String getSource ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Image#getSource <em>Source</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Source</em>' attribute.
     * @see #getSource()
     * @generated
     */
    void setSource ( String value );

    /**
     * Returns the value of the '<em><b>Data</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Data</em>' attribute isn't clear, there really
     * should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Data</em>' attribute.
     * @see #setData(byte[])
     * @see org.openscada.doc.content.ContentPackage#getImage_Data()
     * @model extendedMetaData="kind='element'"
     * @generated
     */
    byte[] getData ();

    /**
     * Sets the value of the '{@link org.openscada.doc.content.Image#getData <em>Data</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Data</em>' attribute.
     * @see #getData()
     * @generated
     */
    void setData ( byte[] value );

} // Image
