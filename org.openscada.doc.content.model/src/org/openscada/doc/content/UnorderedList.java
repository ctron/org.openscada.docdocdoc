/**
 */
package org.openscada.doc.content;

import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unordered List</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.doc.content.ContentPackage#getUnorderedList()
 * @model extendedMetaData="name='UnorderedList'"
 * @generated
 */
public interface UnorderedList extends List, Content
{
} // UnorderedList
