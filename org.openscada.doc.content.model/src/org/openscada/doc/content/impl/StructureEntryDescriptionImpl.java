/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.StructureEntryDescription;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structure Entry Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StructureEntryDescriptionImpl extends PlainTextContainerImpl implements StructureEntryDescription
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StructureEntryDescriptionImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ContentPackage.Literals.STRUCTURE_ENTRY_DESCRIPTION;
    }

} //StructureEntryDescriptionImpl
