/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getContent <em>Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getP <em>P</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getCode <em>Code</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getPlainText <em>Plain Text</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getUl <em>Ul</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getSimpleTable <em>Simple Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getSection <em>Section</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getStructureTable <em>Structure Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SectionImpl#getTitle <em>Title</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SectionImpl extends EObjectImpl implements Section
{
    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected FeatureMap content;

    /**
     * The cached value of the '{@link #getSection() <em>Section</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSection()
     * @generated
     * @ordered
     */
    protected EList<Section> section;

    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected SectionImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.SECTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FeatureMap getContent ()
    {
        if (content == null)
        {
            content = new BasicFeatureMap(this, ContentPackage.SECTION__CONTENT);
        }
        return content;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Section> getSection ()
    {
        if (section == null)
        {
            section = new EObjectContainmentEList<Section>(Section.class, this, ContentPackage.SECTION__SECTION);
        }
        return section;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<StructureTable> getStructureTable()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__STRUCTURE_TABLE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getId()
    {
        return id;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setId(String newId)
    {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.SECTION__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setTitle ( String newTitle )
    {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.SECTION__TITLE, oldTitle, title));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Paragraph> getP ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__P);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<CodeParagraph> getCode ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__CODE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<PlainTextContent> getPlainText ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__PLAIN_TEXT);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<UnorderedList> getUl ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__UL);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<SimpleTable> getSimpleTable ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__SIMPLE_TABLE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
        switch (featureID)
        {
            case ContentPackage.SECTION__CONTENT:
                return ((InternalEList<?>)getContent()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__P:
                return ((InternalEList<?>)getP()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__CODE:
                return ((InternalEList<?>)getCode()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__PLAIN_TEXT:
                return ((InternalEList<?>)getPlainText()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__UL:
                return ((InternalEList<?>)getUl()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__SIMPLE_TABLE:
                return ((InternalEList<?>)getSimpleTable()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__SECTION:
                return ((InternalEList<?>)getSection()).basicRemove(otherEnd, msgs);
            case ContentPackage.SECTION__STRUCTURE_TABLE:
                return ((InternalEList<?>)getStructureTable()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch (featureID)
        {
            case ContentPackage.SECTION__CONTENT:
                if (coreType) return getContent();
                return ((FeatureMap.Internal)getContent()).getWrapper();
            case ContentPackage.SECTION__P:
                return getP();
            case ContentPackage.SECTION__CODE:
                return getCode();
            case ContentPackage.SECTION__PLAIN_TEXT:
                return getPlainText();
            case ContentPackage.SECTION__UL:
                return getUl();
            case ContentPackage.SECTION__SIMPLE_TABLE:
                return getSimpleTable();
            case ContentPackage.SECTION__SECTION:
                return getSection();
            case ContentPackage.SECTION__STRUCTURE_TABLE:
                return getStructureTable();
            case ContentPackage.SECTION__ID:
                return getId();
            case ContentPackage.SECTION__TITLE:
                return getTitle();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch (featureID)
        {
            case ContentPackage.SECTION__CONTENT:
                ((FeatureMap.Internal)getContent()).set(newValue);
                return;
            case ContentPackage.SECTION__P:
                getP().clear();
                getP().addAll((Collection<? extends Paragraph>)newValue);
                return;
            case ContentPackage.SECTION__CODE:
                getCode().clear();
                getCode().addAll((Collection<? extends CodeParagraph>)newValue);
                return;
            case ContentPackage.SECTION__PLAIN_TEXT:
                getPlainText().clear();
                getPlainText().addAll((Collection<? extends PlainTextContent>)newValue);
                return;
            case ContentPackage.SECTION__UL:
                getUl().clear();
                getUl().addAll((Collection<? extends UnorderedList>)newValue);
                return;
            case ContentPackage.SECTION__SIMPLE_TABLE:
                getSimpleTable().clear();
                getSimpleTable().addAll((Collection<? extends SimpleTable>)newValue);
                return;
            case ContentPackage.SECTION__SECTION:
                getSection().clear();
                getSection().addAll((Collection<? extends Section>)newValue);
                return;
            case ContentPackage.SECTION__STRUCTURE_TABLE:
                getStructureTable().clear();
                getStructureTable().addAll((Collection<? extends StructureTable>)newValue);
                return;
            case ContentPackage.SECTION__ID:
                setId((String)newValue);
                return;
            case ContentPackage.SECTION__TITLE:
                setTitle((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.SECTION__CONTENT:
                getContent().clear();
                return;
            case ContentPackage.SECTION__P:
                getP().clear();
                return;
            case ContentPackage.SECTION__CODE:
                getCode().clear();
                return;
            case ContentPackage.SECTION__PLAIN_TEXT:
                getPlainText().clear();
                return;
            case ContentPackage.SECTION__UL:
                getUl().clear();
                return;
            case ContentPackage.SECTION__SIMPLE_TABLE:
                getSimpleTable().clear();
                return;
            case ContentPackage.SECTION__SECTION:
                getSection().clear();
                return;
            case ContentPackage.SECTION__STRUCTURE_TABLE:
                getStructureTable().clear();
                return;
            case ContentPackage.SECTION__ID:
                setId(ID_EDEFAULT);
                return;
            case ContentPackage.SECTION__TITLE:
                setTitle(TITLE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.SECTION__CONTENT:
                return content != null && !content.isEmpty();
            case ContentPackage.SECTION__P:
                return !getP().isEmpty();
            case ContentPackage.SECTION__CODE:
                return !getCode().isEmpty();
            case ContentPackage.SECTION__PLAIN_TEXT:
                return !getPlainText().isEmpty();
            case ContentPackage.SECTION__UL:
                return !getUl().isEmpty();
            case ContentPackage.SECTION__SIMPLE_TABLE:
                return !getSimpleTable().isEmpty();
            case ContentPackage.SECTION__SECTION:
                return section != null && !section.isEmpty();
            case ContentPackage.SECTION__STRUCTURE_TABLE:
                return !getStructureTable().isEmpty();
            case ContentPackage.SECTION__ID:
                return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
            case ContentPackage.SECTION__TITLE:
                return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (content: ");
        result.append(content);
        result.append(", id: ");
        result.append(id);
        result.append(", title: ");
        result.append(title);
        result.append(')');
        return result.toString();
    }

} //SectionImpl
