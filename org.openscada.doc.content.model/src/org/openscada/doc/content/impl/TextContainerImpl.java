/**
 */
package org.openscada.doc.content.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.TextContainer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Text Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getGroupContent <em>Group Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getEmphasis <em>Emphasis</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getObjectName <em>Object Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getXref <em>Xref</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getLink <em>Link</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.TextContainerImpl#getQuote <em>Quote</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class TextContainerImpl extends PlainTextContainerImpl implements TextContainer
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected TextContainerImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.TEXT_CONTAINER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FeatureMap getGroupContent ()
    {
        return (FeatureMap)getContent().<FeatureMap.Entry>list(ContentPackage.Literals.TEXT_CONTAINER__GROUP_CONTENT);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Emphasis> getEmphasis ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__EMPHASIS);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Image> getImage ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__IMAGE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<ObjectName> getObjectName ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__OBJECT_NAME);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<CrossReference> getXref ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__XREF);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Link> getLink ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__LINK);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Quote> getQuote ()
    {
        return getGroupContent().list(ContentPackage.Literals.TEXT_CONTAINER__QUOTE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
        switch (featureID)
        {
            case ContentPackage.TEXT_CONTAINER__GROUP_CONTENT:
                return ((InternalEList<?>)getGroupContent()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__EMPHASIS:
                return ((InternalEList<?>)getEmphasis()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__IMAGE:
                return ((InternalEList<?>)getImage()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__OBJECT_NAME:
                return ((InternalEList<?>)getObjectName()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__XREF:
                return ((InternalEList<?>)getXref()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__LINK:
                return ((InternalEList<?>)getLink()).basicRemove(otherEnd, msgs);
            case ContentPackage.TEXT_CONTAINER__QUOTE:
                return ((InternalEList<?>)getQuote()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch (featureID)
        {
            case ContentPackage.TEXT_CONTAINER__GROUP_CONTENT:
                if (coreType) return getGroupContent();
                return ((FeatureMap.Internal)getGroupContent()).getWrapper();
            case ContentPackage.TEXT_CONTAINER__EMPHASIS:
                return getEmphasis();
            case ContentPackage.TEXT_CONTAINER__IMAGE:
                return getImage();
            case ContentPackage.TEXT_CONTAINER__OBJECT_NAME:
                return getObjectName();
            case ContentPackage.TEXT_CONTAINER__XREF:
                return getXref();
            case ContentPackage.TEXT_CONTAINER__LINK:
                return getLink();
            case ContentPackage.TEXT_CONTAINER__QUOTE:
                return getQuote();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch (featureID)
        {
            case ContentPackage.TEXT_CONTAINER__GROUP_CONTENT:
                ((FeatureMap.Internal)getGroupContent()).set(newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__EMPHASIS:
                getEmphasis().clear();
                getEmphasis().addAll((Collection<? extends Emphasis>)newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__IMAGE:
                getImage().clear();
                getImage().addAll((Collection<? extends Image>)newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__OBJECT_NAME:
                getObjectName().clear();
                getObjectName().addAll((Collection<? extends ObjectName>)newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__XREF:
                getXref().clear();
                getXref().addAll((Collection<? extends CrossReference>)newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__LINK:
                getLink().clear();
                getLink().addAll((Collection<? extends Link>)newValue);
                return;
            case ContentPackage.TEXT_CONTAINER__QUOTE:
                getQuote().clear();
                getQuote().addAll((Collection<? extends Quote>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.TEXT_CONTAINER__GROUP_CONTENT:
                getGroupContent().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__EMPHASIS:
                getEmphasis().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__IMAGE:
                getImage().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__OBJECT_NAME:
                getObjectName().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__XREF:
                getXref().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__LINK:
                getLink().clear();
                return;
            case ContentPackage.TEXT_CONTAINER__QUOTE:
                getQuote().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.TEXT_CONTAINER__GROUP_CONTENT:
                return !getGroupContent().isEmpty();
            case ContentPackage.TEXT_CONTAINER__EMPHASIS:
                return !getEmphasis().isEmpty();
            case ContentPackage.TEXT_CONTAINER__IMAGE:
                return !getImage().isEmpty();
            case ContentPackage.TEXT_CONTAINER__OBJECT_NAME:
                return !getObjectName().isEmpty();
            case ContentPackage.TEXT_CONTAINER__XREF:
                return !getXref().isEmpty();
            case ContentPackage.TEXT_CONTAINER__LINK:
                return !getLink().isEmpty();
            case ContentPackage.TEXT_CONTAINER__QUOTE:
                return !getQuote().isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //TextContainerImpl
