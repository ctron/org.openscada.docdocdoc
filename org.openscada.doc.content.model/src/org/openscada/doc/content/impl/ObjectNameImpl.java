/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.ObjectName;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ObjectNameImpl extends PlainTextContainerImpl implements ObjectName
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ObjectNameImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.OBJECT_NAME;
    }

} //ObjectNameImpl
