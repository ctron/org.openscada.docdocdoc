/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureTable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structure Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.StructureTableImpl#isMaximize <em>Maximize</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureTableImpl#getCaption <em>Caption</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureTableImpl#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StructureTableImpl extends EObjectImpl implements StructureTable
{
    /**
     * The default value of the '{@link #isMaximize() <em>Maximize</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isMaximize()
     * @generated
     * @ordered
     */
    protected static final boolean MAXIMIZE_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isMaximize() <em>Maximize</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isMaximize()
     * @generated
     * @ordered
     */
    protected boolean maximize = MAXIMIZE_EDEFAULT;

    /**
     * The default value of the '{@link #getCaption() <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCaption()
     * @generated
     * @ordered
     */
    protected static final String CAPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCaption() <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCaption()
     * @generated
     * @ordered
     */
    protected String caption = CAPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEntry()
     * @generated
     * @ordered
     */
    protected EList<StructureEntry> entry;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StructureTableImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ContentPackage.Literals.STRUCTURE_TABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public boolean isMaximize()
    {
        return maximize;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setMaximize(boolean newMaximize)
    {
        boolean oldMaximize = maximize;
        maximize = newMaximize;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_TABLE__MAXIMIZE, oldMaximize, maximize));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getCaption()
    {
        return caption;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setCaption(String newCaption)
    {
        String oldCaption = caption;
        caption = newCaption;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_TABLE__CAPTION, oldCaption, caption));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<StructureEntry> getEntry()
    {
        if (entry == null)
        {
            entry = new EObjectContainmentEList<StructureEntry>(StructureEntry.class, this, ContentPackage.STRUCTURE_TABLE__ENTRY);
        }
        return entry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_TABLE__ENTRY:
                return ((InternalEList<?>)getEntry()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_TABLE__MAXIMIZE:
                return isMaximize();
            case ContentPackage.STRUCTURE_TABLE__CAPTION:
                return getCaption();
            case ContentPackage.STRUCTURE_TABLE__ENTRY:
                return getEntry();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_TABLE__MAXIMIZE:
                setMaximize((Boolean)newValue);
                return;
            case ContentPackage.STRUCTURE_TABLE__CAPTION:
                setCaption((String)newValue);
                return;
            case ContentPackage.STRUCTURE_TABLE__ENTRY:
                getEntry().clear();
                getEntry().addAll((Collection<? extends StructureEntry>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_TABLE__MAXIMIZE:
                setMaximize(MAXIMIZE_EDEFAULT);
                return;
            case ContentPackage.STRUCTURE_TABLE__CAPTION:
                setCaption(CAPTION_EDEFAULT);
                return;
            case ContentPackage.STRUCTURE_TABLE__ENTRY:
                getEntry().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_TABLE__MAXIMIZE:
                return maximize != MAXIMIZE_EDEFAULT;
            case ContentPackage.STRUCTURE_TABLE__CAPTION:
                return CAPTION_EDEFAULT == null ? caption != null : !CAPTION_EDEFAULT.equals(caption);
            case ContentPackage.STRUCTURE_TABLE__ENTRY:
                return entry != null && !entry.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (maximize: ");
        result.append(maximize);
        result.append(", caption: ");
        result.append(caption);
        result.append(')');
        return result.toString();
    }

} //StructureTableImpl
