/**
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jens Reimann - initial API and implementation and/or initial documentation
 * 
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureEntryDescription;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structure Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.StructureEntryImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureEntryImpl#getSize <em>Size</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureEntryImpl#getTypeName <em>Type Name</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureEntryImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.StructureEntryImpl#getSizeField <em>Size Field</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StructureEntryImpl extends EObjectImpl implements StructureEntry
{
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSize()
     * @generated
     * @ordered
     */
    protected static final Integer SIZE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSize()
     * @generated
     * @ordered
     */
    protected Integer size = SIZE_EDEFAULT;

    /**
     * The default value of the '{@link #getTypeName() <em>Type Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTypeName()
     * @generated
     * @ordered
     */
    protected static final String TYPE_NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTypeName() <em>Type Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTypeName()
     * @generated
     * @ordered
     */
    protected String typeName = TYPE_NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected StructureEntryDescription description;

    /**
     * The cached value of the '{@link #getSizeField() <em>Size Field</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSizeField()
     * @generated
     * @ordered
     */
    protected StructureEntry sizeField;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StructureEntryImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ContentPackage.Literals.STRUCTURE_ENTRY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName()
    {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setName(String newName)
    {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Integer getSize()
    {
        return size;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setSize(Integer newSize)
    {
        Integer oldSize = size;
        size = newSize;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__SIZE, oldSize, size));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getTypeName()
    {
        return typeName;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setTypeName(String newTypeName)
    {
        String oldTypeName = typeName;
        typeName = newTypeName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__TYPE_NAME, oldTypeName, typeName));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntryDescription getDescription()
    {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetDescription(StructureEntryDescription newDescription, NotificationChain msgs)
    {
        StructureEntryDescription oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__DESCRIPTION, oldDescription, newDescription);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDescription(StructureEntryDescription newDescription)
    {
        if (newDescription != description)
        {
            NotificationChain msgs = null;
            if (description != null)
                msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ContentPackage.STRUCTURE_ENTRY__DESCRIPTION, null, msgs);
            if (newDescription != null)
                msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ContentPackage.STRUCTURE_ENTRY__DESCRIPTION, null, msgs);
            msgs = basicSetDescription(newDescription, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__DESCRIPTION, newDescription, newDescription));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntry getSizeField()
    {
        if (sizeField != null && sizeField.eIsProxy())
        {
            InternalEObject oldSizeField = (InternalEObject)sizeField;
            sizeField = (StructureEntry)eResolveProxy(oldSizeField);
            if (sizeField != oldSizeField)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD, oldSizeField, sizeField));
            }
        }
        return sizeField;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntry basicGetSizeField()
    {
        return sizeField;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setSizeField(StructureEntry newSizeField)
    {
        StructureEntry oldSizeField = sizeField;
        sizeField = newSizeField;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD, oldSizeField, sizeField));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_ENTRY__DESCRIPTION:
                return basicSetDescription(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_ENTRY__NAME:
                return getName();
            case ContentPackage.STRUCTURE_ENTRY__SIZE:
                return getSize();
            case ContentPackage.STRUCTURE_ENTRY__TYPE_NAME:
                return getTypeName();
            case ContentPackage.STRUCTURE_ENTRY__DESCRIPTION:
                return getDescription();
            case ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD:
                if (resolve) return getSizeField();
                return basicGetSizeField();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_ENTRY__NAME:
                setName((String)newValue);
                return;
            case ContentPackage.STRUCTURE_ENTRY__SIZE:
                setSize((Integer)newValue);
                return;
            case ContentPackage.STRUCTURE_ENTRY__TYPE_NAME:
                setTypeName((String)newValue);
                return;
            case ContentPackage.STRUCTURE_ENTRY__DESCRIPTION:
                setDescription((StructureEntryDescription)newValue);
                return;
            case ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD:
                setSizeField((StructureEntry)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_ENTRY__NAME:
                setName(NAME_EDEFAULT);
                return;
            case ContentPackage.STRUCTURE_ENTRY__SIZE:
                setSize(SIZE_EDEFAULT);
                return;
            case ContentPackage.STRUCTURE_ENTRY__TYPE_NAME:
                setTypeName(TYPE_NAME_EDEFAULT);
                return;
            case ContentPackage.STRUCTURE_ENTRY__DESCRIPTION:
                setDescription((StructureEntryDescription)null);
                return;
            case ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD:
                setSizeField((StructureEntry)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID)
    {
        switch (featureID)
        {
            case ContentPackage.STRUCTURE_ENTRY__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case ContentPackage.STRUCTURE_ENTRY__SIZE:
                return SIZE_EDEFAULT == null ? size != null : !SIZE_EDEFAULT.equals(size);
            case ContentPackage.STRUCTURE_ENTRY__TYPE_NAME:
                return TYPE_NAME_EDEFAULT == null ? typeName != null : !TYPE_NAME_EDEFAULT.equals(typeName);
            case ContentPackage.STRUCTURE_ENTRY__DESCRIPTION:
                return description != null;
            case ContentPackage.STRUCTURE_ENTRY__SIZE_FIELD:
                return sizeField != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", size: ");
        result.append(size);
        result.append(", typeName: ");
        result.append(typeName);
        result.append(')');
        return result.toString();
    }

} //StructureEntryImpl
