/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Paragraph;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paragraph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ParagraphImpl extends TextContainerImpl implements Paragraph
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ParagraphImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.PARAGRAPH;
    }

} //ParagraphImpl
