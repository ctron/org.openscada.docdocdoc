/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.HorizontalAlign;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.ListItem;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureEntryDescription;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.UnorderedList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ContentFactoryImpl extends EFactoryImpl implements ContentFactory
{
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static ContentFactory init ()
    {
        try
        {
            ContentFactory theContentFactory = (ContentFactory)EPackage.Registry.INSTANCE.getEFactory("urn:openscada:doc:content"); 
            if (theContentFactory != null)
            {
                return theContentFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ContentFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContentFactoryImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create ( EClass eClass )
    {
        switch (eClass.getClassifierID())
        {
            case ContentPackage.PARAGRAPH: return createParagraph();
            case ContentPackage.EMPHASIS: return createEmphasis();
            case ContentPackage.IMAGE: return createImage();
            case ContentPackage.OBJECT_NAME: return createObjectName();
            case ContentPackage.CODE_PARAGRAPH: return createCodeParagraph();
            case ContentPackage.CROSS_REFERENCE: return createCrossReference();
            case ContentPackage.LINK: return createLink();
            case ContentPackage.QUOTE: return createQuote();
            case ContentPackage.LIST_ITEM: return createListItem();
            case ContentPackage.UNORDERED_LIST: return createUnorderedList();
            case ContentPackage.SIMPLE_TABLE: return createSimpleTable();
            case ContentPackage.COLUMN: return createColumn();
            case ContentPackage.ROW: return createRow();
            case ContentPackage.CELL: return createCell();
            case ContentPackage.SECTION: return createSection();
            case ContentPackage.FRAGMENT: return createFragment();
            case ContentPackage.STRUCTURE_TABLE: return createStructureTable();
            case ContentPackage.STRUCTURE_ENTRY: return createStructureEntry();
            case ContentPackage.STRUCTURE_ENTRY_DESCRIPTION: return createStructureEntryDescription();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString ( EDataType eDataType, String initialValue )
    {
        switch (eDataType.getClassifierID())
        {
            case ContentPackage.HORIZONTAL_ALIGN:
                return createHorizontalAlignFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString ( EDataType eDataType, Object instanceValue )
    {
        switch (eDataType.getClassifierID())
        {
            case ContentPackage.HORIZONTAL_ALIGN:
                return convertHorizontalAlignToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Paragraph createParagraph ()
    {
        ParagraphImpl paragraph = new ParagraphImpl();
        return paragraph;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Fragment createFragment ()
    {
        FragmentImpl fragment = new FragmentImpl();
        return fragment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureTable createStructureTable()
    {
        StructureTableImpl structureTable = new StructureTableImpl();
        return structureTable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntry createStructureEntry()
    {
        StructureEntryImpl structureEntry = new StructureEntryImpl();
        return structureEntry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureEntryDescription createStructureEntryDescription()
    {
        StructureEntryDescriptionImpl structureEntryDescription = new StructureEntryDescriptionImpl();
        return structureEntryDescription;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Emphasis createEmphasis ()
    {
        EmphasisImpl emphasis = new EmphasisImpl();
        return emphasis;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Image createImage ()
    {
        ImageImpl image = new ImageImpl();
        return image;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ObjectName createObjectName ()
    {
        ObjectNameImpl objectName = new ObjectNameImpl();
        return objectName;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CodeParagraph createCodeParagraph ()
    {
        CodeParagraphImpl codeParagraph = new CodeParagraphImpl();
        return codeParagraph;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CrossReference createCrossReference ()
    {
        CrossReferenceImpl crossReference = new CrossReferenceImpl();
        return crossReference;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Link createLink ()
    {
        LinkImpl link = new LinkImpl();
        return link;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Quote createQuote ()
    {
        QuoteImpl quote = new QuoteImpl();
        return quote;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ListItem createListItem ()
    {
        ListItemImpl listItem = new ListItemImpl();
        return listItem;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public UnorderedList createUnorderedList ()
    {
        UnorderedListImpl unorderedList = new UnorderedListImpl();
        return unorderedList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SimpleTable createSimpleTable ()
    {
        SimpleTableImpl simpleTable = new SimpleTableImpl();
        return simpleTable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Column createColumn ()
    {
        ColumnImpl column = new ColumnImpl();
        return column;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Row createRow ()
    {
        RowImpl row = new RowImpl();
        return row;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Cell createCell ()
    {
        CellImpl cell = new CellImpl();
        return cell;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Section createSection ()
    {
        SectionImpl section = new SectionImpl();
        return section;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public HorizontalAlign createHorizontalAlignFromString ( EDataType eDataType, String initialValue )
    {
        HorizontalAlign result = HorizontalAlign.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertHorizontalAlignToString ( EDataType eDataType, Object instanceValue )
    {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ContentPackage getContentPackage ()
    {
        return (ContentPackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static ContentPackage getPackage ()
    {
        return ContentPackage.eINSTANCE;
    }

} //ContentFactoryImpl
