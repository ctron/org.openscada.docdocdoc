/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentContainer;
import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.HorizontalAlign;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.List;
import org.openscada.doc.content.ListItem;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.Span;
import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureEntryDescription;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.Table;
import org.openscada.doc.content.TextContainer;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.model.doc.DocPackage;
import org.openscada.doc.model.doc.fragment.FragmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ContentPackageImpl extends EPackageImpl implements ContentPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass paragraphEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass contentContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass fragmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass structureTableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass structureEntryEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass structureEntryDescriptionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass spanEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass emphasisEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass textContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass imageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass objectNameEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass plainTextContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass codeParagraphEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass crossReferenceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass linkEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass quoteEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass listEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass listItemEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass unorderedListEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass simpleTableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass columnEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass rowEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass cellEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass sectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum horizontalAlignEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.doc.content.ContentPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ContentPackageImpl ()
    {
        super(eNS_URI, ContentFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link ContentPackage#eINSTANCE} when
     * that field is accessed. Clients should not invoke it directly. Instead,
     * they should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ContentPackage init ()
    {
        if (isInited) return (ContentPackage)EPackage.Registry.INSTANCE.getEPackage(ContentPackage.eNS_URI);

        // Obtain or create and register package
        ContentPackageImpl theContentPackage = (ContentPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ContentPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ContentPackageImpl());

        isInited = true;

        // Initialize simple dependencies
        DocPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theContentPackage.createPackageContents();

        // Initialize created meta-data
        theContentPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theContentPackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ContentPackage.eNS_URI, theContentPackage);
        return theContentPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getParagraph ()
    {
        return paragraphEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getContentContainer ()
    {
        return contentContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_P ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_Code ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_PlainText ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_Ul ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_SimpleTable ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContentContainer_Section ()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getContentContainer_StructureTable()
    {
        return (EReference)contentContainerEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getContentContainer_Content ()
    {
        return (EAttribute)contentContainerEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFragment ()
    {
        return fragmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getStructureTable()
    {
        return structureTableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getStructureTable_Entry()
    {
        return (EReference)structureTableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getTable()
    {
        return tableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getTable_Maximize()
    {
        return (EAttribute)tableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getTable_Caption()
    {
        return (EAttribute)tableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getStructureEntry()
    {
        return structureEntryEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getStructureEntry_Name()
    {
        return (EAttribute)structureEntryEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getStructureEntry_Size()
    {
        return (EAttribute)structureEntryEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getStructureEntry_TypeName()
    {
        return (EAttribute)structureEntryEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getStructureEntry_Description()
    {
        return (EReference)structureEntryEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getStructureEntry_SizeField()
    {
        return (EReference)structureEntryEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getStructureEntryDescription()
    {
        return structureEntryDescriptionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSpan ()
    {
        return spanEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEmphasis ()
    {
        return emphasisEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTextContainer ()
    {
        return textContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTextContainer_GroupContent ()
    {
        return (EAttribute)textContainerEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_Emphasis ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_Image ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_ObjectName ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_Xref ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_Link ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTextContainer_Quote ()
    {
        return (EReference)textContainerEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getImage ()
    {
        return imageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getImage_Source ()
    {
        return (EAttribute)imageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getImage_Data ()
    {
        return (EAttribute)imageEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getObjectName ()
    {
        return objectNameEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPlainTextContainer ()
    {
        return plainTextContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPlainTextContainer_Content ()
    {
        return (EAttribute)plainTextContainerEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCodeParagraph ()
    {
        return codeParagraphEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCodeParagraph_Language ()
    {
        return (EAttribute)codeParagraphEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCrossReference ()
    {
        return crossReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCrossReference_Type ()
    {
        return (EAttribute)crossReferenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCrossReference_Id ()
    {
        return (EAttribute)crossReferenceEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLink ()
    {
        return linkEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLink_Ref ()
    {
        return (EAttribute)linkEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLink_Name ()
    {
        return (EAttribute)linkEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getQuote ()
    {
        return quoteEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getList ()
    {
        return listEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getList_Item ()
    {
        return (EReference)listEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getListItem ()
    {
        return listItemEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getUnorderedList ()
    {
        return unorderedListEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSimpleTable ()
    {
        return simpleTableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSimpleTable_Row ()
    {
        return (EReference)simpleTableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSimpleTable_Column ()
    {
        return (EReference)simpleTableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getColumn ()
    {
        return columnEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getColumn_Title ()
    {
        return (EAttribute)columnEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getColumn_HorizontalAlign ()
    {
        return (EAttribute)columnEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRow ()
    {
        return rowEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRow_Cell ()
    {
        return (EReference)rowEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCell ()
    {
        return cellEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSection ()
    {
        return sectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getSection_Id()
    {
        return (EAttribute)sectionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSection_Title ()
    {
        return (EAttribute)sectionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getHorizontalAlign ()
    {
        return horizontalAlignEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ContentFactory getContentFactory ()
    {
        return (ContentFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents ()
    {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        paragraphEClass = createEClass(PARAGRAPH);

        contentContainerEClass = createEClass(CONTENT_CONTAINER);
        createEAttribute(contentContainerEClass, CONTENT_CONTAINER__CONTENT);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__P);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__CODE);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__PLAIN_TEXT);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__UL);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__SIMPLE_TABLE);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__SECTION);
        createEReference(contentContainerEClass, CONTENT_CONTAINER__STRUCTURE_TABLE);

        spanEClass = createEClass(SPAN);

        emphasisEClass = createEClass(EMPHASIS);

        textContainerEClass = createEClass(TEXT_CONTAINER);
        createEAttribute(textContainerEClass, TEXT_CONTAINER__GROUP_CONTENT);
        createEReference(textContainerEClass, TEXT_CONTAINER__EMPHASIS);
        createEReference(textContainerEClass, TEXT_CONTAINER__IMAGE);
        createEReference(textContainerEClass, TEXT_CONTAINER__OBJECT_NAME);
        createEReference(textContainerEClass, TEXT_CONTAINER__XREF);
        createEReference(textContainerEClass, TEXT_CONTAINER__LINK);
        createEReference(textContainerEClass, TEXT_CONTAINER__QUOTE);

        imageEClass = createEClass(IMAGE);
        createEAttribute(imageEClass, IMAGE__SOURCE);
        createEAttribute(imageEClass, IMAGE__DATA);

        objectNameEClass = createEClass(OBJECT_NAME);

        plainTextContainerEClass = createEClass(PLAIN_TEXT_CONTAINER);
        createEAttribute(plainTextContainerEClass, PLAIN_TEXT_CONTAINER__CONTENT);

        codeParagraphEClass = createEClass(CODE_PARAGRAPH);
        createEAttribute(codeParagraphEClass, CODE_PARAGRAPH__LANGUAGE);

        crossReferenceEClass = createEClass(CROSS_REFERENCE);
        createEAttribute(crossReferenceEClass, CROSS_REFERENCE__TYPE);
        createEAttribute(crossReferenceEClass, CROSS_REFERENCE__ID);

        linkEClass = createEClass(LINK);
        createEAttribute(linkEClass, LINK__REF);
        createEAttribute(linkEClass, LINK__NAME);

        quoteEClass = createEClass(QUOTE);

        listEClass = createEClass(LIST);
        createEReference(listEClass, LIST__ITEM);

        listItemEClass = createEClass(LIST_ITEM);

        unorderedListEClass = createEClass(UNORDERED_LIST);

        simpleTableEClass = createEClass(SIMPLE_TABLE);
        createEReference(simpleTableEClass, SIMPLE_TABLE__COLUMN);
        createEReference(simpleTableEClass, SIMPLE_TABLE__ROW);

        columnEClass = createEClass(COLUMN);
        createEAttribute(columnEClass, COLUMN__TITLE);
        createEAttribute(columnEClass, COLUMN__HORIZONTAL_ALIGN);

        rowEClass = createEClass(ROW);
        createEReference(rowEClass, ROW__CELL);

        cellEClass = createEClass(CELL);

        sectionEClass = createEClass(SECTION);
        createEAttribute(sectionEClass, SECTION__ID);
        createEAttribute(sectionEClass, SECTION__TITLE);

        fragmentEClass = createEClass(FRAGMENT);

        structureTableEClass = createEClass(STRUCTURE_TABLE);
        createEReference(structureTableEClass, STRUCTURE_TABLE__ENTRY);

        tableEClass = createEClass(TABLE);
        createEAttribute(tableEClass, TABLE__MAXIMIZE);
        createEAttribute(tableEClass, TABLE__CAPTION);

        structureEntryEClass = createEClass(STRUCTURE_ENTRY);
        createEAttribute(structureEntryEClass, STRUCTURE_ENTRY__NAME);
        createEAttribute(structureEntryEClass, STRUCTURE_ENTRY__SIZE);
        createEAttribute(structureEntryEClass, STRUCTURE_ENTRY__TYPE_NAME);
        createEReference(structureEntryEClass, STRUCTURE_ENTRY__DESCRIPTION);
        createEReference(structureEntryEClass, STRUCTURE_ENTRY__SIZE_FIELD);

        structureEntryDescriptionEClass = createEClass(STRUCTURE_ENTRY_DESCRIPTION);

        // Create enums
        horizontalAlignEEnum = createEEnum(HORIZONTAL_ALIGN);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents ()
    {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        FragmentPackage theFragmentPackage = (FragmentPackage)EPackage.Registry.INSTANCE.getEPackage(FragmentPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        paragraphEClass.getESuperTypes().add(this.getTextContainer());
        spanEClass.getESuperTypes().add(this.getTextContainer());
        emphasisEClass.getESuperTypes().add(this.getSpan());
        textContainerEClass.getESuperTypes().add(this.getPlainTextContainer());
        objectNameEClass.getESuperTypes().add(this.getPlainTextContainer());
        plainTextContainerEClass.getESuperTypes().add(theFragmentPackage.getContent());
        codeParagraphEClass.getESuperTypes().add(this.getPlainTextContainer());
        crossReferenceEClass.getESuperTypes().add(this.getPlainTextContainer());
        quoteEClass.getESuperTypes().add(this.getSpan());
        listItemEClass.getESuperTypes().add(this.getTextContainer());
        unorderedListEClass.getESuperTypes().add(this.getList());
        unorderedListEClass.getESuperTypes().add(theFragmentPackage.getContent());
        simpleTableEClass.getESuperTypes().add(this.getTable());
        cellEClass.getESuperTypes().add(this.getTextContainer());
        sectionEClass.getESuperTypes().add(this.getContentContainer());
        fragmentEClass.getESuperTypes().add(this.getContentContainer());
        structureTableEClass.getESuperTypes().add(this.getTable());
        tableEClass.getESuperTypes().add(theFragmentPackage.getContent());
        structureEntryDescriptionEClass.getESuperTypes().add(this.getPlainTextContainer());

        // Initialize classes and features; add operations and parameters
        initEClass(paragraphEClass, Paragraph.class, "Paragraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(contentContainerEClass, ContentContainer.class, "ContentContainer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getContentContainer_Content(), ecorePackage.getEFeatureMapEntry(), "content", null, 0, -1, ContentContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_P(), this.getParagraph(), null, "p", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_Code(), this.getCodeParagraph(), null, "code", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_PlainText(), theFragmentPackage.getPlainTextContent(), null, "plainText", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_Ul(), this.getUnorderedList(), null, "ul", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_SimpleTable(), this.getSimpleTable(), null, "simpleTable", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_Section(), this.getSection(), null, "section", null, 0, -1, ContentContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getContentContainer_StructureTable(), this.getStructureTable(), null, "structureTable", null, 0, -1, ContentContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(spanEClass, Span.class, "Span", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(emphasisEClass, Emphasis.class, "Emphasis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(textContainerEClass, TextContainer.class, "TextContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTextContainer_GroupContent(), ecorePackage.getEFeatureMapEntry(), "groupContent", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_Emphasis(), this.getEmphasis(), null, "emphasis", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_Image(), this.getImage(), null, "image", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_ObjectName(), this.getObjectName(), null, "objectName", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_Xref(), this.getCrossReference(), null, "xref", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_Link(), this.getLink(), null, "link", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTextContainer_Quote(), this.getQuote(), null, "quote", null, 0, -1, TextContainer.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(imageEClass, Image.class, "Image", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getImage_Source(), ecorePackage.getEString(), "source", null, 0, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getImage_Data(), ecorePackage.getEByteArray(), "data", null, 0, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(objectNameEClass, ObjectName.class, "ObjectName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(plainTextContainerEClass, PlainTextContainer.class, "PlainTextContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPlainTextContainer_Content(), ecorePackage.getEFeatureMapEntry(), "content", null, 0, -1, PlainTextContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(codeParagraphEClass, CodeParagraph.class, "CodeParagraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCodeParagraph_Language(), ecorePackage.getEString(), "language", null, 0, 1, CodeParagraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(crossReferenceEClass, CrossReference.class, "CrossReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCrossReference_Type(), ecorePackage.getEString(), "type", null, 1, 1, CrossReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCrossReference_Id(), ecorePackage.getEString(), "id", null, 1, 1, CrossReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(linkEClass, Link.class, "Link", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLink_Ref(), ecorePackage.getEString(), "ref", null, 1, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLink_Name(), ecorePackage.getEString(), "name", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(quoteEClass, Quote.class, "Quote", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(listEClass, List.class, "List", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getList_Item(), this.getListItem(), null, "item", null, 0, -1, List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(listItemEClass, ListItem.class, "ListItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(unorderedListEClass, UnorderedList.class, "UnorderedList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(simpleTableEClass, SimpleTable.class, "SimpleTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSimpleTable_Column(), this.getColumn(), null, "column", null, 0, -1, SimpleTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSimpleTable_Row(), this.getRow(), null, "row", null, 0, -1, SimpleTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(columnEClass, Column.class, "Column", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getColumn_Title(), ecorePackage.getEString(), "title", null, 0, 1, Column.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getColumn_HorizontalAlign(), this.getHorizontalAlign(), "horizontalAlign", "LEFT", 1, 1, Column.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(rowEClass, Row.class, "Row", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getRow_Cell(), this.getCell(), null, "cell", null, 0, -1, Row.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(cellEClass, Cell.class, "Cell", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(sectionEClass, Section.class, "Section", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getSection_Id(), ecorePackage.getEString(), "id", null, 0, 1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSection_Title(), ecorePackage.getEString(), "title", null, 1, 1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(fragmentEClass, Fragment.class, "Fragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(structureTableEClass, StructureTable.class, "StructureTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getStructureTable_Entry(), this.getStructureEntry(), null, "entry", null, 0, -1, StructureTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(tableEClass, Table.class, "Table", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTable_Maximize(), ecorePackage.getEBoolean(), "maximize", null, 0, 1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTable_Caption(), ecorePackage.getEString(), "caption", null, 0, 1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(structureEntryEClass, StructureEntry.class, "StructureEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStructureEntry_Name(), ecorePackage.getEString(), "name", null, 1, 1, StructureEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStructureEntry_Size(), ecorePackage.getEIntegerObject(), "size", null, 0, 1, StructureEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStructureEntry_TypeName(), ecorePackage.getEString(), "typeName", null, 1, 1, StructureEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStructureEntry_Description(), this.getStructureEntryDescription(), null, "description", null, 1, 1, StructureEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStructureEntry_SizeField(), this.getStructureEntry(), null, "sizeField", null, 0, 1, StructureEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(structureEntryDescriptionEClass, StructureEntryDescription.class, "StructureEntryDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        // Initialize enums and add enum literals
        initEEnum(horizontalAlignEEnum, HorizontalAlign.class, "HorizontalAlign");
        addEEnumLiteral(horizontalAlignEEnum, HorizontalAlign.LEFT);
        addEEnumLiteral(horizontalAlignEEnum, HorizontalAlign.CENTER);
        addEEnumLiteral(horizontalAlignEEnum, HorizontalAlign.RIGHT);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations();
    }

    /**
     * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void createExtendedMetaDataAnnotations ()
    {
        String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
        addAnnotation
          (paragraphEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (contentContainerEClass, 
           source, 
           new String[] 
           {
             "name", "contentContainer"
           });		
        addAnnotation
          (getContentContainer_Content(), 
           source, 
           new String[] 
           {
             "name", "group:0",
             "kind", "group"
           });		
        addAnnotation
          (getContentContainer_P(), 
           source, 
           new String[] 
           {
             "group", "#group:0",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getContentContainer_Code(), 
           source, 
           new String[] 
           {
             "group", "#group:0",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getContentContainer_PlainText(), 
           source, 
           new String[] 
           {
             "group", "#group:0",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getContentContainer_Ul(), 
           source, 
           new String[] 
           {
             "group", "#group:0",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getContentContainer_SimpleTable(), 
           source, 
           new String[] 
           {
             "group", "#group:0",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getContentContainer_Section(), 
           source, 
           new String[] 
           {
             "name", "section",
             "namespace", "##targetNamespace",
             "group", "#group:0"
           });		
        addAnnotation
          (getContentContainer_StructureTable(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "group", "#group:0"
           });		
        addAnnotation
          (spanEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (emphasisEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });			
        addAnnotation
          (textContainerEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (getTextContainer_GroupContent(), 
           source, 
           new String[] 
           {
             "name", "groupContent:0",
             "kind", "group",
             "group", "#:mixed"
           });		
        addAnnotation
          (getTextContainer_Emphasis(), 
           source, 
           new String[] 
           {
             "kind", "element",
             "namespace", "##targetNamespace",
             "name", "em",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getTextContainer_Image(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getTextContainer_ObjectName(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getTextContainer_Xref(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getTextContainer_Link(), 
           source, 
           new String[] 
           {
             "kind", "element",
             "namespace", "##targetNamespace",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getTextContainer_Quote(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element",
             "name", "q",
             "group", "#groupContent:0"
           });		
        addAnnotation
          (getImage_Source(), 
           source, 
           new String[] 
           {
             "kind", "attribute"
           });		
        addAnnotation
          (getImage_Data(), 
           source, 
           new String[] 
           {
             "kind", "element"
           });		
        addAnnotation
          (objectNameEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });			
        addAnnotation
          (plainTextContainerEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (getPlainTextContainer_Content(), 
           source, 
           new String[] 
           {
             "name", ":mixed",
             "kind", "elementWildcard"
           });		
        addAnnotation
          (codeParagraphEClass, 
           source, 
           new String[] 
           {
             "name", "CodeParagraph",
             "kind", "mixed"
           });		
        addAnnotation
          (getCodeParagraph_Language(), 
           source, 
           new String[] 
           {
             "name", "language",
             "kind", "attribute",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (crossReferenceEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (quoteEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (getList_Item(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element",
             "name", "item"
           });		
        addAnnotation
          (listItemEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (unorderedListEClass, 
           source, 
           new String[] 
           {
             "name", "UnorderedList"
           });			
        addAnnotation
          (getSimpleTable_Column(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element"
           });		
        addAnnotation
          (getSimpleTable_Row(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element"
           });		
        addAnnotation
          (getColumn_Title(), 
           source, 
           new String[] 
           {
             "name", "title",
             "kind", "attribute",
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getColumn_HorizontalAlign(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "attribute"
           });		
        addAnnotation
          (getRow_Cell(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "element"
           });		
        addAnnotation
          (cellEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });		
        addAnnotation
          (horizontalAlignEEnum, 
           source, 
           new String[] 
           {
             "name", "HorizontalAlign"
           });			
        addAnnotation
          (getSection_Title(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace",
             "kind", "attribute"
           });		
        addAnnotation
          (fragmentEClass, 
           source, 
           new String[] 
           {
             "name", "fragment"
           });		
        addAnnotation
          (getStructureTable_Entry(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getTable_Maximize(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getStructureEntry_Name(), 
           source, 
           new String[] 
           {
             "name", "name"
           });		
        addAnnotation
          (getStructureEntry_Size(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getStructureEntry_TypeName(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (getStructureEntry_Description(), 
           source, 
           new String[] 
           {
             "namespace", "##targetNamespace"
           });		
        addAnnotation
          (structureEntryDescriptionEClass, 
           source, 
           new String[] 
           {
             "kind", "mixed"
           });
    }

} //ContentPackageImpl
