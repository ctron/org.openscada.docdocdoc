/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Emphasis;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Emphasis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EmphasisImpl extends SpanImpl implements Emphasis
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EmphasisImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.EMPHASIS;
    }

} //EmphasisImpl
