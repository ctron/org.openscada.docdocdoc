/**
 */
package org.openscada.doc.content.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Section;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getContent <em>Content</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getP <em>P</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getCode <em>Code</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getPlainText <em>Plain Text</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getUl <em>Ul</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getSimpleTable <em>Simple Table</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getSection <em>Section</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.FragmentImpl#getStructureTable <em>Structure Table</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FragmentImpl extends EObjectImpl implements Fragment
{
    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected FeatureMap content;

    /**
     * The cached value of the '{@link #getSection() <em>Section</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSection()
     * @generated
     * @ordered
     */
    protected EList<Section> section;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected FragmentImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.FRAGMENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FeatureMap getContent ()
    {
        if (content == null)
        {
            content = new BasicFeatureMap(this, ContentPackage.FRAGMENT__CONTENT);
        }
        return content;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Section> getSection ()
    {
        if (section == null)
        {
            section = new EObjectContainmentEList<Section>(Section.class, this, ContentPackage.FRAGMENT__SECTION);
        }
        return section;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<StructureTable> getStructureTable()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__STRUCTURE_TABLE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Paragraph> getP ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__P);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<CodeParagraph> getCode ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__CODE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<PlainTextContent> getPlainText ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__PLAIN_TEXT);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<UnorderedList> getUl ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__UL);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<SimpleTable> getSimpleTable ()
    {
        return getContent().list(ContentPackage.Literals.CONTENT_CONTAINER__SIMPLE_TABLE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
        switch (featureID)
        {
            case ContentPackage.FRAGMENT__CONTENT:
                return ((InternalEList<?>)getContent()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__P:
                return ((InternalEList<?>)getP()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__CODE:
                return ((InternalEList<?>)getCode()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__PLAIN_TEXT:
                return ((InternalEList<?>)getPlainText()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__UL:
                return ((InternalEList<?>)getUl()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__SIMPLE_TABLE:
                return ((InternalEList<?>)getSimpleTable()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__SECTION:
                return ((InternalEList<?>)getSection()).basicRemove(otherEnd, msgs);
            case ContentPackage.FRAGMENT__STRUCTURE_TABLE:
                return ((InternalEList<?>)getStructureTable()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch (featureID)
        {
            case ContentPackage.FRAGMENT__CONTENT:
                if (coreType) return getContent();
                return ((FeatureMap.Internal)getContent()).getWrapper();
            case ContentPackage.FRAGMENT__P:
                return getP();
            case ContentPackage.FRAGMENT__CODE:
                return getCode();
            case ContentPackage.FRAGMENT__PLAIN_TEXT:
                return getPlainText();
            case ContentPackage.FRAGMENT__UL:
                return getUl();
            case ContentPackage.FRAGMENT__SIMPLE_TABLE:
                return getSimpleTable();
            case ContentPackage.FRAGMENT__SECTION:
                return getSection();
            case ContentPackage.FRAGMENT__STRUCTURE_TABLE:
                return getStructureTable();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch (featureID)
        {
            case ContentPackage.FRAGMENT__CONTENT:
                ((FeatureMap.Internal)getContent()).set(newValue);
                return;
            case ContentPackage.FRAGMENT__P:
                getP().clear();
                getP().addAll((Collection<? extends Paragraph>)newValue);
                return;
            case ContentPackage.FRAGMENT__CODE:
                getCode().clear();
                getCode().addAll((Collection<? extends CodeParagraph>)newValue);
                return;
            case ContentPackage.FRAGMENT__PLAIN_TEXT:
                getPlainText().clear();
                getPlainText().addAll((Collection<? extends PlainTextContent>)newValue);
                return;
            case ContentPackage.FRAGMENT__UL:
                getUl().clear();
                getUl().addAll((Collection<? extends UnorderedList>)newValue);
                return;
            case ContentPackage.FRAGMENT__SIMPLE_TABLE:
                getSimpleTable().clear();
                getSimpleTable().addAll((Collection<? extends SimpleTable>)newValue);
                return;
            case ContentPackage.FRAGMENT__SECTION:
                getSection().clear();
                getSection().addAll((Collection<? extends Section>)newValue);
                return;
            case ContentPackage.FRAGMENT__STRUCTURE_TABLE:
                getStructureTable().clear();
                getStructureTable().addAll((Collection<? extends StructureTable>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.FRAGMENT__CONTENT:
                getContent().clear();
                return;
            case ContentPackage.FRAGMENT__P:
                getP().clear();
                return;
            case ContentPackage.FRAGMENT__CODE:
                getCode().clear();
                return;
            case ContentPackage.FRAGMENT__PLAIN_TEXT:
                getPlainText().clear();
                return;
            case ContentPackage.FRAGMENT__UL:
                getUl().clear();
                return;
            case ContentPackage.FRAGMENT__SIMPLE_TABLE:
                getSimpleTable().clear();
                return;
            case ContentPackage.FRAGMENT__SECTION:
                getSection().clear();
                return;
            case ContentPackage.FRAGMENT__STRUCTURE_TABLE:
                getStructureTable().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.FRAGMENT__CONTENT:
                return content != null && !content.isEmpty();
            case ContentPackage.FRAGMENT__P:
                return !getP().isEmpty();
            case ContentPackage.FRAGMENT__CODE:
                return !getCode().isEmpty();
            case ContentPackage.FRAGMENT__PLAIN_TEXT:
                return !getPlainText().isEmpty();
            case ContentPackage.FRAGMENT__UL:
                return !getUl().isEmpty();
            case ContentPackage.FRAGMENT__SIMPLE_TABLE:
                return !getSimpleTable().isEmpty();
            case ContentPackage.FRAGMENT__SECTION:
                return section != null && !section.isEmpty();
            case ContentPackage.FRAGMENT__STRUCTURE_TABLE:
                return !getStructureTable().isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (content: ");
        result.append(content);
        result.append(')');
        return result.toString();
    }

} //FragmentImpl
