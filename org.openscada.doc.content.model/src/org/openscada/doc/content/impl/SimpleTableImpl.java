/**
 */
package org.openscada.doc.content.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.SimpleTable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.SimpleTableImpl#isMaximize <em>Maximize</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SimpleTableImpl#getCaption <em>Caption</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SimpleTableImpl#getColumn <em>Column</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.SimpleTableImpl#getRow <em>Row</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimpleTableImpl extends EObjectImpl implements SimpleTable
{
    /**
     * The default value of the '{@link #isMaximize() <em>Maximize</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isMaximize()
     * @generated
     * @ordered
     */
    protected static final boolean MAXIMIZE_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isMaximize() <em>Maximize</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isMaximize()
     * @generated
     * @ordered
     */
    protected boolean maximize = MAXIMIZE_EDEFAULT;

    /**
     * The default value of the '{@link #getCaption() <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCaption()
     * @generated
     * @ordered
     */
    protected static final String CAPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCaption() <em>Caption</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCaption()
     * @generated
     * @ordered
     */
    protected String caption = CAPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getColumn() <em>Column</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getColumn()
     * @generated
     * @ordered
     */
    protected EList<Column> column;

    /**
     * The cached value of the '{@link #getRow() <em>Row</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRow()
     * @generated
     * @ordered
     */
    protected EList<Row> row;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected SimpleTableImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.SIMPLE_TABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Row> getRow ()
    {
        if (row == null)
        {
            row = new EObjectContainmentEList<Row>(Row.class, this, ContentPackage.SIMPLE_TABLE__ROW);
        }
        return row;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isMaximize ()
    {
        return maximize;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setMaximize ( boolean newMaximize )
    {
        boolean oldMaximize = maximize;
        maximize = newMaximize;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.SIMPLE_TABLE__MAXIMIZE, oldMaximize, maximize));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getCaption()
    {
        return caption;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setCaption(String newCaption)
    {
        String oldCaption = caption;
        caption = newCaption;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.SIMPLE_TABLE__CAPTION, oldCaption, caption));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Column> getColumn ()
    {
        if (column == null)
        {
            column = new EObjectContainmentEList<Column>(Column.class, this, ContentPackage.SIMPLE_TABLE__COLUMN);
        }
        return column;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove ( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
        switch (featureID)
        {
            case ContentPackage.SIMPLE_TABLE__COLUMN:
                return ((InternalEList<?>)getColumn()).basicRemove(otherEnd, msgs);
            case ContentPackage.SIMPLE_TABLE__ROW:
                return ((InternalEList<?>)getRow()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch (featureID)
        {
            case ContentPackage.SIMPLE_TABLE__MAXIMIZE:
                return isMaximize();
            case ContentPackage.SIMPLE_TABLE__CAPTION:
                return getCaption();
            case ContentPackage.SIMPLE_TABLE__COLUMN:
                return getColumn();
            case ContentPackage.SIMPLE_TABLE__ROW:
                return getRow();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings ( "unchecked" )
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch (featureID)
        {
            case ContentPackage.SIMPLE_TABLE__MAXIMIZE:
                setMaximize((Boolean)newValue);
                return;
            case ContentPackage.SIMPLE_TABLE__CAPTION:
                setCaption((String)newValue);
                return;
            case ContentPackage.SIMPLE_TABLE__COLUMN:
                getColumn().clear();
                getColumn().addAll((Collection<? extends Column>)newValue);
                return;
            case ContentPackage.SIMPLE_TABLE__ROW:
                getRow().clear();
                getRow().addAll((Collection<? extends Row>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.SIMPLE_TABLE__MAXIMIZE:
                setMaximize(MAXIMIZE_EDEFAULT);
                return;
            case ContentPackage.SIMPLE_TABLE__CAPTION:
                setCaption(CAPTION_EDEFAULT);
                return;
            case ContentPackage.SIMPLE_TABLE__COLUMN:
                getColumn().clear();
                return;
            case ContentPackage.SIMPLE_TABLE__ROW:
                getRow().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.SIMPLE_TABLE__MAXIMIZE:
                return maximize != MAXIMIZE_EDEFAULT;
            case ContentPackage.SIMPLE_TABLE__CAPTION:
                return CAPTION_EDEFAULT == null ? caption != null : !CAPTION_EDEFAULT.equals(caption);
            case ContentPackage.SIMPLE_TABLE__COLUMN:
                return column != null && !column.isEmpty();
            case ContentPackage.SIMPLE_TABLE__ROW:
                return row != null && !row.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (maximize: ");
        result.append(maximize);
        result.append(", caption: ");
        result.append(caption);
        result.append(')');
        return result.toString();
    }

} //SimpleTableImpl
