/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.ListItem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>List Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ListItemImpl extends TextContainerImpl implements ListItem
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ListItemImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.LIST_ITEM;
    }

} //ListItemImpl
