/**
 */
package org.openscada.doc.content.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.HorizontalAlign;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.doc.content.impl.ColumnImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.openscada.doc.content.impl.ColumnImpl#getHorizontalAlign <em>Horizontal Align</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ColumnImpl extends EObjectImpl implements Column
{
    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getHorizontalAlign() <em>Horizontal Align</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getHorizontalAlign()
     * @generated
     * @ordered
     */
    protected static final HorizontalAlign HORIZONTAL_ALIGN_EDEFAULT = HorizontalAlign.LEFT;

    /**
     * The cached value of the '{@link #getHorizontalAlign() <em>Horizontal Align</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getHorizontalAlign()
     * @generated
     * @ordered
     */
    protected HorizontalAlign horizontalAlign = HORIZONTAL_ALIGN_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ColumnImpl ()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ContentPackage.Literals.COLUMN;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getTitle ()
    {
        return title;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setTitle ( String newTitle )
    {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.COLUMN__TITLE, oldTitle, title));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HorizontalAlign getHorizontalAlign ()
    {
        return horizontalAlign;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setHorizontalAlign ( HorizontalAlign newHorizontalAlign )
    {
        HorizontalAlign oldHorizontalAlign = horizontalAlign;
        horizontalAlign = newHorizontalAlign == null ? HORIZONTAL_ALIGN_EDEFAULT : newHorizontalAlign;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ContentPackage.COLUMN__HORIZONTAL_ALIGN, oldHorizontalAlign, horizontalAlign));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch (featureID)
        {
            case ContentPackage.COLUMN__TITLE:
                return getTitle();
            case ContentPackage.COLUMN__HORIZONTAL_ALIGN:
                return getHorizontalAlign();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch (featureID)
        {
            case ContentPackage.COLUMN__TITLE:
                setTitle((String)newValue);
                return;
            case ContentPackage.COLUMN__HORIZONTAL_ALIGN:
                setHorizontalAlign((HorizontalAlign)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.COLUMN__TITLE:
                setTitle(TITLE_EDEFAULT);
                return;
            case ContentPackage.COLUMN__HORIZONTAL_ALIGN:
                setHorizontalAlign(HORIZONTAL_ALIGN_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch (featureID)
        {
            case ContentPackage.COLUMN__TITLE:
                return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
            case ContentPackage.COLUMN__HORIZONTAL_ALIGN:
                return horizontalAlign != HORIZONTAL_ALIGN_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (title: ");
        result.append(title);
        result.append(", horizontalAlign: ");
        result.append(horizontalAlign);
        result.append(')');
        return result.toString();
    }

} //ColumnImpl
