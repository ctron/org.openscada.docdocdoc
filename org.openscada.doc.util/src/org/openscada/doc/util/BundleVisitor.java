package org.openscada.doc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.openscada.doc.util.FeatureBuilder.Feature;
import org.openscada.doc.util.FeatureBuilder.Plugin;
import org.osgi.framework.BundleContext;

public class BundleVisitor
{
    public interface Visitor
    {
        public void visit ( String pluginId, IProject project );
    }

    private final String featureId;

    private final BundleContext context;

    private Map<String, Feature> features;

    public BundleVisitor ( final BundleContext context, final String featureId )
    {
        this.featureId = featureId;
        this.context = context;
    }

    public void visit ( final Visitor visitor ) throws Exception
    {
        final FeatureBuilder featureBuilder = new FeatureBuilder ( this.context );
        this.features = featureBuilder.build ();
        featureBuilder.dispose ();

        final Set<String> visitedPlugin = new HashSet<String> ();
        final Set<String> visitedFeature = new HashSet<String> ();
        final Map<String, IProject> plugins = new HashMap<String, IProject> ();

        // record projects
        visitFeature ( new Visitor () {
            @Override
            public void visit ( final String pluginId, final IProject project )
            {
                plugins.put ( pluginId, project );
            }
        }, this.featureId, visitedFeature, visitedPlugin );

        // sort
        final List<String> ids = new ArrayList<String> ( plugins.keySet () );
        Collections.sort ( ids );

        // call
        for ( final String pluginId : ids )
        {
            final IProject p = plugins.get ( pluginId );
            visitor.visit ( pluginId, p );
        }
    }

    private void visitFeature ( final Visitor visitor, final String featureId, final Set<String> visitedFeature, final Set<String> visitedPlugin ) throws Exception
    {
        final Feature feature = this.features.get ( featureId );

        if ( feature == null )
        {
            throw new IllegalStateException ( String.format ( "Feature %s is unknown", featureId ) );
        }

        visitedFeature.add ( featureId );

        for ( final Plugin plugin : feature.getIncludedPlugins () )
        {
            if ( !visitedPlugin.contains ( plugin.getId () ) )
            {
                visitedPlugin.add ( plugin.getId () );
                visitor.visit ( plugin.getId (), plugin.getProject () );
            }
        }

        for ( final String includedFeatureId : feature.getIncludedFeatures () )
        {
            if ( !visitedFeature.contains ( includedFeatureId ) )
            {
                visitedFeature.add ( includedFeatureId );
                visitFeature ( visitor, includedFeatureId, visitedFeature, visitedPlugin );
            }
        }
    }
}
