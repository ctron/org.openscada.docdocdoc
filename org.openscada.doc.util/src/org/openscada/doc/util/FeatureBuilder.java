package org.openscada.doc.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.pde.core.project.IBundleProjectDescription;
import org.eclipse.pde.core.project.IBundleProjectService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class FeatureBuilder
{

    public static class Plugin
    {
        private final String id;

        private final IProject project;

        public Plugin ( final String id, final IProject project )
        {
            this.id = id;
            this.project = project;
        }

        public String getId ()
        {
            return this.id;
        }

        public IProject getProject ()
        {
            return this.project;
        }
    }

    public static class Feature
    {
        private Set<String> includedFeatures = new HashSet<String> ();

        private Set<Plugin> includedPlugins = new HashSet<Plugin> ();

        private final String id;

        public Feature ( final String id, final Set<String> includedFeatures, final Set<Plugin> includedPlugins )
        {
            this.id = id;
            this.includedFeatures = includedFeatures;
            this.includedPlugins = includedPlugins;
        }

        public String getId ()
        {
            return this.id;
        }

        public Set<String> getIncludedFeatures ()
        {
            return this.includedFeatures;
        }

        public Set<Plugin> getIncludedPlugins ()
        {
            return this.includedPlugins;
        }
    }

    private final DocumentBuilderFactory dbf;

    private final DocumentBuilder db;

    private final IProject[] projects;

    private final ServiceReference<IBundleProjectService> ref;

    private IBundleProjectService service;

    private final BundleContext context;

    public FeatureBuilder ( final BundleContext context ) throws ParserConfigurationException
    {
        this.context = context;
        this.dbf = DocumentBuilderFactory.newInstance ();
        this.db = this.dbf.newDocumentBuilder ();
        this.projects = ResourcesPlugin.getWorkspace ().getRoot ().getProjects ();

        this.ref = context.getServiceReference ( IBundleProjectService.class );
        this.service = context.getService ( this.ref );
    }

    public void dispose ()
    {
        this.context.ungetService ( this.ref );
        this.service = null;
    }

    private Feature convert ( final Map<String, IProject> pluginMap, final IFile featureFile ) throws Exception
    {
        final InputStream in = featureFile.getContents ();
        try
        {
            final Document doc = this.db.parse ( in );

            final String id = doc.getDocumentElement ().getAttribute ( "id" );
            final Set<String> includedFeatures = new HashSet<String> ();
            final Set<Plugin> includedPlugins = new HashSet<Plugin> ();

            {
                final NodeList list = doc.getElementsByTagName ( "includes" );
                for ( int i = 0; i < list.getLength (); i++ )
                {
                    final Element ele = (Element)list.item ( i );
                    includedFeatures.add ( ele.getAttribute ( "id" ) );
                }
            }

            {
                final NodeList list = doc.getElementsByTagName ( "plugin" );
                for ( int i = 0; i < list.getLength (); i++ )
                {
                    final Element ele = (Element)list.item ( i );
                    final String pluginId = ele.getAttribute ( "id" );

                    includedPlugins.add ( new Plugin ( pluginId, pluginMap.get ( pluginId ) ) );
                }
            }

            return new Feature ( id, includedFeatures, includedPlugins );
        }
        finally
        {
            in.close ();
        }
    }

    public Map<String, Feature> build () throws Exception
    {
        final Map<String, IProject> projectMap = new HashMap<String, IProject> ();
        final Map<String, Feature> features = new HashMap<String, FeatureBuilder.Feature> ();

        for ( final IProject project : this.projects )
        {
            if ( !project.isAccessible () )
            {
                continue;
            }

            final IBundleProjectDescription desc = this.service.getDescription ( project );
            if ( desc == null )
            {
                continue;
            }

            final String name = desc.getSymbolicName ();
            if ( name == null )
            {
                continue;
            }

            projectMap.put ( name, project );
        }

        for ( final IProject project : this.projects )
        {
            if ( !project.isAccessible () )
            {
                continue;
            }

            final IFile featureFile = project.getFile ( "feature.xml" );
            if ( !featureFile.isAccessible () )
            {
                continue;
            }

            final Feature feature = convert ( projectMap, featureFile );
            if ( feature != null )
            {
                features.put ( feature.getId (), feature );
            }
        }

        return features;
    }

}
