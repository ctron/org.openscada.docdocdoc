/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.openscada.doc.builder.PostProcessor;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Content;

public class CollapseProperties implements PostProcessor
{

    @Override
    public void postProcess ( final IProgressMonitor monitor, final Book book ) throws Exception
    {
        processBookContainer ( book );
    }

    private void processBookContainer ( final BookContainer bookContainter )
    {
        processContent ( bookContainter.getContent () );
        for ( final BookSection section : bookContainter.getSections () )
        {
            processContent ( section.getContent () );
            processBookContainer ( section );
        }
    }

    private void processContent ( final List<Content> contents )
    {
        Properties lastProperties = null;
        for ( final Content content : new ArrayList<Content> ( contents ) )
        {
            if ( content instanceof Properties )
            {
                if ( lastProperties == null )
                {
                    lastProperties = (Properties)content;
                }
                else
                {
                    merge ( lastProperties, (Properties)content );
                    EcoreUtil.remove ( content );
                }
            }
            else
            {
                lastProperties = null;
            }
        }
    }

    private void merge ( final Properties lastProperties, final Properties content )
    {
        // merge plain
        lastProperties.getProperties ().addAll ( content.getProperties () );

        // merge groups
        for ( final PropertiesGroup pg1 : new ArrayList<PropertiesGroup> ( content.getGroups () ) )
        {
            PropertiesGroup pg2 = findGroup ( lastProperties, pg1.getPrefix () );
            if ( pg2 == null )
            {
                pg2 = pg1;
                lastProperties.getGroups ().add ( pg2 );
            }
            else
            {
                pg2.getProperties ().addAll ( pg1.getProperties () );
            }
        }
    }

    private PropertiesGroup findGroup ( final Properties lastProperties, final String prefix )
    {
        for ( final PropertiesGroup pg : lastProperties.getGroups () )
        {
            if ( prefix.equals ( pg.getPrefix () ) )
            {
                return pg;
            }
        }

        return null;
    }

}
