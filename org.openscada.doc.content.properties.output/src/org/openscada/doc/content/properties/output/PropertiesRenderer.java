/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.io.PrintStream;

import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PropertiesRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        final Properties props = (Properties)content;

        out.println ( "<table class=\"properties\">" );
        out.println ( "<thead>" );
        out.println ( "<tr><th>Name</th><th>Type</th><th>Description</th></tr>" );
        out.println ( "</thead>" );

        out.println ( "<tbody>" );

        for ( final Property prop : props.getProperties () )
        {
            renderProperty ( context, out, prop, null );
        }

        for ( final PropertiesGroup group : props.getGroups () )
        {
            out.println ( String.format ( "<tr><td class=\"property_group\" colspan=\"3\">%s.*</td></tr>", Helper.encodeHtml ( group.getPrefix () ) ) );

            for ( final Property prop : group.getProperties () )
            {
                renderProperty ( context, out, prop, group.getPrefix () );
            }
        }

        out.println ( "</tbody>" );

        out.println ( "</table>" );
    }

    private void renderProperty ( final RenderContext context, final PrintStream out, final Property prop, final String prefix )
    {
        out.print ( "<tr class=\"property_ref\">" );

        final String name;
        if ( prefix != null && !prefix.isEmpty () && prop.getName () != null && prop.getName ().startsWith ( prefix ) )
        {
            name = "*" + prop.getName ().substring ( prefix.length () );
        }
        else
        {
            name = prop.getName ();
        }

        final String type = prop.getDataType () == null ? "STRING" : prop.getDataType ().toString ();

        final String xrefId = context.createCrossReference ( "property", prop.getName () );

        out.print ( String.format ( "<td><a href=\"#%4$s\"><code>%1$s</code></a></td><td>%2$s</td><td>%3$s</td>", Helper.encodeHtml ( name ), Helper.encodeHtml ( type ), Helper.encodeHtml ( prop.getShortDescription () ), xrefId ) );

        out.println ( "</tr>" );
    }
}
