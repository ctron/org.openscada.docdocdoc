/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.io.PrintStream;
import java.util.List;

import org.openscada.doc.content.properties.DataType;
import org.openscada.doc.content.properties.PropertiesDefinition;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PropertiesDefinitionRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final List<Property> properties = ( (PropertiesDefinition)content ).getProperties ();

        for ( final Property property : properties )
        {
            renderProperty ( context, context.getCurrentPrintStream (), property );
        }
    }

    private void renderProperty ( final RenderContext context, final PrintStream out, final Property property )
    {
        final String xrefId = context.createLinkTarget ( "property", property.getName () );

        out.println ( String.format ( "<div class=\"property_definition\" id=\"%2$s\">%1$s</div>", property.getName (), xrefId ) );
        out.print ( "<table class=\"property_definition\">" );

        out.print ( String.format ( "<tr><th>Short Description</th><td>%s</td></tr>", Helper.encodeHtml ( property.getShortDescription () ) ) );
        if ( property.getDataType () == null )
        {
            out.print ( "<tr><th>Type</th><td>STRING</td></tr>" );
        }
        else if ( property.getDataType () == DataType.CUSTOM )
        {
            out.print ( String.format ( "<tr><th>Type</th><td>%s</td></tr>", Helper.convertBreaks ( Helper.encodeHtml ( property.getCustomDataTypeDescription () ) ) ) );
        }
        else
        {
            out.print ( String.format ( "<tr><th>Type</th><td>%s</td></tr>", property.getDataType () ) );
        }

        if ( property.getDefaultValue () != null )
        {
            out.println ( "<tr><th>Default Value</th><td>" + Helper.convertBreaks ( Helper.encodeHtml ( property.getDefaultValue () ) ) + "</td></tr>" );
        }

        out.println ( "</table>" );

        if ( property.getLongDescription () != null )
        {
            out.print ( "<div class=\"property_long\">" );
            out.print ( Helper.encodeHtml ( property.getLongDescription () ) );
            out.println ( "</div>" );
        }
    }
}
