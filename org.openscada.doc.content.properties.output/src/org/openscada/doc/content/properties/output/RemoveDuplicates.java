/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.builder.PostProcessor;
import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesGroup;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.model.doc.book.Book;

public class RemoveDuplicates implements PostProcessor
{

    @Override
    public void postProcess ( final IProgressMonitor monitor, final Book book ) throws Exception
    {
        final List<Properties> ps = new LinkedList<Properties> ();

        final TreeIterator<EObject> i = book.eAllContents ();
        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( o instanceof Properties )
            {
                ps.add ( (Properties)o );
            }
        }

        for ( final Properties p : ps )
        {
            cleanup ( p );
        }
    }

    private void cleanup ( final Properties properties )
    {
        // first clean the groups
        for ( final PropertiesGroup pg : properties.getGroups () )
        {
            cleanupList ( pg.getProperties () );
        }

        // propagate to global
        propagate ( properties );
        propagate ( properties ); // twice

        // cleanup global
        cleanupList ( properties.getProperties () );

        // sort groups
        ECollections.sort ( properties.getGroups (), new Comparator<PropertiesGroup> () {
            @Override
            public int compare ( final PropertiesGroup o1, final PropertiesGroup o2 )
            {
                return o1.getPrefix ().compareTo ( o2.getPrefix () );
            }
        } );
    }

    private void propagate ( final Properties properties )
    {

        final Set<String> ps = new HashSet<String> ();

        // pre-fill with globals .. so that all group entries will get pushed up for them
        for ( final Property p : properties.getProperties () )
        {
            ps.add ( p.getName () );
        }

        // check now
        final Set<Property> additions = new HashSet<Property> ();
        final Set<PropertiesGroup> removals = new HashSet<PropertiesGroup> ();
        for ( final PropertiesGroup pg : properties.getGroups () )
        {
            final Iterator<Property> i = pg.getProperties ().iterator ();
            while ( i.hasNext () )
            {
                final Property p = i.next ();
                if ( ps.contains ( p.getName () ) )
                {
                    // push to global group - needed for emf
                    additions.add ( p );
                    i.remove ();
                    if ( pg.getProperties ().isEmpty () )
                    {
                        // mark for removal
                        removals.add ( pg );
                    }
                }
                else
                {
                    ps.add ( p.getName () );
                }
            }
        }
        // perform
        properties.getProperties ().addAll ( additions );
        properties.getGroups ().removeAll ( removals );
    }

    private void cleanupList ( final EList<Property> properties )
    {
        final Map<String, Property> props = new HashMap<String, Property> ();
        for ( final Property p : properties )
        {
            final Property cp = props.get ( p.getName () );
            if ( cp == null )
            {
                props.put ( p.getName (), p );
            }
            else if ( cp.getShortDescription () == null || cp.getShortDescription ().isEmpty () )
            {
                props.put ( p.getName (), p );
            }
        }
        setProperties ( properties, props.values () );
    }

    private void setProperties ( final EList<Property> properties, final Collection<Property> values )
    {
        properties.clear ();
        properties.addAll ( values );
        ECollections.sort ( properties, Helper.COMPARATOR );
    }

}
