/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.model.doc.book.Book;

public class Helper
{

    public static final Comparator<Property> COMPARATOR = new Comparator<Property> () {

        @Override
        public int compare ( final Property o1, final Property o2 )
        {
            return o1.getName ().compareTo ( o2.getName () );
        }
    };

    public static List<Property> findProperties ( final Book book, final boolean sorted )
    {
        final TreeIterator<EObject> i = book.eAllContents ();

        final List<Property> properties = new ArrayList<Property> ();

        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( o instanceof Property )
            {
                properties.add ( (Property)o );
            }
        }

        if ( sorted )
        {
            sort ( properties );
        }

        return properties;
    }

    public static void sort ( final List<Property> properties )
    {
        Collections.sort ( properties, COMPARATOR );
    }

    public static void esort ( final EList<Property> properties )
    {
        ECollections.sort ( properties, COMPARATOR );
    }

}
