/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.openscada.doc.builder.PostProcessor;
import org.openscada.doc.content.properties.PropertiesDefinition;
import org.openscada.doc.content.properties.PropertiesDefinitionGenerator;
import org.openscada.doc.content.properties.PropertiesFactory;
import org.openscada.doc.content.properties.Property;
import org.openscada.doc.model.doc.book.Book;

public class GeneratePropertiesContent implements PostProcessor
{

    @Override
    public void postProcess ( final IProgressMonitor monitor, final Book book ) throws Exception
    {
        final SubProgressMonitor sub = new SubProgressMonitor ( monitor, 1 );
        try
        {
            process ( sub, book );
        }
        finally
        {
            sub.done ();
        }
    }

    private void process ( final IProgressMonitor monitor, final Book book ) throws Exception
    {
        final TreeIterator<EObject> i = book.eAllContents ();

        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( ! ( o instanceof PropertiesDefinitionGenerator ) )
            {
                continue;
            }

            final PropertiesDefinition details = createDefinitions ( book );

            EcoreUtil.replace ( o, details );
        }
    }

    private PropertiesDefinition createDefinitions ( final Book book )
    {
        List<Property> properties = org.openscada.doc.content.properties.output.Helper.findProperties ( book, false );

        properties = collapse ( properties );
        Helper.sort ( properties );

        final PropertiesDefinition details = PropertiesFactory.eINSTANCE.createPropertiesDefinition ();

        details.getProperties ().addAll ( properties );

        return details;
    }

    private List<Property> collapse ( final List<Property> properties )
    {
        final Map<String, Property> map = new HashMap<String, Property> ();

        for ( final Property p : properties )
        {
            if ( map.containsKey ( p.getName () ) )
            {
                final Property op = map.get ( p.getName () );
                if ( op.getShortDescription () == null || op.getShortDescription ().isEmpty () )
                {
                    map.put ( p.getName (), EcoreUtil.copy ( p ) );
                }
            }
            else
            {
                map.put ( p.getName (), EcoreUtil.copy ( p ) );
            }
        }

        return new ArrayList<Property> ( map.values () );
    }
}
