/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.properties.output;

import java.util.HashMap;
import java.util.Map;

import org.openscada.doc.content.properties.Properties;
import org.openscada.doc.content.properties.PropertiesDefinition;
import org.openscada.doc.output.simple.renderer.AbstractCacheFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PropertiesFactory extends AbstractCacheFactory
{

    private final Map<Entry, BookRenderer> cache = new HashMap<Entry, BookRenderer> ();

    public PropertiesFactory ()
    {
        this.cache.put ( new Entry ( "html", Properties.class ), new PropertiesRenderer () );
        this.cache.put ( new Entry ( "html", PropertiesDefinition.class ), new PropertiesDefinitionRenderer () );
    }

    @Override
    protected Map<Entry, BookRenderer> getCache ()
    {
        return this.cache;
    }

}
