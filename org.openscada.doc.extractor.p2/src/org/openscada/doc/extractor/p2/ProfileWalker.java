package org.openscada.doc.extractor.p2;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.IProvisioningAgentProvider;
import org.eclipse.equinox.p2.engine.IProfile;
import org.eclipse.equinox.p2.engine.IProfileRegistry;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.query.IQuery;
import org.eclipse.equinox.p2.query.IQueryResult;
import org.eclipse.equinox.p2.query.QueryUtil;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class ProfileWalker
{

    private final BundleContext context;

    private final ServiceTracker<IProvisioningAgentProvider, IProvisioningAgentProvider> agentTracker;

    private final File base;

    private String profileId;

    public ProfileWalker ( final File base, final BundleContext context )
    {
        this.base = base;
        this.context = context;
        this.agentTracker = new ServiceTracker<IProvisioningAgentProvider, IProvisioningAgentProvider> ( this.context, IProvisioningAgentProvider.class, null );
    }

    private void initBase () throws FileNotFoundException, IOException
    {
        final Properties p = new Properties ();

        final BufferedInputStream stream = new BufferedInputStream ( new FileInputStream ( new File ( this.base, "configuration/config.ini" ) ) );

        try
        {
            p.load ( stream );
        }
        finally
        {
            stream.close ();
        }

        this.profileId = p.getProperty ( "eclipse.p2.profile" );
    }

    public void walk ( final Visitor visitor ) throws Exception
    {
        initBase ();

        this.agentTracker.open ();
        try
        {
            process ( visitor );
        }
        finally
        {
            this.agentTracker.close ();
        }
    }

    private void process ( final Visitor visitor ) throws Exception
    {
        final IProvisioningAgentProvider agentProvider = this.agentTracker.getService ();
        final IProvisioningAgent agent = agentProvider.createAgent ( new File ( this.base, "p2" ).toURI () );
        try
        {
            final IProfileRegistry profileRegistroy = (IProfileRegistry)agent.getService ( IProfileRegistry.SERVICE_NAME );
            final IProfile profile = profileRegistroy.getProfile ( this.profileId );
            processProfile ( profile, visitor );
        }
        finally
        {
            agent.stop ();
        }
    }

    private void processProfile ( final IProfile profile, final Visitor visitor )
    {
        final IQuery<IInstallableUnit> query = QueryUtil.createIUAnyQuery ();
        final IQueryResult<IInstallableUnit> result = profile.query ( query, null );
        final Iterator<IInstallableUnit> i = result.iterator ();
        while ( i.hasNext () )
        {
            visitor.visit ( i.next () );
        }
    }
}
