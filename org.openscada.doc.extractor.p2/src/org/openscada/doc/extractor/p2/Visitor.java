package org.openscada.doc.extractor.p2;

import org.eclipse.equinox.p2.metadata.IInstallableUnit;

public interface Visitor
{
    public void visit ( IInstallableUnit unit );
}
