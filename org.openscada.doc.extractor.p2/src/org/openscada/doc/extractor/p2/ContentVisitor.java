package org.openscada.doc.extractor.p2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.equinox.p2.metadata.IArtifactKey;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.metadata.ILicense;
import org.eclipse.equinox.p2.metadata.IProvidedCapability;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.content.HorizontalAlign;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.util.ContentResourceFactoryImpl;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class ContentVisitor implements Visitor
{

    private final List<IInstallableUnit> bundles = new ArrayList<IInstallableUnit> ();

    private final List<IInstallableUnit> features = new ArrayList<IInstallableUnit> ();

    private final List<IInstallableUnit> groups = new ArrayList<IInstallableUnit> ();

    @Override
    public void visit ( final IInstallableUnit unit )
    {
        if ( Boolean.parseBoolean ( unit.getProperty ( "org.eclipse.equinox.p2.type.group" ) ) )
        {
            // we don't process groups
            return;
        }
        else if ( isFeature ( unit ) )
        {
            this.features.add ( unit );
        }
        else
        {
            this.bundles.add ( unit );
        }
    }

    private boolean isFeature ( final IInstallableUnit unit )
    {
        for ( final IProvidedCapability cap : unit.getProvidedCapabilities () )
        {
            if ( !"org.eclipse.equinox.p2.eclipse.type".equals ( cap.getNamespace () ) )
            {
                continue;
            }
            if ( "feature".equals ( cap.getName () ) )
            {
                return true;
            }
        }
        return false;
    }

    public void write ( final String uri ) throws IOException
    {
        Fragment fragment;
        fragment = ContentFactory.eINSTANCE.createFragment ();

        process ( fragment );

        final ContentResourceFactoryImpl rsf = new ContentResourceFactoryImpl ();
        final Resource resource = rsf.createResource ( URI.createURI ( uri ) );
        resource.getContents ().add ( fragment );
        resource.save ( null );
    }

    private void process ( final Fragment fragment )
    {
        Collections.sort ( this.features );
        Collections.sort ( this.bundles );

        final Multimap<String, String> featureMap = HashMultimap.create ();

        {
            final Paragraph p = ContentFactory.eINSTANCE.createParagraph ();
            FeatureMapUtil.addText ( p.getContent (), "The following features are installed:" );
            fragment.getP ().add ( p );

            final SimpleTable table = ContentFactory.eINSTANCE.createSimpleTable ();
            fragment.getSimpleTable ().add ( table );

            table.getColumn ().add ( createColumn ( "Symbolic Name" ) );
            table.getColumn ().add ( createColumn ( "Version" ) );
            table.getColumn ().add ( createColumn ( "Name" ) );
            table.getColumn ().add ( createColumn ( "Provider" ) );

            for ( final IInstallableUnit unit : this.features )
            {
                processFeature ( table, unit, featureMap );
            }
        }

        {
            final Paragraph p = ContentFactory.eINSTANCE.createParagraph ();
            FeatureMapUtil.addText ( p.getContent (), "The following bundles are installed:" );
            fragment.getP ().add ( p );

            final SimpleTable table = ContentFactory.eINSTANCE.createSimpleTable ();
            fragment.getSimpleTable ().add ( table );

            table.getColumn ().add ( createColumn ( "Symbolic Name" ) );
            table.getColumn ().add ( createColumn ( "Version" ) );
            table.getColumn ().add ( createColumn ( "Name" ) );
            table.getColumn ().add ( createColumn ( "Provider" ) );

            for ( final IInstallableUnit unit : this.bundles )
            {
                processBundle ( table, unit, featureMap );
            }
        }
    }

    private Column createColumn ( final String title )
    {
        final Column col = ContentFactory.eINSTANCE.createColumn ();

        col.setHorizontalAlign ( HorizontalAlign.LEFT );
        col.setTitle ( title );

        return col;
    }

    private void processFeature ( final SimpleTable table, final IInstallableUnit unit, final Multimap<String, String> featureMap )
    {
        final String id = unit.getId ();
        final String version = unit.getVersion ().toString ();

        for ( final IArtifactKey key : unit.getArtifacts () )
        {
            if ( "osgi.bundle".equals ( key.getClassifier () ) )
            {
                featureMap.put ( key.getId (), id );
            }
        }

        final java.net.URI copyrightUrl = unit.getCopyright ( null ).getLocation ();
        final String copyright = unit.getCopyright ( null ).getBody ();

        final ILicense lic = unit.getLicenses ( null ).isEmpty () ? null : unit.getLicenses ( null ).iterator ().next ();
        final java.net.URI licenseUrl = lic != null ? lic.getLocation () : null;
        final String license = lic != null ? lic.getBody () : null;

        final String name = unit.getProperty ( "org.eclipse.equinox.p2.name", null );
        final String provider = unit.getProperty ( "org.eclipse.equinox.p2.provider", null );
        final String description = unit.getProperty ( "org.eclipse.equinox.p2.description", null );
        final String url = unit.getProperty ( "org.eclipse.equinox.p2.doc.url", null );

        addRow ( table, id, version, name, provider );
    }

    private void addRow ( final SimpleTable table, final String symbolicName, final String version, final String name, final String provider )
    {
        final Row row = ContentFactory.eINSTANCE.createRow ();

        addTextCell ( row, symbolicName, true );
        addTextCell ( row, version, true );
        addTextCell ( row, name, false );
        addTextCell ( row, provider, false );

        table.getRow ().add ( row );
    }

    public void addTextCell ( final Row row, final String value, final boolean objectName )
    {
        final Cell cell = ContentFactory.eINSTANCE.createCell ();
        row.getCell ().add ( cell );
        if ( value != null )
        {
            if ( objectName )
            {
                final ObjectName on = ContentFactory.eINSTANCE.createObjectName ();
                FeatureMapUtil.addText ( on.getContent (), value );
                cell.getObjectName ().add ( on );
            }
            else
            {
                FeatureMapUtil.addText ( cell.getContent (), value );
            }
        }
    }

    private void processBundle ( final SimpleTable table, final IInstallableUnit unit, final Multimap<String, String> featureMap )
    {
        final String id = unit.getId ();
        final String version = unit.getVersion ().toString ();

        final String name = unit.getProperty ( "org.eclipse.equinox.p2.name", null );
        final String description = unit.getProperty ( "org.eclipse.equinox.p2.description", null );
        final String provider = unit.getProperty ( "org.eclipse.equinox.p2.provider", null );
        final String url = unit.getProperty ( "org.eclipse.equinox.p2.doc.url", null );

        addRow ( table, id, version, name, provider );
    }

}
