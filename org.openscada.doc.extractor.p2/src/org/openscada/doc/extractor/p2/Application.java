package org.openscada.doc.extractor.p2;

import java.io.File;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

public class Application implements IApplication
{

    @Override
    public Object start ( final IApplicationContext context ) throws Exception
    {
        return run ( (String[])context.getArguments ().get ( "application.args" ) ); //$NON-NLS-1$
    }

    private Object run ( final String[] strings ) throws Exception
    {

        for ( final String args : strings )
        {
            final String[] toks = args.split ( "#" );
            final ContentVisitor visitor = new ContentVisitor ();
            new ProfileWalker ( new File ( toks[0] ), Activator.getContext () ).walk ( visitor );
            visitor.write ( toks[1] );
        }

        return null;
    }

    @Override
    public void stop ()
    {
    }

}
