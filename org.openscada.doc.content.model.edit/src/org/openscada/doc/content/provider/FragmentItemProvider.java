/**
 */
package org.openscada.doc.content.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.openscada.doc.content.ContentFactory;
import org.openscada.doc.content.ContentPackage;
import org.openscada.doc.content.Fragment;
import org.openscada.doc.model.doc.fragment.FragmentFactory;

/**
 * This is the item provider adapter for a {@link org.openscada.doc.content.Fragment} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FragmentItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public FragmentItemProvider ( AdapterFactory adapterFactory )
    {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors ( Object object )
    {
        if (itemPropertyDescriptors == null)
        {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures ( Object object )
    {
        if (childrenFeatures == null)
        {
            super.getChildrenFeatures(object);
            childrenFeatures.add(ContentPackage.Literals.CONTENT_CONTAINER__CONTENT);
            childrenFeatures.add(ContentPackage.Literals.CONTENT_CONTAINER__SECTION);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature ( Object object, Object child )
    {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns Fragment.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage ( Object object )
    {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/Fragment"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText ( Object object )
    {
        return getString("_UI_Fragment_type");
    }

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged ( Notification notification )
    {
        updateChildren(notification);

        switch (notification.getFeatureID(Fragment.class))
        {
            case ContentPackage.FRAGMENT__CONTENT:
            case ContentPackage.FRAGMENT__SECTION:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors ( Collection<Object> newChildDescriptors, Object object )
    {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__P,
                     ContentFactory.eINSTANCE.createParagraph())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__CODE,
                     ContentFactory.eINSTANCE.createCodeParagraph())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__PLAIN_TEXT,
                     FragmentFactory.eINSTANCE.createPlainTextContent())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__UL,
                     ContentFactory.eINSTANCE.createUnorderedList())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__SIMPLE_TABLE,
                     ContentFactory.eINSTANCE.createSimpleTable())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__CONTENT,
                 FeatureMapUtil.createEntry
                    (ContentPackage.Literals.CONTENT_CONTAINER__STRUCTURE_TABLE,
                     ContentFactory.eINSTANCE.createStructureTable())));

        newChildDescriptors.add
            (createChildParameter
                (ContentPackage.Literals.CONTENT_CONTAINER__SECTION,
                 ContentFactory.eINSTANCE.createSection()));
    }

    /**
     * Return the resource locator for this item provider's resources.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator ()
    {
        return ((IChildCreationExtender)adapterFactory).getResourceLocator();
    }

}
