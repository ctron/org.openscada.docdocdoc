/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.ExcludePatternRule;
import org.openscada.doc.model.doc.map.MapFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Exclude Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.ExcludePatternRule#checkRule(java.lang.String)
 * <em>Check Rule</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ExcludePatternRuleTest extends PatternRuleTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( ExcludePatternRuleTest.class );
    }

    /**
     * Constructs a new Exclude Pattern Rule test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ExcludePatternRuleTest ( final String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Exclude Pattern Rule test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected ExcludePatternRule getFixture ()
    {
        return (ExcludePatternRule)this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createExcludePatternRule () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.ExcludePatternRule#checkRule(java.lang.String)
     * <em>Check Rule</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.ExcludePatternRule#checkRule(java.lang.String)
     * @generated
     */
    @Override
    public void testCheckRule__String ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //ExcludePatternRuleTest
