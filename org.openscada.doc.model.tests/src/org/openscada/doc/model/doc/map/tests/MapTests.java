/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>map</b></em>' package.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class MapTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite ()
    {
        final TestSuite suite = new MapTests ( "map Tests" );
        suite.addTestSuite ( MapTest.class );
        suite.addTestSuite ( FileTest.class );
        suite.addTestSuite ( MapSectionTest.class );
        suite.addTestSuite ( FeatureTest.class );
        suite.addTestSuite ( IncludePatternRuleTest.class );
        suite.addTestSuite ( ExcludePatternRuleTest.class );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapTests ( final String name )
    {
        super ( name );
    }

} //MapTests
