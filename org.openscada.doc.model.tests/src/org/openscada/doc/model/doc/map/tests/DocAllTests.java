/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Doc</b></em>' model.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class DocAllTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite ()
    {
        final TestSuite suite = new DocAllTests ( "Doc Tests" );
        suite.addTest ( MapTests.suite () );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public DocAllTests ( final String name )
    {
        super ( name );
    }

} //DocAllTests
