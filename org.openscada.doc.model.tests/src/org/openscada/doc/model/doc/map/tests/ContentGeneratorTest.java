/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.map.ContentGenerator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Content Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
 * <em>Visit</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ContentGeneratorTest extends TestCase
{

    /**
     * The fixture for this Content Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ContentGenerator fixture = null;

    /**
     * Constructs a new Content Generator test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ContentGeneratorTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Content Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final ContentGenerator fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Content Generator test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ContentGenerator getFixture ()
    {
        return this.fixture;
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * <em>Visit</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * @generated
     */
    public void testVisit__Visitor ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //ContentGeneratorTest
