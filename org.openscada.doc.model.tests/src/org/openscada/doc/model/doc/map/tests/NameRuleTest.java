/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.map.NameRule;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Name Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.NameRule#checkRule(java.lang.String)
 * <em>Check Rule</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class NameRuleTest extends TestCase
{

    /**
     * The fixture for this Name Rule test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected NameRule fixture = null;

    /**
     * Constructs a new Name Rule test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NameRuleTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Name Rule test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final NameRule fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Name Rule test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected NameRule getFixture ()
    {
        return this.fixture;
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.NameRule#checkRule(java.lang.String)
     * <em>Check Rule</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.NameRule#checkRule(java.lang.String)
     * @generated
     */
    public void testCheckRule__String ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //NameRuleTest
