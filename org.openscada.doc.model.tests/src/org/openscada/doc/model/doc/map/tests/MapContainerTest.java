/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.map.MapContainer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class MapContainerTest extends TestCase
{

    /**
     * The fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapContainer fixture = null;

    /**
     * Constructs a new Container test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapContainerTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final MapContainer fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapContainer getFixture ()
    {
        return this.fixture;
    }

} //MapContainerTest
