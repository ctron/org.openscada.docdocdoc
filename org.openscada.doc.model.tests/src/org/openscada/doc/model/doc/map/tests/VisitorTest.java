/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.map.Visitor;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.Visitor#openSection(org.openscada.doc.model.doc.map.MapSection)
 * <em>Open Section</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.map.Visitor#closeSection(org.openscada.doc.model.doc.map.MapSection)
 * <em>Close Section</em>}</li>
 * <li>
 * {@link org.openscada.doc.model.doc.map.Visitor#visitFragment(org.openscada.doc.model.doc.fragment.Fragment)
 * <em>Visit Fragment</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class VisitorTest extends TestCase
{

    /**
     * The fixture for this Visitor test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Visitor fixture = null;

    /**
     * Constructs a new Visitor test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public VisitorTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Visitor test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Visitor fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Visitor test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Visitor getFixture ()
    {
        return this.fixture;
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.Visitor#openSection(org.openscada.doc.model.doc.map.MapSection)
     * <em>Open Section</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.Visitor#openSection(org.openscada.doc.model.doc.map.MapSection)
     * @generated
     */
    public void testOpenSection__MapSection ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.Visitor#closeSection(org.openscada.doc.model.doc.map.MapSection)
     * <em>Close Section</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.Visitor#closeSection(org.openscada.doc.model.doc.map.MapSection)
     * @generated
     */
    public void testCloseSection__MapSection ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.Visitor#visitFragment(org.openscada.doc.model.doc.fragment.Fragment)
     * <em>Visit Fragment</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.Visitor#visitFragment(org.openscada.doc.model.doc.fragment.Fragment)
     * @generated
     */
    public void testVisitFragment__Fragment ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //VisitorTest
