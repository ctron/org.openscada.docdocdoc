/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.MapFactory;
import org.openscada.doc.model.doc.map.MapSection;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
 * <em>Visit</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MapSectionTest extends TestCase
{

    /**
     * The fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapSection fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( MapSectionTest.class );
    }

    /**
     * Constructs a new Section test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public MapSectionTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final MapSection fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MapSection getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createMapSection () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * <em>Visit</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * @generated
     */
    public void testVisit__Visitor ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //MapSectionTest
