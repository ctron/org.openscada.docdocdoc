/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.IncludePatternRule;
import org.openscada.doc.model.doc.map.MapFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Include Pattern Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.IncludePatternRule#checkRule(java.lang.String)
 * <em>Check Rule</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class IncludePatternRuleTest extends PatternRuleTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( IncludePatternRuleTest.class );
    }

    /**
     * Constructs a new Include Pattern Rule test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public IncludePatternRuleTest ( final String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Include Pattern Rule test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IncludePatternRule getFixture ()
    {
        return (IncludePatternRule)this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createIncludePatternRule () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.IncludePatternRule#checkRule(java.lang.String)
     * <em>Check Rule</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.IncludePatternRule#checkRule(java.lang.String)
     * @generated
     */
    @Override
    public void testCheckRule__String ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //IncludePatternRuleTest
