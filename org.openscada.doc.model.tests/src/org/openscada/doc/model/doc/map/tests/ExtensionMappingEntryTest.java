/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.ExtensionMappingEntry;
import org.openscada.doc.model.doc.map.MapFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Extension Mapping Entry</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ExtensionMappingEntryTest extends TestCase
{

    /**
     * The fixture for this Extension Mapping Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtensionMappingEntry fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( ExtensionMappingEntryTest.class );
    }

    /**
     * Constructs a new Extension Mapping Entry test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ExtensionMappingEntryTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Extension Mapping Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final ExtensionMappingEntry fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Extension Mapping Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtensionMappingEntry getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createExtensionMappingEntry () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //ExtensionMappingEntryTest
