/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.map.Import;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
 * <em>Visit</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ImportTest extends TestCase
{

    /**
     * The fixture for this Import test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Import fixture = null;

    /**
     * Constructs a new Import test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ImportTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Import test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Import fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Import test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Import getFixture ()
    {
        return this.fixture;
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * <em>Visit</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * @generated
     */
    public void testVisit__Visitor ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //ImportTest
