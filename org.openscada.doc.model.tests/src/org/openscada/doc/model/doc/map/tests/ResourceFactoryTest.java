/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.MapFactory;
import org.openscada.doc.model.doc.map.ResourceFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Resource Factory</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ResourceFactoryTest extends TestCase
{

    /**
     * The fixture for this Resource Factory test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ResourceFactory fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( ResourceFactoryTest.class );
    }

    /**
     * Constructs a new Resource Factory test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ResourceFactoryTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Resource Factory test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final ResourceFactory fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Resource Factory test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ResourceFactory getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createResourceFactory () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //ResourceFactoryTest
