/**
 */
package org.openscada.doc.model.doc.map.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.map.Feature;
import org.openscada.doc.model.doc.map.MapFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
 * <em>Visit</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FeatureTest extends TestCase
{

    /**
     * The fixture for this Feature test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Feature fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( FeatureTest.class );
    }

    /**
     * Constructs a new Feature test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FeatureTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Feature test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Feature fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Feature test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Feature getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( MapFactory.eINSTANCE.createFeature () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * <em>Visit</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.map.MapElement#visit(org.openscada.doc.model.doc.map.Visitor)
     * @generated
     */
    public void testVisit__Visitor ()
    {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //FeatureTest
