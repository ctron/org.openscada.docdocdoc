/**
 */
package org.openscada.doc.model.doc.fragment.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>fragment</b></em>' package.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class FragmentTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite ()
    {
        final TestSuite suite = new FragmentTests ( "fragment Tests" );
        suite.addTestSuite ( FragmentTest.class );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentTests ( final String name )
    {
        super ( name );
    }

} //FragmentTests
