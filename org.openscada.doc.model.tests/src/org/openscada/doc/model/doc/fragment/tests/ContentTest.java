/**
 */
package org.openscada.doc.model.doc.fragment.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.fragment.Content;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Content</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class ContentTest extends TestCase
{

    /**
     * The fixture for this Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Content fixture = null;

    /**
     * Constructs a new Content test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ContentTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Content fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Content getFixture ()
    {
        return this.fixture;
    }

} //ContentTest
