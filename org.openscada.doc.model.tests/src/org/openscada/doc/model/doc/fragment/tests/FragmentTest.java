/**
 */
package org.openscada.doc.model.doc.fragment.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.fragment.Fragment;
import org.openscada.doc.model.doc.fragment.FragmentFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getAnyContent()
 * <em>Any Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getSection() <em>
 * Section</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FragmentTest extends TestCase
{

    /**
     * The fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Fragment fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( FragmentTest.class );
    }

    /**
     * Constructs a new Fragment test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public FragmentTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Fragment fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Fragment test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Fragment getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( FragmentFactory.eINSTANCE.createFragment () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.fragment.Container#getSection()
     * <em>Section</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Container#getSection()
     * @generated
     */
    public void testGetSection ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.fragment.Container#getAnyContent()
     * <em>Any Content</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Container#getAnyContent()
     * @generated
     */
    public void testGetAnyContent ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //FragmentTest
