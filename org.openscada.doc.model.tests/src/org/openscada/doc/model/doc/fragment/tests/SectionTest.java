/**
 */
package org.openscada.doc.model.doc.fragment.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.fragment.Section;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getAnyContent()
 * <em>Any Content</em>}</li>
 * <li>{@link org.openscada.doc.model.doc.fragment.Container#getSection() <em>
 * Section</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class SectionTest extends TestCase
{

    /**
     * The fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Section fixture = null;

    /**
     * Constructs a new Section test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SectionTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Section fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Section getFixture ()
    {
        return this.fixture;
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.fragment.Container#getSection()
     * <em>Section</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Container#getSection()
     * @generated
     */
    public void testGetSection ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

    /**
     * Tests the '
     * {@link org.openscada.doc.model.doc.fragment.Container#getAnyContent()
     * <em>Any Content</em>}' feature getter.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.openscada.doc.model.doc.fragment.Container#getAnyContent()
     * @generated
     */
    public void testGetAnyContent ()
    {
        // TODO: implement this feature getter test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail ();
    }

} //SectionTest
