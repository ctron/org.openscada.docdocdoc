/**
 */
package org.openscada.doc.model.doc.fragment.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.fragment.FragmentFactory;
import org.openscada.doc.model.doc.fragment.PlainTextContent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Plain Text Content</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class PlainTextContentTest extends TestCase
{

    /**
     * The fixture for this Plain Text Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PlainTextContent fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( PlainTextContentTest.class );
    }

    /**
     * Constructs a new Plain Text Content test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public PlainTextContentTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Plain Text Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final PlainTextContent fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Plain Text Content test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PlainTextContent getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( FragmentFactory.eINSTANCE.createPlainTextContent () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //PlainTextContentTest
