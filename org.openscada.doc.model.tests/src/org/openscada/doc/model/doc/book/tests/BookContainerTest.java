/**
 */
package org.openscada.doc.model.doc.book.tests;

import junit.framework.TestCase;

import org.openscada.doc.model.doc.book.BookContainer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class BookContainerTest extends TestCase
{

    /**
     * The fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookContainer fixture = null;

    /**
     * Constructs a new Container test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BookContainerTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final BookContainer fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Container test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookContainer getFixture ()
    {
        return this.fixture;
    }

} //BookContainerTest
