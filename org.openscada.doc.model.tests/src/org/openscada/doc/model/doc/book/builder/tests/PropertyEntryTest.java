/**
 */
package org.openscada.doc.model.doc.book.builder.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.book.builder.BuilderFactory;
import org.openscada.doc.model.doc.book.builder.PropertyEntry;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Property Entry</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class PropertyEntryTest extends TestCase
{

    /**
     * The fixture for this Property Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PropertyEntry fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( PropertyEntryTest.class );
    }

    /**
     * Constructs a new Property Entry test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public PropertyEntryTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Property Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final PropertyEntry fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Property Entry test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PropertyEntry getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( BuilderFactory.eINSTANCE.createPropertyEntry () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //PropertyEntryTest
