/**
 */
package org.openscada.doc.model.doc.book.builder.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>builder</b></em>' package.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BuilderTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite ()
    {
        final TestSuite suite = new BuilderTests ( "builder Tests" );
        suite.addTestSuite ( BookBuilderTest.class );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BuilderTests ( final String name )
    {
        super ( name );
    }

} //BuilderTests
