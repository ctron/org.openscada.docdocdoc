/**
 */
package org.openscada.doc.model.doc.book.builder.tests;

import junit.textui.TestRunner;

import org.openscada.doc.model.doc.book.builder.BookBuilder;
import org.openscada.doc.model.doc.book.builder.BuilderFactory;
import org.openscada.doc.model.doc.map.tests.MapTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Book Builder</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BookBuilderTest extends MapTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( BookBuilderTest.class );
    }

    /**
     * Constructs a new Book Builder test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BookBuilderTest ( final String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Book Builder test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected BookBuilder getFixture ()
    {
        return (BookBuilder)this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( BuilderFactory.eINSTANCE.createBookBuilder () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //BookBuilderTest
