/**
 */
package org.openscada.doc.model.doc.book.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.book.BookFactory;
import org.openscada.doc.model.doc.book.BookSection;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class BookSectionTest extends TestCase
{

    /**
     * The fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookSection fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( BookSectionTest.class );
    }

    /**
     * Constructs a new Section test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public BookSectionTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final BookSection fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Section test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BookSection getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( BookFactory.eINSTANCE.createBookSection () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //BookSectionTest
