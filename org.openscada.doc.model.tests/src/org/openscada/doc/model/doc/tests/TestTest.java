/**
 */
package org.openscada.doc.model.doc.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.DocFactory;
import org.openscada.doc.model.doc.Test;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Test</b></em>'.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class TestTest extends TestCase
{

    /**
     * The fixture for this Test test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Test fixture = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( TestTest.class );
    }

    /**
     * Constructs a new Test test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public TestTest ( final String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Test test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture ( final Test fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Test test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Test getFixture ()
    {
        return this.fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( DocFactory.eINSTANCE.createTest () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //TestTest
