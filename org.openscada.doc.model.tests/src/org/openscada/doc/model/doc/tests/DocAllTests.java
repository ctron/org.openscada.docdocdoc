/**
 */
package org.openscada.doc.model.doc.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.openscada.doc.model.doc.book.builder.tests.BuilderTests;
import org.openscada.doc.model.doc.fragment.tests.FragmentTests;
import org.openscada.doc.model.doc.map.tests.MapTests;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Doc</b></em>' model.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class DocAllTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main ( final String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite ()
    {
        final TestSuite suite = new DocAllTests ( "Doc Tests" );
        suite.addTest ( MapTests.suite () );
        suite.addTest ( FragmentTests.suite () );
        suite.addTest ( BuilderTests.suite () );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public DocAllTests ( final String name )
    {
        super ( name );
    }

} //DocAllTests
