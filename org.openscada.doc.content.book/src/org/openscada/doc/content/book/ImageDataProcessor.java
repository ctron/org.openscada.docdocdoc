/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book;

import java.net.URL;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.openscada.doc.builder.VisitorProcessor;
import org.openscada.doc.content.Image;
import org.openscada.doc.model.doc.fragment.Content;

import com.google.common.io.ByteStreams;

public class ImageDataProcessor implements VisitorProcessor
{

    @Override
    public void visit ( final URI uri, final Content content ) throws Exception
    {
        final TreeIterator<EObject> i = content.eAllContents ();
        while ( i.hasNext () )
        {
            final EObject o = i.next ();
            if ( o instanceof Image )
            {
                processImage ( uri, (Image)o );
            }
        }
    }

    private void processImage ( final URI baseUri, final Image image ) throws Exception
    {
        image.setData ( readImageData ( baseUri, image.getSource () ) );
    }

    private byte[] readImageData ( final URI baseUri, final String source ) throws Exception
    {
        if ( source == null )
        {
            return null;
        }

        final URI uri = URI.createURI ( source ).resolve ( baseUri );
        return ByteStreams.toByteArray ( new URL ( uri.toString () ).openStream () );
    }
}
