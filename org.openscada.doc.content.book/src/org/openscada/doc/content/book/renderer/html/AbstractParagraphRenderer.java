/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;
import java.util.List;

import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.utils.str.StringHelper;

public abstract class AbstractParagraphRenderer implements BookRenderer
{

    protected final String tag;

    public AbstractParagraphRenderer ( final String tag )
    {
        this.tag = tag;
    }

    protected abstract List<String> getClassNames ( final Object content );

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        out.print ( "<" + this.tag );

        final List<String> classNames = getClassNames ( content );

        if ( classNames != null && !classNames.isEmpty () )
        {
            out.print ( " class=\"" + StringHelper.join ( classNames, " " ) + "\"" );
        }
        out.print ( ">" );
        context.render ( ( (PlainTextContainer)content ).getContent () );
        out.println ( "</" + this.tag + ">" );

    }

}