/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.tex;

import java.io.PrintStream;
import java.util.UUID;

import org.openscada.doc.content.Image;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class ImageRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final Image img = (Image)content;
        final PrintStream out = context.getCurrentPrintStream ();

        final UUID uuid = UUID.randomUUID ();

        final String name = String.format ( "images/" + uuid.toString () );

        out.print ( "<img src=\"" + name + "\" alt=\"" + img.getSource () + "\">" );

        context.writeData ( name, img.getData () );

        out.print ( "</src>" );
    }
}
