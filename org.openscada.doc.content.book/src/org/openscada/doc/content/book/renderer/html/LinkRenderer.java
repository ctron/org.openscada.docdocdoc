/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;

import org.openscada.doc.content.Link;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class LinkRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final Link ref = (Link)content;

        final PrintStream out = context.getCurrentPrintStream ();

        out.print ( String.format ( "<a class=\"link\" href=\"%s\">", ref.getRef () ) );
        if ( ref.getName () != null )
        {
            out.print ( Helper.encodeHtml ( ref.getName () ) );
        }
        else
        {
            out.print ( ref.getRef () );
        }
        out.print ( "</a>" );
    }

}
