/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.util.List;

public class ParagraphRenderer extends AbstractParagraphRenderer
{

    final List<String> classNames;

    public ParagraphRenderer ( final String tag, final List<String> classNames )
    {
        super ( tag );
        this.classNames = classNames;
    }

    @Override
    protected List<String> getClassNames ( final Object content )
    {
        return this.classNames;
    }
}
