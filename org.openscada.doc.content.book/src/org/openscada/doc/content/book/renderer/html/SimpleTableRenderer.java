/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;
import java.util.LinkedList;

import org.eclipse.emf.common.util.EList;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.HorizontalAlign;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.utils.str.StringHelper;

public class SimpleTableRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        final SimpleTable table = (SimpleTable)content;

        final LinkedList<String> classes = new LinkedList<String> ();
        classes.add ( "dcm_simple" );
        if ( table.isMaximize () )
        {
            classes.add ( "dcm_maximize" );
        }

        out.print ( String.format ( "<table class=\"%s\">", StringHelper.join ( classes, " " ) ) );

        Table.makeCaption ( out, table );

        out.println ( "<colgroup>" );
        for ( final Column col : table.getColumn () )
        {
            out.println ( String.format ( "<col class=\"dcm_%s\" />", col.getHorizontalAlign ().getLiteral ().toLowerCase () ) );
        }
        out.println ( "</colgroup>" );

        out.println ( "<thead><tr>" );
        for ( final Column col : table.getColumn () )
        {
            out.println ( String.format ( "<th>%s</th>", Helper.encodeHtml ( col.getTitle () ) ) );
        }
        out.println ( "</tr></thead>" );

        out.println ( "<tbody>" );
        for ( final Row row : table.getRow () )
        {
            int i = 0;
            out.println ( "<tr>" );
            for ( final Cell cell : row.getCell () )
            {
                final HorizontalAlign align = findAlign ( table.getColumn (), i );
                if ( align == null )
                {
                    out.print ( "<td>" );
                }
                else
                {
                    out.print ( String.format ( "<td class=\"dcm_%s\">", align.getLiteral ().toLowerCase () ) );
                }
                context.render ( cell.getContent () );
                out.println ( "</td>" );
                i++;
            }
            out.println ( "</tr>" );
        }
        out.println ( "</tbody>" );

        out.println ( "</table>" );
    }

    private HorizontalAlign findAlign ( final EList<Column> column, final int i )
    {
        if ( i >= column.size () )
        {
            return null;
        }
        final Column col = column.get ( i );
        if ( col == null )
        {
            return null;
        }
        return col.getHorizontalAlign ();
    }

}
