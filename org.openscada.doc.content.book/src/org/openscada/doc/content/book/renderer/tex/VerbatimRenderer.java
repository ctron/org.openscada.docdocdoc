/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.tex;

import java.io.PrintStream;

import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class VerbatimRenderer implements BookRenderer
{

    private final String tag;

    public VerbatimRenderer ( final String tag )
    {
        this.tag = tag;
    }

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.print ( "\\" + this.tag + "|" );
        context.render ( ( (PlainTextContainer)content ).getContent () );
        out.print ( "|" );
    }
}
