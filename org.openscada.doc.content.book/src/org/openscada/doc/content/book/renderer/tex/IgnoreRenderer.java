package org.openscada.doc.content.book.renderer.tex;

import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class IgnoreRenderer implements BookRenderer
{
    public static IgnoreRenderer INSTANCE = new IgnoreRenderer ();

    @Override
    public void render ( final RenderContext context, final Object content )
    {
    }

}
