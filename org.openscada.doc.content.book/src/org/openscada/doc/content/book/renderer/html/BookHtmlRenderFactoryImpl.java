/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.util.HashMap;
import java.util.Map;

import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.output.simple.renderer.AbstractCacheFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class BookHtmlRenderFactoryImpl extends AbstractCacheFactory
{

    private final Map<Entry, BookRenderer> cache = new HashMap<Entry, BookRenderer> ();

    public BookHtmlRenderFactoryImpl ()
    {
        this.cache.put ( new Entry ( "html", Paragraph.class ), new ParagraphRenderer ( "p", null ) );
        this.cache.put ( new Entry ( "html", Emphasis.class ), new SpanRenderer ( "em" ) );
        this.cache.put ( new Entry ( "html", Quote.class ), new SpanRenderer ( "q" ) );
        this.cache.put ( new Entry ( "html", Image.class ), new ImageRenderer () );
        this.cache.put ( new Entry ( "html", ObjectName.class ), new ObjectNameRenderer () );
        this.cache.put ( new Entry ( "html", CodeParagraph.class ), new CodeRenderer () );
        this.cache.put ( new Entry ( "html", CrossReference.class ), new CrossReferenceRenderer () );
        this.cache.put ( new Entry ( "html", Link.class ), new LinkRenderer () );
        this.cache.put ( new Entry ( "html", UnorderedList.class ), new ListRenderer ( "ul" ) );
        this.cache.put ( new Entry ( "html", SimpleTable.class ), new SimpleTableRenderer () );
        this.cache.put ( new Entry ( "html", StructureTable.class ), new StructureTableRenderer () );
    }

    @Override
    protected Map<Entry, BookRenderer> getCache ()
    {
        return this.cache;
    }

}
