/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.tex;

import java.util.HashMap;
import java.util.Map;

import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.CrossReference;
import org.openscada.doc.content.Emphasis;
import org.openscada.doc.content.Image;
import org.openscada.doc.content.Link;
import org.openscada.doc.content.ObjectName;
import org.openscada.doc.content.Paragraph;
import org.openscada.doc.content.Quote;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.content.UnorderedList;
import org.openscada.doc.output.simple.renderer.AbstractCacheFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class BookTexRenderFactoryImpl extends AbstractCacheFactory
{

    private final Map<Entry, BookRenderer> cache = new HashMap<Entry, BookRenderer> ();

    public BookTexRenderFactoryImpl ()
    {
        this.cache.put ( new Entry ( "tex", Paragraph.class ), new ParagraphRenderer () );
        this.cache.put ( new Entry ( "tex", CodeParagraph.class ), new BeginEndRenderer ( "verbatim" ) );

        this.cache.put ( new Entry ( "tex", Emphasis.class ), new SpanRenderer ( "emph" ) );
        this.cache.put ( new Entry ( "tex", Quote.class ), new WrapRenderer ( "``", "''" ) );
        this.cache.put ( new Entry ( "tex", Image.class ), IgnoreRenderer.INSTANCE );
        this.cache.put ( new Entry ( "tex", ObjectName.class ), new VerbatimRenderer ( "verb" ) );
        this.cache.put ( new Entry ( "tex", CrossReference.class ), IgnoreRenderer.INSTANCE );
        this.cache.put ( new Entry ( "tex", Link.class ), new LinkRenderer () );
        this.cache.put ( new Entry ( "tex", UnorderedList.class ), new ListRenderer ( "itemize" ) );
        this.cache.put ( new Entry ( "tex", SimpleTable.class ), new SimpleTableRenderer () );
    }

    @Override
    protected Map<Entry, BookRenderer> getCache ()
    {
        return this.cache;
    }

}
