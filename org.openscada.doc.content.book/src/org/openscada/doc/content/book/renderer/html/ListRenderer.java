/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;

import org.openscada.doc.content.List;
import org.openscada.doc.content.ListItem;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class ListRenderer implements BookRenderer
{

    private final String elementName;

    public ListRenderer ( final String elementName )
    {
        this.elementName = elementName;
    }

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( String.format ( "<%s>", this.elementName ) );

        final List list = (List)content;

        for ( final ListItem li : list.getItem () )
        {
            out.print ( "<li>" );
            context.render ( li.getContent () );
            out.println ( "</li>" );
        }

        out.println ( String.format ( "</%s>", this.elementName ) );
    }

}
