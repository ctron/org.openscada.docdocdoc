/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;

import org.openscada.doc.content.CrossReference;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class CrossReferenceRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final CrossReference ref = (CrossReference)content;

        final String xrefId = context.createCrossReference ( ref.getType (), ref.getId () );

        final PrintStream out = context.getCurrentPrintStream ();

        out.print ( String.format ( "<a class=\"xref %1$s\" href=\"#%2$s\">", Helper.encodeHtml ( ref.getType () ), xrefId ) );

        if ( ref.getContent () == null || ref.getContent ().isEmpty () )
        {
            out.print ( Helper.encodeHtml ( ref.getId () ) );
        }
        else
        {
            context.render ( ( (CrossReference)content ).getContent () );
        }

        out.print ( "</a>" );
    }

}
