/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;

import org.openscada.doc.output.simple.html.renderer.Helper;

public final class Table
{
    private Table ()
    {
    }

    public static void makeCaption ( final PrintStream out, final org.openscada.doc.content.Table table )
    {
        if ( table.getCaption () != null )
        {
            out.println ( "<caption>" );
            out.println ( Helper.encodeHtml ( table.getCaption () ) );
            out.println ( "</caption>" );
        }
    }
}
