/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.tex;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openscada.doc.content.Cell;
import org.openscada.doc.content.Column;
import org.openscada.doc.content.Row;
import org.openscada.doc.content.SimpleTable;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.doc.output.simple.tex.TexHelper;
import org.openscada.utils.str.StringHelper;

public class SimpleTableRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        final SimpleTable table = (SimpleTable)content;

        // begin

        final String formatSpec = makeFormatSpec ( table.isMaximize (), table.getColumn () );

        String tableName;
        if ( table.isMaximize () )
        {
            tableName = "tabu";
            out.println ( "\\begin{tabu} to \\linewidth {" + formatSpec + "}" );
        }
        else
        {
            tableName = "tabular";
            out.println ( "\\begin{tabular}{" + formatSpec + "}" );
        }

        // columns

        final List<String> headNames = new LinkedList<String> ();
        for ( final Column col : table.getColumn () )
        {
            headNames.add ( TexHelper.encode ( col.getTitle () ) );
        }

        if ( !headNames.isEmpty () )
        {
            out.println ( StringHelper.join ( headNames, " & " ) + " \\\\" );
            out.println ( "\\hline" );
        }

        // rows

        for ( final Row row : table.getRow () )
        {
            int c = 0;

            for ( final Cell cell : row.getCell () )
            {
                if ( c > 0 )
                {
                    out.println ( " & " );
                }
                context.render ( cell.getContent () );
                c++;
            }
            out.println ( "\\\\" );
        }

        out.println ( "\\end{" + tableName + "}" );
    }

    private String makeFormatSpec ( final boolean maximize, final EList<Column> columns )
    {
        final StringBuilder sb = new StringBuilder ();

        for ( final Column col : columns )
        {
            switch ( col.getHorizontalAlign () )
            {
                default: // default to left
                case LEFT:
                    if ( maximize )
                    {
                        sb.append ( "X[l]" );
                    }
                    else
                    {
                        sb.append ( 'l' );
                    }
                    break;
                case CENTER:
                    if ( maximize )
                    {
                        sb.append ( "X[c]" );
                    }
                    else
                    {
                        sb.append ( 'c' );
                    }
                    break;
                case RIGHT:
                    if ( maximize )
                    {
                        sb.append ( "X[r]" );
                    }
                    else
                    {
                        sb.append ( 'r' );
                    }
                    break;
            }
        }

        return sb.toString ();
    }
}
