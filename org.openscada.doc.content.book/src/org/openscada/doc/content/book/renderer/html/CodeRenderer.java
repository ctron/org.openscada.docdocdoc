/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import org.openscada.doc.content.CodeParagraph;
import org.openscada.doc.content.PlainTextContainer;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.utils.str.StringHelper;

public class CodeRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        out.print ( "<pre class=\"prettyprint\"><code" );

        final List<String> classNames = getClassNames ( content );

        if ( classNames != null && !classNames.isEmpty () )
        {
            out.print ( " class=\"" + StringHelper.join ( classNames, " " ) + "\"" );
        }
        out.print ( ">" );
        context.render ( ( (PlainTextContainer)content ).getContent () );
        out.println ( "</code></pre>" );
    }

    protected List<String> getClassNames ( final Object content )
    {
        if ( content instanceof CodeParagraph )
        {
            final String lang = ( (CodeParagraph)content ).getLanguage ();
            if ( lang != null )
            {
                return Arrays.asList ( "language-" + lang.toLowerCase () );
            }
        }
        return null;
    }

}
