/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.content.book.renderer.html;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.openscada.doc.content.StructureEntry;
import org.openscada.doc.content.StructureTable;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.html.renderer.Helper;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.utils.str.StringHelper;

public class StructureTableRenderer implements BookRenderer
{

    private class SizeInfo
    {
        int size;

        List<String> fields = new LinkedList<String> ();
    }

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        final StructureTable table = (StructureTable)content;

        final List<String> classes = new LinkedList<String> ();
        classes.add ( "dcm_simple dcm_structure" );
        if ( table.isMaximize () )
        {
            classes.add ( "dcm_maximize" );
        }

        out.print ( String.format ( "<table class=\"%s\">", StringHelper.join ( classes, " " ) ) );

        Table.makeCaption ( out, table );

        out.println ( "<colgroup>" );
        out.println ( "<col class=\"dcm_left\" />" );
        out.println ( "</colgroup>" );

        out.println ( "<thead><tr>" );
        out.println ( "<th>Index</th>" );
        out.println ( "<th>Length</th>" );
        out.println ( "<th>Name</th>" );
        out.println ( "<th>Type</th>" );
        out.println ( "<th>Description</th>" );
        out.println ( "</tr></thead>" );

        out.println ( "<tbody>" );

        final SizeInfo sizeInfo = new SizeInfo ();

        for ( final StructureEntry entry : table.getEntry () )
        {
            renderEntry ( context, entry, sizeInfo );
        }

        out.println ( "</tbody>" );

        out.println ( "</table>" );
    }

    private void renderEntry ( final RenderContext context, final StructureEntry entry, final SizeInfo sizeInfo )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        out.println ( "<tr>" );

        out.println ( String.format ( "<td>%s</td>", Helper.encodeHtml ( makeIndex ( sizeInfo ) ) ) );

        if ( entry.getSizeField () != null )
        {
            if ( entry.getSize () != null )
            {
                sizeInfo.fields.add ( String.format ( "(%s * %s)", entry.getSizeField ().getName (), entry.getSize () ) );
            }
            else
            {
                sizeInfo.fields.add ( entry.getSizeField ().getName () );
            }
            out.println ( String.format ( "<td>%s</td>", Helper.encodeHtml ( entry.getSizeField ().getName () ) ) );
        }
        else if ( entry.getSize () != null )
        {
            sizeInfo.size += entry.getSize ();
            out.println ( String.format ( "<td>%s</td>", entry.getSize () ) );
        }
        else
        {
            sizeInfo.fields.add ( entry.getName () );
            out.println ( String.format ( "<td>…</td>" ) );
        }

        out.println ( String.format ( "<td>%s</td>", Helper.encodeHtml ( entry.getName () ) ) );
        out.println ( String.format ( "<td>%s</td>", Helper.encodeHtml ( entry.getTypeName () ) ) );

        out.println ( "<td>" );
        if ( entry.getDescription () != null )
        {
            context.render ( entry.getDescription ().getContent () );
        }
        out.println ( "</td>" );

        out.println ( "</tr>" );
    }

    private String makeIndex ( final SizeInfo sizeInfo )
    {
        if ( sizeInfo.fields.isEmpty () )
        {
            return String.format ( "%s", sizeInfo.size );
        }
        else
        {
            return String.format ( "%s + %s", sizeInfo.size, StringHelper.join ( sizeInfo.fields, " + " ) );
        }
    }
}
