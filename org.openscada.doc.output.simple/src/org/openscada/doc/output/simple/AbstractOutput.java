/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.output.simple.actions.Action;
import org.openscada.doc.output.simple.renderer.BookRenderFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public abstract class AbstractOutput
{

    protected final Book book;

    protected final Locale locale;

    protected static final String EXTP_HTML_RENDERER_FACTORY = "rendererFactory";

    protected final List<BookRenderFactory> renderFactories = new LinkedList<BookRenderFactory> ();

    protected final List<Action> actions = new LinkedList<Action> ();

    private final Map<Class<?>, BookRenderer> cache = new HashMap<Class<?>, BookRenderer> ();

    public AbstractOutput ( final Book book, final Locale locale ) throws CoreException
    {
        this.book = book;

        this.locale = locale;

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( "org.openscada.doc.output.simple.bookRenderer" ) )
        {
            if ( !EXTP_HTML_RENDERER_FACTORY.equals ( ele.getName () ) )
            {
                continue;
            }

            final Object o = ele.createExecutableExtension ( "class" );
            if ( o instanceof BookRenderFactory )
            {
                this.renderFactories.add ( (BookRenderFactory)o );
            }
        }
    }

    protected void processActions ( final RenderContext context, final IFolder container, final IProgressMonitor monitor ) throws Exception
    {
        for ( final Action action : this.actions )
        {
            action.run ( context, container, monitor );
        }
    }

    public void output ( final IProgressMonitor monitor, final IContainer parent ) throws Exception
    {
        try
        {
            try
            {
                final IFolder container = parent.getFolder ( new Path ( "book" ) );
                processOutput ( monitor, container );
            }
            finally
            {
                parent.refreshLocal ( IResource.DEPTH_INFINITE, monitor );
            }
        }
        finally
        {
            monitor.done ();
        }
    }

    protected void processOutput ( final IProgressMonitor monitor, final IFolder container ) throws Exception
    {
        final RenderContext context = createRenderContext ( container );

        try
        {
            if ( container.exists () )
            {
                container.delete ( true, monitor );
            }
            container.create ( true, true, monitor );

            processActions ( context, container, monitor );

            processContent ( context );

            // finally validate
            context.validate ();
        }
        finally
        {
            context.dispose ();
        }
    }

    protected abstract RenderContext createRenderContext ( IFolder container );

    protected abstract void processContent ( final RenderContext context );

    protected BookRenderer findRenderer ( final String outputType, final Class<?> clazz )
    {
        BookRenderer renderer = this.cache.get ( clazz );
        if ( renderer != null )
        {
            return renderer;
        }

        for ( final BookRenderFactory factory : this.renderFactories )
        {
            renderer = factory.createRenderer ( outputType, clazz );
            if ( renderer != null )
            {
                this.cache.put ( clazz, renderer );
                return renderer;
            }
        }
        return null;
    }

}
