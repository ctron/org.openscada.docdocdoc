/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.actions;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.IPath;
import org.osgi.framework.Bundle;

public class CopyBundleResource extends CopyResource
{

    private final Bundle bundle;

    private final String name;

    public CopyBundleResource ( final IPath targetPath, final Bundle bundle, final String name )
    {
        super ( targetPath );

        this.bundle = bundle;
        this.name = name;
    }

    @Override
    protected InputStream getInputStream () throws IOException
    {
        return this.bundle.getResource ( this.name ).openStream ();
    }

}
