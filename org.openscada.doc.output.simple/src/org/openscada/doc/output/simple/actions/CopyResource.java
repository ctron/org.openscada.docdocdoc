/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.actions;

import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.openscada.doc.output.simple.Helper;
import org.openscada.doc.output.simple.RenderContext;

public abstract class CopyResource implements Action
{
    private final IPath targetPath;

    public CopyResource ( final IPath targetPath )
    {
        this.targetPath = targetPath;
    }

    @Override
    public void run ( final RenderContext context, final IFolder targetFolder, final IProgressMonitor monitor ) throws Exception
    {
        final IFile targetFile = targetFolder.getFile ( this.targetPath );

        Helper.mkdirs ( targetFile.getParent () );

        if ( targetFile.exists () )
        {
            targetFile.setContents ( getInputStream (), IResource.FORCE | IResource.KEEP_HISTORY, monitor );
        }
        else
        {
            targetFile.create ( getInputStream (), IResource.FORCE | IResource.KEEP_HISTORY, monitor );
        }
    }

    protected abstract InputStream getInputStream () throws Exception;

}
