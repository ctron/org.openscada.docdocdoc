/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple.actions;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.IPath;

public class CopyUrlResource extends CopyResource
{

    private final URL url;

    public CopyUrlResource ( final IPath targetPath, final URL url )
    {
        super ( targetPath );

        this.url = url;
    }

    @Override
    protected InputStream getInputStream () throws IOException
    {
        return this.url.openStream ();
    }

}
