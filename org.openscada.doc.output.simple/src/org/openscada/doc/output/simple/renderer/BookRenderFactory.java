package org.openscada.doc.output.simple.renderer;

public interface BookRenderFactory
{
    public BookRenderer createRenderer ( String outputType, Class<?> clazz );
}
