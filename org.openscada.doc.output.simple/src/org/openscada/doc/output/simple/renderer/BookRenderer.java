package org.openscada.doc.output.simple.renderer;

import org.openscada.doc.output.simple.RenderContext;


public interface BookRenderer
{
    public void render ( RenderContext context, Object content );
}
