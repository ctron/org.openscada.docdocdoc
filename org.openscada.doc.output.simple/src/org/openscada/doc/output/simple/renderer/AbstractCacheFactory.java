package org.openscada.doc.output.simple.renderer;

import java.util.Map;

public abstract class AbstractCacheFactory implements BookRenderFactory
{

    public static class Entry
    {
        private final String outputType;

        Class<?> clazz;

        public Entry ( final String outputType, final Class<?> clazz )
        {
            super ();
            this.outputType = outputType;
            this.clazz = clazz;
        }

        @Override
        public int hashCode ()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ( this.clazz == null ? 0 : this.clazz.hashCode () );
            result = prime * result + ( this.outputType == null ? 0 : this.outputType.hashCode () );
            return result;
        }

        @Override
        public boolean equals ( final Object obj )
        {
            if ( this == obj )
            {
                return true;
            }
            if ( obj == null )
            {
                return false;
            }
            if ( getClass () != obj.getClass () )
            {
                return false;
            }
            final Entry other = (Entry)obj;
            if ( this.clazz == null )
            {
                if ( other.clazz != null )
                {
                    return false;
                }
            }
            else if ( !this.clazz.equals ( other.clazz ) )
            {
                return false;
            }
            if ( this.outputType == null )
            {
                if ( other.outputType != null )
                {
                    return false;
                }
            }
            else if ( !this.outputType.equals ( other.outputType ) )
            {
                return false;
            }
            return true;
        }

    }

    protected abstract Map<Entry, BookRenderer> getCache ();

    @Override
    public BookRenderer createRenderer ( final String outputType, final Class<?> clazz )
    {
        final Map<Entry, BookRenderer> cache = getCache ();

        final Entry key = new Entry ( outputType, clazz );

        if ( cache.containsKey ( key ) )
        {
            return cache.get ( key );
        }

        for ( final Class<?> iface : clazz.getInterfaces () )
        {
            final Entry interfaceKey = new Entry ( outputType, iface );
            if ( cache.containsKey ( interfaceKey ) )
            {
                return cache.get ( interfaceKey );
            }
        }

        return null;
    }
}
