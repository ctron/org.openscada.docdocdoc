/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public final class Helper
{
    private Helper ()
    {
    }

    public static void mkdirs ( final IContainer parent ) throws CoreException
    {
        if ( !parent.exists () && parent instanceof IFolder )
        {
            mkdirs ( parent.getParent () );
            ( (IFolder)parent ).create ( IResource.FORCE, true, null );
        }
    }

}
