/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Path;
import org.openscada.doc.model.doc.book.Book;

public abstract class RenderContextImpl implements RenderContext
{
    public class Stream extends ByteArrayOutputStream
    {
        private final IFile file;

        public Stream ( final IFile file ) throws IOException
        {
            this.file = file;
        }

        @Override
        public void close () throws IOException
        {
            super.close ();
            handleClose ( toByteArray (), this.file );
        }
    }

    private final Map<IFile, PrintStream> streams = new HashMap<IFile, PrintStream> ();

    private final IContainer container;

    private PrintStream currentStream;

    private final Book book;

    // the handed out link targets
    private final Set<String> linkTargets = new HashSet<String> ();

    // the referenced link targets
    private final Set<String> crossReferences = new HashSet<String> ();

    private final Locale locale;

    public RenderContextImpl ( final IContainer container, final Book book, final Locale locale )
    {
        this.container = container;
        this.book = book;
        this.locale = locale;
    }

    @Override
    public Locale getLocale ()
    {
        return this.locale;
    }

    @Override
    public Book getBook ()
    {
        return this.book;
    }

    @Override
    public void validate ()
    {
        final Set<String> xrefs = new HashSet<String> ( this.crossReferences );
        xrefs.removeAll ( this.linkTargets );

        if ( !xrefs.isEmpty () )
        {
            throw new IllegalStateException ( String.format ( "%s cross references have invalid link targets: %s", xrefs.size (), xrefs ) );
        }
    }

    @Override
    public void writeData ( final String file, final byte[] data )
    {
        try
        {
            final IFile newFile = this.container.getFile ( new Path ( file ) );
            Helper.mkdirs ( newFile.getParent () );
            if ( newFile.exists () )
            {
                newFile.setContents ( new ByteArrayInputStream ( data ), IResource.FORCE | IResource.KEEP_HISTORY, null );
            }
            else
            {
                newFile.create ( new ByteArrayInputStream ( data ), IResource.FORCE | IResource.KEEP_HISTORY, null );
            }
        }
        catch ( final Exception e )
        {
            throw new IllegalStateException ( e );
        }
    }

    @Override
    public PrintStream getCurrentPrintStream ()
    {
        return this.currentStream;
    }

    @Override
    public void setCurrentPrintStream ( final String file )
    {
        this.currentStream = getPrintStream ( file );
    }

    @SuppressWarnings ( "resource" )
    @Override
    public PrintStream getPrintStream ( final String file )
    {
        try
        {

            final IFile newFile = this.container.getFile ( new Path ( file ) );

            PrintStream stream = this.streams.get ( newFile );
            if ( stream != null )
            {
                return stream;
            }

            final Stream bos = new Stream ( newFile );

            stream = new PrintStream ( bos, true, "UTF-8" );
            this.streams.put ( newFile, stream );

            return stream;
        }
        catch ( final Exception e )
        {
            throw new IllegalStateException ( e );
        }
    }

    public void handleClose ( final byte[] buf, final IFile file )
    {
        try
        {
            if ( file.exists () )
            {
                file.setContents ( new ByteArrayInputStream ( buf ), IResource.FORCE | IResource.KEEP_HISTORY, null );
            }
            else
            {
                file.create ( new ByteArrayInputStream ( buf ), IResource.FORCE | IResource.KEEP_HISTORY, null );
            }
        }
        catch ( final Exception e )
        {
            throw new RuntimeException ( e );
        }
    }

    @Override
    public abstract void render ( Object object );

    @Override
    public void dispose ()
    {
        for ( final PrintStream stream : this.streams.values () )
        {
            stream.close ();
        }
        this.streams.clear ();
    }

    @Override
    public String createCrossReference ( final String type, final String id )
    {
        final String xrefId = makeLinkTarget ( type, id );

        this.crossReferences.add ( xrefId );

        return xrefId;
    }

    @Override
    public String createLinkTarget ( final String type, final String id )
    {
        final String xrefId = makeLinkTarget ( type, id );

        if ( !this.linkTargets.add ( xrefId ) )
        {
            throw new IllegalStateException ( String.format ( "Link target '%s' is a duplicate!", xrefId ) );
        }

        return xrefId;
    }

    private String makeLinkTarget ( final String type, final String id )
    {
        return String.format ( "%s_%s", type, id );
    }
}
