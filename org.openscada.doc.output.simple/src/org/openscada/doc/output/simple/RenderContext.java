/*******************************************************************************
 * Copyright (c) 2013 Jens Reimann.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.openscada.doc.output.simple;

import java.io.PrintStream;
import java.util.Locale;

import org.openscada.doc.model.doc.book.Book;

public interface RenderContext
{
    public Book getBook ();

    public PrintStream getPrintStream ( String file );

    public PrintStream getCurrentPrintStream ();

    public void setCurrentPrintStream ( String file );

    public void render ( Object object );

    public void writeData ( String file, byte[] data );

    public String createLinkTarget ( String string, String name );

    public String createCrossReference ( String string, String name );

    public Locale getLocale ();

    public void validate ();

    public void dispose ();
}
