package org.openscada.doc.output.simple.tex;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.openscada.doc.model.doc.book.Book;
import org.openscada.doc.model.doc.book.BookContainer;
import org.openscada.doc.model.doc.book.BookSection;
import org.openscada.doc.model.doc.fragment.Author;
import org.openscada.doc.model.doc.fragment.Content;
import org.openscada.doc.model.doc.fragment.Copyright;
import org.openscada.doc.output.simple.AbstractOutput;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.RenderContextImpl;
import org.openscada.doc.output.simple.renderer.BookRenderer;
import org.openscada.ui.databinding.AdapterHelper;

import com.ibm.icu.text.MessageFormat;

public class TexOutput extends AbstractOutput
{

    public class RenderContextExtension extends RenderContextImpl
    {
        public RenderContextExtension ( final IContainer container, final Book book, final Locale locale )
        {
            super ( container, book, locale );
        }

        @Override
        public void render ( final Object object )
        {
            processObject ( this, object );
        }
    }

    private final ResourceBundle bundle;

    private static enum DocumentType
    {
        BOOK ( "book" );

        private String name;

        private DocumentType ( final String name )
        {
            this.name = name;
        }

        public String getName ()
        {
            return this.name;
        }
    }

    private final String[] sectionTypes = new String[] { "chapter", "section", "subsection", "subsubsection", "paragraph", "subparagraph" };

    private final DocumentType documentType = DocumentType.BOOK;

    public TexOutput ( final Book book, final String profileId, final Locale locale ) throws CoreException
    {
        super ( book, locale );

        this.bundle = ResourceBundle.getBundle ( "resources/output", locale );
    }

    @Override
    protected void processContent ( final RenderContext context )
    {
        context.setCurrentPrintStream ( "book.tex" );

        start ( context );

        processBody ( context );

        end ( context );
    }

    @Override
    protected RenderContextExtension createRenderContext ( final IFolder container )
    {
        return new RenderContextExtension ( container, this.book, this.locale );
    }

    private void processBookHeader ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        // final boolean showAuthorRefs = !Boolean.parseBoolean ( this.profile.getProperties ().getProperty ( "org.openscada.doc.output.simple.html.book.hideAuthorRefs" ) );
        out.println ( "\\title{" + TexHelper.encode ( this.book.getTitle () ) + "}" );
    }

    private String makeCopyright ( final List<Copyright> copyright )
    {
        if ( copyright.isEmpty () )
        {
            return this.bundle.getString ( "copyright.empty" );
        }

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for ( final Copyright c : copyright )
        {
            min = Math.min ( min, c.getYear () );
            max = Math.max ( max, c.getYear () );
        }

        String text;
        if ( min == max )
        {
            text = String.format ( "%s", min );
        }
        else
        {
            text = String.format ( "%s-%s", min, max );
        }

        return new MessageFormat ( this.bundle.getString ( "copyright.format" ), this.locale ).format ( new Object[] { text } );
    }

    private void start ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( String.format ( "\\documentclass{%s}", this.documentType.getName () ) );
        out.println ( "\\usepackage{hyperref}" );
        out.println ( "\\usepackage{tabu}" );
    }

    private void end ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();
        out.println ( "\\end{document}" );
    }

    protected void processObject ( final RenderContext context, final Object content )
    {
        if ( content instanceof String )
        {
            final PrintStream out = context.getCurrentPrintStream ();
            out.print ( TexHelper.encode ( (String)content ) );
        }
        else if ( content instanceof FeatureMap )
        {
            for ( final FeatureMap.Entry entry : (FeatureMap)content )
            {
                processObject ( context, entry.getValue () );
            }
        }
        else
        {
            render ( context, content );
        }
    }

    private void render ( final RenderContext context, final Object content )
    {
        final BookRenderer renderer = findRenderer ( "tex", content.getClass () );

        if ( renderer != null )
        {
            renderer.render ( context, content );
        }
        else
        {
            throw new IllegalStateException ( "Unable to render object type: " + content.getClass () );
        }
    }

    List<Integer> sectionNumber ( final BookSection section )
    {
        final List<Integer> nrs = new LinkedList<Integer> ();

        BookSection currentSection = section;
        do
        {
            nrs.add ( 0, currentSection.getNumber () );
            currentSection = AdapterHelper.adapt ( currentSection.eContainer (), BookSection.class );
        } while ( currentSection != null );

        return nrs;
    }

    private void processBody ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        processBookHeader ( context );

        out.println ( "\\begin{document}" );

        makeAuthors ( context );

        if ( this.documentType == DocumentType.BOOK )
        {
            out.println ( "\\frontmatter" );
            out.println ( "\\maketitle" );
            out.println ( "\\mainmatter" );
        }

        processTableOfContents ( context, this.book );

        processContainer ( context, this.book );
    }

    private void makeAuthors ( final RenderContext context )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        for ( final Author author : this.book.getAuthors () )
        {
            out.print ( "\\author{" );
            out.print ( author.getName () );
            if ( author.getRef () != null )
            {
                out.print ( " (\\texttt{" );
                out.print ( TexHelper.encode ( author.getRef () ) );
                out.print ( "})" );
            }
            out.println ( "}" );
        }
    }

    private void processTableOfContents ( final RenderContext context, final Book book )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        out.println ( "\\tableofcontents" );
    }

    private void processContainer ( final RenderContext context, final BookContainer container )
    {
        for ( final Content content : container.getContent () )
        {
            processContent ( context, content );
        }
        for ( final BookSection section : container.getSections () )
        {
            processSection ( context, section );
        }
    }

    private void processSection ( final RenderContext context, final BookSection section )
    {
        final String title = section.getTitle ();
        final List<Integer> list = sectionNumber ( section );
        final int level = list.size ();

        final PrintStream out = context.getCurrentPrintStream ();

        final String xrefId = context.createLinkTarget ( "section", section.getId () );

        final String sectionType = this.sectionTypes[level - 1];

        out.println ( "\\" + sectionType + "{" + TexHelper.encode ( section.getTitle () ) + "}" );

        processContainer ( context, section );
    }

    private void processContent ( final RenderContext context, final Content content )
    {
        processObject ( context, content );
    }
}
