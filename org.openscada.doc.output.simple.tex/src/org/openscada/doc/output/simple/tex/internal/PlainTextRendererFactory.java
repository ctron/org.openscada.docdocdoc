package org.openscada.doc.output.simple.tex.internal;

import java.util.HashMap;
import java.util.Map;

import org.openscada.doc.model.doc.fragment.PlainTextContent;
import org.openscada.doc.output.simple.renderer.AbstractCacheFactory;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PlainTextRendererFactory extends AbstractCacheFactory
{

    private final Map<Entry, BookRenderer> cache = new HashMap<Entry, BookRenderer> ();

    public PlainTextRendererFactory ()
    {
        this.cache.put ( new Entry ( "tex", PlainTextContent.class ), new PlainTextRenderer () );
    }

    @Override
    protected Map<Entry, BookRenderer> getCache ()
    {
        return this.cache;
    }

}
