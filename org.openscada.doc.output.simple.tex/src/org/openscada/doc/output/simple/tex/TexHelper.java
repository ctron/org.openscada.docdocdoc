package org.openscada.doc.output.simple.tex;

public class TexHelper
{
    public static String encode ( final String text )
    {
        return text.replaceAll ( "[\\#\\$\\%\\^\\&\\_\\{\\}\\~\\\\]", "\\\\\\0" );
    }
}
