package org.openscada.doc.output.simple.tex.internal;

import java.io.PrintStream;

import org.openscada.doc.model.doc.fragment.PlainTextContent;
import org.openscada.doc.output.simple.RenderContext;
import org.openscada.doc.output.simple.renderer.BookRenderer;

public class PlainTextRenderer implements BookRenderer
{

    @Override
    public void render ( final RenderContext context, final Object content )
    {
        final PrintStream out = context.getCurrentPrintStream ();

        out.println ( "\\begin{verbatim}" );
        out.println ( ( (PlainTextContent)content ).getValue () );
        out.println ( "\\end{verbatim}" );
    }

}
